package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/go-gota/gota/dataframe"
)

const (
	GITHUB_URL = "https://github.COMPANY.ca/"
)

var (
	FILE   string
	TOKEN  string
	DRYRUN bool
)

type RESULT struct {
	Name      string
	Count     int
	Workflows string
}

func init() {
	// isbrown := flag.Bool("bf", false, "is brownfield")
	flag.BoolVar(&DRYRUN, "dryrun", false, "dryrun just print out matched values, this also serve as a list")
	flag.StringVar(&TOKEN, "t", "", "Github PAT")
	flag.StringVar(&FILE, "f", "", "File content repositories new line separated")
	flag.Parse()
	if TOKEN == "" || FILE == "" {
		log.Fatalf("[!] missing file or token or both, please see --help")
	}
}

func main() {
	ch := make(chan RESULT, 1)
	done := make(chan bool, 1)

	REPOS := LoadFiles(FILE)
	result := []RESULT{}
	go CaptureRequests(REPOS, ch, done)

	// REPOS := []string{"COMPANY/PROJECT-regression-test"}
	if DRYRUN {
		fmt.Println(REPOS)

	} else {
		result = Orchestrator(ch, done)
		file, _ := os.Create("/tmp/gha_report.csv")
		defer file.Close()

		re := dataframe.LoadStructs(result)
		re.WriteCSV(file)
	}

}

func Orchestrator(ch chan RESULT, done chan bool) (R []RESULT) {
	for {
		select {
		case v := <-ch:
			R = append(R, v)
		case <-done:
			return R
		}
	}
}

func CaptureRequests(repos []string, ch chan RESULT, done chan bool) {
	for _, repo := range repos {
		go RepoGhaReport(repo, ch)
	}
	time.Sleep(2000 * time.Millisecond)
	done <- true
}

func LoadFiles(file string) (result []string) {
	file_, err := os.ReadFile(file)
	if err != nil {
		log.Fatalf("[E] error: %#v", err)
	}
	file_ = bytes.TrimSpace(file_)
	result = strings.Split(string(file_), "\n")
	return result
}

func RepoGhaReport(REPO string, ch chan RESULT) {

	client := &http.Client{}
	request := GITHUB_URL + "api/v3/repos/" + REPO + "/actions/workflows"
	req, err := http.NewRequest("GET", request, nil)
	if err != nil {
		result_ := RESULT{
			Name:      REPO,
			Workflows: "",
			Count:     0,
		}
		ch <- result_
	}

	req.Header.Set("Accept", "application/vnd.github+json")
	req.Header.Set("Authorization", "token "+TOKEN)
	resp, err := client.Do(req)
	if err != nil {
		result_ := RESULT{
			Name:      REPO,
			Workflows: "",
			Count:     0,
		}
		ch <- result_
	}
	defer resp.Body.Close()
	bodyText, err := io.ReadAll(resp.Body)
	if err != nil {
		result_ := RESULT{
			Name:      REPO,
			Workflows: "",
			Count:     0,
		}
		ch <- result_
	}

	var result map[string]interface{}
	json.Unmarshal([]byte(bodyText), &result)

	switch result["workflows"].(type) {
	case interface{}:
		names := func(res []interface{}) (resu string) {
			for _, i := range res {
				name := i.(map[string]interface{})["name"].(string)
				resu += "|" + name
			}
			return resu
		}(result["workflows"].([]interface{}))

		result_ := RESULT{
			Name:      REPO,
			Workflows: names,
			Count:     int(result["total_count"].(float64)),
		}
		ch <- result_
	default:
		result_ := RESULT{
			Name:      REPO,
			Workflows: "",
			Count:     0,
		}
		ch <- result_
	}
}

// func GithubRepoList(ctx context.Context, user string, client github.Client) string {

// 	opt := &github.RepositoryListOptions{Type: "all", Sort: "updated", Direction: "desc"}
// 	repo, _, err := client.Repositories.List(ctx, user, opt)
// 	result := fmt.Sprintf("%#v\n%#v\n", repo, err)

// 	return result
// }
