package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
)

const (
	GITHUB_URL = "https://github.COMPANY.ca/"
)

var (
	FILE   string
	TOKEN  string
	DRYRUN bool
)

func init() {
	// isbrown := flag.Bool("bf", false, "is brownfield")
	flag.BoolVar(&DRYRUN, "dryrun", false, "dryrun just print out matched values, this also serve as a list")
	flag.StringVar(&TOKEN, "t", "", "Github PAT")
	flag.StringVar(&FILE, "f", "", "File content repositories new line separated")
	flag.Parse()
	if TOKEN == "" || FILE == "" {
		log.Fatalf("[!] missing file or token or both, please see --help")
	}
}

func main() {
	files := LoadFiles(FILE)
	if DRYRUN {
		fmt.Println(files)
	} else {
		RepoInternalVisibility(files)
	}
}

func LoadFiles(file string) (result []string) {
	file_, err := os.ReadFile(file)
	if err != nil {
		log.Fatalf("[E] error: %#v", err)
	}
	file_ = bytes.TrimSpace(file_)
	result = strings.Split(string(file_), "\n")
	return result
}

func RepoInternalVisibility(repositories []string) string {
	var data = strings.NewReader(`{"visibility":"internal"}`)

	for _, k := range repositories {
		client := &http.Client{}
		request := GITHUB_URL + "api/v3/repos/" + k
		req, err := http.NewRequest("PATCH", request, data)
		if err != nil {
			log.Println(err)
			return ""
		}
		req.Header.Set("Accept", "application/vnd.github+json")
		req.Header.Set("Authorization", "Bearer "+TOKEN)
		resp, err := client.Do(req)
		if err != nil {
			log.Println(err)
			return ""
		}
		defer resp.Body.Close()
		bodyText, err := io.ReadAll(resp.Body)
		if err != nil {
			log.Println(err)
			return ""
		}

		var result map[string]interface{}
		json.Unmarshal([]byte(bodyText), &result)
		fmt.Printf("[!] %#v %#v\n", k, result["visibility"])
	}

	return ""
}
