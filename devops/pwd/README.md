
# This creates a k8s version of PWD

## Deps

- Taskgo  : sh -c "$(curl --location https://taskfile.dev/install.sh)" -- -d -b /usr/local/bin
- PWD     : git clone git@github.com:MushiTheMoshi/play-with-docker.git
- kubectl : latest version
- k3d     : installation must include local registry to push images, registry HOST and PORT must be updated in the taskfile.yml
- dnsmasq : 
  - server=8.8.8.8
  - address=/myapp.com/127.0.0.1
  - address=/pwd.com/127.0.0.1
  - address=/registry.pwd.com/127.0.0.1
  - address=/*.direct.pwd.com/127.0.0.1
  - listen-address=127.0.0.1
  - cname=*.direct.myapp.com,myapp.com

## Instructions

```.bash
git clone git@github.com:MushiTheMoshi/play-with-docker.git

task all

```




