package main

import (
	"context"
	"flag"
	"fmt"
	"path/filepath"

	appv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/util/homedir"
)

func main() {

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	clientSet := getClientSet()

	ListSecrets(ctx, clientSet)
	CreateSecret(ctx, clientSet)

	CreateDeployment(ctx, clientSet)
	DeleteDeployment(ctx, clientSet)

	ListPods(ctx, clientSet)
}

func ListPods(ctx context.Context, clientSet *kubernetes.Clientset) {

	pods, err := clientSet.CoreV1().Pods("foo").List(ctx, metav1.ListOptions{})
	panicIfError(err)

	// fmt.Printf("Listing pods in %#v namespace.\n", pods.Items)
	for _, k := range pods.Items {
		fmt.Println("[!]", k.Name)
	}

}

func CreateSecret(ctx context.Context, clientSet *kubernetes.Clientset) {
	secret, err := clientSet.CoreV1().
		Secrets("foo").
		Create(
			ctx,
			&corev1.Secret{
				ObjectMeta: metav1.ObjectMeta{
					Name: "mysecret",
				},
				Data: map[string][]byte{
					"password": []byte("BigSecret"),
				},
			},
			metav1.CreateOptions{},
		)
	passIfError(err)

	fmt.Printf("[!] %#v", secret)
}

func DeleteDeployment(ctx context.Context, clientSet *kubernetes.Clientset) {
	deployment := clientSet.AppsV1().Deployments("foo")

	deployment.Delete(ctx, "nginx", metav1.DeleteOptions{})

}

func CreateDeployment(ctx context.Context, clientSet *kubernetes.Clientset) *appv1.Deployment {
	matchLabel := map[string]string{"app": "nginx"}
	INT := int32(2)
	deployment := &appv1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "nginx",
			Namespace: "foo",
			Labels:    matchLabel,
		},
		Spec: appv1.DeploymentSpec{
			Replicas: &INT,
			Selector: &metav1.LabelSelector{MatchLabels: map[string]string{"app": "nginx"}},
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: map[string]string{"app": "nginx"},
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  "nginx",
							Image: "nginx/nginx",
							Ports: []corev1.ContainerPort{
								{
									Name:          "http",
									ContainerPort: 80,
								},
							},
						},
					},
				},
			},
		},
	}

	dep, err := clientSet.AppsV1().Deployments("foo").Create(ctx, deployment, metav1.CreateOptions{})
	panicIfError(err)

	return dep
}

func ListSecrets(ctx context.Context, clientSet *kubernetes.Clientset) {
	secrets, err := clientSet.CoreV1().Secrets("foo").List(ctx, metav1.ListOptions{})
	panicIfError(err)

	for _, k := range secrets.Items {
		fmt.Printf("[!] %#v\n", k.Data)
		fmt.Printf("[!] %#v\n", string(k.Data["password"]))
		fmt.Printf("[!] %#v\n", string(k.Data["username"]))
	}
}

func getClientSet() *kubernetes.Clientset {
	var kubeconfig *string

	if home := homedir.HomeDir(); home != "" {
		kubeconfig = flag.String("kubeconfig", filepath.Join(home, ".kube", "config"), "(optional) absolute path to the kubeconfig file")
	} else {
		kubeconfig = flag.String("kubeconfig", "", "absolute path to the kubeconfig file")
	}

	flag.Parse()

	// use the current context in kubeconfig
	config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)
	panicIfError(err)

	// create the clientSet
	cs, err := kubernetes.NewForConfig(config)
	panicIfError(err)
	return cs
}

func panicIfError(err error) {
	if err != nil {
		panic(err.Error())
	}
}

func passIfError(err error) {
	if err != nil {
		fmt.Errorf("%#v", err.Error())
	}
}
