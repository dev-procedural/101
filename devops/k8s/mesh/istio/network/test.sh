# create 2 pods 'web' and 'test'
kubectl run web --image nginx --labels app=web --expose --port 80
kubectl run test --image alpine -- sleep 3600

# check pod "test" can access pod "web"
kubectl exec -it test -- wget -qO- --timeout=2 http://web

cat <<EOF | kubectl apply -f -
kind: NetworkPolicy
apiVersion: networking.k8s.io/v1
metadata:
  name: web-deny-all
spec:
  podSelector:
    matchLabels:
      app: web
  ingress: []
EOF

# check pod "test" cannot access pod "web"
kubectl exec -it test -- wget -qO- --timeout=2 http://web

