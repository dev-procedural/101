package main

import (
   "os"
	"github.com/gin-gonic/gin"
	cors "github.com/rs/cors/wrapper/gin"
)

func main() {
	r := gin.Default()
	r.Use(cors.AllowAll())
	r.GET("/", func(c *gin.Context) {

		a := os.Environ()
		c.ShouldBind(&a)
		//take action
		c.JSON(200, gin.H{"msg": a})
	})

	r.Run(":8889")
}
