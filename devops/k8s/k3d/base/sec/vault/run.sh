helm uninstall vault hashicorp/vault --set "server.dev.enabled=true,injector.enabled=true,server.extraEnvironmentVars.VAULT_DEV_LISTEN_ADDRESS=0.0.0.0:8200"

CMD="$(cat vault_0_config.sh)" && kubectl exec -it vault-0 -c vault -n vv -- sh -c "$CMD"
