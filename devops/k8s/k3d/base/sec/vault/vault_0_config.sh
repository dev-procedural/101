# install vault
# helm install vault hashicorp/vault --set server.dev.enabled=true --set injector.enabled=true
vault secrets enable --tls-skip-verify -ns vv -path secrets kv
vault kv put --tls-skip-verify -ns vv /secret/hello foo=world

vault auth enable --tls-skip-verify -ns vv kubernetes
vault write --tls-skip-verify -ns vv auth/kubernetes/config \
   token_reviewer_jwt="$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)" \
   kubernetes_host="https://$KUBERNETES_PORT_443_TCP_ADDR:443" \
   kubernetes_ca_cert=@/var/run/secrets/kubernetes.io/serviceaccount/ca.crt \
   disable_local_ca_jwt=true
   # disable_iss_validation=true 

vault policy write  --tls-skip-verify -ns vv app1 - <<'EOF'
path "/secret/hello" { 
   capabilities = ["create", "read", "update", "delete", "list"]
}
EOF

vault write -ns vv --tls-skip-verify auth/kubernetes/role/app1 \
   bound_service_account_names=vault-app \
   bound_service_account_namespaces=vv \
   policies=app1 ttl=24h
