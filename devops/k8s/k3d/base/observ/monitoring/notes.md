### Monitoring
- Observes cluster metrics, meaning resources
- Prometheus stores the data taken from the docker api
- Graphana reads the information stored in prometheus
- Alermanager alerts whatever the channel it need to be aware
