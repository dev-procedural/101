
helm repo add prometheus-community  https://prometheus-community.github.io/helm-charts
kubectl apply -f ns.yaml
helm install prometheus prometheus-community/kube-prometheus-stack -f values.yml -n monitoring

# grafana password
# kubectl exec -it -n monitoring prometheus-grafana-6c75b6644-mzqgt -c grafana -- sh
# grafana-cli --homepath "/usr/share/grafana" admin reset-admin-password <new password>
