# git clone https://github.com/kubernetes/kubernetes

# cd kubernetes/cluster/addons

kubectl create ns logging
# ES
# kubectl apply -f ./fluentd-elasticsearch/es-statefulset.yaml
# kubectl apply -f ./fluentd-elasticsearch/es-service.yaml

# # FluentD
# kubectl apply -f ./fluentd-elasticsearch/fluentd-es-configmap.yaml
# kubectl apply -f ./fluentd-elasticsearch/fluentd-es-ds.yaml

# # Kibana
# kubectl apply -f ./fluentd-elasticsearch/kibana-deployment.yaml
# kubectl apply -f ./fluentd-elasticsearch/kibana-service.yaml


# helm install elasticsearch elastic/elasticsearch -n logging
# helm install kibana elastic/kibana -n logging
# helm install fluentd bitnami/fluentd -n logging

DOCKER_REGISTRY_SERVER=docker.io
DOCKER_USER=julieto
DOCKER_PASSWORD=REPLACE_WITH_PASS
DOCKER_EMAIL=julio.cesar.guerrero.olea@gmail.com
kubectl create secret docker-registry myregistrysecret --docker-server=$DOCKER_REGISTRY_SERVER --docker-username=$DOCKER_USER --docker-password=$DOCKER_PASSWORD --docker-email=$DOCKER_EMAIL --dry-run -o yaml > dockerlogin.cm.yml

helm install elasticsearch elastic/elasticsearch --version="7.9.0" -n logging
helm install kibana elastic/kibana --version="7.9.0" -n logging 
helm install fluentd bitnami/fluentd --version="2.0.1" -n logging
