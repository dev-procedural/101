#!/bin/env bash

if [ ! "$BASH_VERSION" ] ; then
    exec /bin/bash "$0" "$@"
fi

# > 
# k3d cluster create gitops3 --port 80:80@loadbalancer --agents 2 --servers 2
#k3d cluster create gitops3 --port 80:80@loadbalancer --agents 2 --servers 2 --k3s-server-arg '--no-deploy=traefik'

k3d cluster create gitops3 \
  --servers 1 \
  --agents 3 \
  --port '443:443@loadbalancer' \
  --port '80:80@loadbalancer' \
  --k3s-arg '--disable=traefik@server:0' \
  --k3s-arg '--flannel-backend=none@server:*' \
  --volume "$(pwd)/network/calico.yaml:/var/lib/rancher/k3s/server/manifests/calico.yaml" \
  --volume "/tmp/k3dvol:/data" \
  --registry-create gitops

[[ -n $(ls -1 | grep -i istio ) ]] && \
curl -L https://istio.io/downloadIstio | sh - 

export PATH=$PATH:/home/julieto/Projects/101/k8s/k3d/istio-1.10.1/bin/

istioctl install -y --set profile=default

kubectl label namespace default istio-injection=enabled

# kubeaudit
# curl -sLf -o kubeaudit.tar.gz  https://github.com/Shopify/kubeaudit/releases/download/v0.7.0/kubeaudit_0.7.0_linux_amd64.tar.gz 
