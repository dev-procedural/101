provider "aws" {
  profile = "cloudops-support" 
  region = "us-east-1"
}

resource "null_resource" "gpg" {
  provisioner "local-exec" {
    command = "gpg --quick-generate-key --yes eks2 "
  }
}

resource "null_resource" "gpg_key" {
  provisioner "local-exec" {
    command = "gpg --export -a eks2 | tail -n +2 | head -n -2 | tr -d '\n'"

  }
}

# The below variable is my pgp public key base64 encoded.  This is used to create the password
# and secret_key of the iam user below.  You can change this to be your own key. 
variable "pgp_key" {
  type = string
  # GPG LOCAL EKS
  default = null_resource.gpg_key
}

########################################################
# Create the EKS Full Access Policy
########################################################

data "aws_iam_policy_document" "EKSFullAccess" {
  statement {
    sid = "EKSFullAccess"
    actions = [
      "eks:*",
      "ecr:*"
    ]
    resources = [
      "*"
    ]
  }
}

resource "aws_iam_policy" "EKSFullAccess" {
  name = "EKSFullAccess"
  path = "/"
  policy = data.aws_iam_policy_document.EKSFullAccess.json
}


########################################################
# Create the EKS Demo Group
########################################################

resource "aws_iam_group" "EKSDemoGroup" {
  name = "EKSDemoGroup"
  path = "/"
}

########################################################
# Attach AWS managed policies to the group
########################################################

resource "aws_iam_group_policy_attachment" "AmazonEC2FullAccess" {
  group = aws_iam_group.EKSDemoGroup.name
  policy_arn  = "arn:aws:iam::aws:policy/AmazonEC2FullAccess"
}
resource "aws_iam_group_policy_attachment" "AmazonS3FullAccess" {
  group = aws_iam_group.EKSDemoGroup.name
  policy_arn  = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}
resource "aws_iam_group_policy_attachment" "AmazonSNSReadOnlyAccess" {
  group = aws_iam_group.EKSDemoGroup.name
  policy_arn  = "arn:aws:iam::aws:policy/AmazonSNSReadOnlyAccess"
}
resource "aws_iam_group_policy_attachment" "AmazonVPCFullAccess" {
  group = aws_iam_group.EKSDemoGroup.name
  policy_arn  = "arn:aws:iam::aws:policy/AmazonVPCFullAccess"
}
resource "aws_iam_group_policy_attachment" "IAMReadOnlyAccess" {
  group = aws_iam_group.EKSDemoGroup.name
  policy_arn  = "arn:aws:iam::aws:policy/IAMReadOnlyAccess"
}
resource "aws_iam_group_policy_attachment" "AWSCloudFormationFullAccess" {
  group = aws_iam_group.EKSDemoGroup.name
  policy_arn  = "arn:aws:iam::aws:policy/AWSCloudFormationFullAccess"
}

resource "aws_iam_group_policy_attachment" "DynamoFullAccess" {
  group = aws_iam_group.EKSDemoGroup.name
  policy_arn  = "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"
}
resource "aws_iam_group_policy_attachment" "EKSFullAccess" {
  group = aws_iam_group.EKSDemoGroup.name
  policy_arn  = aws_iam_policy.EKSFullAccess.arn
}



########################################################
# Attach AWS inline policy to the group
########################################################

data "aws_caller_identity" "current" {}

data "aws_iam_policy_document" "iamPassRole" {
  statement {
    actions = [
      "ssm:GetParameter",
      "iam:PassRole",
      "iam:CreateServiceLinkedRole",
      "iam:CreateRole",
      "iam:DeleteRole",
      "iam:AttachRolePolicy",
      "iam:DetachRolePolicy",
      "iam:PutRolePolicy",
      "iam:DeleteRolePolicy",
      "iam:CreateInstanceProfile",
      "iam:CreateOpenIDConnectProvider",
      "iam:DeleteOpenIDConnectProvider"
    ]
    resources = [
      "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/*",
      "arn:aws:iam::${data.aws_caller_identity.current.account_id}:oidc-provider/oidc.eks.us-east-1.amazonaws.com",
      "arn:aws:iam::${data.aws_caller_identity.current.account_id}:oidc-provider/oidc.eks.us-east-1.amazonaws.com/*",
      "arn:aws:ssm:*"
    ]
  }
}

resource "aws_iam_group_policy" "iamPassRole" {
  group = aws_iam_group.EKSDemoGroup.name
  name = "iamPassRole"
  policy = data.aws_iam_policy_document.iamPassRole.json
}


########################################################
# Create EKS Demo user and add to group
########################################################

resource "aws_iam_user" "eksdude" {
  name = "eksdude"
  force_destroy = "true"
}

resource "aws_iam_user_group_membership" "eksdude" {
  user = aws_iam_user.eksdude.name
  groups = [
    aws_iam_group.EKSDemoGroup.name
  ]
}

data "aws_iam_policy_document" "eksdude" {
  statement {
    actions = [
      "iam:ChangePassword",
      "iam:GetAccountPasswordPolicy"
    ]
    resources = [
      "arn:aws:iam::${data.aws_caller_identity.current.account_id}:user/${aws_iam_user.eksdude.name}"
    ]
  }
}

resource "aws_iam_user_policy" "eksdude" {
  name = "ConsolePolicy"
  user = aws_iam_user.eksdude.name
  policy = data.aws_iam_policy_document.eksdude.json
}

resource "aws_iam_user_login_profile" "eksdude" {
  user = aws_iam_user.eksdude.name
  pgp_key = var.pgp_key
  password_length = 10

  lifecycle {
    ignore_changes = [password_length, password_reset_required, pgp_key]
  }
}

output "password" {
  value = aws_iam_user_login_profile.eksdude.encrypted_password
}

resource "aws_iam_access_key" "eksdude" {
  user = aws_iam_user.eksdude.name
  pgp_key = var.pgp_key
}

output "secret" {
  value = aws_iam_access_key.eksdude.encrypted_secret
}

output "key" {
  value = aws_iam_access_key.eksdude.id
}

########################################################
# Create EKS Service Role for Manual Kubernetes Clusters
########################################################

resource "aws_iam_role" "EKSServiceRole" {
  name = "EKSServiceRole"
  assume_role_policy = <<-EOF
    {
      "Version": "2012-10-17",
      "Statement": [
        {
          "Action": "sts:AssumeRole",
          "Principal": {
            "Service": "eks.amazonaws.com"
          },
          "Effect": "Allow",
          "Sid": ""
        }
      ]
    }
EOF
}

resource "aws_iam_role_policy_attachment" "EKSServiceRoleAttachmentsCluster" {
  role = aws_iam_role.EKSServiceRole.name
  policy_arn  = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
}

resource "aws_iam_role_policy_attachment" "EKSServiceRoleAttachmentsService" {
  role = aws_iam_role.EKSServiceRole.name
  policy_arn  = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
}


########################################################
# Create Cluster Autoscaling Policy
########################################################

data "aws_iam_policy_document" "EKSClusterAutoscaling" {
  statement {
    sid = "EKSClusterAutoscaling"
    actions = [
      "autoscaling:DescribeAutoScalingGroups",
      "autoscaling:DescribeAutoScalingInstances",
      "autoscaling:DescribeLaunchConfigurations",
      "autoscaling:DescribeTags",
      "autoscaling:SetDesiredCapacity",
      "autoscaling:TerminateInstanceInAutoScalingGroup",
      "ec2:DescribeLaunchTemplateVersions"
    ]
    resources = [
      "*"
    ]
  }
}

resource "aws_iam_policy" "EKSClusterAutoscaling" {
  name = "EKSClusterAutoscaling"
  path = "/"
  policy = data.aws_iam_policy_document.EKSClusterAutoscaling.json
}



########################################################
# Create Serverless Policy and attach to the EKSDemoGroup
########################################################

data "aws_iam_policy_document" "Serverless" {
  statement {
    sid = "Serverless"
    actions = [
      "apigateway:*",
      "logs:*",
      "lambda:*"
    ]
    resources = [
      "*"
    ]
  }
}
resource "aws_iam_policy" "Serverless" {
  name = "Serverless"
  path = "/"
  policy = data.aws_iam_policy_document.Serverless.json
}

resource "aws_iam_group_policy_attachment" "EKSDemoGroupServerless" {
  group = aws_iam_group.EKSDemoGroup.name
  policy_arn  = aws_iam_policy.Serverless.arn
}


########################################################
# Create KubeLambda Role and attach Policies
########################################################

resource "aws_iam_role" "kubeLambda" {
  name = "kubeLambda"
  description = "Allows Lambda functions to make kubernetes calls on our cluster."
  assume_role_policy = <<-EOF
    {
      "Version": "2012-10-17",
      "Statement": [
        {
          "Action": "sts:AssumeRole",
          "Principal": {
            "Service": "lambda.amazonaws.com"
          },
          "Effect": "Allow",
          "Sid": ""
        }
      ]
    }
EOF
}


resource "aws_iam_role_policy_attachment" "kubeLambdaEKSFullAccess" {
  role = aws_iam_role.kubeLambda.name
  policy_arn  = aws_iam_policy.EKSFullAccess.arn
}

resource "aws_iam_role_policy_attachment" "kubeLambdaServerless" {
  role = aws_iam_role.kubeLambda.name
  policy_arn  = aws_iam_policy.Serverless.arn
}

resource "aws_iam_role_policy_attachment" "kubeLambdaLambda" {
  role = aws_iam_role.kubeLambda.name
  policy_arn  = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

########################################################
# Add Dynamo Role 
########################################################


