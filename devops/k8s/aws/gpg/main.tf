resource "null_resource" "gpg" {
  provisioner "local-exec" {
    command = "gpg --quick-generate-key --yes eks2 "
  }
}

resource "null_resource" "gpg_key" {
  provisioner "local-exec" {
    command = "gpg --export -a eks2 | tail -n +2 | head -n -2 | tr -d '\n'"

  }
}


output "gpg_key" {
   value = null_resource.gpg_key

}
