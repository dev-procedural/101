# Sonarqube PR_DECORATION ACTIVATOR

## Instructions
- It only works over onboarded sonarqube projects
- Token can be generated here: https://sonar.PROJECT.COMPANY.io/account/security/
- Can update main branch


Usage:
```
Usage of /tmp/go-build1926101483/b001/exe/main:
  -b string
        BRANCH, sonarqube repository main branch (default "master")
  -dryrun
        dryrun just print out matched values
  -r string
        GIT REPOSITORY, can be comma separated or a file
  -t string
        TOKEN, sonarqube token

```

E.G.
```
go run main.go -r "actions-sample-git" -t TOKEN 

```
