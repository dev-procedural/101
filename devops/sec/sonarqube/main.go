package main

import (
	"encoding/base64"
	"flag"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"strings"
)

var (
	DRYRUN   bool
	TOKEN64  string
	GIT_REPO *string
	BRANCH   *string
)

const (
	URL              = "https://sonar.PROJECT.COMPANY.io"
	PR_DECORATION_EP = "/api/alm_settings/set_github_binding"
	BRANCH_EP        = "/api/project_branches/rename"
)

func init() {
	// isbrown := flag.Bool("bf", false, "is brownfield")
	flag.BoolVar(&DRYRUN, "dryrun", false, "dryrun just print out matched values")
	TOKEN := flag.String("t", os.Getenv("TOKEN"), "TOKEN, sonarqube token")
	BRANCH = flag.String("b", "master", "BRANCH, sonarqube repository main branch")
	GIT_REPO = flag.String("r", os.Getenv("GIT_REPO"), "GIT REPOSITORY, can be comma separated or a file ")
	flag.Parse()

	TOKEN64 = base64.StdEncoding.EncodeToString([]byte(*TOKEN + ":"))
}

func REQUEST(URL, EP, TOKEN64 string, data url.Values) string {

	client := &http.Client{}

	r, err := http.NewRequest("POST", URL+EP, strings.NewReader(data.Encode()))
	if err != nil {
		panic(err)
	}

	r.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	r.Header.Add("Authorization", "Basic "+TOKEN64)

	resp, err := client.Do(r)
	if err != nil {
		panic(err)
	}

	return fmt.Sprintf("%#v", resp.Status)
}

func PR_DECORATION(GIT_REPO_ string) string {
	data := url.Values{}
	EP := ""
	RESULT := []string{}
	RES := ""

	for _, i := range []string{"PR_DECORATION", "MAIN_BRANCH"} {
		switch i {
		case "PR_DECORATION":
			data.Set("almSetting", "GitHub Prod")
			data.Set("project", GIT_REPO_)
			data.Set("repository", "COMPANY/"+GIT_REPO_)
			data.Set("summaryCommentEnabled", "true")
			data.Set("monorepo", "false")

			EP = PR_DECORATION_EP

			STATUS := REQUEST(URL, EP, TOKEN64, data)
			RESULT = append(RESULT, STATUS)

		// main branch
		case "MAIN_BRANCH":
			data.Set("project", GIT_REPO_)
			data.Set("name", *BRANCH)

			EP = BRANCH_EP

			STATUS := REQUEST(URL, EP, TOKEN64, data)
			RESULT = append(RESULT, STATUS)
		}
	}

	for _, i := range RESULT {
		if !strings.Contains(i, "204") {
			RES = "Error"
		} else {
			RES = "Success"
		}
	}

	return RES

}

func main() {

	_, err := os.Stat(*GIT_REPO)
	if os.IsNotExist(err) {
		log.Printf("[+] Adding PR decoration to: %s .", *GIT_REPO)
		if DRYRUN {
			log.Printf("[+] DRYRUN: %#v,%#v", *GIT_REPO, *BRANCH)
		} else {
			log.Printf("[+] %#v,%#v,%#v", *GIT_REPO, *BRANCH, PR_DECORATION(*GIT_REPO))
		}

	} else {
		file, _ := os.ReadFile(*GIT_REPO)
		str := strings.TrimSpace(string(file))
		lines := strings.Split(str, "\n")
		for _, REPO := range lines {
			REPO = regexp.MustCompile(`["']`).ReplaceAllString(REPO, ``)

			if DRYRUN {
				log.Printf("[+] DRYRUN:  %#v,%#v", REPO, *BRANCH)
			} else {
				// actual run
				RESULT := PR_DECORATION(REPO)
				log.Printf("[+] %#v,%#v,%#v", REPO, *BRANCH, RESULT)
			}
		}
	}
}
