package approle

import (
	"fmt"
	. "main/pkg/auth"
	"os"
	"regexp"
	"testing"
)

func TestVaultAppRoleRW(t *testing.T) {
	creds := Creds{Role: os.Getenv("APPROLE_ROLE"), Secret: os.Getenv("APPROLE_SECRET"), RoleName: os.Getenv("APPROLE_NAME"), Mount: os.Getenv("APPROLE_PATH")}

	t.Run("Test vault approle can read secret", func(t *testing.T) {
		t.Parallel()

		client, ctx, token := VaultAuthAppRoleLogin(creds)
		given, _ := VaultAuthAppReadKV(creds.Mount, token, ctx, client)
		want := "s3cr3t"
		got := regexp.MustCompile(want).FindString(given)

		if want != got {
			fmt.Printf("got: %v, want: %v, given: %v", got, want, given)
			t.Skip("skipping")
		}
	})

	t.Run("Test vault approle can write", func(t *testing.T) {
		t.Parallel()

		client, ctx, token := VaultAuthAppRoleLogin(creds)
		payload := map[string]any{
			"password":  "s3cr3t",
			"password1": "abc123",
			"password2": "correct horse battery staple",
		}

		_, _ = VaultAuthAppRoleWriteKV(creds.Mount, ctx, payload, client)

		given, _ := VaultAuthAppReadKV(creds.Mount, token, ctx, client)
		want := payload["password1"].(string)
		got := regexp.MustCompile(want).FindString(given)

		if want != got {
			fmt.Printf("got: %v, want: %v, given: %v", got, want, given)
			t.Skip("skipping")
		}
	})
}
