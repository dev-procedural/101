package approle

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/hashicorp/vault-client-go"
	"github.com/hashicorp/vault-client-go/schema"
)

func VaultAuthAppReadKV(path, token string, ctx context.Context, client *vault.Client) (string, error) {

	s, err := client.Secrets.KvV2Read(
		ctx,
		path,
		vault.WithMountPath("secret"),
	)
	if err != nil {
		// fmt.Printf("%#v", err)
		return "", err
	}
	json, _ := json.Marshal(s.Data.Data)
	result := fmt.Sprintf("%s", json)

	return result, nil
}

func VaultAuthAppRoleWriteKV(path string, ctx context.Context, payload map[string]any, client *vault.Client) (bool, error) {

	s, err := client.Secrets.KvV2Write(
		ctx,
		path,
		schema.KvV2WriteRequest{
			Data: payload,
		},
		vault.WithMountPath("secret"),
	)
	if err != nil {
		fmt.Printf("%#v", err)
		return false, err
	}
	if fmt.Sprintf("%T", s.Data.CreatedTime) == "time.Time" {
		return true, nil
	}
	// result := fmt.Sprintf("%T", s.Data.CreatedTime)
	return true, nil

}
