package auth

import (
	"context"
	"log"
	"time"

	"github.com/google/go-github/github"
	"github.com/hashicorp/vault-client-go"
	"github.com/hashicorp/vault-client-go/schema"
	"golang.org/x/oauth2"
)

type Creds struct {
	Role, Secret, RoleName, Mount string
}

const VaultUrl = "https://prod.vault.COMPANY.io"

func VaultAuth(token string) (*vault.Client, context.Context, string) {
	ctx := context.Background()

	client, err := vault.New(
		vault.WithAddress(VaultUrl),
		vault.WithRequestTimeout(30*time.Second),
	)
	if err != nil {
		log.Println(err)
	}

	resp, err := client.Auth.GithubLogin(
		ctx,
		schema.GithubLoginRequest{
			Token: token,
		},
	)
	if err != nil {
		log.Println(err)
	}

	TOKEN := resp.Auth.ClientToken
	if err := client.SetToken(TOKEN); err != nil {
		log.Println(err)
	}

	return client, ctx, TOKEN
}

func VaultAuthAppRoleLogin(creds Creds) (*vault.Client, context.Context, string) {
	ctx := context.Background()

	client, err := vault.New(
		vault.WithAddress(VaultUrl),
		vault.WithRequestTimeout(30*time.Second),
	)
	if err != nil {
		log.Println(err)
	}

	resp, err := client.Auth.AppRoleLogin(
		ctx,
		schema.AppRoleLoginRequest{
			RoleId:   creds.Role,
			SecretId: creds.Secret,
		},
	)
	if err != nil {
		log.Println("[!]", err)
	}
	token := resp.Auth.ClientToken

	if err := client.SetToken(token); err != nil {
		log.Println(err)
	}

	return client, ctx, token

}

func VaultIsAdmin(token string, client *vault.Client, ctx context.Context) bool {
	t, err := client.Auth.TokenLookUpSelf(ctx)
	if err != nil {
		log.Println("ERROR:", err)
		return false
	}
	for _, i := range t.Data["policies"].([]interface{}) {
		switch {
		case i == "admin":
			return true
		default:
			return false
		}
	}
	return false
}

func VaultAuthAppRoleAssumeAWS(path string, creds Creds, client *vault.Client) map[string]interface{} {

	resp2, err := client.Secrets.AwsGenerateStsCredentials(
		context.Background(),
		creds.RoleName,
		vault.WithMountPath("aws_v2"),
		// vault.WithRequestCallbacks(func(r *http.Request) {
		// 	log.Printf("%v", *r)
		// }),
	)
	if err != nil {
		log.Printf("[E] %v ", err)
	}
	return resp2.Data

}

// ensure token have perms enough
func GithubAuth(token string) github.Client {
	ctx := context.Background()
	ts := oauth2.StaticTokenSource(
		&oauth2.Token{AccessToken: token},
	)

	tc := oauth2.NewClient(ctx, ts)

	client, err := github.NewEnterpriseClient(
		"https://github.COMPANY.ca/api/v3",
		"https://github.COMPANY.ca/api/v3",
		tc,
	)
	if err != nil {
		log.Println(err)
	}

	return *client
}
