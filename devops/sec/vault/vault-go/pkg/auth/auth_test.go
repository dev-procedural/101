package auth

import (
	"fmt"
	"os"
	"testing"

	"github.com/hashicorp/vault-client-go"
)

func TestVaultAuth(t *testing.T) {
	t.Run("Test github vault token authentication return a client", func(t *testing.T) {
		t.Parallel()

		var want_ *vault.Client
		got_, _, _ := VaultAuth(os.Getenv("GIT_TOKEN"))
		got := fmt.Sprintf("%T", got_)
		want := fmt.Sprintf("%T", want_)

		// test, err := got_.System.Unwrap(context.Background(), schema.UnwrapRequest{Token: token})
		if want != got {
			t.Errorf("got %#v want %#v\n", got_, want)
		}
	})

	t.Run("Test github vault token authentication is admin", func(t *testing.T) {
		t.Parallel()

		client, ctx, token := VaultAuth(os.Getenv("GIT_TOKEN"))
		isAdmin := VaultIsAdmin(token, client, ctx)

		if isAdmin == false {
			fmt.Printf(">>> %#v\n", isAdmin)
			t.Skip("Skip Test failed ")
		}
	})

	creds := Creds{Role: os.Getenv("APPROLE_ROLE"), Secret: os.Getenv("APPROLE_SECRET"), RoleName: os.Getenv("APPROLE_NAME")}
	t.Run("Test vault approle auth", func(t *testing.T) {
		given, _, _ := VaultAuthAppRoleLogin(creds)
		got := fmt.Sprintf("%T", given)
		want := fmt.Sprintf("%T", &vault.Client{})
		if want != got {
			fmt.Printf("got %v want %v given, %v", got, want, given)
			t.Skip("skipping")
		}
	})

	t.Run("Test vault approle can assume aws creds", func(t *testing.T) {
		t.Parallel()

		client, _, _ := VaultAuthAppRoleLogin(creds)
		given := VaultAuthAppRoleAssumeAWS("path", creds, client)
		got := given["access_key"]
		want := ""
		if got == "" {
			t.Skip("skipping")
			t.Errorf("got %v want %v given, %v", got, want, given)
		}
	})
}

func TestGithubAuth(t *testing.T) {
	t.Run("Test Github api authentication", func(t *testing.T) {
		t.Parallel()

		github_client := GithubAuth(os.Getenv("GIT_TOKEN"))
		// fmt.Println(github_client)
		got := ""
		want := ""
		if want != got {
			t.Errorf("got %v want %v given, %v", got, want, github_client)
		}
	})

}
