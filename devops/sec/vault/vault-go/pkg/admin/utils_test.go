package admin

import (
	"context"
	"fmt"
	"os"
	"regexp"
	"strings"
	"testing"

	. "main/pkg/auth"
)

// _____________________________[ GITHUB

func TestGitRepo(t *testing.T) {
	t.Run("Test Github api read repo file", func(t *testing.T) {
		client := GithubAuth(os.Getenv("GIT_TOKEN"))
		given := GithubRepoReadFile(
			context.Background(),
			"COMPANY",
			"COMPANY-vault-tf",
			"secrets-config/active_COMPANY.yaml",
			client,
		)
		// fmt.Println(given)
		want := "vault-active_COMPANY_platform-devs"
		got := regexp.MustCompile(want).FindString(given)
		if got != want {
			fmt.Printf("got %v want %v given, %v", got, want, given)
			t.Skip("Skip Test failed ")
		}
	})

	t.Run("Test Github api list files", func(t *testing.T) {
		client := GithubAuth(os.Getenv("GIT_TOKEN"))
		given := GithubRepoReadFile(
			context.Background(),
			"COMPANY",
			"COMPANY-vault-tf",
			".",
			client,
		)

		want := "secrets-config"
		got := regexp.MustCompile(want).FindString(given)
		if want != got {
			fmt.Printf("got %v want %v given, %v", got, want, given)
			t.Skip("Skip Test failed ")
		}
	})

	t.Run("Test Github Team match secrets file", func(t *testing.T) {
		client := GithubAuth(os.Getenv("GIT_TOKEN"))
		teams := GithubRepoReadFile(
			context.Background(),
			"COMPANY",
			"COMPANY-vault-tf",
			"secrets-config/active_COMPANY.yaml",
			client,
		)
		given := GithubGetTeam(
			context.Background(),
			"secrets-config/abacus.yaml",
			teams,
			client,
		)
		want := "vault-active_COMPANY_platform-devs"
		got := regexp.MustCompile(want).FindString(given)
		if want != got {
			fmt.Printf("got %v want %v given, %v", got, want, given)
			t.Skip("Skip Test failed ")
		}
	})

	t.Run("Test Github api list repos", func(t *testing.T) {
		client := GithubAuth(os.Getenv("GIT_TOKEN"))
		given := GithubRepoList(
			context.Background(),
			"jolea",
			client,
		)
		// fmt.Println(given)
		got := ""
		want := ""
		if want != got {
			fmt.Printf("got %v want %v given, %v", got, want, given)
			t.Skip("Skip Test failed ")
		}
	})
}

// _____________________________[ VAULT

func TestVaultSecretFactoryPolicies(t *testing.T) {
	t.Run("Test vault file name have aws policy attached", func(t *testing.T) {
		t.Parallel()

		client, _, _ := VaultAuth(os.Getenv("GIT_TOKEN"))
		role := "regression-test-nonprod"
		given, _ := VaultGetRole(role, client)
		policies := given[0]["policies"]
		want := "aws-regression-test-nonprod"
		got := ""
		for _, i := range policies.([]interface{}) {
			switch {
			case strings.Contains(i.(string), "aws"):
				got += i.(string)
			}
		}
		if want != got {
			fmt.Printf("got %v want %v given, %v", got, want, given)
			t.Skip("Skip Test failed ")
		}

	})

	t.Run("Test vault policies attached to role exists", func(t *testing.T) {
		t.Parallel()

		given := ""
		got := ""
		want := ""
		if want != got {
			fmt.Printf("got %v want %v given, %v", got, want, given)
			t.Skip("Skip Test failed ")
		}
	})
}
