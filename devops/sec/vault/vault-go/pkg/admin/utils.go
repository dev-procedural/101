package admin

import (
	"context"
	"fmt"
	"log"
	"regexp"
	"strings"
	"time"

	_ "main/pkg/auth"

	"github.com/google/go-github/github"
	"github.com/hashicorp/vault-client-go"
	"gopkg.in/yaml.v3"
)

// _________________________[ GITHUB

func GithubRepoReadFile(ctx context.Context, org, repo, file string, client github.Client) string {

	// var cont string
	file_, dirs, _, _ := client.Repositories.GetContents(ctx, org, repo, file, &github.RepositoryContentGetOptions{})
	result := ""
	switch {
	case file != ".":

		yaml_file, _ := file_.GetContent()
		var data map[string]interface{}
		if err := yaml.Unmarshal([]byte(yaml_file), &data); err != nil {
			log.Println(err)
		}
		for _, x := range data["github_role"].(map[string]interface{}) {
			result += fmt.Sprintf("%#v\n", x)
		}

	case file == ".":
		for _, i := range dirs {
			result += "\n" + strings.Trim(*i.Path, "\"")
		}
	}
	// fmt.Printf("\n[!] %#v", result)
	return result
}

func GithubRepoList(ctx context.Context, user string, client github.Client) string {

	opt := &github.RepositoryListOptions{Type: "owner", Sort: "updated", Direction: "desc"}
	repo, _, err := client.Repositories.List(ctx, user, opt)
	result := fmt.Sprintf("%#v\n%#v\n", repo, err)

	return result
}

func GithubGetTeam(ctx context.Context, file, teams string, client github.Client) string {
	const org = "COMPANY"

	opts := &github.ListOptions{
		PerPage: 100,
	}

	lista := make(chan string)
	quit := make(chan bool)
	teams_list := regexp.MustCompile("[^\n\"]+").FindAllString(teams, -1)
	result := ""

	for _, team := range teams_list {
		go func(team string) {
			for {
				teams_, resp, err := client.Teams.ListTeams(ctx, org, opts)
				if err != nil {
					log.Println(err)
				}
				for _, t := range teams_ {
					if t.GetName() == team {
						// fmt.Printf(">>> %#v\n", team)
						lista <- "\n" + t.GetName()
					}
				}
				if resp.NextPage == 0 {
					break
				}
				opts.Page = resp.NextPage
			}
		}(team)
	}

	go func() {
		for {
			select {
			case v := <-lista:
				result += v
			case <-time.After(1 * time.Second):
				quit <- true
			}
		}
	}()
	<-quit

	// fmt.Printf("\n[!] %#v\n", result)
	return result

}

// _________________________[ VAULT

func VaultGetRole(ROLE string, client *vault.Client) (result []map[string]interface{}, err error) {

	resp, err := client.Auth.AwsReadAuthRole(
		context.Background(),
		ROLE,
		vault.WithMountPath("aws"),
	)
	if err != nil {
		log.Println(err)
	}

	// result = append(result, map[string]interface{}{"iam_roles": resp.Data["bound_iam_principal_arn"].([]interface{})})
	result = append(result, map[string]interface{}{"policies": resp.Data["token_policies"].([]interface{})})

	return result, nil
}

func VaultListRole(Role_Pattern string, client vault.Client) (result []string, err error) {

	resp, err := client.Auth.AwsListAuthRoles(
		context.Background(),
		vault.WithMountPath("aws"),
	)
	if err != nil {
		log.Printf("[E] %v ", err)
	}

	for _, i := range resp.Data["keys"].([]interface{}) {

		MATCHES := regexp.MustCompile(Role_Pattern).FindAllString(i.(string), -1)
		if MATCHES != nil {
			// log.Printf("[!] %#v", i)
			result = append(result, i.(string))
		}

	}

	return result, nil
}
