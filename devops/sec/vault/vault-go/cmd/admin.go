package admin

import (
	"flag"
	auth "main/pkg/auth"
	"os"
)

var (
	DRYRUN bool
	ROLE   string
	POLICY string
	TOKEN  string
)

func init() {
	flag.BoolVar(&DRYRUN, "dryrun", false, "dryrun do not execute code")
	flag.StringVar(&TOKEN, "t", os.Getenv("GIT_TOKEN"), "GITHUB - admin - token")
	flag.StringVar(&ROLE, "r", os.Getenv("VAULT_ROLE"), "ROLE exported variable or argument")
	flag.StringVar(&POLICY, "r", os.Getenv("VAULT_POLICY"), "Policy exported variable or argument")
	flag.Parse()
	const VaultUrl = "https://prod.vault.COMPANY.io"
}

func main() {

	client, ctx, token := auth.VaultAuth(TOKEN)

	//// TEST SECRETS FILE
	// get repo file issues:
	//   git team uses vault-team-env definition
	//   git team exist
	//   aws_role account
	//   file name match secret space
	//   file name extension is right
	//
	//// TEST AWS ROLE
	// test role and principal id are synced
}
