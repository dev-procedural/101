package main

import (
	"flag"
	"fmt"
	approle "main/pkg/approle"
	auth "main/pkg/auth"
	"os"
)

var (
	DRYRUN      bool
	TOKEN       string
	ROLE_ID     string
	ROLE_NAME   string
	SECRET_ID   string
	VAULT_PATH  string
	ENVIRONMENT string
)

const VaultUrl = "https://prod.vault.COMPANY.io"

func init() {
	flag.BoolVar(&DRYRUN, "dryrun", false, "dryrun do not execute code")
	flag.StringVar(&ROLE_ID, "r", os.Getenv("ROLE_ID"), "ROLE_ID exported variable or use as argument")
	flag.StringVar(&ROLE_NAME, "n", os.Getenv("ROLE_NAME"), "ROLE_NAME exported variable or use as argument")
	flag.StringVar(&SECRET_ID, "s", os.Getenv("SECRET_ID"), "SECRET_ID exported variable or argument")
	flag.StringVar(&VAULT_PATH, "p", os.Getenv("VAULT_PATH"), "PATH to secret, must be abs path")
	flag.Parse()
}

func main() {

	creds := auth.Creds{
		Role:     ROLE_ID,
		Secret:   SECRET_ID,
		RoleName: ROLE_NAME,
		Mount:    VAULT_PATH,
	}

	client, ctx, token := auth.VaultAuthAppRoleLogin(creds)
	resp, _ := approle.VaultAuthAppReadKV(creds.Mount, token, ctx, client)

	fmt.Printf("%s", resp)

}
