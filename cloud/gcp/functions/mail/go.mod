module functions

go 1.16

require (
	github.com/sendgrid/rest v2.6.9+incompatible // indirect
	github.com/sendgrid/sendgrid-go v3.11.1+incompatible
	github.com/stretchr/testify v1.7.1 // indirect
	golang.org/x/net v0.0.0-20220412020605-290c469a71a5 // indirect
)
