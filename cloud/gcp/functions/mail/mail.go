package functions

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
)

// HelloGet is an HTTP Cloud Function.
func HelloGet(w http.ResponseWriter, r *http.Request) {
	for _, i := range os.Environ() {
		fmt.Fprintf(w, "%s\n", i)
	}

	log.Println("Running...")
	from := mail.NewEmail("Julieto ", "julio.cesar.guerrero.olea@gmail.com")
	subject := "Sending with Twilio SendGrid is Fun"
	to := mail.NewEmail("Example User", "julio.cesar.guerrero.olea@gmail.com")
	plainTextContent := "and easy to do anywhere, even with Go"
	htmlContent := "<strong>and easy to do anywhere, even with Go</strong>"
	message := mail.NewSingleEmail(from, subject, to, plainTextContent, htmlContent)
	client := sendgrid.NewSendClient(os.Getenv("secret"))
	response, err := client.Send(message)
	if err != nil {
		log.Println(err)
	} else {
		fmt.Fprintf(w, "%s\n", response.Body)
	}

}
