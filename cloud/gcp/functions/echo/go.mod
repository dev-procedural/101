module echo

go 1.16

require (
	github.com/gin-gonic/gin v1.7.7
	github.com/rs/cors/wrapper/gin v0.0.0-20220223021805-a4a5ce87d5a2
)
