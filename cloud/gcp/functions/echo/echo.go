package echo

import (
	"fmt"
	"os"
	"github.com/gin-gonic/gin"
	// cors "github.com/rs/cors/wrapper/gin"
)

func gin() {
	fmt.Println("This is a test msg")

	r := gin.Default()

	// r.Use(cors.AllowAll())
	r.GET("/", func(c *gin.Context) {

		a := os.Environ()
		c.ShouldBind(&a)
		//take action
		c.JSON(200, gin.H{"msg": a})
	})

	r.Run()
}
