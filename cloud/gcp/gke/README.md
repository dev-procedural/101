#Creates a gke cluster and secure it with service acc

##GKE autopilot
```
gcloud container --project "MYPROJECT" clusters create-auto "app1-clone-1" --region "us-central1" --release-channel "regular" --network "projects/MYPROJECT/global/networks/default" --subnetwork "projects/MYPROJECT/regions/us-central1/subnetworks/default"
```

##Connect to cluster
```
gcloud container clusters get-credentials app1 --region us-central1 --project "MYPROJECT"
```
