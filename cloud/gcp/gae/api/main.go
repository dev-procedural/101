package main

import (
	"fmt"

	"github.com/gin-gonic/gin"
	cors "github.com/rs/cors/wrapper/gin"
)

func main() {
	r := gin.Default()
	r.Use(cors.AllowAll())

	r.POST("/ping", func(c *gin.Context) {

		a := make(map[string]string)
		c.ShouldBind(&a)
		//take action
		c.JSON(200, gin.H{"msg": a})
		fmt.Println(a)
	})

	r.Run(":8080")
}
