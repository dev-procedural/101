package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"strings"

	// "sort"
	// "errors"
	. "main/conn"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/ec2"
	"github.com/aws/aws-sdk-go-v2/service/ec2/types"
	"github.com/go-gota/gota/dataframe"
	"github.com/go-gota/gota/series"
)

type Vols struct {
	Account    string
	InstanceId string
	VolumeId   string
	VolumeType string
	Team       string
	Tags       string
}

var cfg aws.Config

var (
	MASTER_   string
	ROLE_     string
	REGION_   string
	UPGRADE_  bool
	SNAP_DEL_ bool
)

var ACCOUNT_ interface{}

func init() {
	isbrown := flag.Bool("bf", false, "is brownfield")
	subacc := flag.String("a", "All accounts in org", "account id comma separated")
	flag.StringVar(&REGION_, "r", "us-east-1", "region")
	flag.BoolVar(&UPGRADE_, "u", false, "update ebs to gp3")
	flag.BoolVar(&SNAP_DEL_, "rm-snap", false, "rm - remove snaps not matching")
	flag.Parse()

	if *isbrown {
		MASTER_ = "master_brown"
		ROLE_ = "OrganizationAccountAccessRole"
	} else {
		MASTER_ = "master"
		ROLE_ = "AWSControlTowerExecution"
	}

	cfg, _ = config.LoadDefaultConfig(context.TODO(),
		config.WithRegion(REGION_),
		config.WithSharedConfigProfile(MASTER_),
	)

	// 1 or Many Accounts or ALL o f them
	switch {
	case strings.Contains(*subacc, ","):
		fmt.Println("[!] Running in", strings.Split(strings.Trim(*subacc, " "), ","))
		ACCOUNT_ = strings.Split(strings.Trim(*subacc, " "), ",")
	case *subacc != "":
		fmt.Println("[!] Running in acc", *subacc)
		ACCOUNT_ = []string{*subacc}
	default:
		ACCOUNT_ = SUBACCOUNTS(cfg)
		fmt.Println("[!] Running in all accounts...")
	}

}

func LIST_EBS(subaccount, region string) []Vols {

	VOLUMES := []Vols{}
	TAG_VAL := "COMPANY_NAME:TeamEmail"

	cfgSub, _ := ASSUME(cfg, subaccount, ROLE_, region)
	ec2sub := ec2.NewFromConfig(cfgSub)

	resp, _ := ec2sub.DescribeVolumes(
		context.TODO(),
		&ec2.DescribeVolumesInput{},
	)
	// fmt.Printf("[!] %#v",resp.Volumes)
	for _, v := range resp.Volumes {
		TAGS := v.Tags
		TEAM := ""
		TT := ""
		ATTCHS := v.Attachments
		INSTANCEID := ""

		// ATTACHMENTS
		for _, ATTCH := range ATTCHS {
			// fmt.Printf("%#v", *ATTCH.InstanceId)
			INSTANCEID = *ATTCH.InstanceId
		}

		// TAGS
		for _, TAG := range TAGS {
			if *TAG.Key == TAG_VAL {
				TEAM = *TAG.Value
			}
			TT += fmt.Sprintf("%v-%v|", *TAG.Key, *TAG.Value)
		}

		VOLUMES = append(VOLUMES, Vols{
			Account:    subaccount,
			VolumeType: fmt.Sprintf("%v", v.VolumeType),
			VolumeId:   *v.VolumeId,
			Team:       TEAM,
			Tags:       TT,
			InstanceId: INSTANCEID,
		},
		)
	}

	TOKEN := resp.NextToken
	if TOKEN != nil {
		for {
			resp, err := ec2sub.DescribeVolumes(context.TODO(),
				&ec2.DescribeVolumesInput{
					NextToken: TOKEN,
				})
			if err != nil {
				fmt.Printf("%#v", err.Error())
				break
			}

			TEAM := ""
			TT := ""
			INSTANCEID := ""
			for _, v := range resp.Volumes {
				TAGS := v.Tags
				ATTCHS := v.Attachments

				// ATTACHMENTS
				for _, ATTCH := range ATTCHS {
					INSTANCEID = *ATTCH.InstanceId
				}

				// TAGS
				for _, TAG := range TAGS {
					if *TAG.Key == TAG_VAL {
						TEAM = *TAG.Value
					}
					TT += fmt.Sprintf("%v|%v|", *TAG.Key, *TAG.Value)
				}
				VOLUMES = append(VOLUMES, Vols{
					Account:    subaccount,
					VolumeType: fmt.Sprintf("%v", v.VolumeType),
					VolumeId:   *v.VolumeId,
					Team:       TEAM,
					Tags:       TT,
					InstanceId: INSTANCEID,
				},
				)
			}

			TOKEN = resp.NextToken
			if aws.ToString(resp.NextToken) == "" {
				break
			}

		}
	}

	// df := dataframe.LoadStructs(VOLUMES)
	return VOLUMES

}

func UPDATE_EBS(subaccount, region string, vols []string) {
	var Type types.VolumeType
	Type = "gp3"

	cfgSub, _ := ASSUME(cfg, subaccount, ROLE_, region)
	ec2sub := ec2.NewFromConfig(cfgSub)

	for _, vol := range vols {
		fmt.Printf("[!] Updating : %#v\n", vol)
		_, err := ec2sub.ModifyVolume(
			context.TODO(),
			&ec2.ModifyVolumeInput{
				VolumeId:   &vol,
				VolumeType: Type,
			},
		)
		if err != nil {
			fmt.Printf("[?] %#v %#v \n", subaccount, err.Error())
		}
	}
}

// func DELETE_EBS(subaccount, region string, vols []string) {
// 	cfgSub, _ := ASSUME(cfg, subaccount, ROLE_, region)
// 	ec2sub := ec2.NewFromConfig(cfgSub)

// 	ec2sub.DeleteSnapshot

// }

func _UPDATE(ACCS interface{}) {
	EBS_DF := [][]Vols{}
	EBS_ALL := []Vols{}

	// RUN IN ALL ACCOUNTS
	for _, ACC := range ACCS.([]string) {
		DF := LIST_EBS(ACC, REGION_)
		EBS_DF = append(EBS_DF, DF)
	}

	// PARSE RESULT
	for _, DD := range EBS_DF {
		for _, j := range DD {
			EBS_ALL = append(EBS_ALL, j)
		}
	}

	// fmt.Printf("%#v\n", EBS_ALL)
	file, _ := os.Create("/tmp/ebsreport.csv")
	defer file.Close()

	if len(EBS_ALL) == 0 {
		fmt.Println("[!] Not snapshots in accounts")
		return
	}
	result := dataframe.LoadStructs(EBS_ALL)
	result.WriteCSV(file)

	// fmt.Println(result.String())
	fmt.Println("[+] Please check result report /tmp/ebsreport.csv")

	if UPGRADE_ {
		GP2 := result.Filter(dataframe.F{
			Colname:    "VolumeType",
			Comparator: series.Eq,
			Comparando: "gp2"})

		fmt.Println(GP2)

		for _, ACC := range ACCS.([]string) {

			VOLS := GP2.Filter(dataframe.F{
				Colname:    "Account",
				Comparator: series.Eq,
				Comparando: ACC,
			})

			UPDATE_EBS(ACC, REGION_, VOLS.Col("VolumeId").Records())
		}
	}
}

type SNAP struct {
	SNAPID string
	DESC   string
}

func LIST_SNAPSHOTS(subaccount, region, target_acc string) (RESULT []SNAP) {
	cfgSub, _ := ASSUME(cfg, subaccount, ROLE_, region)
	ec2sub := ec2.NewFromConfig(cfgSub)
	// N := int32(2)

	SNP, err := ec2sub.DescribeSnapshots(
		context.TODO(),
		&ec2.DescribeSnapshotsInput{
			OwnerIds: []string{"self"},
			// MaxResults: &N,
		},
	)
	if err != nil {
		fmt.Printf("%#v", err.Error())
	}

	// TOKEN := SNP.NextToken
	// if TOKEN != nil {
	// 	for {
	// 		SNP, err := ec2sub.DescribeSnapshots(
	// 			context.TODO(),
	// 			&ec2.DescribeSnapshotsInput{
	// 				OwnerIds: []string{"self"},
	// 				// MaxResults: &N,
	// 			},
	// 		)
	// 		if err != nil {
	// 			fmt.Printf("%#v", err.Error())
	// 			break
	// 		}

	// 		TOKEN = SNP.NextToken
	// 		if aws.ToString(SNP.NextToken) == "" {
	// 			break
	// 		}

	// 	}
	// }

	NON_DELETE := []SNAP{}
	for _, S := range SNP.Snapshots {
		if strings.Contains(*S.Description, target_acc) {
			NON_DELETE = append(NON_DELETE, SNAP{
				SNAPID: *S.SnapshotId,
				DESC:   *S.Description,
			})
		}
	}

	for _, SS := range SNP.Snapshots {
		if !strings.Contains(*SS.Description, target_acc) {
			RESULT = append(RESULT, SNAP{
				SNAPID: *SS.SnapshotId,
				DESC:   *SS.Description,
			})
		}
	}

	if SNAP_DEL_ {
		for _, SN := range RESULT[len(RESULT)-20 : len(RESULT)] {
			_, err := ec2sub.DeleteSnapshot(
				context.TODO(),
				&ec2.DeleteSnapshotInput{
					SnapshotId: &SN.SNAPID,
				},
			)
			if err != nil {
				fmt.Println("[?]", err.Error())
			}
			fmt.Println("[!] Deleting: ", SN.SNAPID)
		}
	}

	return RESULT
}

func main() {
	// REGIONS := REGIONS()
	var ACCS interface{}

	switch ACCOUNT_.(type) {
	case []Account:
		df := dataframe.LoadStructs(ACCOUNT_)
		ACCS = df.Col("ID").Records()
	case []string:
		ACCS = ACCOUNT_
	}

	// RUN IN ALL ACCOUNTS
	for _, ACC := range ACCS.([]string) {
		fmt.Println("[!]", ACC)
		DF := LIST_SNAPSHOTS(ACC, REGION_, "i-0e69164879af8a7c4")
		fmt.Println(len(DF), DF[:2])
	}
}
