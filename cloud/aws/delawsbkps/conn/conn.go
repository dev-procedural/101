package conn

import (
	"context"
	"fmt"

	// "os"
	"errors"
	"sort"

	// "io"
	// "encoding/json"
	// "log"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/credentials"
	"github.com/aws/aws-sdk-go-v2/service/ec2"
	"github.com/aws/aws-sdk-go-v2/service/organizations"
	"github.com/aws/aws-sdk-go-v2/service/sts"
	// "github.com/aws/aws-sdk-go-v2/service/organizations/types"
)

type CONN interface {
	ASSUME(cfg aws.Config, Account, Role, Region string) (aws.Config, error)
	REGIONS(cfg aws.Config) []string
	SUBACCOUNTS(cfg aws.Config) []Account
}

func ASSUME(cfg aws.Config, Account, Role, Region string) (aws.Config, error) {

	client := sts.NewFromConfig(cfg)

	ROLE := "arn:aws:sts::" + Account + ":role/" + Role
	SESS := "ASSUMED-GO"
	input := &sts.AssumeRoleInput{
		RoleArn:         &ROLE,
		RoleSessionName: &SESS,
	}

	result, err := client.AssumeRole(context.TODO(), input)
	if err != nil {
		fmt.Printf("Got an error assuming the role: %s", err)
		return aws.Config{}, errors.New(err.Error())
	}

	creds := *result.Credentials
	// fmt.Printf("[!] %#v", cfg)

	cfgSub, err := config.LoadDefaultConfig(context.TODO(),
		config.WithRegion(Region),
		config.WithCredentialsProvider(credentials.StaticCredentialsProvider{
			Value: aws.Credentials{
				AccessKeyID:     *creds.AccessKeyId,
				SecretAccessKey: *creds.SecretAccessKey,
				SessionToken:    *creds.SessionToken,
				Source:          "assumerole",
			},
		}))

	if err != nil {
		fmt.Println("error:", err)
		return aws.Config{}, errors.New(err.Error())
	}

	return cfgSub, nil
}

type Account struct {
	Name   string
	ID     string
	Status string
}

func REGIONS(cfg aws.Config) []string {
	EC2 := ec2.NewFromConfig(cfg)

	REGIONS := []string{}

	result, _ := EC2.DescribeRegions(
		context.TODO(),
		&ec2.DescribeRegionsInput{},
	)

	for _, v := range result.Regions {
		// fmt.Printf("%#v\n", *v.RegionName)
		REGIONS = append(REGIONS, *v.RegionName)
	}

	return REGIONS
}

func SUBACCOUNTS(cfg aws.Config) []Account {

	accounts := []Account{}

	orgs := organizations.NewFromConfig(cfg)
	response, _ := orgs.ListAccounts(context.TODO(), &organizations.ListAccountsInput{})
	for _, v := range response.Accounts {
		accounts = append(accounts, Account{
			Name:   *v.Name,
			ID:     *v.Id,
			Status: fmt.Sprintf("%v", v.Status),
		})
	}

	TOKEN := response.NextToken
	if TOKEN != nil {
		for {
			response, err := orgs.ListAccounts(context.TODO(),
				&organizations.ListAccountsInput{
					NextToken: TOKEN,
				})

			if err != nil {
				fmt.Printf("%#v", err.Error())
				break
			}

			for _, v := range response.Accounts {
				accounts = append(accounts,
					Account{
						Name:   *v.Name,
						ID:     *v.Id,
						Status: fmt.Sprintf("%v", v.Status),
					})
			}

			TOKEN = response.NextToken
			if aws.ToString(response.NextToken) == "" {
				break
			}

		}
	}

	sort.SliceStable(accounts, func(i, j int) bool {
		return accounts[i].Name < accounts[j].Name
	})

	// fmt.Println(len(accounts))
	result := []Account{}
	for _, v := range accounts {
		if v.Status == "ACTIVE" {
			// fmt.Println(v.Name,v.Status)
			result = append(result,
				Account{
					Name:   v.Name,
					ID:     v.ID,
					Status: v.Status,
				})
		}
	}

	// fmt.Printf("%#v", result)
	return result
}
