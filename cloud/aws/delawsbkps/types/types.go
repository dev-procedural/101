package types

type Account struct {
  Name   string
  ID     string
  Status string
}
