# Dynamo Lock remover

## this repo aims to help to remove tf state locks from dynamodb to avoid manual intervention

## Instructions

### setup credentials
- Exporting credentials as environment variables
```
export AWS_ACCESS_KEY_ID="CRED_ID"
export AWS_SECRET_ACCESS_KEY="CRED_SECRET"
export AWS_SESSION_TOKEN="CRED_SESSION_TOKEN"
```
- Pass as argument to the script
- Obtain credentials as you need, either vault or assuming a role

### script help
```
Usage of dynamodb:
  -dryrun
        dryrun just print out matched values
  -e string
        environment target dev/qa/uat/prod
  -k string
        key, default env AWS_ACCESS_KEY_ID
  -p string
        project or target lock
  -r string
        region (default "us-east-1")
  -s string
        secret, default env AWS_SECRET_ACCESS_KEY
  -t string
        token, default env AWS_SESSION_TOKEN
  -table string
        table (default "terraform-locks")
```

### example
- Exported credentials
```
export AWS_ACCESS_KEY_ID="CRED_ID"
export AWS_SECRET_ACCESS_KEY="CRED_SECRET"
export AWS_SESSION_TOKEN="CRED_SESSION_TOKEN"

go run dynamodb.go -p dev_mwaa/tss/terraform.tfstate
```

- None exported credentials
```
go run dynamodb.go -p dev_mwaa/tss/terraform.tfstate -k YOUR_KEY -s YOU_SECRET -t YOU_TOKEN
```
