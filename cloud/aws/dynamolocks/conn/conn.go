package conn

import (
	"context"
	"log"

	// "os"
	"errors"

	// "io"
	// "encoding/json"
	// "log"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/credentials"
	"github.com/aws/aws-sdk-go-v2/service/ec2"
	// "github.com/aws/aws-sdk-go-v2/service/organizations/types"
)

var (
	ACCOUNT_ string
	MASTER_  string
	ROLE_    string
	REGION_  string
	cfg      aws.Config
)

// type CONN interface {
// 	ASSUME(cfg aws.Config, Account, Role, Region string) (aws.Config, error)
// 	REGIONS(cfg aws.Config) []string
// 	SUBACCOUNTS(cfg aws.Config) []Account
// }

type CONFIG struct {
	MASTER_ string
	ROLE_   string
	REGION_ string
}

type Creds struct {
	AccessKeyId, SecretAccessKey, SessionToken, Profile string
}

func ASSUME(Region, SessionName string, creds Creds) (cfgSub aws.Config, err error) {

	switch {
	case creds.Profile == "":

		if creds.AccessKeyId == "" ||
			creds.SecretAccessKey == "" ||
			creds.SessionToken == "" {
			log.Fatal("[!] ANY or ALL of: AccessKeyId, SecretAccessKey, SessionToken is EMPTY, please check ")
		}

		cfgSub, err := config.LoadDefaultConfig(
			context.TODO(),
			config.WithRegion(Region),
			config.WithCredentialsProvider(credentials.StaticCredentialsProvider{
				Value: aws.Credentials{
					AccessKeyID:     creds.AccessKeyId,
					SecretAccessKey: creds.SecretAccessKey,
					SessionToken:    creds.SessionToken,
					Source:          SessionName,
				},
			}))

		if err != nil {
			log.Fatalf("error:", err)
			return aws.Config{}, errors.New(err.Error())
		}
		return cfgSub, nil

	default:
		if creds.Profile == "" {
			log.Fatal("[!] Profile empty")
		}

		cfgSub, _ = config.LoadDefaultConfig(
			context.TODO(),
			config.WithRegion("us-east-1"),
			config.WithSharedConfigProfile(creds.Profile))

		return cfgSub, nil
	}

	return aws.Config{}, nil
}

type Account struct {
	Name   string
	ID     string
	Status string
}

func REGIONS(cfg aws.Config) []string {
	EC2 := ec2.NewFromConfig(cfg)

	REGIONS := []string{}

	result, _ := EC2.DescribeRegions(
		context.TODO(),
		&ec2.DescribeRegionsInput{},
	)

	for _, v := range result.Regions {
		// fmt.Printf("%#v\n", *v.RegionName)
		REGIONS = append(REGIONS, *v.RegionName)
	}

	return REGIONS
}
