#!/bin/env bash

CWD=$( dirname $(realpath $0))
if [[ -f /tmp/list_`date +%Y_%m_%d`.lock ]]
then
  echo "[+] file updated, skipping update..."
else
  export AWS_PROFILE=master
  aws organizations list-accounts --profile master       | jq '.[][] |.Id + " " + .Name' > $CWD/green.acc.txt 
  echo [+] updated greenfield file $CWD/green.acc.txt

  export AWS_PROFILE=master_brown
  aws organizations list-accounts --profile master_brown | jq '.[][] |.Id + " " + .Name' > $CWD/brown.acc.txt
  echo [+] updated brownfield file $CWD/brown.acc.txt

  touch /tmp/list_`date +%Y_%m_%d`.lock
fi
