#!/bin/env python3

import boto3, time, os, sys, argparse, itertools

#__________________________[ Args 
parser = argparse.ArgumentParser(description='This takes care of account enrollment')
parser.add_argument('--targetAccount','-a', type=str, help='account id', nargs='?')
args = parser.parse_args()

#__________________________[ Managing connection 
session = boto3.Session(
      profile_name='master',
      region_name='us-east-1')

sso_client = session.client('sso-admin')
id_client  = session.client('identitystore')

instanceinfo = sso_client.list_instances(
        MaxResults=1,
        NextToken='string')

instance_arn     = instanceinfo['Instances'][0]['InstanceArn']
identitystore_id = instanceinfo['Instances'][0]['IdentityStoreId']

# GET ACCOUNTS
def getAccIds(account=''):
   '''
   WARNING only supports acc id
   account variable can be a string or a comma separated value string eg
   account='000000'
   allAccPerms = {v: accPerms(k) for k,v in getAccIds()} 
   account='000000,11111,22222'
   '''
   account = str(account)
   orgs = session.client('organizations')
   response = orgs.list_accounts()
   results_acc = response["Accounts"]
   while "NextToken" in response:
       response = orgs.list_accounts(NextToken=response["NextToken"])
       results_acc.extend(response["Accounts"])
   # FILTER FIELDS USEFUL FIELDS
   results_acc = [[i['Id'], i['Name']] for i in results_acc]
   # FILTER ACCOUNTS COMMA SEPARATED
   results_acc = [i for i in results_acc if any([account != '' and i[0] in account.split(','), account == ''])]
   return results_acc

# PERM FROM ACC
def accPerms(account):
   response = sso_client.list_permission_sets_provisioned_to_account(InstanceArn=instance_arn,AccountId=account)
   result_a = []
   result_a.append(response['PermissionSets'])
   while 'NextToken' in response:
       nt = response['NextToken']
       response = sso_client.list_permission_sets_provisioned_to_account(InstanceArn=instance_arn,AccountId=account,NextToken=nt)
       result_a.append(response['PermissionSets'])
   result_a = list(itertools.chain(*result_a))
   result = {}
   result['Id']    = account
   result['Perms'] = result_a
   return result

# REDUCE PERMISSIONS
reducePerms = lambda D: {k:",".join([str(i[1]) for i in D if i[0] == k]) for k,v in D}
# ab = lambda a: dict(map(lambda el  : (el, list(a).count(el)), a))
# SlicedDict1 = lambda di,n:   {i:a[i] for i in list(di.keys())[n:]}
# SlicedDict2 = lambda di,a,b: {list(di.keys())[list(di.keys()).index(i)]:di[i] for i in list(di.keys())[a:b]}


def getUser(userId):
   try:
      return id_client.describe_user(
      IdentityStoreId=identitystore_id,
      UserId=userId
      )['UserName']
   except:
      # TODO identify unused users and remove them
      pass

# FILTER ONLY USERS
def usersInAcc(acc,perm):

    response = sso_client.list_account_assignments(InstanceArn=instance_arn,
                                                   AccountId=acc,
                                                   PermissionSetArn=perm)
    result_a = []
    result_a.append(response['AccountAssignments'])

    while 'NextToken' in response:
       nt = response['NextToken']
       response = sso_client.list_account_assignments(InstanceArn=instance_arn,
                                                      AccountId=acc,
                                                      PermissionSetArn=perm,
                                                      NextToken=nt)
       result_a.append( response['AccountAssignments'] )

    result_a = list(itertools.chain(*result_a))
    result = [ [i['PrincipalId'], i['PermissionSetArn']] for i in result_a ]

    return result



getPermName = lambda perm: [i['Name'] for i in permSetAll if i['PermissionSetArn'] == perm]

# ALL PERM SET
response = sso_client.list_permission_sets(InstanceArn=instance_arn)
result   = []
while 'NextToken' in response:
    nt = response['NextToken']
    response = sso_client.list_permission_sets(InstanceArn=instance_arn, NextToken=nt)
    result.append(response['PermissionSets'])
result = list(itertools.chain(*result))
permSetAll = [sso_client.describe_permission_set(InstanceArn=instance_arn,PermissionSetArn=i)['PermissionSet'] for i in result]

allAccPerms = {v: accPerms(k) for k,v in getAccIds(args.targetAccount if args.targetAccount else "")}

# GET ALL USERS IN ALL ACCOUNTS
print('[+] Getting all users from account')
allUsersInAcc = {k:[ usersInAcc(v['Id'], i) for i in v['Perms']] for k,v in allAccPerms.items()}

# FLAT VAL LIST
allUsersInAcc = {k:list(itertools.chain(*v)) for k,v in allUsersInAcc.items() }

# REPLACE USER ID FOR MAIL AND PERM NAME
print('[+] Replacing ids for emails')
allUsersInAcc = {k:[[getUser(i[0]),getPermName(i[1])] for i in v ] for k,v in allUsersInAcc.items() }

# Validate empties and nones
allUsersInAcc = {k:[i for i in v if i[0]] for k,v in allUsersInAcc.items() }

allUsersInAcc = {k:[[i[0],i[1][0]] for i in v if i[1]] for k,v in allUsersInAcc.items() }

# REDUCE PERMS
allUsersInAcc = {k:reducePerms(v) for k,v in allUsersInAcc.items() }

temp = []
for k,v in allUsersInAcc.items():
   for q,w in v.items():
      temp.append(f"{k}|{q}|{w}")

with open('/tmp/report_creds.csv', 'w+') as FILE:
   for i in temp:
      print(i)
      FILE.write(i + '\n')

