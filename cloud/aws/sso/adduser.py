#!/bin/env python3

import boto3, time, os, sys, argparse


#__________________________[ Args 
parser = argparse.ArgumentParser(description='This takes care of account enrollment')
group = parser.add_mutually_exclusive_group(required=True)
group.add_argument('--dry',   help='run dry run',    action="store_true")
group.add_argument('--apply', help='apply changes, be sure env.yaml is ok', action="store_true")
parser.add_argument('--power', help='ok if poweruser in higher envs', action="store_true")
parser.add_argument('targetAccount', type=str, help='account id')
parser.add_argument('userEmail',     type=str, help='user mail, can set bulk allocation by separate by comma')
parser.add_argument('accRole',       type=str, help='account role, you can set buil allocation by separete by comma')
args = parser.parse_args()


target_account = args.targetAccount
username       = args.userEmail
target_role    = args.accRole
accountID      = target_account 


# todo add policy listing
# todo remove role
def getPolicies(account, instance_arn):
   # this is not working
	session = boto3.Session(
			profile_name=account,
			region_name='us-east-1')
	orgs = session.client('sso-admin')
	response = orgs.list_permission_sets(InstanceArn=instance_arn)
	results = response["PermissionSets"]

	while "NextToken" in response:
		 response = orgs.list_permission_sets(InstanceArn=instance_arn,NextToken=response["NextToken"])
		 results.extend(response["PermissionSets"])

	match = [True for i in results if ACC == i['Id']]
	match = True if match else False
	return match


def checkAccounts(ACC,account='master'):
	session = boto3.Session(
			profile_name=account,
			region_name='us-east-1')
	orgs = session.client('organizations')
	response = orgs.list_accounts()
	results = response["Accounts"]
	while "NextToken" in response:
		 response = orgs.list_accounts(NextToken=response["NextToken"])
		 results.extend(response["Accounts"])
	match = [True for i in results if ACC == i['Id']]
	match = True if match else False
	return match

try:
	print('Please make sure you have master sso login granted')
	checkAcc = checkAccounts(accountID)
except Exception as ex:
	os.popen('aws sso login --profile master').read()
	checkAcc = checkAccounts(accountID)

if not checkAcc:
	print('Please make sure you have master_brown sso login granted')
	try:
		checkAccounts(accountID, account='master_brown')
		acc = 'master_brown'
	except Exception as ex:
		print(ex)
		os.popen('aws sso login --profile master_brown')
		sys.exit(1)
else:
	acc = 'master'


#__________________________[ Managing connection 
session = boto3.Session(
      profile_name=acc,
      region_name='us-east-1')

sso_client = session.client('sso-admin')
id_client  = session.client('identitystore')


#__________________________[ Account info
instanceinfo = sso_client.list_instances(
        MaxResults=1,
        NextToken='string')

instance_arn     = instanceinfo['Instances'][0]['InstanceArn']
identitystore_id = instanceinfo['Instances'][0]['IdentityStoreId']

# todo add day file
update_file = os.popen('bash ' + os.path.dirname(os.path.realpath(__file__)) + '/accs.sh').read()
update_pols = os.popen(os.path.dirname(os.path.realpath(__file__))+ '/helpers/getpols.py --apply').read()

for username in args.userEmail.split(','):
   userinfo = id_client.list_users(
     IdentityStoreId=identitystore_id,
     MaxResults=10,
     Filters=[
         {
             'AttributePath': 'UserName',
             'AttributeValue': username
         },
     ]
   )

   user_id   = userinfo['Users'][0]['UserId']
   user_name = userinfo['Users'][0]['UserName']

   for target_role in args.accRole.split(','):
      # THIS CHECK CORRECT ROLES
      # if not a policy id identifying from a file
      # checks NAME
      if not "arn:aws:sso" in target_role:
         file = 'greenpols.txt' if acc == 'master' else 'brownpols.txt'
         file = os.path.dirname(os.path.realpath(__file__)) + '/' + file
         f    = open(file, 'r').read()
         try:
            #filter role from file
            target_id = [i.split('\t') for i in f.split('\n') if target_role == i.split('\t')[0] ][0][1]
         except Exception:
            print(f"[-] Role {target_role} not found in {file}")
            break
      # checks ID
      else:
         try:
            target_id = [i.split('\t') for i in f.split('\n') if target_role == i.split('\t')[1] ][0][1] 
         except Exception:
            print(f"[-] Role {target_role} not found in {file}")  
            break


      file = 'green.acc.txt' if acc == 'master' else 'brown.acc.txt'
      file = os.path.dirname(os.path.realpath(__file__)) + '/' + file 

      f    = open(file, 'r').read() 
      # filters acc name from id list
      target_acc_name = [i.split(" ")[1] for i in f.split('\n') if target_account in i.split('\n')[0] ][0].strip('"')

      if not args.power and "PowerUser" in target_role and "Prod" in target_acc_name:
         print(f'Ensure --power arg and double check if possible to add to this env')
         break

      else:
         print(f'adding {username} {target_role} to {target_acc_name} {acc}')

         assignmentinfo = sso_client.create_account_assignment(
           InstanceArn=instance_arn,
           TargetId=target_account,
           TargetType='AWS_ACCOUNT',
           PermissionSetArn=target_id,
           PrincipalType='USER',
           PrincipalId=user_id
         )

         request_id = assignmentinfo['AccountAssignmentCreationStatus']['RequestId']
         # status = assignmentinfo['AccountAssignmentCreationStatus']['Status']
         statusinfo = sso_client.describe_account_assignment_creation_status(
           InstanceArn=instance_arn,
           AccountAssignmentCreationRequestId=request_id
         )

         status = statusinfo['AccountAssignmentCreationStatus']['Status']

         print(status)

