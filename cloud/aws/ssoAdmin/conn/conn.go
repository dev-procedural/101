package conn

import (
	"context"
	"fmt"
	"log"

	// "os"
	"errors"
	"sort"

	// "io"
	// "encoding/json"
	// "log"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/credentials"
	"github.com/aws/aws-sdk-go-v2/service/ec2"
	"github.com/aws/aws-sdk-go-v2/service/organizations"
	"github.com/aws/aws-sdk-go-v2/service/sts"
	// "github.com/aws/aws-sdk-go-v2/service/organizations/types"
)

var (
	ACCOUNT_ string
	MASTER_  string
	ROLE_    string
	REGION_  string
	cfg      aws.Config
)

// type CONN interface {
// 	ASSUME(cfg aws.Config, Account, Role, Region string) (aws.Config, error)
// 	REGIONS(cfg aws.Config) []string
// 	SUBACCOUNTS(cfg aws.Config) []Account
// }

type CONFIG struct {
	MASTER_ string
	ROLE_   string
	REGION_ string
}

func init() {
	cfg, _ = config.LoadDefaultConfig(context.TODO(),
		config.WithRegion("us-east-1"),
		config.WithSharedConfigProfile("master"))
}

func CONF(Account string) (*CONFIG, aws.Config) {

	var isbrown bool
	isbrown = false

	if Account != "" {
		ALL_ACCOUNTS := SUBACCOUNTS(cfg)
		for _, i := range ALL_ACCOUNTS {
			if Account == i.ID {
				isbrown = false
				break
			} else {
				isbrown = true
			}
		}
	}

	if isbrown {
		MASTER_ = "master_brown"
		ROLE_ = "OrganizationAccountAccessRole"
		REGION_ = "us-east-1"
	} else {
		MASTER_ = "master"
		ROLE_ = "AWSControlTowerExecution"
		REGION_ = "us-east-1"
	}

	cfg, _ = config.LoadDefaultConfig(context.TODO(),
		config.WithRegion(REGION_),
		config.WithSharedConfigProfile(MASTER_))

	return &CONFIG{
		MASTER_: MASTER_,
		ROLE_:   ROLE_,
		REGION_: REGION_,
	}, cfg
}

func ASSUME(Account, Role, Region string) (aws.Config, error) {
	CONF(Account)

	client := sts.NewFromConfig(cfg)

	ROLE := "arn:aws:sts::" + Account + ":role/" + Role
	SESS := "ASSUMED-GO"
	input := &sts.AssumeRoleInput{
		RoleArn:         &ROLE,
		RoleSessionName: &SESS,
	}

	result, err := client.AssumeRole(context.TODO(), input)
	if err != nil {
		log.Printf("Got an error assuming the role: %s", err)
		return aws.Config{}, errors.New(err.Error())
	}

	creds := *result.Credentials
	// fmt.Printf("[!] %#v", cfg)

	cfgSub, err := config.LoadDefaultConfig(context.TODO(),
		config.WithRegion(Region),
		config.WithCredentialsProvider(credentials.StaticCredentialsProvider{
			Value: aws.Credentials{
				AccessKeyID:     *creds.AccessKeyId,
				SecretAccessKey: *creds.SecretAccessKey,
				SessionToken:    *creds.SessionToken,
				Source:          "assumerole",
			},
		}))

	if err != nil {
		log.Println("error:", err)
		return aws.Config{}, errors.New(err.Error())
	}

	return cfgSub, nil
}

type Account struct {
	Name   string
	ID     string
	Status string
}

func REGIONS(cfg aws.Config) []string {
	EC2 := ec2.NewFromConfig(cfg)

	REGIONS := []string{}

	result, _ := EC2.DescribeRegions(
		context.TODO(),
		&ec2.DescribeRegionsInput{},
	)

	for _, v := range result.Regions {
		// fmt.Printf("%#v\n", *v.RegionName)
		REGIONS = append(REGIONS, *v.RegionName)
	}

	return REGIONS
}

func SUBACCOUNTS(cfg aws.Config) []Account {

	accounts := []Account{}

	orgs := organizations.NewFromConfig(cfg)
	response, _ := orgs.ListAccounts(context.TODO(), &organizations.ListAccountsInput{})
	for _, v := range response.Accounts {
		accounts = append(accounts, Account{
			Name:   *v.Name,
			ID:     *v.Id,
			Status: fmt.Sprintf("%v", v.Status),
		})
	}

	TOKEN := response.NextToken
	if TOKEN != nil {
		for {
			response, err := orgs.ListAccounts(context.TODO(),
				&organizations.ListAccountsInput{
					NextToken: TOKEN,
				})

			if err != nil {
				log.Printf("%#v", err.Error())
				break
			}

			for _, v := range response.Accounts {
				accounts = append(accounts,
					Account{
						Name:   *v.Name,
						ID:     *v.Id,
						Status: fmt.Sprintf("%v", v.Status),
					})
			}

			TOKEN = response.NextToken
			if aws.ToString(response.NextToken) == "" {
				break
			}

		}
	}

	sort.SliceStable(accounts, func(i, j int) bool {
		return accounts[i].Name < accounts[j].Name
	})

	// fmt.Println(len(accounts))
	result := []Account{}
	for _, v := range accounts {
		if v.Status == "ACTIVE" {
			// fmt.Println(v.Name,v.Status)
			result = append(result,
				Account{
					Name:   v.Name,
					ID:     v.ID,
					Status: v.Status,
				})
		}
	}

	// fmt.Printf("%#v", result)
	return result
}
