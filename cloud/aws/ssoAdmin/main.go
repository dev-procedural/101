package main

import (
	"context"
	"flag"
	"log"
	"sort"
	"strings"

	// "sort"
	// "errors"
	"module/conn"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/identitystore"
	"github.com/aws/aws-sdk-go-v2/service/identitystore/types"
	"github.com/aws/aws-sdk-go-v2/service/ssoadmin"
	ssotypes "github.com/aws/aws-sdk-go-v2/service/ssoadmin/types"
	"github.com/go-gota/gota/dataframe"
	"github.com/samber/lo"
)

var (
	MASTER_ string
	ROLE_   string
	REGION_ string
	cfg     aws.Config
)

var ACCOUNT_ interface{}

type TL struct {
	sso_client ssoadmin.Client
	id_client  identitystore.Client
}

func init() {
	// isbrown := flag.Bool("bf", false, "is brownfield")
	subacc := flag.String("a", "", "account id comma separated")
	flag.StringVar(&REGION_, "r", "us-east-1", "region")
	flag.Parse()

	CONF, cfgo := conn.CONF(*subacc)
	log.Println(CONF)
	cfg = cfgo
	// 1 or Many Accounts or ALL o f them

	switch {
	case strings.Contains(*subacc, ","):
		log.Println("[!] Running in", strings.Split(strings.Trim(*subacc, " "), ","))
		ACCOUNT_ = strings.Split(strings.Trim(*subacc, " "), ",")
	case *subacc != "":
		log.Println("[!] Running in acc", *subacc)
		ACCOUNT_ = []string{*subacc}
	default:
		ACCOUNT_ = conn.SUBACCOUNTS(cfg)
		log.Println("[!] Running in all accounts...")
	}
}

func SSOADM(subaccount, region string) (InstanceArn, IdentityStoreId string) {

	// cfgSub, _ := conn.ASSUME(cfg, subaccount, ROLE_, region)
	sso_client := ssoadmin.NewFromConfig(cfg)

	resp, _ := sso_client.ListInstances(
		context.TODO(),
		&ssoadmin.ListInstancesInput{},
	)

	return *resp.Instances[0].InstanceArn, *resp.Instances[0].IdentityStoreId
}

type ACCIDS struct {
	ID   string
	Name string
}

func GetAccIDS() []ACCIDS {
	SUBACCS := conn.SUBACCOUNTS(cfg)

	accs := []ACCIDS{}
	for _, i := range SUBACCS {
		accs = append(accs, ACCIDS{ID: i.ID, Name: i.Name})
	}

	sort.SliceStable(accs, func(i, j int) bool {
		return accs[i].Name < accs[j].Name
	})

	return accs
}

type ACCPERM struct {
	Name  string
	Perms []string
}

func AccPerms(account, instance_arn string) ACCPERM {

	sso_client := ssoadmin.NewFromConfig(cfg)

	resp, err := sso_client.ListPermissionSetsProvisionedToAccount(
		context.TODO(),
		&ssoadmin.ListPermissionSetsProvisionedToAccountInput{
			AccountId:   &account,
			InstanceArn: &instance_arn,
		},
	)
	if err != nil {
		log.Printf("%#v", err.Error())
	}

	PERMS := resp.PermissionSets

	TOKEN := resp.NextToken
	if TOKEN != nil {
		for {
			resp, err := sso_client.ListPermissionSetsProvisionedToAccount(
				context.TODO(),
				&ssoadmin.ListPermissionSetsProvisionedToAccountInput{
					AccountId:   &account,
					InstanceArn: &instance_arn,
					NextToken:   TOKEN,
				},
			)
			if err != nil {
				log.Printf("%#v", err.Error())
				break
			}

			TOKEN = resp.NextToken
			// log.Printf("%#v", PERMS)

			PERMS = append(PERMS, resp.PermissionSets...)

			if aws.ToString(resp.NextToken) == "" {
				break
			}
		}
	}

	Result := ACCPERM{Name: account, Perms: PERMS}
	// log.Println(Result)

	return Result

}

func GetUser(userId, idstore string) string {
	id_client := identitystore.NewFromConfig(cfg)

	resp, err := id_client.DescribeUser(
		context.TODO(),
		&identitystore.DescribeUserInput{
			IdentityStoreId: &idstore,
			UserId:          &userId,
		},
	)
	if err != nil {
		// log.Printf("%+v ERR:%+v", err)
		return ""
	}

	var NAME []types.Email
	NAME = resp.Emails

	return *NAME[0].Value
}

type AccUser struct {
	PermSet string
	UserID  string
}

func UsersInAcc(account, instance_arn, permissionset string) (result []AccUser) {

	sso_client := ssoadmin.NewFromConfig(cfg)

	resp, err := sso_client.ListAccountAssignments(
		context.TODO(),
		&ssoadmin.ListAccountAssignmentsInput{
			AccountId:        &account,
			InstanceArn:      &instance_arn,
			PermissionSetArn: &permissionset,
		},
	)

	if err != nil {
		log.Println("[+] ", err)
		return
	}

	// log.Printf("%#v \n%#v", resp.AccountAssignments, len(resp.AccountAssignments))
	for _, i := range resp.AccountAssignments {
		PERMSET := strings.Trim(*i.PermissionSetArn, "\"")
		PRINCIPALID := strings.Trim(*i.PrincipalId, "\"")
		result = append(result, AccUser{PermSet: PERMSET, UserID: PRINCIPALID})
	}

	// log.Printf("%#v", result)
	return result
}

// func GetPermName(PRM string, PERMS []ACCPERM) string {
// 	for _, PERM := range PERMS {
// 		if PERM.Permission == PRM {
// 			return PERM.Name
// 		}
// 	}

// 	return nil
// }

func ALL_PERMSETS(instance_arn string) []string {

	sso_client := ssoadmin.NewFromConfig(cfg)
	resp, err := sso_client.ListPermissionSets(
		context.TODO(),
		&ssoadmin.ListPermissionSetsInput{
			InstanceArn: &instance_arn,
		},
	)
	if err != nil {
		log.Printf("%#v", err.Error())
	}

	PERMS := resp.PermissionSets

	TOKEN := resp.NextToken
	if TOKEN != nil {
		for {
			resp, err := sso_client.ListPermissionSets(
				context.TODO(),
				&ssoadmin.ListPermissionSetsInput{
					InstanceArn: &instance_arn,
					NextToken:   TOKEN,
				},
			)
			if err != nil {
				log.Printf("ERR: %#v", err.Error())
				break
			}

			TOKEN = resp.NextToken
			// log.Printf("%#v", PERMS)

			PERMS = append(PERMS, resp.PermissionSets...)

			if aws.ToString(resp.NextToken) == "" {
				break
			}
		}
	}

	// log.Printf("%#v", PERMS)
	return PERMS
}

func PermSetName(perm, instance_arn string) string {
	sso_client := ssoadmin.NewFromConfig(cfg)

	resp, err := sso_client.DescribePermissionSet(
		context.TODO(),
		&ssoadmin.DescribePermissionSetInput{
			PermissionSetArn: &perm,
			InstanceArn:      &instance_arn,
		},
	)
	if err != nil {
		log.Printf("%#v", err.Error())
	}

	var permset ssotypes.PermissionSet

	permset = *resp.PermissionSet
	name := *permset.Name

	return name
}

func AccountUsersAndPermset(instance_arn, idstore string, ACCS interface{}) map[string][]string {
	AllUsersInAccount := []string{}
	PERMS_USERS := make(map[string][]string)
	for _, ACC := range ACCS.([]string) {
		// log.Println(ACC)
		PermsInAccount := AccPerms(ACCS.([]string)[0], instance_arn).Perms
		for _, perm := range PermsInAccount {
			AccUsers := UsersInAcc(ACC, instance_arn, perm)
			for _, user := range AccUsers {
				USER := GetUser(user.UserID, idstore)
				AllUsersInAccount = append(AllUsersInAccount, USER)
				AllUsersInAccount = lo.FindUniques[string](AllUsersInAccount)
			}
			PERM := PermSetName(perm, instance_arn)
			PERMS_USERS[PERM] = AllUsersInAccount
		}
	}
	return PERMS_USERS
}

func main() {
	var ACCS interface{}

	switch ACCOUNT_.(type) {
	case []conn.Account:
		df := dataframe.LoadStructs(ACCOUNT_)
		ACCS = df.Col("ID").Records()
	case []string:
		ACCS = ACCOUNT_
	}

	instance, idstore := SSOADM(ACCS.([]string)[0], "us-east-1")
	// RES := GetAccIDS()
	log.Println(instance, idstore)

	AccountUsersAndPermset(instance, idstore, ACCS)

	// log.Println(AllUsersInAccount)

	// allAccPerms := map[string][]string{}
	// for _, i := range GetAccIDS() {
	// 	allAccPerms[i.Name] = AccPerms(i.ID, instance).Perms
	// }
	// log.Printf("%+v", allAccPerms)
	// allUsersInAcc := map[string][]string{}

	// RES := []AccUser{}
	// for _, PERM := range allAccPermsk {
	// 	RR := UsersInAcc(ACCS.([]string)[0], instance, PERM)
	// 	RES = append(RES, RR...)
	// }
	// log.Println(RES)

	// ALL_PERMSETS(instance)

	// PERMS := AccPerms(ACCS.([]string)[0], instance)

	// for _, PERM := range PERMS.Perms {
	// 	log.Println(PERM)
	// 	UsersInAcc(ACCS.([]string)[0], instance, PERM)
	// }

	// for _, i := range USERS {
	// 	log.Printf("%#v", i.UserID)
	// 	GetUser(i.UserID, arn)
	// }
	// for _, i := range RES.Perms {
	// 	log.Println("[+]", i)
	// 	UsersInAcc("ACCOUNT_ID", "arn:aws:sso:::instance/ssoins-PERM_SET_ID", i)
	// }
}
