module "test_serviceaccount" {
  source = "../modules/serviceaccount"

  subaccount    = "MYACCOUNT"

  requester = "jolea"
  emails    = ["myuser@mycompany"]
  asumable_sa_services = ["s3.amazonaws.com", "lambda.amazonaws.com"]
  asumable_sa_users    = ["AmazonS3FullAccess"]
  zone      = "GF"
}
