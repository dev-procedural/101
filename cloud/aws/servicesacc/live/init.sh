#!/bin/bash

terraform init  \
    -backend-config="bucket=myaccount-saaccount" \
    -backend-config="key=terraform/terraform.tfstate" \
    -backend-config="region=us-east-1"
