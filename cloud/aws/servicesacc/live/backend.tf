terraform {
  backend "s3" {
    bucket = "saaccount"
    key    = "terraform/terraform.tfstate"
    region = "us-east-1"
    profile = "master"
    role_arn = "arn:aws:iam::MYACCOUNT:role/AWSControlTowerExecution"
    session_name = "ServiceAccount-State"
  }
}
