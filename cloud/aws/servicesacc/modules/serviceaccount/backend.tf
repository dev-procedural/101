provider "aws" {
	profile = "${var.masteraccount}"
  assume_role {
    role_arn     = "arn:aws:iam::${local.accountid[0][0]}:role/${var.ROLE[var.zone]}"
    session_name = "ServiceAccount-tf"
  }
  region = "us-east-1"
}

provider "aws" {
  profile = "${var.masteraccount}"
  region  = "us-east-1"
  alias   = "master"
}

data "aws_caller_identity" "subaccount" {}

data "aws_caller_identity" "master" {
  provider  = aws.master
}

data "aws_organizations_organization" "orgs" {
  provider = aws.master
}

# TODO search for the policies
data "aws_iam_policy" "policies" {
  for_each = toset(var.assumable_sa_users)
  name     = "${each.value}"
}

