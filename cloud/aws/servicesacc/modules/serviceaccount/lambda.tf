resource "aws_lambda_function" "lambda_saaccount" {
  filename      = "${path.module}/lambda/saaccount.zip"
  function_name = "svc-${var.requester}-keyrotation"
  role          = aws_iam_role.lambda_role.arn
  handler       = "saaccount.lambda_handler"
  package_type  = "Zip"

  source_code_hash = filebase64sha256("${path.module}/lambda/saaccount.zip")

  runtime     = "python3.6"
  memory_size = "128"
  timeout     = 12

  environment {
    variables = {
      SNS_TOPIC_ARN = "${aws_sns_topic.saaccount_sns.arn}"
    }
  }

  depends_on = [
    aws_iam_role.lambda_role,
    aws_sns_topic.saaccount_sns
  ]

  tags = local.tags
}

resource "aws_lambda_permission" "allow_events_to_call" {
  for_each      = toset(["create", "deactivate", "delete"])
  statement_id  = "AWSEvents_${var.requester}-${each.value}"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda_saaccount.function_name
  principal     = "events.amazonaws.com"
  source_arn    =  aws_cloudwatch_event_rule.saaccount_event[each.value].arn
}

