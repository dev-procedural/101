resource "aws_secretsmanager_secret" "saaccount_secret" {
  name = "svc-${var.requester}-keyrotation"
  recovery_window_in_days = 0
}

# TODO review permission policy
resource "aws_secretsmanager_secret_version" "saaccount_version" {
  secret_id     = aws_secretsmanager_secret.saaccount_secret.id
  secret_string = <<EOF
  ${jsonencode({
    "AccessKey" : "${jsonencode(aws_iam_access_key.sauser_key.id)}",
    "SecretKey" : "${jsonencode(aws_iam_access_key.sauser_key.secret)}"}
  )}
  EOF
  depends_on = [aws_iam_access_key.sauser_key]
}

