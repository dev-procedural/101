locals {
  tags = {
  }

  accountid = [ for i in data.aws_organizations_organization.orgs.accounts: 
      ["${i.id}", "${i.name}"] if "${i.name}" == "${var.subaccount}" || "${i.id}" == "${var.subaccount}"]

  assumable_sa_services = "${var.services}"
  assumable_sa_users    = "${var.users}"

}

# BACKEND
variable "subaccount" {
  description = "this should be the target account id"
  type = string
}

variable "masteraccount" {
  description = "this should be master account in case of greenfield"
  default     = "master"
}

variable "stateaccount" {
  default = "Support"
}

# SERVICES
variable "requester" {
  description = "this is used for naming purposes"
  type = string
}

variable "emails" {
  description = "email list for sns subscription"
  type = list
}


variable "zone" {
  description = "this should be bf or gf"
  type = string
  default = "GF"
}

variable "ROLE" {
  type = object({
    GF = string
    BF = string
  })
  default = {
    GF = "AWSControlTowerExecution"
    BF = "OrganizationAccountAccessRole"
  }
}

variable "PROFILE" {
  type = object({
    GF = string
    BF = string
  })
  default = {
    GF = "master"
    BF = "master_brown"
  }
}

variable "services" {
  description = "this is provided by requester is a list of services"
  type = list
  default = [""]
}

variable "userpol" {
  description = "this is the assumers on this saacount"
  type = list
  default = [""]
}
