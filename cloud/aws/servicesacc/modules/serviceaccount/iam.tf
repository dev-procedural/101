# SUB ACC
resource "aws_iam_user" "sauser" {
  name = "svc-${var.requester}"
  path = "/"

}

resource "aws_iam_access_key" "sauser_key" {
  user = aws_iam_user.sauser.name
}


resource "aws_iam_user_policy" "sauser_pol" {
  name = "svc-secret-read-${var.requester}"
  user = aws_iam_user.sauser.name

  depends_on = [aws_secretsmanager_secret.saaccount_secret]
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "secretsmanager:GetResourcePolicy",
                "secretsmanager:GetSecretValue",
                "secretsmanager:DescribeSecret",
                "secretsmanager:ListSecretVersionIds"
            ],
            "Resource": [
                "${aws_secretsmanager_secret.saaccount_secret.arn}"
            ]
        },
        {
            "Sid": "VisualEditor1",
            "Effect": "Allow",
            "Action": [
                "secretsmanager:GetRandomPassword",
                "secretsmanager:ListSecrets"
            ],
            "Resource": "*"
        }
    ]
}
  EOF
}

resource "aws_iam_role" "saaccount_role" {
  count = var.zone == "GF" ? 1 : 0
  name = "svc-${var.requester}"
  assume_role_policy = <<EOF
${jsonencode(
{
  "Version": "2012-10-17",
  "Statement": [
     {
      "Sid": "",
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": var.services,
        "AWS": var.assumables_for_saaacount
      },
      "Effect": "Allow"
    }
  ]
}
)}
  EOF
}

resource "aws_iam_role" "saaccount_role_master" {
  count    = var.zone == "BF" ? 1 : 0
  provider = aws.master
  name = "svc-${var.requester}"
  assume_role_policy = <<EOF
${jsonencode(
{
  "Version": "2012-10-17",
  "Statement": [
     {
      "Sid": "",
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": var.assumable_sa_services,
        "AWS": var.assumable_sa_users
      },
      "Effect": "Allow"
    }
  ]
}
)}
  EOF
}

resource "aws_iam_policy" "saaccount_pol" {
  count = var.zone == "GF" ? 1 : 0
  name = "svc-${var.requester}"

  policy = <<EOF
${jsonencode(
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "secretsmanager:GetResourcePolicy",
                "secretsmanager:GetSecretValue",
                "secretsmanager:DescribeSecret",
                "secretsmanager:ListSecretVersionIds"
            ],
            "Resource": [
                "${aws_secretsmanager_secret.saaccount_secret.arn}"
            ]
        },
        {
            "Sid": "VisualEditor1",
            "Effect": "Allow",
            "Action": [
                "secretsmanager:GetRandomPassword",
                "secretsmanager:ListSecrets"
            ],
            "Resource": "*"
        },
        {
            "Sid": "",
            "Effect": "Allow",
            "Action": [
                "sts:AssumeRole"
            ],
            "Resource": "*"
        }
    ]
}
)}
  EOF
}

resource "aws_iam_policy" "saaccount_pol_master" {
  count    = var.zone == "BF" ? 1 : 0
  provider = aws.master

  name = "svc-${var.requester}"

  policy = <<EOF
${jsonencode(
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "secretsmanager:GetResourcePolicy",
                "secretsmanager:GetSecretValue",
                "secretsmanager:DescribeSecret",
                "secretsmanager:ListSecretVersionIds"
            ],
            "Resource": [
                "${aws_secretsmanager_secret.saaccount_secret.arn}"
            ]
        },
        {
            "Sid": "VisualEditor1",
            "Effect": "Allow",
            "Action": [
                "secretsmanager:GetRandomPassword",
                "secretsmanager:ListSecrets"
            ],
            "Resource": "*"
        },
        {
            "Sid": "",
            "Effect": "Allow",
            "Action": [
                "sts:AssumeRole"
            ],
            "Resource": "*"
        }
    ]
}
)}
  EOF
}

resource "aws_iam_policy_attachment" "saaccount_attch" {
  count      = var.zone == "GF" ? 1 : 0
  name       = "svc-${var.requester}-attch" 

  roles      = [aws_iam_role.saaccount_role[count.index].name]
  policy_arn = aws_iam_policy.saaccount_pol[count.index].arn 

}

resource "aws_iam_policy_attachment" "saaccount_attch_master" {
  count    = var.zone == "BF" ? 1 : 0
  provider = aws.master
  name       = "svc-${var.requester}-attch" 

  roles      = [aws_iam_role.saaccount_role_master[count.index].name]
  policy_arn = aws_iam_policy.saaccount_pol_master[count.index].arn

}


# LAMDBA
resource "aws_iam_policy" "lambda_pol" {
  name = "svc-lambda-${var.requester}"
  # provider   = aws.subaccount

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "logs:CreateLogGroup",
            "Resource": "arn:aws:logs:us-east-1:${data.aws_caller_identity.subaccount.id}:*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": [
              "arn:aws:logs:us-east-1:${data.aws_caller_identity.subaccount.id}:log-group:/aws/lambda/svc-${var.requester}"
            ]
        },
        {
            "Effect": "Allow",
            "Action": "sns:Publish",
            "Resource": "${aws_sns_topic.saaccount_sns.arn}"
        }
    ]
}
  EOF
}

resource "aws_iam_role" "lambda_role" {
  name = "svc-lambda-${var.requester}"
  # provider = aws.subaccount

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
     {
      "Sid": "",
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": ["lambda.amazonaws.com", "events.amazonaws.com"]
      },
      "Effect": "Allow"
    }
  ]
}
  EOF
}


resource "aws_iam_policy_attachment" "lambda_attch" {
	for_each   = {for k,v in [
		"arn:aws:iam::aws:policy/SecretsManagerReadWrite",
		"arn:aws:iam::aws:policy/IAMFullAccess",
		"${aws_iam_policy.lambda_pol.arn}"
]: k => v}

  name       = "svc-lambda-${var.requester}_attch_${each.key}" 
  # provider   = aws.subaccount

  roles      = [aws_iam_role.lambda_role.name]
  policy_arn = "${each.value}"
}

