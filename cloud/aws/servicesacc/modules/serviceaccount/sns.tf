resource "aws_sns_topic" "saaccount_sns" {
  name            = "svc-${var.requester}-keyrotation"
	display_name    = "svc-${var.requester}-keyrotation"
  tags            = local.tags
}

resource "aws_sns_topic_policy" "saaccount_topic_pol" {
  arn    = aws_sns_topic.saaccount_sns.arn
  policy = data.aws_iam_policy_document.sns_topic_policy.json
}

data "aws_iam_policy_document" "sns_topic_policy" {
  policy_id = "__default_policy_ID"

  statement {
    actions = [
      "SNS:Subscribe",
      "SNS:SetTopicAttributes",
      "SNS:RemovePermission",
      "SNS:Receive",
      "SNS:Publish",
      "SNS:ListSubscriptionsByTopic",
      "SNS:GetTopicAttributes",
      "SNS:DeleteTopic",
      "SNS:AddPermission",
    ]

    condition {
      test     = "StringEquals"
      variable = "AWS:SourceOwner"

      values = [
        data.aws_caller_identity.subaccount.account_id
      ]
    }

    effect = "Allow"

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    resources = [
      aws_sns_topic.saaccount_sns.arn
    ]

    sid = "__default_statement_ID"
  }
}

resource "aws_sns_topic_subscription" "saaccount_subs" {
  for_each  = {for k,v in var.emails: k => v }
  topic_arn = aws_sns_topic.saaccount_sns.arn
  protocol  = "email"
  endpoint  = "${each.value}" 
}

