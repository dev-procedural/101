resource "aws_cloudwatch_event_rule" "saaccount_event" {
  for_each  = {
    create     = "90"
    deactivate = "100"
    delete     = "110"
  }
  name        = "${var.requester}-${each.key}"
  description = "key rotation event ${each.key}"

	schedule_expression = "rate(${each.value} days)"
  role_arn            = aws_iam_role.lambda_role.arn
}

resource "aws_cloudwatch_event_target" "saaccount_event_target" {
  for_each    = { 
     create     = jsonencode({"action": "create","username":"${aws_iam_user.sauser.id}"})
     deactivate = jsonencode({"action": "deactivate","username":"${aws_iam_user.sauser.id}"})
     delete     = jsonencode({"action": "delete","username":"${aws_iam_user.sauser.id}"})
  }
  # target_id = "svc-${var.requester}-keyrotation-${each.key}"
  rule      = aws_cloudwatch_event_rule.saaccount_event[each.key].id
  arn       = aws_lambda_function.lambda_saaccount.arn

  input = <<EOF
  ${jsonencode(each.value)}
  EOF
}

