provider "aws" { 
  profile = var.subaccount
  region  = "us-east-1"
}

provider "aws" { 
  profile = var.subaccount
  region  = "us-east-1"
  alias   = "subaccount"
}

provider "aws" {
  profile = var.master
  region  = "us-east-1"
  alias   = "master"
}

resource "aws_kms_key" "cops_bucket_key" {
  description = "This key is used to manage terraform state"
	tags        = merge(local.tags, {
    "Name" = "terraform"
  })
}

resource "aws_kms_alias" "cops_kms_alias" {
  name          = "alias/kmstfstate"
  target_key_id = aws_kms_key.cops_bucket_key.key_id
}

resource "aws_s3_bucket" "cops_bucket" {
  bucket   = "${var.bucket}"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = aws_kms_key.cops_bucket_key.arn
        sse_algorithm     = "aws:kms"
      }
    }
  }
}

resource "aws_s3_bucket_object" "tfdir" {
  depends_on = [ aws_s3_bucket.cops_bucket ]

  bucket = aws_s3_bucket.cops_bucket.id
  key    = "terraform/"
}

