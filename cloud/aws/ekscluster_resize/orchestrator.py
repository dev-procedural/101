import os, json, boto3, base64, re

from botocore.signers import RequestSigner
from kubernetes import client, config

# ROLE        = os.getenv("ROLE") or "arn:aws:iam::ACCONT_ID:role/ROLE_TO_USE"
# ENVIRON     = os.getenv("ENVIRONMENT") or "dev"
BUCKET_BKP  = "BUCKET_TO_USE"
PREFIX_BKP  = "lambda_cache/cache.txt"
STS_TOKEN_EXPIRES_IN = 120

session     = boto3.Session(region_name="us-east-1")
sts = session.client('sts')
eks = session.client('eks')
s3  = session.client('s3')
rds = session.client('rds')

def lambda_handler(event, context):

   state = event["state"]
   environ = event["environ"]
   # session = assumeMendix()

   if state == "up":
      rds_orchestration(rds, state, environ)
      eks_orchestration(eks, sts, state, environ)

   elif state == "down"
      eks_orchestration(eks, sts, state, environ)
      rds_orchestration(rds, state, environ)


def rds_orchestration(rds, state, environ):
   """
   This function orchestrate rds cluster scaling
   """

   db_clusters = [i["DBClusterIdentifier"] for i in rds.describe_db_clusters()["DBClusters"]]
   DBClusterID = [i for in db_clusters if i.endswith(environ)]
   
   if state == "up":
      response = rds.start_db_cluster(DBClusterIdentifier=DBClusterID)

   elif state == "down":
      response = rds.stop_db_cluster(DBClusterIdentifier=DBClusterID)


def eks_orchestration(eks, sts, state, environ):
   """
   This function orchestrate eks apps and cluster meaning that start and stop apps and scales down and up eks cluster
   """

   service_id   = sts.meta.service_model.service_id
   cluster_name = [i for i in eks.list_clusters()["clusters" ] if i.endswith(environ)][0]
   cluster      = get_cluster_info(eks, cluster_name)

   setClient(cluster, get_bearer_token, config, service_id)
   core = client.CoreV1Api() 
   apps = client.AppsV1Api() 

   if state == "down":
      # saving current state
      BKP = eks.describe_nodegroup( clusterName=CLUSTER, nodegroupName=NODEGROUP)
      s3.put_object(Bucket=BUCKET_BKP, Key=PREFIX_BKP, Body=json.dumps(BKP, default=str))

      # First scale apps
      scaleApps(eks, apps, state)
      time.sleep(33)
      # Second scale cluster
      scaleCluster(eks, s3, state, cluster_name)

      return True

   elif state == "up"

      # GET S3 CONFIG
      scale_cfg = s3.get_object(Bucket=BUCKET_BKP,Key=PREFIX_BKP)["Body"].read()
      scale_cfg = json.loads(scale_cfg)["nodegroup"]["scalingConfig"]

      # First scale apps
      scaleCluster(eks, s3, state, cluster_name, scale_cfg)

      while True:
         if core.list_node().items == scale_cfg["desiredSize"]:
            # Second scale cluster
            scaleApps(eks, apps, state)
            return True

         time.sleep(5)

   return False


def setClient(cluster, get_bearer_token, config, service_id):
   kubeconfig = {
        'apiVersion': 'v1',
        'clusters': [{
          'name': 'cluster1',
          'cluster': {
            'certificate-authority-data': cluster["ca"],
            'server': cluster["endpoint"]}
        }],
        'contexts': [{'name': 'context1', 'context': {'cluster': 'cluster1', "user": "user1"}}],
        'current-context': 'context1',
        'kind': 'Config',
        'preferences': {},
        'users': [{'name': 'user1', "user" : {'token': get_bearer_token(service_id)}}]
   }

   config.load_kube_config_from_dict(config_dict=kubeconfig)


def scaleCluster(eks, s3, state, cluster_name, scale_cfg="") :
   CLUSTER = cluster_name
   NODEGROUP = eks.list_nodegroups(clusterName=CLUSTER)["nodegroups"][0]

   if state == "down":
      # TODO: ENHANCEMENT add asg template version to support lower instance eg micro

      print(f"[!] Scaling: {state} {BKP['nodegroup']['scalingConfig']}")
      eks.update_nodegroup_config(
         clusterName=CLUSTER,
         nodegroupName=NODEGROUP,
         scalingConfig={
             'minSize': 1,
             'maxSize': 1,
             'desiredSize': 1
         }
            )

      return True

   elif state == "up":
      print(f"[!] Scaling: {state} {scale_cfg}")
      eks.update_nodegroup_config(
         clusterName=CLUSTER,
         nodegroupName=NODEGROUP,
         scalingConfig=scale_cfg)

      # TODO: ENHANCEMENT - add retry if fail

      return True

   return False


def scaleApps(eks, apps, state):
   DEPLOYS     = apps.list_deployment_for_all_namespaces()
   RUNTIMES   = { i.metadata.namespace:i.metadata.name for i in DEPLOYS.items if "-master"  in i.metadata.name }
   OPERATORS  = { i.metadata.namespace:i.metadata.name for i in DEPLOYS.items if "operator" in i.metadata.name }

   if state == "up":
      REPS = 1
   elif state == "down":
      REPS = 0

   # RUNTIME
   print(f"[!] Runtime: {state}")
   for namespace,deployment in RUNTIME.items():
       api_response = apps.patch_namespaced_deployment_scale(
            name=deployment,
            namespace=namespace,
            body={'spec': {'replicas': REPS}})
      
   # OPERATOR
   print(f"[!] Operator: {state}")
   for namespace,deployment in OPERATOR.items():
       api_response = apps.patch_namespaced_deployment_scale(
            name=deployment,
            namespace=namespace,
            body={'spec': {'replicas': REPS}})
      

   return True

# def assume(ROLE):
#    client   = boto3.client('sts')
#    response = client.assume_role(RoleArn=ROLE, RoleSessionName="MendixRestartEnv")
#    session  = boto3.Session(
#       aws_access_key_id=response['Credentials']['AccessKeyId'],
#       aws_secret_access_key=response['Credentials']['SecretAccessKey'],
#       aws_session_token=response['Credentials']['SessionToken']
#    )


# def assumeMendix(ACCOUNT="ACCOUNT_TO_ASSUME"):
#    ROLE={
#          "master":       "AWSControlTowerExecution",
#          "master_brown": "OrganizationAccountAccessRole"
#    }
#    PROFILE = "master" 
#    session  = boto3.Session(profile_name=PROFILE,region_name="us-east-1")
#    sts      = session.client('sts')
#    response = sts.assume_role(
#                      RoleArn=f"arn:aws:iam::{ACCOUNT}:role/{ROLE[PROFILE]}", 
#                      RoleSessionName="CloudOpsMigrationPolicyUpdater"
#    )
#    session  = boto3.Session(
#                      aws_access_key_id=response['Credentials']['AccessKeyId'],
#                      aws_secret_access_key=response['Credentials']['SecretAccessKey'],
#                      aws_session_token=response['Credentials']['SessionToken'],
#                      region_name="us-east-1"
#    )
#    return session

# ONLY WAY ACCESSING FROM OUTSIDE
# IS TO GET EPHEMERAL TOKEN
#
def get_cluster_info(eks, cluster_name):

    "Retrieve cluster endpoint and certificate"
    cluster_info = eks.describe_cluster(name=cluster_name)
    endpoint = cluster_info['cluster']['endpoint']
    cert_authority = cluster_info['cluster']['certificateAuthority']['data']
    cluster_info = {
        "endpoint" : endpoint,
        "ca" : cert_authority
    }
    return cluster_info


def get_bearer_token(service_id):
    "Create authentication token"
    signer = RequestSigner(
        service_id,
        session.region_name,
        'sts',
        'v4',
        session.get_credentials(),
        session.events
    )

    params = {
        'method': 'GET',
        'url': 'https://sts.{}.amazonaws.com/'
               '?Action=GetCallerIdentity&Version=2011-06-15'.format(session.region_name),
        'body': {},
        'headers': {
            'x-k8s-aws-id': cluster_name
        },
        'context': {}
    }

    signed_url = signer.generate_presigned_url(
        params,
        region_name=session.region_name,
        expires_in=STS_TOKEN_EXPIRES_IN,
        operation_name=''
    )

    base64_url = base64.urlsafe_b64encode(signed_url.encode('utf-8')).decode('utf-8')

    # remove any base64 encoding padding:
    return 'k8s-aws-v1.' + re.sub(r'=*', '', base64_url)
 
