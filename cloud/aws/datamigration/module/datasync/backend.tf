terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = ">= 4.5.0"
    }
  }
}

# GF MASTER
provider "aws" {
  profile = "master"
  region  = "us-east-1"
  alias   = "gf_master"
}

# BF MASTER
provider "aws" {
  profile = "master_brown"
  region  = "us-east-1"
  alias   = "bf_master"
}

# SOURCE ACC
/* provider "aws" { */
/*   profile = "${local.src_profile}" */
/*   assume_role { */
/*     role_arn     = "arn:aws:iam::${var.src_acc}:role/${var.ROLE[local.src_profile]}" */
/*     session_name = "-DataSync-tf" */
/*   } */
/*   region  = "us-east-1" */
/*   alias   = "source" */
/* } */

# DEST ACC
provider "aws" {
  profile = "${local.dst_profile}"
  assume_role {
    role_arn     = "arn:aws:iam::${var.dst_acc}:role/${var.ROLE[local.dst_profile]}"
    session_name = "-DataSync-tf"
  }
  region = "us-east-1"
  # alias  = "destination"
}

/* data "aws_caller_identity" "source_acc" { */
/*   provider = aws.source */
/* } */

data "aws_caller_identity" "dest_acc" {
  # provider  = aws.destination
}

data "aws_organizations_organization" "gf_orgs" {
  provider = aws.gf_master
}

