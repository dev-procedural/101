output "ttt" {
  value = [
    "src-${local.src_profile}-${var.src_acc}",
    "dst-${local.dst_profile}-${var.dst_acc}",
    [ for i in  var.bucket_list: i[1] ],
    toset( distinct( [ for i in flatten(var.bucket_list): split("/",i)[0] ] ) ),
    "${join("," , toset( distinct( [ for i in flatten(var.bucket_list): split("/",i)[0] ] ) ) )}"
  ]
}


