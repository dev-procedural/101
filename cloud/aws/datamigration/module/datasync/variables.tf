locals {
  tags = {
  }

  src_profile = length( compact( distinct( [ 
      for i in data.aws_organizations_organization.gf_orgs.accounts:
        "${i.id}" == "${var.src_acc}" ? "yes" : ""
  ] )) ) != 0 ? "master" : "master_brown"

  dst_profile = length( compact( distinct( [ 
      for i in data.aws_organizations_organization.gf_orgs.accounts:
        "${i.id}" == "${var.dst_acc}" ? "yes" : ""
  ] )) ) != 0 ? "master" : "master_brown"

  buckets_dedup = "${join("," , toset( distinct( [ for i in flatten(var.bucket_list): split("/",i)[0] ] ) ) )}"

  buckets_src   = "${join("," , toset( distinct( [ for i in var.bucket_list: i[0] ] ) ) )}"
  buckets_dst   = "${join("," , toset( distinct( [ for i in var.bucket_list: i[1] ] ) ) )}"
}

# BACKEND
variable "ROLE" {
  type = object({
    master       = string
    master_brown = string
  })
  default = {
    master       = "AWSControlTowerExecution"
    master_brown = "OrganizationAccountAccessRole"
  }
}

variable "src_acc" {
  description = "this should be the target account id"
  type = string
}

variable "dst_acc" {
  description = "this should be master account in case of greenfield"
  default     = "master"
}

variable "stateaccount" {
  default = "-Support"
}

# SERVICE
variable "bucket_list" {
	type = 	list
	description = "must look like bucket = prefix"
}

variable "create" {
}
