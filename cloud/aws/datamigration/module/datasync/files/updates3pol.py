#!/bin/python3

import boto3, json, argparse
from jinja2 import Template  

parser = argparse.ArgumentParser(description='This script updates a bucket policy')
group = parser.add_mutually_exclusive_group(required=True)
group.add_argument('--create',   help='update policy',    action="store_true")
group.add_argument('--delete',   help='delete update as it was before',    action="store_true")
group.add_argument('--backup',   help='backup policy',    action="store_true")
group.add_argument('--block',    help='blocks s3 write',    action="store_true")
parser.add_argument('accounts', type=str, help='accounts can be 1 or many comma separated')
parser.add_argument('roleArn', type=str, help='role arn of the target account')
parser.add_argument('src_bucket', type=str, help='source bucket')
args = parser.parse_args()


def isGreenfield(ACC,account='master'):
   session = boto3.Session(
          profile_name=account,
          region_name='us-east-1')
   orgs = session.client('organizations')
   response = orgs.list_accounts()
   results = response["Accounts"]
   while "NextToken" in response:
       response = orgs.list_accounts(NextToken=response["NextToken"])
       results.extend(response["Accounts"])
   match = [True for i in results if ACC == i['Id']]
   match = True if match else False
   return match

ROLE={
      "master":       "AWSControlTowerExecution",
      "master_brown": "OrganizationAccountAccessRole"
}

for ACCOUNT in args.accounts.split(","):

   PROFILE = "master" if isGreenfield(ACCOUNT) else "master_brown"
   VARS    = {
         'SOURCE_ROLE': args.roleArn, 
         'SOURCE_BUCKET': args.src_bucket
   }

   session  = boto3.Session(profile_name=PROFILE)
   sts      = session.client('sts')
   response = sts.assume_role(
                     RoleArn=f"arn:aws:iam::{ACCOUNT}:role/{ROLE[PROFILE]}", 
                     RoleSessionName="MigrationPolicyUpdater"
   )
   session  = boto3.Session(
                     aws_access_key_id=response['Credentials']['AccessKeyId'],
                     aws_secret_access_key=response['Credentials']['SecretAccessKey'],
                     aws_session_token=response['Credentials']['SessionToken']
   )
   s3       = session.resource('s3')

   bucket_policy = s3.BucketPolicy(args.src_bucket)
   bucket_pol    = json.loads(bucket_policy.policy) 

   if args.backup:
      with open(f'./{ACCOUNT}.policy.json.bkp') as FILE:
         FILE.write(bucket_pol)

   elif args.block:
      file = Template( open('./files/block.json','r').read() )
      file = json.loads(file.render(**VARS))
      bucket_pol['Statement'].append(file)
      try:
         response = bucket_policy.put(
           Policy=json.dumps(bucket_pol)
         )
         if response["ResponseMetadata"]["HTTPStatusCode"] == "204":
            print("Policy Updated")
      except Exception as ex:
         print(ex)

   elif args.create:

      file = Template( open('./files/s3policy.json','r').read() )
      file = json.loads(file.render(**VARS))
      bucket_pol['Statement'].append(file)

      try:
         response = bucket_policy.put(
           Policy=json.dumps(bucket_pol)
         )
         if response["ResponseMetadata"]["HTTPStatusCode"] == "204":
            print("Policy Updated")
      except Exception as ex:
         print(ex)

   elif args.delete:

      bucket_poldel = [i for i in bucket_pol['Statement'] if i['Sid'] != '-MIGRATION']
      bucket_pol['Statement'] = bucket_poldel

      try:
         response = bucket_policy.put(
           Policy=json.dumps(bucket_pol)
         )
         if response["ResponseMetadata"]["HTTPStatusCode"] == "204":
            print("Policy Updated")
      except Exception as ex:
         print(ex)

