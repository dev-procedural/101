data "aws_s3_bucket" "bucket_list" {
  for_each = toset( flatten(var.bucket_list) )
  bucket = split("/",each.value)[0]

  /* depends_on = [ null_resource.sync-migration-create ] */
}

resource "aws_datasync_location_s3" "sync_migration_location" {
	for_each = toset( flatten(var.bucket_list) )

  s3_bucket_arn = data.aws_s3_bucket.bucket_list[each.value].arn
  subdirectory  = "/${ join("/", slice( split("/", each.value ), 1,length( split("/", each.value )  )) ) }"

  s3_config {
    bucket_access_role_arn = aws_iam_role.sync_role_migration.arn
  }
  /* depends_on = [ null_resource.sync-migration-create ] */
}

resource "aws_datasync_task" "sync_migration_task" {
  count  = length(var.bucket_list)

  name                     = "sync-migration-${count.index}"
  source_location_arn      = aws_datasync_location_s3.sync_migration_location[var.bucket_list[count.index][0]].arn
  destination_location_arn = aws_datasync_location_s3.sync_migration_location[var.bucket_list[count.index][1]].arn

  preserve_deleted_files = "REMOVE"
  verify_mode            = "ONLY_FILES_TRANSFERRED"
  log_level              = "BASIC"

  /* depends_on = [ null_resource.sync-migration-create ] */

}
