resource "aws_iam_role" "sync_role_migration" {

  name = "sync--migration"
  assume_role_policy = <<EOF
${jsonencode(
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "Service": "datasync.amazonaws.com"
            },
            "Action": "sts:AssumeRole"
        }
    ]
}
)}
  EOF
}


resource "aws_iam_policy" "sync_policy_migration" {
  for_each = toset( distinct( [ for i in flatten(var.bucket_list): split("/",i)[0] ] ) )

  name = "sync-policy--${uuid()}"
  #name = "sync-policy--${split("/", each.value)[0]}"

  policy = <<EOF
${jsonencode(
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "s3:GetBucketLocation",
                "s3:ListBucket",
                "s3:ListBucketMultipartUploads"
            ],
            "Effect": "Allow",
            "Resource": "arn:aws:s3:::${each.value}"
        },
        {
            "Action": [
                "s3:AbortMultipartUpload",
                "s3:DeleteObject",
                "s3:GetObject",
                "s3:ListMultipartUploadParts",
                "s3:PutObjectTagging",
                "s3:GetObjectTagging",
                "s3:PutObject"
            ],
            "Effect": "Allow",
            "Resource": "arn:aws:s3:::${each.value}/*"
        }
    ]
}
)}
  EOF
}

resource "aws_iam_policy_attachment" "sync-migration-attachment" {
  for_each   = aws_iam_policy.sync_policy_migration
  name       = "sync-migration--attch-${length(each.key)}" 

  roles      = [ aws_iam_role.sync_role_migration.name ]
  policy_arn = each.value["arn"]
}
