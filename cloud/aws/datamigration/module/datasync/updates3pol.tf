resource "null_resource" "sync-migration-create" {
  count = var.create == true ? 1 : 0

  provisioner "local-exec" {
    command = "echo files/updates3pol.py --create ${local.buckets_src} ${aws_iam_role.sync_role_migration.arn}"
  }

  provisioner "local-exec" {
    when    = "destroy"
    command = "echo files/updates3pol.py --delete ${local.buckets_dedup} ${aws_iam_role.sync_role_migration.arn}"
  }

  depends_on = [aws_iam_role.sync_role_migration]

}

