provider "aws" {
  profile = "cloudops-support"
  region  = "us-east-1"
}

terraform {
  backend "s3" {
    bucket = "cloudops-sec-report"
    key    = "terraform/ssm.tfstate"
    region = "us-east-1"
  }
}

# data "terraform_remote_state" "base_state" {
#   backend = "s3"

#   config = {
#     bucket = "cloudops-sec-report" 
#     region = "us-east-1"
#     key    = "terraform/ssm.tfstate"
#   }
# }


