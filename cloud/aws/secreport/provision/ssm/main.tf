resource "aws_s3_bucket" "b" {
  bucket = "copsagentqualys"
}

resource "aws_s3_bucket_policy" "b" {
  bucket = aws_s3_bucket.b.id

  # Terraform's "jsonencode" function converts a
  # Terraform expression's result to valid JSON syntax.
  policy      = jsonencode({
    Id        = "Policy1634737828656"
    Version   = "2012-10-17"
    Statement =  [
      {
        Sid       = "Stmt1634737825964"
        Action    = "s3:*"
        Effect    = "Allow"
        Resource  = ["arn:aws:s3:::copsagentqualys/", "arn:aws:s3:::copsagentqualys/*"]
        Principal = "*"
      }
    ]
  })
}

