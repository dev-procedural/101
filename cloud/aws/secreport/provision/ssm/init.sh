#!/bin/bash -e
terraform init \
          -backend-config="bucket=cloudops-sec-report" \
          -backend-config="key=terraform/ssm.tfstate" \
          -backend-config="region=us-east-1"


