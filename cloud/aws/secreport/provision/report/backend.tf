provider "aws" {
  profile = "cloudops-support"
  region  = "us-east-1"
}

provider "aws" { 
  profile = "audit"
  region  = "us-east-1"
  alias   = "audit"
}

terraform {
  backend "s3" {
    bucket = "cloudops-sec-report"
    key    = "terraform/report.tfstate"
    region = "us-east-1"
  }
}

data "terraform_remote_state" "base_state" {
  backend = "s3"

  config = {
    bucket = "cloudops-sec-report" 
    region = "us-east-1"
    key    = "terraform/report.tfstate"
  }
}


