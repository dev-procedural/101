resource "aws_lambda_layer_version" "lambda_layer_pandas_secreport" {
  filename   = "${path.module}/lambda/pandas_layer.zip"
  layer_name = "pandas_secreport"
  source_code_hash    = filebase64sha256("${path.module}/lambda/pandas_layer.zip")
  compatible_runtimes = ["python3.7"]
}


resource "aws_lambda_function" "lambda_report" {
  filename      = "${path.module}/lambda/report.zip"
  function_name = "cloudops_sec_report"
  role          = aws_iam_role.lambda_sec_report_sec_cops.arn
  handler       = "report.lambda_handler"
	layers 				= [aws_lambda_layer_version.lambda_layer_pandas_secreport.arn]
  package_type  = "Zip"

  source_code_hash = filebase64sha256("${path.module}/lambda/report.zip")

  runtime     = "python3.7"
  memory_size = "180"
  timeout     = 60*5

  environment {
    variables = {
      ACCOUNT              = aws_iam_role.lambda_sec_report_sec_cops.arn
      ACCOUNT_ROLE_AUDIT   = aws_iam_role.lambda_sec_report_sec_aud.arn
      ACCOUNT_ROLE_INFOSEC = aws_iam_role.lambda_sec_report_sec_br.arn
      CLOUDOPS_TOPIC       = aws_sns_topic.cloudops_secreport.arn
    }
  }

  tags = local.tags
}

# resource "aws_lambda_function_event_invoke_config" "secreport_sns" {
#   function_name = aws_lambda_alias.

#   destination_config {
#     on_failure {
#       destination = aws_sqs_queue.example.arn
#     }

#     on_success {
#       destination = aws_sns_topic.example.arn
#     }
#   }
# }

