output "roles" {
  value = [
    aws_iam_role.lambda_sec_report_sec_cops.arn,
    aws_iam_role.lambda_sec_report_sec_br.arn,
    aws_iam_role.lambda_sec_report_sec_aud.arn,

  ]
}

output "sns" {
  value = aws_sns_topic.cloudops_secreport.arn
}
