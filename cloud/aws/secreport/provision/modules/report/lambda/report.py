import os, json, boto3, itertools, shutil, glob
import pandas as pd
from datetime import datetime as dt


T1 = dt.today().strftime('%Y_%m_%d')

def lambda_handler(event, context):
   OUTPUTDIR        = '/tmp/report'
   ACCOUNT          = os.environ['ACCOUNT']
   ACCOUNT_AUDIT    = os.environ['ACCOUNT_ROLE_AUDIT']
   ACCOUNT_INFOSEC  = os.environ['ACCOUNT_ROLE_INFOSEC']
   CLOUDOPS_TOPIC   = os.environ['CLOUDOPS_TOPIC'] 

   os.makedirs(OUTPUTDIR)

   try:
      # run report and save it in /tmp
      # profile: audit - infosec
      for ZONE,ACCOUNT_ in zip(['greenfield','brownfield'],[ACCOUNT_AUDIT,ACCOUNT_INFOSEC]):
         GD_FILE  = f'{OUTPUTDIR}/{T1}_{ZONE}_report_gd.csv'
         SH_FILE  = f'{OUTPUTDIR}/{T1}_{ZONE}_report_sechub.csv'
         XLS_FILE = f'{OUTPUTDIR}/{T1}_{ZONE}_report.xlsx'
         report_guard_duty(ACCOUNT_, ZONE, GD_FILE, SH_FILE)
         report_sechub(ACCOUNT_, ZONE, GD_FILE, SH_FILE)
         report_merge(ZONE, GD_FILE, SH_FILE, XLS_FILE)

   except Exception as ex:
      print(ex)
      raise ex

   # save merged report into bucket
   # profile: cloudops
   report_placement(ACCOUNT, OUTPUTDIR)

def report_placement(ACCOUNT, OUTPUTDIR):
   client   = boto3.client('sts')
   response = client.assume_role(RoleArn=ACCOUNT, RoleSessionName="secreport")
   session  = boto3.Session(aws_access_key_id=response['Credentials']['AccessKeyId'],aws_secret_access_key=response['Credentials']['SecretAccessKey'],aws_session_token=response['Credentials']['SessionToken'])

   # zip
   REPORT = f'{OUTPUTDIR}/{T1}_report.zip'
   LINK = f"https://cloudops-sec-report.s3.us-east-1.amazonaws.com/reports/{T1}_report.zip"
   REPORT = shutil.make_archive(REPORT, 'zip', OUTPUTDIR)
   REMOVE_CSV = [os.remove(i) for i in glob.glob('/tmp/report/*csv')]

   s3       = session.client('s3')
   report   = s3.upload_file(Bucket='cloudops-sec-report', Key=f'reports/{T1}_report.zip' , Filename=REPORT)
   #check    = s3.list_objects(Bucket='cloudops-sec-report', Prefix=f'reports/{T1}_report.zip')['Contents'][0]['Key']
   #report1  = s3.upload_file(Bucket='cloudops-sec-report', Key=f'reports/{T1}/{REPORT_GREEN}' , Filename=f'/tmp/{REPORT_GREEN}')
   #report2  = s3.upload_file(Bucket='cloudops-sec-report', Key=f'reports/{T1}/{REPORT_BROWN}' , Filename=f'/tmp/{REPORT_BROWN}')

   sns      = session.client('sns')

   response = sns.publish(
         TopicArn= os.environ['CLOUDOPS_TOPIC'],
         Message=f'Hi team,\nPlease find the link to the weekly security report\n\n{LINK}\n\nCloudOps',
         Subject='Weekly Security Report',
   )


def report_merge(ZONE, GD_FILE, SH_FILE, XLS_FILE):
   writer = pd.ExcelWriter(XLS_FILE, engine='xlsxwriter')

   guard = pd.read_csv(GD_FILE, delimiter='|')
   sechub= pd.read_csv(SH_FILE, delimiter='|')

   guard.to_excel(writer, sheet_name='GuardDuty')
   sechub.to_excel(writer, sheet_name='SecHub')
   writer.save()


def report_guard_duty(ACCOUNT_ARN, ZONE, GD_FILE, SH_FILE):
   client   = boto3.client('sts')
   response = client.assume_role(RoleArn=ACCOUNT_ARN, RoleSessionName="secreport")
   session  = boto3.Session(aws_access_key_id=response['Credentials']['AccessKeyId'],aws_secret_access_key=response['Credentials']['SecretAccessKey'],aws_session_token=response['Credentials']['SessionToken'])

   FindingCriteria= {
      'Criterion': {
         'severity': {
             'Gte': 7,
          }
      }
   }

   GUARD      = session.client('guardduty')
   detectorID = GUARD.list_detectors()['DetectorIds'][0]
   findings   = GUARD.list_findings(DetectorId=detectorID, FindingCriteria=FindingCriteria)
   nxt        = GUARD.list_findings(DetectorId=detectorID, FindingCriteria=FindingCriteria, NextToken=findings['NextToken']) 
   findings_  = []

   counter = 0
   while True:
      try:
         nxt = GUARD.list_findings(DetectorId=detectorID, FindingCriteria=FindingCriteria, NextToken=nxt['NextToken']) 
         findings_.append(nxt['FindingIds'])
         if nxt['NextToken'] != '':
            counter += 1
         else:
            break
         print(f"page:{counter}")
      except Exception as ex:
         pass
   findings_ = list(itertools.chain.from_iterable(findings_))

   findings = []
   start = 0
   stop  = 50
   for i in range(round(len(findings_) / 50)):
      try:
         req = GUARD.get_findings(DetectorId=detectorID, FindingIds=findings_[start:stop])['Findings']
         findings.append(req)
         start += stop
         stop  += start
      except Exception as ex:
         pass
   findings = list(itertools.chain.from_iterable(findings))
   findings = [i for i in findings if i['Severity'] >= 7]

   with open(GD_FILE, 'w+') as file_:
      header = ['AccountId',
                'Severity',
                'Title',
                'Description',
                # 'Region',
                'Type',
      ]
      header = '|'.join([i for i in header])
      file_.write(header)
      file_.write('\n')
      for entry in findings:
         fields = [entry['AccountId'],
                   entry['Severity'],
                   entry['Title'],
                   entry['Description'],
                   # entry['Region'],
                   entry['Resource']['ResourceType'],
         ]
         fields = '|'.join([str(i) for i in fields])
         file_.write(fields)
         file_.write('\n')
   print(f'GuardDuty Total entries: {len(findings)}')


def report_sechub(ACCOUNT_ARN, ZONE, GD_FILE, SH_FILE):
   client   = boto3.client('sts')
   response = client.assume_role(RoleArn=ACCOUNT_ARN, RoleSessionName="secreport")
   session  = boto3.Session(aws_access_key_id=response['Credentials']['AccessKeyId'],aws_secret_access_key=response['Credentials']['SecretAccessKey'],aws_session_token=response['Credentials']['SessionToken'])

   sechub   = session.client('securityhub')

   FILTER_1 = {
        'WorkflowState' : [
           {
               'Value' : 'NEW',
               'Comparison': 'EQUALS'
           },
           {
               'Value' : 'NOTIFIED',
               'Comparison': 'EQUALS'
           }
        ],
        'SeverityLabel' : [
           {
               'Value' : 'CRITICAL',
               'Comparison': 'EQUALS'
           },
           {
               'Value' : 'HIGH',
               'Comparison': 'EQUALS'
           }
        ],
        'ComplianceStatus': [
           {
               'Value': 'FAILED',
               'Comparison': 'EQUALS'
           }
        ],
        'RecordState': [
           {
                   'Value': 'ACTIVE',
                   'Comparison': 'EQUALS'
           },
        ]
   }

   result = sechub.get_findings(
     Filters= FILTER_1
   )

   findings = []

   nxt = sechub.get_findings(
     Filters= FILTER_1,
     NextToken=result['NextToken']
   )

   counter = 0
   while True:
      try:
         nxt = sechub.get_findings(
           Filters= FILTER_1,
           NextToken=nxt['NextToken']
         )
         if "NextToken" in nxt:
            counter += 1
            findings.append(nxt['Findings'][0])
         else:
            break

      except Exception as ex:
         print(ex)
         raise ex

   print(f'SecHub Total entries: {counter}')

   with open(SH_FILE, 'w+') as file_:
      header = ['AccountId', 
                'Severity',
                'Title',
                'Description',
                'Region',
                'Recommendation',
                'Type',
                'Id',
      ]
      header = '|'.join([i for i in header])
      file_.write(header)
      file_.write('\n')
      for entry in findings:
         fields = [entry['AwsAccountId'],
                   entry['Severity']['Label'],
                   entry['Title'],
                   entry['Description'],
                   entry['Region'],
                   entry['ProductFields']['RecommendationUrl'],
                   entry['Resources'][0]['Type'],
                   entry['Resources'][0]['Id'],
         ]
         fields = '|'.join([i for i in fields])
         file_.write(fields)
         file_.write('\n')


