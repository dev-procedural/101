# BROWNFIELD INFO SEC ACC
resource "aws_iam_role" "lambda_sec_report_sec_br" {
  name = "cloudops_lambda_sec_report_infosec"
  provider = aws.InfoSec_brown

  assume_role_policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "sts:AssumeRole",
            "Principal" = { "AWS" : "arn:aws:iam::${data.aws_caller_identity.cloudsupport.account_id}:root" }
        }
    ]
  })
}

resource "aws_iam_policy" "lambda_sec_report_sec_br" {
  name = "cloudops_lambda_sec_report"
  provider = aws.InfoSec_brown

  policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "guardduty:*",
                "securityhub:*"
            ],
            "Resource": "*"
        }
    ]
  })
}

resource "aws_iam_policy_attachment" "lambda_sec_report_sec_br" {
  name       = "cloudops_lambda_sec_report" 
  provider   = aws.InfoSec_brown

  roles      = [aws_iam_role.lambda_sec_report_sec_br.name]
  policy_arn = aws_iam_policy.lambda_sec_report_sec_br.arn

}

# AUDIT ACC
resource "aws_iam_role" "lambda_sec_report_sec_aud" {
  name = "cloudops_lambda_sec_report_audit"
  provider = aws.audit

  assume_role_policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "sts:AssumeRole",
            "Principal" = { "AWS" : "arn:aws:iam::${data.aws_caller_identity.cloudsupport.account_id}:root" }
        }
    ]
  })
}

resource "aws_iam_policy" "lambda_sec_report_sec" {
  name = "cloudops_lambda_sec_report"
  provider = aws.audit

  policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "guardduty:*",
                "securityhub:*"
            ],
            "Resource": "*"
        }
    ]
  })
}

resource "aws_iam_policy_attachment" "lambda_sec_report_sec" {
  name       = "cloudops_lambda_sec_report" 
  provider   = aws.audit

  roles      = [aws_iam_role.lambda_sec_report_sec_aud.name]
  policy_arn = aws_iam_policy.lambda_sec_report_sec.arn

}


# CLOUPSUPPORT ACC
resource "aws_iam_role" "lambda_sec_report_sec_cops" {
  name = "cloudops_lambda_sec_report_cloudops"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
     {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}


resource "aws_iam_policy" "lambda_sec_report_cops_pol" {
  name = "cloudops_lambda_sec_report"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:PutLogEvents",
                "s3:*",
                "sts:AssumeRole",
                "sns:Publish"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_iam_policy_attachment" "lambda_sec_report_sec_cops" {
  name       = "cloudopssecreport"
  roles      = [aws_iam_role.lambda_sec_report_sec_cops.name]
  policy_arn = aws_iam_policy.lambda_sec_report_cops_pol.arn
}


# SNS
resource "aws_iam_role" "lambda_sec_report_sec_sns" {
  name = "cloudops_lambda_sec_report_sns"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
     {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}


resource "aws_iam_policy" "lambda_sec_report_sns_pol" {
  name = "cloudops_lambda_sec_report_sns"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": "*"
        },
        {
            "Action": [
                "sns:*"
            ],
            "Resource": "${aws_sns_topic.cloudops_secreport.arn}",
            "Effect": "Allow"
        }

    ]
}
EOF
}

resource "aws_iam_policy_attachment" "lambda_sec_report_sns_cops" {
  name       = "cloudopssecreport"
  roles      = [aws_iam_role.lambda_sec_report_sec_sns.name]
  policy_arn = aws_iam_policy.lambda_sec_report_sns_pol.arn
}

