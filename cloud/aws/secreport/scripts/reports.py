#!/bin/env python3

import os, json, boto3, itertools, sys, shutil
import pandas as pd
from datetime import datetime as dt

T1       = dt.today().strftime('%Y_%m_%d')
ZONE     = sys.argv[1]
GD_FILE  = f'/tmp/{T1}_{ZONE}_report_gd.csv'
SH_FILE  = f'/tmp/{T1}_{ZONE}_report_sechub.csv'
XLS_FILE = f'/tmp/{T1}_{ZONE}_report.xlsx'

def report_merge(ZONE):
   writer = pd.ExcelWriter(XLS_FILE, engine='xlsxwriter')

   guard = pd.read_csv(GD_FILE, delimiter='|')
   sechub= pd.read_csv(SH_FILE, delimiter='|')

   guard.to_excel(writer, sheet_name='GuardDuty')
   sechub.to_excel(writer, sheet_name='SecHub')
   writer.save()
   
   # TODO SH UTLS ZIP FILE


def report_guard_duty(ZONE):

   session = boto3.Session(
         profile_name=os.environ['AWS_PROFILE'],
         region_name='us-east-1')

   FindingCriteria= {
      'Criterion': {
         'severity': {
             'Gte': 7,
          }
      }
   }

   GUARD      = session.client('guardduty')
   detectorID = GUARD.list_detectors()['DetectorIds'][0]
   findings   = GUARD.list_findings(DetectorId=detectorID, FindingCriteria=FindingCriteria)
   nxt        = GUARD.list_findings(DetectorId=detectorID, FindingCriteria=FindingCriteria, NextToken=findings['NextToken']) 
   findings_  = []

   counter = 0
   while True:
      try:
         nxt = GUARD.list_findings(DetectorId=detectorID, FindingCriteria=FindingCriteria, NextToken=nxt['NextToken']) 
         findings_.append(nxt['FindingIds'])
         if nxt['NextToken'] != '':
            counter += 1
         else:
            break
         print(f"page:{counter}")
      except Exception as ex:
         pass
   findings_ = list(itertools.chain.from_iterable(findings_))

   findings = []
   start = 0
   stop  = 50
   for i in range(round(len(findings_) / 50)):
      try:
         req = GUARD.get_findings(DetectorId=detectorID, FindingIds=findings_[start:stop])['Findings']
         findings.append(req)
         start += stop
         stop  += start
      except Exception as ex:
         pass
   findings = list(itertools.chain.from_iterable(findings))
   findings = [i for i in findings if i['Severity'] >= 7]

   with open(GD_FILE, 'w+') as file_:
      header = ['AccountId',
                'Severity',
                'Title',
                'Description',
                'Region',
                'Type',
      ]
      header = '|'.join([i for i in header])
      file_.write(header)
      file_.write('\n')
      for entry in findings:
         fields = [entry['AccountId'],
                   entry['Severity'],
                   entry['Title'],
                   entry['Description'],
                   entry['Region'],
                   entry['Resource']['ResourceType'],
         ]
         fields = '|'.join([str(i) for i in fields])
         file_.write(fields)
         file_.write('\n')
   print(f'GuardDuty Total entries: {len(findings)}')


def report_sechub(ZONE):

   session = boto3.Session(
         profile_name=os.environ['AWS_PROFILE'],
         region_name='us-east-1')

   sechub   = session.client('securityhub')

   FILTER_1 = {
        'WorkflowState' : [
           {
               'Value' : 'NEW',
               'Comparison': 'EQUALS'
           },
           {
               'Value' : 'NOTIFIED',
               'Comparison': 'EQUALS'
           }
        ],
        'SeverityLabel' : [
           {
               'Value' : 'CRITICAL',
               'Comparison': 'EQUALS'
           },
           {
               'Value' : 'HIGH',
               'Comparison': 'EQUALS'
           }
        ],
        'ComplianceStatus': [
           {
               'Value': 'FAILED',
               'Comparison': 'EQUALS'
           }
        ],
        'RecordState': [
           {
                   'Value': 'ACTIVE',
                   'Comparison': 'EQUALS'
           },
        ]
   }

   result = sechub.get_findings(
     Filters= FILTER_1
   )

   findings = []

   nxt = sechub.get_findings(
     Filters= FILTER_1,
     NextToken=result['NextToken']
   )

   counter = 0
   while True:
      try:
         nxt = sechub.get_findings(
           Filters= FILTER_1,
           NextToken=nxt['NextToken']
         )
         if "NextToken" in nxt:
            counter += 1
            findings.append(nxt['Findings'][0])
         else:
            break

      except Exception as ex:
         print(ex)
         raise ex

   print(f'SecHub Total entries: {counter}')

   with open(SH_FILE, 'w+') as file_:
      header = ['AccountId', 
                'Severity',
                'Title',
                'Description',
                'Region',
                'Recommendation',
                'Type',
                'Id',
      ]
      header = '|'.join([i for i in header])
      file_.write(header)
      file_.write('\n')
      for entry in findings:
         fields = [entry['AwsAccountId'],
                   entry['Severity']['Label'],
                   entry['Title'],
                   entry['Description'],
                   entry['Region'],
                   entry['ProductFields']['RecommendationUrl'],
                   entry['Resources'][0]['Type'],
                   entry['Resources'][0]['Id'],
         ]
         fields = '|'.join([i for i in fields])
         file_.write(fields)
         file_.write('\n')


report_guard_duty(ZONE)
report_sechub(ZONE)

report_merge(ZONE)
