#!/bin/env python

import boto3, os

session = boto3.Session(
      profile_name=os.environ['AWS_PROFILE'],
      region_name='us-east-1')

sechub   = session.client('securityhub')

FILTER_1 = {
     'WorkflowState' : [
        {
            'Value' : 'NEW',
            'Comparison': 'EQUALS'
        },
        {
            'Value' : 'NOTIFIED',
            'Comparison': 'EQUALS'
        }
     ],
     'SeverityLabel' : [
        {
            'Value' : 'CRITICAL',
            'Comparison': 'EQUALS'
        },
        {
            'Value' : 'HIGH',
            'Comparison': 'EQUALS'
        }
     ],
     'ComplianceStatus': [
        {
            'Value': 'FAILED',
            'Comparison': 'EQUALS'
        }
     ],
     'RecordState': [
        {
                'Value': 'ACTIVE',
                'Comparison': 'EQUALS'
        },
     ]
}

result = sechub.get_findings(
  Filters= FILTER_1
)

findings = []

nxt = sechub.get_findings(
  Filters= FILTER_1,
  NextToken=result['NextToken']
)

counter = 0
while True:
   try:
      nxt = sechub.get_findings(
        Filters= FILTER_1,
        NextToken=nxt['NextToken']
      )
      if "NextToken" in nxt:
         counter += 1
         findings.append(nxt['Findings'][0])
      else:
         break
      print(counter)
   except Exception as ex:
      pass

with open('/tmp/report_sechub.csv', 'w+') as file_:
   header = ['AccountId', 
             'Severity',
             'Title',
             'Description',
             'Region',
             'ResourceRegion',
             'Recommendation',
             'Type',
             'Id',
   ]
   header = '|'.join([i for i in header])
   file_.write(header)
   file_.write('\n')
   for entry in findings:
      fields = [
                entry['AwsAccountId'],
                entry['Severity']['Label'],
                entry['Title'],
                entry['Description'],
                entry['Region'],
                entry['Resources'][0]['Region'],
                entry['ProductFields']['RecommendationUrl'],
                entry['Resources'][0]['Type'],
                entry['Resources'][0]['Id'],
      ]
      fields = '|'.join([i for i in fields])
      file_.write(fields)
      file_.write('\n')

