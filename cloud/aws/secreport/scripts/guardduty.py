#!/bin/env python

import boto3, os, itertools

session = boto3.Session(
      profile_name=os.environ['AWS_PROFILE'],
      region_name='us-east-1')

FindingCriteria= {
   'Criterion': {
      'severity': {
          'Gte': 7,
       }
   }
}

GUARD      = session.client('guardduty')
detectorID = GUARD.list_detectors()['DetectorIds'][0]
findings   = GUARD.list_findings(DetectorId=detectorID, FindingCriteria=FindingCriteria)
nxt        = GUARD.list_findings(DetectorId=detectorID, FindingCriteria=FindingCriteria, NextToken=findings['NextToken']) 
findings_  = []

counter = 0
while True:
   try:
      nxt = GUARD.list_findings(DetectorId=detectorID, FindingCriteria=FindingCriteria, NextToken=nxt['NextToken']) 
      findings_.append(nxt['FindingIds'])
      if nxt['NextToken'] != '':
         counter += 1
      else:
         break
      print(f"page:{counter}")
   except Exception as ex:
      pass
findings_ = list(itertools.chain.from_iterable(findings_)) 

findings = []
start = 0
stop  = 50
for i in range(round(len(findings_) / 50)):
   try:
      req = GUARD.get_findings(DetectorId=detectorID, FindingIds=findings_[start:stop])['Findings']
      findings.append(req)
      start += stop
      stop  += start
   except Exception as ex:
      pass
findings = list(itertools.chain.from_iterable(findings))
findings = [i for i in findings if i['Severity'] >= 7]

with open('/tmp/report_gd.csv', 'w+') as file_:
   header = ['AccountId',
             'Title',
             'Severity'
             'Description',
             'Region',
             'Type',
   ]
   header = '|'.join([i for i in header])
   file_.write(header)
   file_.write('\n')
   for entry in findings:
      fields = [entry['AccountId'],
                entry['Title'],
                entry['Severity'],
                entry['Description'],
                entry['Region'],
                entry['Resource']['ResourceType'],
      ]
      fields = '|'.join([str(i) for i in fields])
      file_.write(fields)
      file_.write('\n')


