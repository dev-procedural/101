#!/bin/bash

[[ ! -f /tmp/group.map.txt ]] && echo "please create the file /tmp/group.map.txt with csv content"

./mapper.py > deploy.groups.sh

echo "[+] Creating relationships"
sh deploy.groups.sh &> deploy.log 

echo "[+] Relationships completed"
echo "[+] Validating..."
wc -l deploy.groups.sh deploy.log 

