#!/bin/env python3

import boto3, time, os, sys, argparse


parser = argparse.ArgumentParser(description='This script adds a permission set to a group')
group = parser.add_mutually_exclusive_group(required=True)
group.add_argument('--dry', help='run dry run', action="store_true")
group.add_argument('--apply', help='add user to account',  action="store_true")
parser.add_argument('--account',  '-a', type=str, help='account id',                 required=True)
parser.add_argument('--groupName','-g', type=str, help='append last access of user', required=True)
parser.add_argument('--roleName', '-r', type=str, help='append perm inline policy',  required=True)
parser.add_argument('--listonly', '-l', help='list only',  action="store_true")
args = parser.parse_args()

#__________________________[ Args 
target_account = args.account
groupname      = args.groupName
target_role    = args.roleName
accountID      = target_account

if args.dry:
  print(f"""
    account: {target_account}
    group:   {groupname}
    role:    {target_role}
  """)

  sys.exit()

if args.apply:
  #__________________________[ Managing connection 

  session = boto3.Session(
      profile_name='master',
      region_name='us-east-1')

  sso_client = session.client('sso-admin')
  id_client  = session.client('identitystore')


  #__________________________[ Account info
  instanceinfo = sso_client.list_instances(MaxResults=1)

  instance_arn     = instanceinfo['Instances'][0]['InstanceArn']
  identitystore_id = instanceinfo['Instances'][0]['IdentityStoreId']

  try:
     groupinfo = id_client.list_groups(
       IdentityStoreId=identitystore_id,
       MaxResults=10,
       Filters=[
         {
            'AttributePath': 'DisplayName',
            'AttributeValue': groupname
         },
       ]
     )
  except Exception as ex:
    print(f'[+] {target_account} Adding Role:{target_role} set to Group:{groupname}: [?] NOT ADDED -not {groupname}')
    sys.exit()

  group_id   = groupinfo['Groups'][0]['GroupId']
  group_name = groupinfo['Groups'][0]['DisplayName']

  result     = sso_client.list_permission_sets(InstanceArn=instance_arn, MaxResults=100)['PermissionSets']

  try:
    permSet    = [sso_client.describe_permission_set(InstanceArn=instance_arn,PermissionSetArn=i)['PermissionSet'] for i in result ]
    permSet    = [i['PermissionSetArn'] for i in permSet if i['Name'] == target_role ][0]
  except:
    print(f'[+] {target_account} Adding Role:{target_role} set to Group:{groupname}: [?] NOT ADDED - not permset {target_role}')
    sys.exit()


  if not args.listonly:

     assignmentinfo = sso_client.create_account_assignment(
       InstanceArn=instance_arn,
       TargetId=target_account,
       TargetType='AWS_ACCOUNT',
       PermissionSetArn=permSet,
       PrincipalType='GROUP',
       PrincipalId=group_id
     )

  time.sleep(2)
  try:

    VALIDATION  = sso_client.list_account_assignments(InstanceArn=instance_arn,AccountId=target_account,PermissionSetArn=permSet)
    VALIDATION2 = ["[!] DONE" for i in VALIDATION['AccountAssignments'] if i['PrincipalId'] == group_id ][0] or "NOT ADDED"

  except:
    print(f'[+] {target_account} Adding Role:{target_role} set to Group:{groupname}: [?] NOT ADDED - validation {VALIDATION}')
    sys.exit()

  print(f'[+] {target_account} Adding Role:{target_role} set to Group:{groupname}: {VALIDATION2}')
