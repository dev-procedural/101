#!/bin/python3

import sys

FILE = open('/tmp/group.map.txt').read()
FILE = [i for i in FILE.split('\n') if i]

# ZIPPER CHECK LENGTH MATCHES TO PROCEED WITH ZIP
ZIPPER = lambda STRING: \
   list(zip( 
      STRING[1].split(','), 
      STRING[2].split(',') 
   )) if len(STRING[1].split(',')) == len(STRING[2].split(',')) else ""

# COUNTER = lambda STRING: \
#    list(
#       list(STRING[1].split(',')), 
#       list(STRING[2].split(',')) 
#    )  

RESULT = list(map(lambda LINE: [LINE.split(',"')[0], ZIPPER(LINE.split(',"'))] ,  FILE ))
RE     = [ i for i in RESULT if i[1] ]
RE     = [[*i, len(i[1])] for i in RE ]

RENOT  = [ i for i in RESULT if not i[1] ]

getCMD = lambda X: [ f'./addgroup.py -a {X[0]} -g {X[1][q][0]} -r {X[1][q][1]} --apply' for q in range(len(X[1])) ]

LIST = list(map( lambda x: getCMD(x) , RE) )
LIST = [ x.replace('"','') for i in LIST for x in i ]

for i in LIST:
   print(i)

sys.exit()
