from mesa import Agent, Model
from mesa.time import RandomActivation
from mesa.space import MultiGrid
from mesa.datacollection import DataCollector


class BuyerAgent(Agent):
    """ An agent with fixed initial wealth."""
    def __init__(self, unique_id, model):
        super().__init__(unique_id, model)
        self.Tickets = 0
    def buy(self, seller):
        # buy
        return

    def findSeller(self):
        # find seller
        return

    def step(self):
        seller = self.findSeller()
        if seller.Tickets > 1:
            self.buy(seller)

class SellerAgent(Agent):
    """ An agent with fixed tickets."""
    def __init__(self, unique_id, model):
        super().__init__(unique_id, model)
        self.events  = 52
        self.tickets = 100 # this might be random

    def sellTicket():
        # sell tickets
        return

    def step(self):
        self.sellTicket()


class GameTicketModel(Model):
    """A model with some number of agents."""
    def __init__(self, N):
        self.num_agents = N
        self.schedule = RandomActivation(self)
        self.running = True # Batchrunner activator
        # Create agents
        for i in range(self.num_agents):
            a = BuyerAgent(i, self)
            self.schedule.add(a)

        self.datacollector = DataCollector(
            model_reporters={"DailyBuy": "DailyBuy" },
            agent_reporters={"Assistance": "Assistance"})

    def step(self):
        '''Advance the model by one step.'''
        self.schedule.step()
        self.datacollector.collect(self)
