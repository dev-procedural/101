import math

from mesa.visualization.ModularVisualization import ModularServer
from mesa.visualization.UserParam import UserSettableParameter
from mesa.visualization.modules import ChartModule
from mesa.visualization.modules import NetworkModule
from mesa.visualization.modules import TextElement
from .model import ProducerConsumerModel, State


def network_portrayal(G):
    # The model ensures there is always 1 agent per node

    def node_color(agent):
        return {"Event": "#FF0000", "Users": "#008000"}.get(
            agent.state, "#808080"
        )

    def edge_color(agent1, agent2):
        if "LOYAL" in (agent1.state, agent2.state):
            return "#000000"
        return "#e8e8e8"

    def edge_width(agent1, agent2):
        if State.LOYAL in (agent1.state, agent2.state):
            return 3
        return 2

    def get_agents(source, target):
        return G.nodes[source]["agent"][0], G.nodes[target]["agent"][0]

    portrayal = dict()
    portrayal["nodes"] = [
        {
            "size": 6,
            "color": node_color(agents[0]),
            "tooltip": f"id: {agents[0].unique_id}<br>state: {agents[0].state.name}",
        }
        for (_, agents) in G.nodes.data("agent")
    ]

    portrayal["edges"] = [
        {
            "source": source,
            "target": target,
            "color": edge_color(*get_agents(source, target)),
            "width": edge_width(*get_agents(source, target)),
        }
        for (source, target) in G.edges
    ]

    return portrayal


network = NetworkModule(network_portrayal, 500, 500, library="d3")
chart = ChartModule(
    [
        {"Label": "Assistance", "Color": "#FF0000"},
        {"Label": "Inassistance", "Color": "#008000"},
        {"Label": "Tickets", "Color": "#808080"},
    ]
)


# class MyTextElement(TextElement):
#     def render(self, model):
#         ratio = model.assistance_ratio()
#         ratio_text = "&infin;" if ratio is math.inf else f"{ratio:.2f}"
#         assitance_text = str(number_assistance(model))

#         return "Assistance/Assistance Ratio: {}<br>Events Remaining: {}".format(
#             ratio_text, assistance_text
#         )


model_params = {
    "num_nodes": UserSettableParameter(
        "slider",
        "Number of agents",
        10,
        10,
        100,
        1,
        description="Choose how many agents to include in the model",
    ),
}

server = ModularServer(
    ProducerConsumerModel, [network, chart], "Model", model_params
    #ProducerConsumerModel, [network, MyTextElement(), chart], "Model", model_params
)
server.port = 8521
