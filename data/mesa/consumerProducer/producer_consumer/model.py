import math
from enum import Enum
import networkx as nx

from mesa import Agent, Model
from mesa.time import RandomActivation
from mesa.datacollection import DataCollector
from mesa.space import NetworkGrid


class State(Enum):
    SOLDOUT   = 0
    AVAILABLE = 1
    LOYAL     = 2


# REPORT FN
def number_state(model, state):
    return sum(1 for a in model.grid.get_all_cell_contents() if a.state is state)

def number_loyal(model):
    return number_state(model, State.LOYAL)


class ProducerConsumerModel(Model):
    """Tickets model token base"""

    def __init__(
        self,
        num_nodes=10,
    ):

        self.num_nodes = num_nodes
        prob = 3 / self.num_nodes
        self.G = nx.erdos_renyi_graph(n=self.num_nodes, p=prob)
        self.grid = NetworkGrid(self.G)
        self.schedule = RandomActivation(self)

        self.datacollector = DataCollector(
            {
                "Tickets": "Tickets",
            }
        )

        # Create consumers
        for i,node in enumerate(self.G.nodes()):
            a = ConsumerAgent(node, self,  State.LOYAL)
            self.schedule.add(a)
            self.grid.place_agent(a, node)
            if i == num_nodes-1:
                break

        # b = ProducerAgent(num_nodes, self)
        # self.schedule.add(b)
        # self.grid.place_agent(b, num_nodes)

        self.running = True
        self.datacollector.collect(self)

    def step(self):
        self.schedule.step()
        # collect data
        self.datacollector.collect(self)

    def run_model(self, n):
        for i in range(n):
            self.step()


class ConsumerAgent(Agent):
    """ An agent with fixed initial wealth."""
    def __init__(self, unique_id, model, state):
        super().__init__(unique_id, model)
        self.Tickets = 0
        self.state   = state

    def findProducer(self):
        producers = [i for i in a.schedule.agents if "Producer" in i.__str__() ]
        return self.random.choice(producers)
    
    def step(self):
        steps = self.model.schedule.steps
        seller = self.findProducer()
        if seller.Tickets >= 1:
            seller.Tickets -= 1
            self.Tickets   += 1 
            #self.model.dc["Strategies"]["step"].append( steps )
            
        # row = {"step"  : self.model.schedule.steps, 
        #        "id"   : self.unique_id,
        #        "score": score()+str(self.unique_id),
        #        "tickets": self.Tickets,
        #        "eventcount": seller.Tickets
        #       }
        #self.model.dc.add_table_row("Strategies", row , ignore_missing=False )
        # print (f"Consumer {str(self.unique_id)} buys #{self.Tickets} for event:{seller.unique_id} TktCount:{seller.Tickets}")
        # print (f"Consumer")

class ProducerAgent(Agent):
    """ An agent with fixed initial wealth."""
    def __init__(self, unique_id, model, tickets=10):
        super().__init__(unique_id, model)
        self.Events = 52
        self.Tickets = tickets

    def step(self):
        return 
