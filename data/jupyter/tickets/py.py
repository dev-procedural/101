import pandas as pd
import numpy  as np
from datetime import datetime

import iplantuml

pd.set_option("display.max_rows", None, "display.max_columns", None)




actor_producer  = pd.DataFrame(columns=['ProducerID','EventID'])
actor_consumer  = pd.DataFrame(columns=['ActorID','EventID'])
actor_validator = pd.DataFrame()

# TODO provide relationship

# HELPERS
createYear   = lambda x=1,year=datetime.now().year: pd.date_range(start=f'{year+x}-01-01', end=f'{year+x+1}-01-01',freq='D')
lastDayMonth = pd.Period(datetime.now().strftime("%Y-%m")).days_in_month
daysInYear   = pd.Period(datetime.now().strftime("%Y")).day_of_year

def _actor_producer():
    """
    producer creates events
    R single mode, weekly , review consumer  
    TODO function calculates detail once year is in place
    R event size - fixed
    TODO add more effective event uid taken hash from all columns
    TODO add final detail https://numpy.org/doc/stable/reference/generated/numpy.split.html
    """
    # INIT
    df               = pd.DataFrame(index=np.arange(daysInYear+1), columns=['Events', 'EventSize', 'Producers', 'Date'])
    # FX Helpers
    #create_producer = lambda: np.random.binomial(2,0.1) 
    create_events    = lambda: [1 if i.weekday() >= 5 else 0 for i in df.index.date ]
    create_eventSize = lambda: np.random.randint(100,200)
    # Date fix
    df['Date']      = createYear()
    df.set_index('Date', inplace=True)
    # Real actions    TODO single mode, weekly , review consumer 

    df['Events']    = create_events()
    df['EventSize'] = df.loc[(df.Events == 1), 'EventSize'].apply(lambda x: int(create_eventSize()) )
    #df['Event'] = np.where((df.Event == 'Painting'),'Art',df.Event)

    df['EventSize'] = df['EventSize'].fillna(0)
    #df.loc[df.Events != 1, 'Producers'] = 0

    df['Producers'] = np.where(df['Events'] == 1, "Max DJ", "")
 
    #df['Producers'] = df['Producers'].apply(lambda x: create_producer())
    # df['Producers'] = np.where(df['Producers'] == 0, 1,df['Producers'])
    df['TotalEvents']    = df['Events'].cumsum()
    #df['ProducersSum'] = df['Producers'].cumsum()
    
    # replace for a list with sequence from only
    #df['Id'] = df[['Events', 'EventSize']].sum(axis=1).map(hash)
    df['Id'] = np.where(df.EventSize == 0, 0,  df.TotalEvents )
    #df['Id'] = df.groupby(['Events', 'EventSize']).ngroup()

    return df


def _actor_consumer(producer_table=producer):
    """
    consumers buys producer's events
    consumers can or can't attend to events
    event are associated to consumers
    R randomize assitance
    R reframe weekly based on event size
    
    
    R from producer table calculate assistance
    R from producer table creata fx that randomly subscribe consumer to multiple events
        R consumer randomly pick N events
        R consumer randomly Assist N events, hense drop N events
        R should look like - Tickets : id1,id2
    TODO add distribution https://stackoverflow.com/questions/16312006/python-numpy-random-normal-only-positive-values
    R buy distribution
    R assistance distribution
    """
    # INIT
    df = pd.DataFrame(columns=['Id', 'Tickets', 'Assistance']) 

    # FX
    consumerBuyTickets   = lambda  : np.random.choice(producer_table[producer_table['Id'] > 0]['Id'].values,np.random.binomial(52,0.6), replace=False)
    consumerAssistance   = lambda x: np.random.choice(a=x, size=np.random.binomial(len(x),0.98), replace=False ) if len(x) > 1 else []
    #consumerNoAssistance = lambda x: set(x) - set(x)
    #consumerAssistance = lambda x: df['Tickets'].apply(lambda x: np.random.choice(a=x, size=int(len(x))) if len(x) > 1 else 0 )
    #print(producer_table[producer_table['Id'] > 0]['Id'].values)
    #print(producer_table['Id'])
    
    df['Id']         = np.arange(100)
    df['Tickets']    = df['Tickets'].apply( lambda x: sorted(consumerBuyTickets()) )
    df['Assistance'] = df['Tickets'].apply( lambda x: sorted(consumerAssistance(x)))
    df['NotShowed']  = df['Tickets'].map(set) - df['Assistance'].map(set)
    df['NotShowed']  = df['NotShowed'].apply(list)
    df['NotShowed']  = df['NotShowed'].apply( lambda x: [len(x), x]) 
    df['Buy']        = df['Tickets'].apply(lambda x: len(x))
    #df['Total']      = df['Buy'] + df['NotShowed']
    #df['NoAssistance'] = df['Tickets'].apply( lambda x: list(set(x['Tickets']) - set(x['Assistance'])) )
    #print(np.random.choice( df['Tickets'][0], len(df['Tickets'][0])  ))
    #print(df['NotShowed'][0][0] + df['Buy'])
    df['Total']      = df.apply( lambda x: x['Buy'] + x['NotShowed'][0], axis=1 )  
    #df['Total']      = df[['Buy','NotShowed']].sum(axis=1, skipna=True)  
    return df

def _actor_validator(consumer_table=consumer):
    """
    validator check consumer assistance
    R consumer tracking aka loyalty
    R ranked user    

    """

    df = pd.DataFrame(columns=['Id', 'UserId', 'Assistance%']) 

    df['Id']          = np.arange(len(consumer_table))
    df['UserId']      = consumer_table['Id']
    df['Assistance%'] = df.apply(lambda x: f"{((consumer_table['Total'][x.name]*100)/52):.2f}", axis=1)
    #df['Rank']        = sorted(df['Assistance%'], reverse=True)
    
    return df.sort_values(by='Assistance%', ascending=False)

producer   = _actor_producer()                        # table 1 > productores, eventos por productor
consumer   = _actor_consumer(producer_table=producer) # table 2 > consumidores, compra, asistencia    : tabla 1
validator  =_actor_validator(consumer_table=consumer) # table 3 > valida asistencia, precio token     : table 2

