#include <stdlib.h>
#include <stdio.h>


char* RandomShape() ;

int main(void) {

    for (int i = 0; i <= 10; i++){
        char* STR = RandomShape() ;
        printf("%s\n", STR) ;
    }

    return 0;
}


char* RandomShape(){

    int random_int = rand() % 3;
    printf("%d ", random_int);

    switch (random_int) {
        case 0:
            return "ROCK";
        case 1:
            return "SCISSORS";
        case 2:
            return "PAPER";
    }

}

