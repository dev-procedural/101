#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

enum GameStatus {CONTINUE, GAME_WON, GAME_LOST};
enum RoundStatus {DRAW, WON, LOST};
enum Shape {ROCK, PAPER, SCISSORS};

enum Shape RandomShape();
enum RoundStatus getRoundStatus();

int main(void) {
    srand(time(NULL));    
    enum GameStatus gameStatus = CONTINUE; 
    int bestOfN = 3;
    int winThreshold = (bestOfN / 2) + 1;

    int playerWinCount = 0;
    int computerWinCount = 0;
    int roundNum = 0; 

    while (CONTINUE == gameStatus){
        enum Shape playerShape = RandomShape() ;
        enum Shape computerShape = RandomShape() ;

        enum RoundStatus roundStatus = getRoundStatus(playerShape, computerShape) ;

        switch (roundStatus){
            case DRAW:;
                /* printf("p %s vs com %s ", str(playerShape), str(computerShape)); */
                printf("this is coming from draw\n");
                gameStatus = CONTINUE ;
                break;
            case WON:
                /* printf("p %s vs com %s ", playerShape, computerShape); */
                printf("this is coming from won\n");
                gameStatus = GAME_WON;
                break;
            case LOST:
                /* printf("p %s vs com %s ", playerShape, computerShape); */
                /* printf("you lost!"); */

                printf("this is coming from lost\n");
                gameStatus = GAME_LOST;
                break;
        }
    }

    switch (gameStatus){
        case GAME_WON:
            printf("you Won!");
            break; 
        case GAME_LOST:
            printf("you Lost!");
            break; 
        case CONTINUE:
        deafault:
            printf("[!] continue");

    }

    return 0;

}

enum RoundStatus getRoundStatus(enum Shape playerShape, enum Shape computerShape){
    if (playerShape == computerShape){
        return DRAW;
    } else if ((playerShape == ROCK && computerShape == SCISSORS) || 
               (playerShape == SCISSORS && computerShape == PAPER) || 
               (playerShape == PAPER && computerShape == ROCK)) {
        return WON;
    } else {
        return LOST;
    }
}


enum Shape RandomShape() {

    int random_int = rand() % 3;
    /* printf("%d", random_int); */

    switch (random_int) {
        case 0:
            return ROCK;
        case 1:
            return SCISSORS;
        case 2: 
            return PAPER;
    }

}


