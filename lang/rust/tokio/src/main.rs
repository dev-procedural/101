use tokio::task;
use tokio::task::yield_now;
use std::rc::Rc;



async fn say_world() {
    println!("world");
}

#[derive(Debug)]
struct RESULT {
	a: String,
}

#[tokio::main]
async fn main() {
	// async call
    let op = say_world();
    println!("hello");
    op.await;
	
	
    // spawn
    let handle = tokio::spawn(async {
        RESULT{ a: "this".to_string()}
    });
    // catch
    let out = handle.await.unwrap();
    println!("GOT {:#?}", out);

	
    let ii = task::spawn(async move {
        {
            let rc = Rc::new("hello");
            println!("{}", rc);
        }
        yield_now().await;
    });
	println!("{ii:?}")

}

