#![allow(dead_code)]
// use std::collections::HashMap;
// use std::io::{self};
use clap::Parser;

#[derive(Debug, Parser)]
#[command(author, version, about, long_about = None)]
struct Options {
    /// Load files or file comma separated
    #[arg(short, long, default_value_t = String::from("file.txt"))]
    file: String,

    /// Load names comma separated
    #[arg(short, long, value_delimiter = ',', num_args = 1)]
    name: Option<Vec<String>>,

    /// Read the message from STDIN instead of the argument
    #[clap(short, long, env)]
    read: bool,
}

#[derive(Clone, Debug)]
struct FileResult {
    name: String,
    data: String,
    err: String,
}

fn main() {
    let options = Options::parse();
    let files: Vec<String> = {
        if options.file.contains(",") {
            let v: Vec<String> = options.file.split(",").map(str::to_string).collect();
            v
        } else {
            vec![options.file.to_string()]
        }
    };
    let data = {
        let mut d: Vec<FileResult> = vec![];
        for file in &files {
            // let dd = std::fs::read_to_string(file).unwrap();
            let dd = match std::fs::read_to_string(file) {
                Ok(file_cont) => file_cont,
                Err(e) => format!("file {file} {e}"),
            };
            if !!!dd.contains("error") {
                d.push(FileResult {
                    name: file.to_string(),
                    data: dd,
                    err: "".to_string(),
                });
            } else {
                d.push(FileResult {
                    name: file.to_string(),
                    data: "".to_string(),
                    err: dd,
                });
                continue;
            }
        }
        d
    };
    let fff = Clone::clone(&data);
    let ff: Vec<String> = fff
        .into_iter()
        .map(|x| format!(">>>{}<<<", x.name))
        .collect();

    println!("files: {files:?}");
    println!("data files: {data:#?}");
    println!("dd: {ff:#?}");
    println!(
        "options: name: {:#?}, read: {:#?}",
        options
            .name
            .into_iter()
            .flat_map(|s| s)
            .collect::<Vec<String>>()
            .join("/"),
        options.read,
    );
}
