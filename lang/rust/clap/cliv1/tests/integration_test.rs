use assert_cmd::prelude::*;
use predicates::prelude::*;
use std::process::Command;

#[test]
fn run_with_defaults() {
    Command::cargo_bin("cliv1")
        .expect("binary exists")
        .assert()
        .success();
}

#[test]
fn run_with_defaults2() {
    Command::cargo_bin("cliv1")
        .expect("binary exists")
        .assert()
        .success()
        .stdout(predicate::str::contains("this text"));
}

#[test]
fn fail_on_non_existing_file() -> Result<(), Box<dyn std::error::Error>> {
    Command::cargo_bin("cliv1")
        .expect("binary exists")
        .args(&["-f", "file2.tx"])
        .assert()
        .stdout(predicate::str::contains("error"));
    Ok(())
}
