#![allow(dead_code)]
// use std::collections::HashMap;
// use std::io::{self};
use clap::{ArgAction, Args, Parser, Subcommand};

#[derive(Debug, Parser)]
#[command(author, version, about, long_about = None)]
#[command(propagate_version = true)]
struct Options {
    #[command(subcommand)]
    command: Commands,
}

#[derive(Debug, Subcommand)]
enum Commands {
    /// Adds files to myapp
    Add(AddArgs),
    /// Del something that need to be del
    Del(DelArgs),
}

#[derive(Debug, Args)]
struct AddArgs {
    /// this name
    #[arg(short, long)]
    name: Option<String>,

    /// this lastname
    #[arg(short, long)]
    last: Option<String>,
}

#[derive(Debug, Args)]
struct DelArgs {
    /// this name
    #[arg(short, long, value_delimiter = ',', num_args = 1..=3)]
    name: Option<Vec<String>>,

    /// this queue
    #[arg(short, long="queue", action=ArgAction::SetTrue)]
    q: Option<bool>,
}

fn main() {
    let opt = Options::parse();

    match &opt.command {
        Commands::Add(add) => {
            println!("'myapp add' was used: {:?}", add)
        }

        Commands::Del(del) => {
            println!("'myapp del' was used: {:?}", del)
        }
    }
}
