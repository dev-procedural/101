use clap::Parser;
use colored::Colorize;
use std::io::{self};

#[derive(Debug, Parser)]
struct Options {
    #[clap(default_value = "SolveEtCoagula!")]
    /// What does the alchemist say
    msg: String,

    #[clap(short = 'i', long = "is")]
    /// Make the cat appear dead
    is: bool,

    #[clap(short = 'f', long = "file")]
    /// Load the alchemist recipe from the specified file
    recipe: Option<std::path::PathBuf>,

    #[clap(short = 's', long = "stdin")]
    /// Read the message from STDIN instead of the argument
    stdin: bool,
}

fn main() {
    let options = Options::parse();

    let message: String = match options {
        ref o @ Options { .. } if o.msg != "SolveEtCoagula!" => {
            format!("{:#?}", o.msg)
        }
        ref o @ Options { .. } if o.msg == "SolveEtCoagula!" => {
            format!("{:#?}", o.msg)
        }
        Options { stdin: true, .. } => {
            format!(
                "stdin true {:#?} ",
                io::read_to_string(io::stdin()).unwrap()
            )
        }
        Options { recipe: v, .. } => match v {
            Some(path) => {
                let file = match std::fs::read_to_string(path) {
                    Ok(file_cont) => file_cont,
                    Err(e) => format!("error-senior {}", e),
                };
                if file.contains("error") {
                    panic!("Err {:?}", file)
                } else {
                    format!("file: {:?}", file)
                }
            }
            _ => format!(""),
        },
    };
    // message = if message == "" { options.msg } else { message };
    let iss = if options.is { "Y" } else { "N" };
    println!(
        "msg: {} is: {}",
        message.bright_red().on_blue(),
        iss.bright_blue().on_red()
    );
}
