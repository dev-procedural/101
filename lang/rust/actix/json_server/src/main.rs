use actix_cors::Cors;
use actix_web::{http, web, App, HttpResponse, HttpServer, Responder};

use anyhow::Error;
use clap::Parser;
use serde::Serialize;
use serde_json::Value;
use std::env;

#[derive(Debug, Parser)]
struct Options {
    /// Load files or file comma separated
    #[clap(short, long, default_value_t = String::from("file.json"), env)]
    file: String,
}

async fn json_ep() -> impl Responder {
    let file = env::var("FILE_JSON")
        .or::<String>(Ok("file.json".to_string()))
        .unwrap();
    let json_ = match std::fs::read_to_string(file) {
        Ok(file_) => file_,
        Err(_) => std::fs::read_to_string("file.json".to_string()).unwrap(),
    };
    let f: Value = serde_json::from_str(&json_).expect("unable serialize");

    return web::Json(f);
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let opt = Options::parse();

    println!("Listening on localhost:8080/json");
    HttpServer::new(|| {
        let cors = Cors::permissive().max_age(3600);
        App::new().wrap(cors).route("/json", web::get().to(json_ep))
    })
    .bind("localhost:8080")?
    .run()
    .await
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    fn typex<K>(_: &K) -> String {
        format!("{}", std::any::type_name::<K>())
    }

    #[tokio::test]
    async fn test_json_file() -> Result<(), Error> {
        let json_file = env::var("FILE_JSON").ok().expect("ok this errored");
        println!("typex; {}", typex(&json_file));
        println!("{:#?}", json_file);
        Ok(())
    }
}
