use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use serde::Serialize;

async fn hello() -> impl Responder {
    HttpResponse::Ok().body("Hello world")
}

#[derive(Serialize)]
pub struct Alchemist {
    pub name: String,
    pub country: String,
    pub phrase: String,
}

async fn alchemy_endpoint() -> impl Responder {
    let alchemists = vec![
        Alchemist {
            name: "Paracelsus".to_string(),
            country: "swiss".to_string(),
            phrase: "All things are poison, and nothing is without poison".to_string(),
        },
        Alchemist {
            name: "Flamel".to_string(),
            country: "france".to_string(),
            phrase: "Good or bad is a matter of perspective".to_string(),
        },
    ];
    return web::Json(alchemists);
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    println!("Listening on port 8080");
    HttpServer::new(|| {
        App::new()
            .service(web::scope("/api").route("/alchem", web::get().to(alchemy_endpoint)))
            .route("/hello", web::get().to(hello))
    })
    .bind("127.0.0.1:8080")?
    .run()
    .await
}
