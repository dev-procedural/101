#![allow(dead_code)]
#![allow(unused)]
#![allow(unused_variables)]

// turbofish
#[derive(Debug)]
struct Bag<T> {
    item: T,
}

// option
#[derive(Debug)]
struct BagOption<T> {
    item: Option<T>,
}

#[derive(Debug)]
struct Bar {
    item: String,
}

#[derive(Debug)]
struct Foo {
    x: Bar,
}

#[derive(Debug)]
struct Person {
    name: String,
    age: i32,
}

impl Person {
    fn year_of_birth(&self) -> i32 {
        2023 - self.age
    }
}

#[derive(Debug)]
struct Persona<T, U> {
    name: T,
    addr: U,
}

// result
fn result_test(i: i32) -> Result<i32, String> {
    if i == 77 {
        return Ok(33);
    } else {
        Err(String::from("this is not the number"))
    }
}

#[derive(Debug)]
enum IpAddrKind {
    V4,
    V6,
}

#[derive(Debug)]
enum IpAddrKind2 {
    V4(String),
    V6(String),
}

#[derive(Debug)]
struct IpAddr {
    kind: IpAddrKind,
    address: String,
}

#[derive(Debug)]
struct IpAddr2 {
    kind: IpAddrKind2,
}

fn main() -> Result<(), String> {
    let i32_bag = Bag::<i32> { item: 10 };
    let bool_bag = Bag::<bool> { item: true };
    let float = Bag { item: 3.14 };

    println!("Turbofish: {i32_bag:?} {bool_bag:?} {float:?}");

    //-----------------------------------
    let opt_i32_bag = BagOption::<i32> { item: Some(10) };
    let opt_bool_bag = BagOption::<bool> { item: Some(true) };
    let opt_float = BagOption::<f32> { item: Some(3.14) };

    match opt_i32_bag.item {
        Some(v) => println!("found {} in bag!", v),
        None => println!("found nothing"),
    }

    // structs
    let st1 = Foo {
        x: Bar {
            item: String::from("item1"),
        },
    };
    let st2 = Foo {
        x: Bar {
            item: String::from("item2"),
        },
    };
    println!("{st1:#?} {st2:#?}");
    println!("{:#?} {:#?}", st1.x.item, st2.x.item);

    // method
    let juju = Person {
        name: String::from("julio"),
        age: 37,
    };
    println!("method: yob: {:#?}", juju.year_of_birth());

    let jiji = Persona {
        name: "Juju",
        addr: 123,
    };
    println!("persona: {:#?}", jiji);

    let home = IpAddr {
        kind: IpAddrKind::V4,
        address: String::from("127.0.0.0"),
    };

    println!("enum: {:#?}", home);
    let home2 = IpAddr2 {
        kind: IpAddrKind2::V4(String::from("127.0.0.0")),
    };
    println!("enum: {:#?}", home2);

    Ok(())
}
