#![allow(dead_code)]
#![allow(unused)]
#![allow(unused_variables)]

use std::collections::HashMap;

#[derive(Debug)]
struct Matrix(f32, f32, f32, f32);

use rand::Rng;

// result
fn result_test(i: i32) -> Result<i32, String> {
    if i == 77 {
        return Ok(33);
    } else {
        Err(String::from("this is not the number"))
    }
}

fn print_variable_type<K>(_: &K) -> String {
    format!("{}", std::any::type_name::<K>())
}

#[derive(Debug)]
enum Myvec {
    Text(String),
    Int(i32),
}

fn main() -> Result<(), String> {
    // vars
    let _logical: bool = true;
    let _an_integer = 5i32; // Suffix annotation
    let _floater = 1.0;

    let _x = 5 + /* 90 + */ 5;

    // containers
    let long_tuple = (
        1u8, 2u16, 3u32, 4u64, -1i8, -2i16, -3i32, -4i64, 0.1f32, 0.2f64, 'a', true,
    );

    // accessing tuple
    println!("Long tuple first value: {}", long_tuple.0);
    println!("Long tuple second value: {}", long_tuple.1);

    let tup = ("1", 11, 1.0);
    let (_a, _b, _c) = tup;

    // array
    let arr: [i32; 3] = [1, 2, 3];
    // array init with val
    let _arr2: [i32; 3] = [0; 3];

    println!("val1:{}, val2:{}", arr[0], arr[1]);
    // println!("val1:{:?}", arr );
    // vect
    let mut i32_vec = Vec::<i32>::new();
    i32_vec.push(1);

    let mut stringo = Vec::new();
    stringo.push("this");
    println!("vec: {}", print_variable_type(&stringo));

    let stringo_macro = vec![String::from("this"), String::from("that")];
    let v = vec!["x", "y"];

    println!(
        "vec: {i32_vec:?} {stringo:?} {stringo_macro:?} {v:?} {:?}",
        &v[0]
    );

    let mut multi_vec = vec![Myvec::Text(String::from("blue")), Myvec::Int(3)];
    println!("multi_vec {:?}", multi_vec);

    // collections
    let mut scores = HashMap::new();
    scores.insert(String::from("blue"), 10);
    scores.insert(String::from("yello"), 20);

    for (k, v) in &scores {
        println!("{k:?} {v:?}")
    }

    let text = "this is a large of a text, this is a large thing too";
    let mut map = HashMap::new();
    println!("hash: {}", print_variable_type(&map));

    for word in text.split_whitespace() {
        let count = map.entry(word).or_insert(0);
        *count += 1;
    }
    println!("hash freq: {:?}", map);

    // struct
    let matrix = Matrix(1.1, 1.2, 2.1, 2.2);
    println!("matrix: {:#?}", matrix);

    // functions
    let (x, y) = (10, 20);
    fn add(x: u32, y: u32) -> u32 {
        return x + y;
    }

    // function using result
    let x = "yes";
    fn printer(x: &str) -> Result<bool, String> {
        println!("this is: {}", x);
        if x == "yes" {
            Ok(true)
        } else {
            Err(String::from("this crap errored"))
        }
    }

    fn d() -> String {
        let s = String::from("hello");
        return s;
    }

    let q = d();
    printer("yes");
    println!("{}::{}", add(40, 20), q);

    // conditions
    let x = 7;
    if x > 9 {
        println!("this is x: {x}");
    } else if x < 10 && x > 6 {
        println!("this is x: {x}");
    } else {
        println!("this is x: {x}");
    }

    // loops
    let mut x = 10;

    loop {
        x += 1;
        if x == 13 {
            print!("break from loop\n");
            break;
        }
    }
    let mut x = 1;
    while x != 10 {
        x += 1;
    }
    println!("break while {x}");

    for i in 0..5 {
        println!("{}", i)
    }
    // loop VEC
    for i in stringo_macro.iter() {
        println!("vec for: {i}");
    }

    // switch | match
    let x = rand::thread_rng().gen_range(0..=10);

    match x {
        0 => {
            println!("match x is 0");
        }
        1 | 2 => {
            println!("match {}", x);
        }
        3..=7 => {
            println!("match {}", x);
        }
        matched @ 8..=10 => {
            println!("match {}", matched);
        }
        _ => {
            println!("default")
        }
    }

    let result = result_test(77)?;
    println!("result?: {:#?}", result);

    let re1 = result_test(77).unwrap();
    let re2 = match result_test(77)? {
        u32 => "yes",
        _ => "no",
    };
    // let re2 = result_test(7)?;
    println!("unwrap: re1:{:#?} re2:{}", re1, re2);

    let ree = match result_test(33) {
        Ok(v) => match v {
            x @ u32 => format!("this is u32 {:?}", x),
            n @ 33 => format!("this is u32 {:?}", n),
            y @ _ => String::from("not"),
        },
        Err(v) => format!("error: {}", v),
    };
    println!("ree: {:#?}", ree);

    let ree2 = match result_test(77).unwrap() {
        u32 => format!("this is u32"),
        _ => format!("not"),
    };
    println!("ree2: {:#?}", ree2);

    Ok(())
}
