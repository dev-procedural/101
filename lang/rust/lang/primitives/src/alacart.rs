#[derive(Debug)]
#[allow(dead_code)]

struct Matrix(f32, f32, f32, f32);

use rand::Rng;

fn main() {
    // vars
    let _logical: bool = true;
    let _an_integer = 5i32; // Suffix annotation
    let _floater = 1.0;

    let _x = 5 + /* 90 + */ 5;

    // containers
    let long_tuple = (
        1u8, 2u16, 3u32, 4u64, -1i8, -2i16, -3i32, -4i64, 0.1f32, 0.2f64, 'a', true,
    );

    // accessing tuple
    println!("Long tuple first value: {}", long_tuple.0);
    println!("Long tuple second value: {}", long_tuple.1);

    let tup = ("1", 11, 1.0);
    let (_a, _b, _c) = tup;

    // array
    let arr: [i32; 3] = [1, 2, 3];
    // array init with val
    let _arr2: [i32; 3] = [0; 3];

    println!("val1:{}, val2:{}", arr[0], arr[1]);
    // println!("val1:{:?}", arr );

    // struct
    let matrix = Matrix(1.1, 1.2, 2.1, 2.2);
    println!("{:?}", matrix);

    // functions
    fn add(x: u32, y: u32) -> u32 {
        return x + y;
    }

    fn printer(x: &str) {
        println!("this is: {}", x);
    }

    fn d() -> String {
        let s = String::from("hello");
        return s;
    }

    let q = d();
    printer("this too");
    println!("{}::{}", add(40, 20), q);

    // conditions
    let x = 7;
    if x > 9 {
        println!("this is x: {x}");
    } else if x < 10 && x > 6 {
        println!("this is x: {x}");
    } else {
        println!("this is x: {x}");
    }

    // loops
    let mut x = 10;

    loop {
        x += 1;
        if x == 13 {
            print!("break from loop\n");
            break;
        }
    }
    let mut x = 1;
    while x != 10 {
        x += 1;
    }
    println!("break while {x}");

    for i in 0..5 {
        println!("{}", i)
    }

    // switch | match
    let x = rand::thread_rng().gen_range(1..=10);

    match x {
        0 => {
            println!("x is 0");
        }
        1 | 2 => {
            println!("{}", x);
        }
        3..=7 => {
            println!("{}", x);
        }
        matched @ 8..=10 => {
            println!("{}", matched);
        }
        _ => {
            println!("default")
        }
    }
}
