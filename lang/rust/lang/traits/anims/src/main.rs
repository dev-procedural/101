#[derive(Debug, Clone)]
struct Dog {
    name: String,
    dead: bool,
    find: bool,
}

#[derive(Debug, Clone)]
struct Cow {
    name: String,
    find: bool,
    dead: bool,
}

trait Animal {
    fn new(name: &String) -> Self
    where
        Self: Sized;
    fn name(&self) -> String;
    fn noise(&self) -> String;
}

impl Dog {
    fn find(self) -> bool {
        self.find
    }
}

impl Animal for Dog {
    // default value
    fn new(name: &String) -> Dog {
        Dog {
            name: name.to_string(),
            dead: false,
            find: false,
        }
    }
    fn name(&self) -> String {
        self.clone().name
    }
    fn noise(&self) -> String {
        "woof".to_string()
    }
}

impl Cow {
    fn find(&self) -> bool {
        self.find
    }
}

impl Animal for Cow {
    // default value
    fn new(name: &String) -> Self {
        Cow {
            name: name.to_string(),
            dead: false,
            find: false,
        }
    }
    fn name(&self) -> String {
        self.clone().name
    }
    fn noise(&self) -> String {
        "mooo".to_string()
    }
}

// Returns some struct that implements Animal, but we don't know which one at compile time.
fn random_animal(random_number: f64) -> Box<dyn Animal> {
    if random_number < 0.5 {
        Box::new(Dog::new(&"a".to_string()))
    } else {
        Box::new(Cow::new(&"b".to_string()))
    }
}

fn main() {
    let GG: Dog = Dog::new(&"GG".to_string());
    let Vaquita: Cow = Cow::new(&"vaquita".to_string());
    let random_number = 0.3;
    let anim = random_animal(random_number);

    println!(
        "{:#?}, {:#?} \n{:#?}",
        GG.clone().noise(),
        GG.clone().name(),
        GG
    );

    println!(
        "{:#?}, {:#?} \n{:#?}",
        Vaquita.clone().noise(),
        Vaquita.clone().name(),
        Vaquita
    );

    println!("random: {:#?}", anim.noise());
}
