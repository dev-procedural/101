use crate::shapes::Tools;

mod shapes {
    use std::f64::consts;

    pub trait Tools {
        fn area(&self) -> f64;
        fn printer(&self) -> String;
    }

    pub struct Circle {
        pub x: f64,
        pub y: f64,
        pub radius: f64,
    }

    impl Tools for Circle {
        fn area(&self) -> f64 {
            consts::PI * (self.radius * self.radius)
        }
        fn printer(&self) -> String {
            "I am a circle".to_string()
        }
    }

    pub struct Square {
        pub x: f64,
        pub y: f64,
        pub side: f64,
    }

    impl Tools for Square {
        fn area(&self) -> f64 {
            self.side * self.side
        }
        fn printer(&self) -> String {
            "I am a square".to_string()
        }
    }
}

fn print_area<T: Tools>(shape: T) {
    println!(
        "This shape {}->{} has an area of {}",
        shape.printer(),
        std::any::type_name::<T>(),
        shape.area()
    );
}

fn main() {
    let c = shapes::Circle {
        x: 0.0f64,
        y: 0.0f64,
        radius: 1.0f64,
    };

    let s = shapes::Square {
        x: 0.0f64,
        y: 0.0f64,
        side: 3.0f64,
    };

    println!("area c: {}", c.area());
    println!("area s: {}", s.area());
    print_area(s);
    print_area(c);
}
