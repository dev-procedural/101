struct Owner(i32);

impl Owner {
    // Annotate lifetimes as in a standalone function.
    fn add_one<'a>(&'a mut self) {
        self.0 += 1;
    }
    fn print<'a>(&'a self) {
        println!("`print`: {}", self.0);
    }
}

struct Owner2(i32);

impl Owner2 {
    // Annotate lifetimes as in a standalone function.
    fn add_one(&mut self) {
        self.0 += 1;
    }
    fn print(&self) {
        println!("`print`: {}", self.0);
    }
}

fn main() {
    let mut owner = Owner(99);
    let mut owner2 = Owner2(22);

    owner.add_one();
    owner.print();

    owner2.add_one();
    owner2.print();
}
