use serde::{Deserialize, Serialize};
use lambda_runtime::{service_fn, Error, LambdaEvent};
use aws_sdk_eks::{Client as EKSClient};
use std::time::{SystemTime, Duration};
use aws_credential_types::provider::ProvideCredentials;
use aws_sigv4::{ http_request::{SignableBody, SignableRequest, SigningSettings}, sign::v4 };
use http;
//use aws_sdk_eksauth::{Client as EKSAUTHClient};
//use aws_smithy_runtime_api::client::identity::Identity;
//use urlencoding::encode;
#[allow(unused)]
#[allow(dead_code)]
use base64::{engine::general_purpose::STANDARD, Engine as _};
use aws_config::BehaviorVersion;

use k8s_openapi::api::core::v1::Pod;
use kube::{Client, Config, Api, api::ListParams};
use http::uri::Uri;
use secrecy::Secret;


#[allow(dead_code)]

#[allow(unused)]
#[derive(Deserialize, Debug)]
struct Request_ {
    msg: String,
}

#[allow(unused)]
#[derive(Serialize, Debug)]
struct Response {
    req_id: String,
    msg: String,
    req: String,
}

#[allow(unused)]
#[derive(Debug, Clone)]
struct ClusterCC {
	ca: String,
	ep: String,
	name: String,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::INFO)
        .with_target(false)
        .without_time()
        .init();

    let config = aws_config::load_defaults(BehaviorVersion::v2023_11_09()).await;
	let eks = EKSClient::new(&config);

	let cluster_name = "dev-platform".to_string();
	let namespace = "dashboard";

    lambda_runtime::run(service_fn(|event: LambdaEvent<Request_>| async {
        my_handler(&config, &eks, &cluster_name, namespace, event).await
    })).await?;

    Ok(())
}

pub(crate) async fn my_handler(config: &aws_config::SdkConfig, eks: &EKSClient, cluster_name: &String, namespace: &str, event: LambdaEvent<Request_>) -> Result<Response, Error> {
    // extract some useful info from the request
    // let command = event.payload.command;
	let pods = eks_get_pods(&config, &eks, cluster_name.clone(), namespace.clone()).await;
	
	Ok(Response{
		req_id: event.context.request_id,
		msg: format!("clusters: {cluster_name:#?}, namespace: {namespace:#?}, pods: {pods:#?}"),
        req: format!("{:#?}",event.payload),
	})
}


async fn eks_get_pods(config_: &aws_config::SdkConfig, eks: &EKSClient, cluster_name: String, namespace: &str) ->  Result<Vec<String>, Error> {

    let cluster_opts = eks_get_configs(&eks, &cluster_name.clone()).await.unwrap();
    let cluster_token = eks_get_token(config_, &cluster_name).await;

    ///////////////////
    let mut config = Config::new(cluster_opts.ep.parse::<Uri>().unwrap());
    let decoded = String::from_utf8(STANDARD.decode(cluster_opts.ca.to_string()).unwrap()).unwrap();
    let decoded = pem::parse_many(decoded)
        .unwrap()
        .into_iter()
        .filter_map(|p| {
            if p.tag() == "CERTIFICATE" {
                Some(p.into_contents())
            } else {
                None
            }
        })
        .collect::<Vec<_>>();

    config.root_cert = Some(decoded);
    config.auth_info.token = Some(Secret::new(cluster_token.unwrap()));

    let client =  Client::try_from(config).expect("failed to create kube client");

    let pods: Api<Pod> = Api::namespaced(client, namespace);
    let pods = pods.list(&ListParams::default())
                .await
                .unwrap()
                .items
                .into_iter()
                .map(|x| x.metadata.name.unwrap())
                .collect::<Vec<String>>();

    Ok(pods)
}

async fn eks_get_configs(eks: &EKSClient, cluster_name: &String) ->  Result<ClusterCC, Error> {
	//let config = aws_config::load_defaults(BehaviorVersion::v2023_11_09()).await;
	// let eks = EKSClient::new(&config);

	let result = &eks.describe_cluster()
			.name(&cluster_name.clone())
			.send()
			.await
			.expect("Error unpacking");
	
	let ep = &result.cluster().unwrap().endpoint;
	let ca = result.cluster().unwrap().certificate_authority.clone().unwrap().data;

	let cluster_opts = ClusterCC{
			ca: ca.unwrap().to_string(), 
			ep: ep.clone().unwrap().to_string(), 
			name: cluster_name.to_string(),
	};

	Ok(cluster_opts)
	
}

async fn eks_get_token(config: &aws_config::SdkConfig, cluster_name: &String) -> Result<String, Error> {
    //let config = aws_config::load_defaults(BehaviorVersion::v2023_11_09()).await;
	let credentials = config
		.credentials_provider()
		.expect("no credentials provider found")
		.provide_credentials()
		.await
		.expect("unable to load credentials");

	let identity = credentials.into();
	let region = config.region().unwrap().to_string();

	let mut signing_settings = SigningSettings::default();
	signing_settings.expires_in = Some(Duration::from_secs(900));
	signing_settings.signature_location = aws_sigv4::http_request::SignatureLocation::QueryParams;

	let signing_params = v4::SigningParams::builder()
		.identity(&identity)
		.region(&region)
		.name("sts")
		.time(SystemTime::now())
		.settings(signing_settings)
		.build()
		.unwrap();
	

	let url = format!("https://sts.{region}.amazonaws.com/?Action=GetCallerIdentity&Version=2011-06-15");
	let signable_request = SignableRequest::new("GET", url.clone(), [("x-k8s-aws-id", &cluster_name[..])].into_iter(), SignableBody::Bytes(&[]))
			.expect("signable request");

	let (signing_instructions, _signature) = aws_sigv4::http_request::sign(
		signable_request,
		&aws_sigv4::http_request::SigningParams::V4(signing_params)
	).expect("Error connecting").into_parts();

	let mut my_req = http::Request::builder().uri(url).body(()).unwrap();

	signing_instructions.apply_to_request_http0x(&mut my_req);
	let uri = my_req.uri().to_string().to_string(); //.split_off("https://".len());
	
	let token = format!("k8s-aws-v1.{}", STANDARD.encode(&uri));
	
	Ok(token)
}
	

// pub(crate) async fn my_handler(client: &EKSClient, event: LambdaEvent<Request_>) -> Result<Response, Error> {
//     // extract some useful info from the request
//     // let command = event.payload.command;
// 	let result = format!("{:#?}",client
//                 .list_clusters()
//                 .send()
//                 .await
//                 .unwrap()
//                 .clusters
//                 .unwrap()
//         );
	
// 	Ok(Response{
// 		req_id: event.context.request_id,
// 		msg: format!("clusters: {:#?}",result),
//         req: format!("{:#?}",event.payload),
// 	})
// }





#[cfg(test)]
mod tests {

    #[allow(unused)]
    #[allow(dead_code)]
    use crate::{my_handler, eks_get_configs, eks_get_token, eks_get_pods, Request_};
    use lambda_runtime::{Context, LambdaEvent};
    #[allow(unused)]
    #[allow(dead_code)]
    use aws_sdk_eks::{Client as EKSClient};
	use base64::{engine::general_purpose::STANDARD,  Engine as _};
	use aws_config::BehaviorVersion;
	// use k8s_openapi::api::core::v1::Pod;
	// use kube::{Client, Config, Api, api::ListParams, ResourceExt};
	// use http::uri::Uri;
	// use secrecy::Secret;
    // use std::process::Command;

    #[tokio::test]
    async fn k8s_get_pds() {
    	let config_ = aws_config::load_defaults(BehaviorVersion::v2023_11_09()).await;
		let eks = EKSClient::new(&config_);
		let cluster_name = "dev-platform".to_string();
		let namespace = "dashboard";

		let pods = eks_get_pods(&config_, &eks, cluster_name, namespace).await;

		println!("{pods:#?}")
	}


    #[tokio::test]
    async fn eks_configs() {
    	let config = aws_config::load_defaults(BehaviorVersion::v2023_11_09()).await;
        let eks = EKSClient::new(&config);
		let cluster_name = "dev-platform".to_string();

		let cluster_opts = eks_get_configs(&eks, &cluster_name).await;

		println!("{:#?}", cluster_opts.unwrap());
	}

    #[tokio::test]
    async fn eks_token() {

    	let config = aws_config::load_defaults(BehaviorVersion::v2023_11_09()).await;
		let cluster_name = "dev-platform".to_string();

		let cluster_token = eks_get_token(&config, &cluster_name).await;
		let cluster = cluster_token.unwrap().split_off("k8s-aws-v1.".len());

		let decoded = String::from_utf8(STANDARD.decode(&cluster).unwrap()).unwrap();
		println!("{:#?}\n{:#?}", cluster, decoded);

		assert!(decoded.contains("X-Amz-Credential"));
		
	}

    #[tokio::test]
    async fn lambda_handler() {

        let config = aws_config::load_defaults(BehaviorVersion::v2023_11_09()).await;
        let eks = EKSClient::new(&config);

        let id = "ID";
		let cluster_name = "dev-platform".to_string();
		let namespace = "dashboard";

        let mut context = Context::default();
        context.request_id = id.to_string();

        let payload = Request_{msg:"this is a payload".to_string()};
        let event = LambdaEvent { payload, context };

        // let result = my_handler(&eks, event).await.unwrap();
        let result = my_handler(&config, &eks, &cluster_name, namespace, event).await.unwrap();
		// let json = serde_json::to_string_pretty(&result).unwrap();

        println!("{:#?}", result);
        // assert_eq!(result.msg, "Command X executed.");
        assert_eq!(result.req_id, id.to_string());
        assert!(result.msg.contains("dev-platform"));
    }
}
