use lambda_runtime::{service_fn, Error, LambdaEvent};
use serde::{Deserialize, Serialize};
use aws_sdk_eks::{Client as EKSClient, types::NodegroupScalingConfig, config::BehaviorVersion};
// use aws_sdk_eks::operation::list_nodegroups::ListNodegroupsOutput;
// use aws_sdk_eks::operation::update_nodegroup_config::UpdateNodegroupConfigOutput;
// use std::collections::HashMap;


#[derive(Deserialize, Debug)]
struct Request {
    scale: String,
}

#[derive(Serialize, Debug)]
struct Response {
    req_id: String,
    msg: String,
    action: String,
}

#[derive(Debug, Clone)]
struct Conf {
    min: i32,
    max: i32,
    des: i32,
}

#[derive(Debug, Clone)]
struct NODEGROUP {
    name: String,
    status: String,
    config: Conf
}

enum Scale {
    Down,
    Up,
}

const TAG_CONFIG: &str = "COMPANY:ScalingConfig";

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::INFO)
        .with_target(false)
        .without_time()
        .init();

	let cluster_name = std::env::var("CLUSTER_NAME")
							.map_err(|_| {
								eprintln!("No variable: CLUSTER_NAME");
								std::process::exit(1)
							});

	let config = aws_config::load_defaults(BehaviorVersion::v2023_11_09()).await;
	let eks = EKSClient::new(&config);

    lambda_runtime::run(service_fn(|event: LambdaEvent<Request>| async {
        my_handler(&eks, cluster_name.clone().expect("Error ENV VAR"), event).await
    })).await?;

    Ok(())
}


async fn update_nodegroup_config(client: &EKSClient, mut nodegroup_list: Vec<NODEGROUP>, scale: Scale, cluster_name: String) -> Result<Vec<NODEGROUP>, Error> {

    for (i,n) in nodegroup_list.clone().iter().enumerate(){
        let config: Conf = match scale {
            Scale::Up => {
                Conf{
                    min: nodegroup_list[i].config.min,
                    max: nodegroup_list[i].config.max,
                    des: nodegroup_list[i].config.des,
                }
            }
            Scale::Down => {
                // must be 0
                Conf{
                    min: 4,
                    max: 5,
                    des: 5,
                }
            }
        };

        let cmd = client
            .update_nodegroup_config()
            .cluster_name(cluster_name.clone())
            .nodegroup_name(n.name.clone())
            .scaling_config(
                NodegroupScalingConfig::builder()
                .min_size(config.min)
                .max_size(config.max)
                .desired_size(config.des)
                .build()
                )
            .send()
            .await
            .unwrap();

        nodegroup_list[i].status = format!( "errors: {:?}, status: {:?}, update-conf: {:?}", 
                            cmd.update.clone().unwrap().errors.unwrap(), 
                            cmd.update.unwrap().status.unwrap(),
                            config)
    }

    Ok(nodegroup_list)

}

async fn get_nodegroup_config(client: &EKSClient, cluster_name: String) -> Result<Vec<NODEGROUP>, Error> {

    // let mut result: Vec<NODEGROUP> = vec![];
    let mut nodegroup_list: Vec<NODEGROUP> = vec![];

	let nodegroups = client
                .list_nodegroups()
                .cluster_name(cluster_name.clone())
                .send()
                .await
                .unwrap()
                .nodegroups
                .unwrap();

    for n in nodegroups.clone().iter(){
        let tags: Vec<Conf> = client
            .describe_nodegroup()
            .cluster_name(cluster_name.clone())
            .nodegroup_name(n)
            .send()
            .await
            .unwrap()
            .nodegroup
            .unwrap()
            .tags
            .unwrap()
            .into_iter()
            .filter(|(k,_)| *k == TAG_CONFIG.to_string() )
            .map(|(_,v)| {
                let v = v.split(":").into_iter().map(|s| s.to_string()).collect::<Vec<String>>();
                return Conf{
                    min: v[0].to_owned().split_off("min".len()).trim().parse::<i32>().unwrap(),
                    max: v[1].to_owned().split_off("max".len()).trim().parse::<i32>().unwrap(),
                    des: v[1].to_owned().split_off("des".len()).trim().parse::<i32>().unwrap(),
                }
            })
            .collect::<Vec<_>>();

        // println!("{:?}", tags);
        // let tags = tags.split(":").into_iter().map(|s| s.to_string()).collect::<Vec<String>>()

        nodegroup_list.push(NODEGROUP{
            name: n.to_string(), 
            status: "".to_string(), 
            config: tags[0].to_owned(),
        });
    }

    Ok(nodegroup_list)

}

pub(crate) async fn my_handler(client: &EKSClient, cluster_name: String, event: LambdaEvent<Request>) -> Result<Response, Error> {

    let scale = {
		if event.payload.scale.clone() == "up"{
			Scale::Up
		} else {
			Scale::Down
		}
    };

    let nodegroup_list = get_nodegroup_config(client, cluster_name.clone()).await.unwrap();
    let result = update_nodegroup_config(client, nodegroup_list, scale, cluster_name.clone()).await.unwrap();

	Ok(Response{
		req_id: event.context.request_id,
		msg: format!("nodegroups: {:?}",result),
		action: event.payload.scale.clone()
	})
}

#[cfg(test)]
mod tests {
    use crate::{my_handler, Request};
    use lambda_runtime::{Context, LambdaEvent};
	use aws_sdk_eks::{Client as EKSClient, config::BehaviorVersion};

    #[tokio::test]
    async fn response_is_good() {

		let cluster_name = std::env::var("CLUSTER_NAME")
								.map_err(|_| {
									eprintln!("No variable: CLUSTER_NAME");
									std::process::exit(1)
								});
		assert_eq!(cluster_name, std::env::var("CLUSTER_NAME"));

        let config = aws_config::load_defaults(BehaviorVersion::v2023_11_09()).await;

        let eks = EKSClient::new(&config);

        let id = "ID";

        let mut context = Context::default();
        context.request_id = id.to_string();

        let payload = Request{scale:"down".to_string()};
        let event = LambdaEvent { payload, context };

        let result = my_handler(&eks, cluster_name.clone().expect("ENV VAR ERR"), event).await.unwrap();
		// let json = serde_json::to_string_pretty(&result).unwrap();

        println!("{:?}", result);
        // assert_eq!(result.msg, "Command X executed.");
        assert_eq!(result.req_id, id.to_string());
    }
}
