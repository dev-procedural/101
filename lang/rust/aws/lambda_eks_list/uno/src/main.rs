#![allow(clippy::result_large_err)]

// use aws_config::BehaviorVersion;
use aws_sdk_eks::{Client as EKSClient, Error as EKSError};
use lambda_runtime::{service_fn, Error as LAMBDA_Error, LambdaEvent};

use std::fmt::Debug;
use serde::{Deserialize, Serialize};
use aws_types::sdk_config::SdkConfig;
// use anyhow::Error;
// use tokio_stream::StreamExt;


// #[derive(Debug, Deserialize,Clone)]
// struct Request {
//     body: String,
// }

#[derive(Debug, Serialize)]
struct Response {
    req_id: String,
    body: String,
}

// impl std::fmt::Display for Response {
//     /// Display the response struct as a JSON string
//     fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
//         let err_as_json = serde_json::json!(self).to_string();
//         write!(f, "{err_as_json}")
//     }
// }

// impl std::error::Error for Response {}


#[tracing::instrument(skip(client, event), fields(req_id = %event.context.request_id))]
async fn ekslist(
    client: &EKSClient,
    event: LambdaEvent<Request>
	) -> Result<Response, LAMBDA_Error> {
	
	let result = format!("{:#?}",client
                .list_clusters()
                .send()
                .await
                .unwrap()
                .clusters
                .unwrap()
        );

	
	Ok(Response{
		req_id: event.context.request_id,
		body: format!("clusters: {:#?}",result)
	})

    // match result {
    //     Ok(_) => {
    //         tracing::info!(
    //             result = %result,
    //             "eks clusters founds",
    //         );
    //         Ok(Response{
				// req_id: event.context.request_id,
				// body: format!("clusters: {:#?}",result)
			// })

    //     }
    //     Err(err) => {
    //         tracing::error!(
    //             err = %err,
    //             "filed to found eks clusters"
    //         );
    //         Err(Box::new(Response {
    //             req_id: event.context.request_id,
    //             body: "The Lambda function encountered an error and your data was not saved"
    //                 .to_owned(),
    //         }))

    //     }
    // }
}



#[tokio::main]
async fn main() -> Result<(), LAMBDA_Error> {
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::INFO)
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        .init();

	let config = aws_config::load_from_env().await;
    let eks = EKSClient::new(&config);
    
    lambda_runtime::run(service_fn(|event: LambdaEvent<Request>| async {
		print!("REQ:{:#?}", event.clone() );
        ekslist(&eks, event).await
    })).await?;
	Ok(())
}
