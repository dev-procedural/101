use lambda_runtime::{service_fn, Error, LambdaEvent};
use serde::{Deserialize, Serialize};
use aws_sdk_eks::{Client as EKSClient};

#[derive(Deserialize, Debug)]
struct Request {
    msg: String,
}

#[derive(Serialize, Debug)]
struct Response {
    req_id: String,
    msg: String,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::INFO)
        .with_target(false)
        .without_time()
        .init();

    // let func = service_fn(my_handler);
    // lambda_runtime::run(func).await?;
	let config = aws_config::load_from_env().await;
	let eks = EKSClient::new(&config);

    lambda_runtime::run(service_fn(|event: LambdaEvent<Request>| async {
        my_handler(&eks, event).await
    })).await?;

    Ok(())
}

pub(crate) async fn my_handler(client: &EKSClient, event: LambdaEvent<Request>) -> Result<Response, Error> {
    // extract some useful info from the request
    // let command = event.payload.command;
	let result = format!("{:#?}",client
                .list_clusters()
                .send()
                .await
                .unwrap()
                .clusters
                .unwrap()
        );
	
	Ok(Response{
		req_id: event.context.request_id,
		msg: format!("clusters: {:#?}",result)
	})
}

#[cfg(test)]
mod tests {
    use crate::{my_handler, Request};
    use lambda_runtime::{Context, LambdaEvent};
    use aws_sdk_eks::{Client as EKSClient};

    #[tokio::test]
    async fn response_is_good() {

        let config = aws_config::load_from_env().await;
        let eks = EKSClient::new(&config);

        let id = "ID";

        let mut context = Context::default();
        context.request_id = id.to_string();

        let payload = Request{msg:"".to_string()};
        let event = LambdaEvent { payload, context };

        let result = my_handler(&eks, event).await.unwrap();
		// let json = serde_json::to_string_pretty(&result).unwrap();

        println!("{:#?}", result);
        // assert_eq!(result.msg, "Command X executed.");
        assert_eq!(result.req_id, id.to_string());
    }
}
