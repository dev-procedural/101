use aws_config::meta::region::RegionProviderChain;
use aws_types::{sdk_config::SdkConfig, region::Region};
use std::env;
use anyhow::Error;

pub async fn get_config(region: Option<String>, profile: String) -> Result<SdkConfig, Error> {

    let region_provider = RegionProviderChain::first_try(region.map(Region::new))
        .or_default_provider()
        .or_else(Region::new("us-east-1"));

        match assume(profile, region_provider).await {
			Ok(v) => Ok(v),
			Err(e) => panic!("err: {e}")
    }
}

pub async fn assume(profile: String, region_provider: RegionProviderChain) -> Result<SdkConfig, Box<dyn std::error::Error>> {

    let config: SdkConfig ;

    match profile.as_str() {
        
        pp @ _str if pp == "" => match env::var("AWS_ACCESS_KEY_ID") {
                Ok(_) => {
                    config = aws_config::from_env().region(region_provider).load().await;
                    Ok(config)
                },
                Err(e) => {
                    panic!("Err: AWS {e}")
                }
        }
        p @ _str => {
            config = aws_config::from_env()
                .profile_name(p.to_string())
                .region(region_provider)
                .load()
                .await ;
            Ok(config)
        }
    }
} 
