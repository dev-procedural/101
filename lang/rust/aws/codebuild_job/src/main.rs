#![allow(clippy::result_large_err)]

use aws_config::meta::region::{RegionProviderChain};
use aws_sdk_codebuild::{Client as CBClient, 
						Error as CBError, 
						operation::start_build::StartBuildOutput,
						operation::stop_build::StopBuildError,
						operation::stop_build::StopBuildOutput,
						operation::list_builds_for_project::ListBuildsForProjectOutput ,
						operation::list_builds_for_project::ListBuildsForProjectError};
use aws_types::region::{Region};
use clap::Parser;
use std::fmt::Debug;
use serde::{Serialize};
use std::{thread, time::Duration};
use aws_types::sdk_config::SdkConfig;
use anyhow::Error;
use tokio_stream::StreamExt;
use std::process;


mod assume;

#[derive(Debug, Parser)]
struct Opt {
    /// The AWS Region.
    #[clap(short, long)]
    region: Option<String>,

    /// The AWS profile
    #[clap(short, long, default_value_t = String::from("devops_dev"))]
    profile: String,

    /// The AWS codebuild job name
    #[clap(short, long, default_value_t = String::from(""))]
    name: String,

    /// The AWS codebuild job branch to run from
    #[clap(short, long, default_value_t = String::from("master"))]
    branch: String,

    /// The AWS codebuild stop job
    #[clap(short, long, action)]
    stop: bool,

    /// The AWS codebuild build job id name:id (style)
    #[clap(short='i', long="id", default_value_t = String::from(""))]
    build_id: String,
}

#[derive(Debug, Serialize)]
struct Res{
    ip: String,
    id: String,
    name: String,
}

fn typex<K>(_: &K) -> String {
    format!("{}", std::any::type_name::<K>())
}

// start build
async fn start_build(client: &CBClient, 
                          name: String, 
                          branch: String
	) -> Result<StartBuildOutput, CBError> {
    let resp = client 
        .start_build()
        .project_name(name)
        .source_version(branch)
        .send()
        .await?;

    Ok(resp)
}

// stop build
async fn stop_build(client:&CBClient, 
					id: String
	) -> Result<StopBuildOutput, StopBuildError> {
	let result = client.stop_build()
		.id(id)
		.send()
		.await;
	Ok(result.unwrap())
}

// get build status
// async fn get_builds(client:&CBClient, project: String) -> Result<ListBuildsForProjectOutput, ListBuildsForProjectError> {
pub async fn get_build_status(client:&CBClient, project: String) -> Result<(), ListBuildsForProjectError> {

	// getting last build
	let builds = client
		.list_builds_for_project()
		.project_name(project.to_owned())
		.sort_order("DESCENDING".into())
		.send()
		.await
		.unwrap()
		.ids.expect("err:");

	let last_build = builds.first();
	
	let get_build = client
		.batch_get_builds()
		.ids(&last_build.expect("this").to_string())
		.send()
		.await
		.unwrap().builds.unwrap()[0]
		.build_status.clone().unwrap();

	println!("{}", format!("job:{:#?}\nstatus:{:#?}\n", last_build.unwrap(), get_build));

	Ok(())
}

// get build status
// async fn get_builds(client:&CBClient, project: String) -> Result<ListBuildsForProjectOutput, ListBuildsForProjectError> {
pub async fn get_builds(client:&CBClient, project: String) -> Result<(), ListBuildsForProjectError> {

	let mut builds: Vec<String> = Vec::new();
	let mut pages = client
			.list_builds_for_project()
			.project_name(project.to_owned())
			.sort_order("DESCENDING".into())
			.into_paginator()
			.items()
			.send();

	while let Ok(Some(next_page)) = pages.try_next().await {
		builds.push(next_page);
	}

	println!("{:#?}", builds);

	Ok(())

}

async fn get_config(region: Option<String>, profile: String) -> Result<SdkConfig, Error> {

    let region_provider = RegionProviderChain::first_try(region.map(Region::new))
        .or_default_provider()
        .or_else(Region::new("us-east-1"));

	match assume::assume(profile, region_provider).await {
        Ok(v) => Ok(v),
        Err(e) => panic!("err: {e}")
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    // tracing_subscriber::fmt::init();
    let Opt { region, profile, name, branch, stop, build_id} = Opt::parse();

	let config = get_config(region, profile).await.unwrap();

    let cb = CBClient::new(&config);

	match stop {
		false => {
			let resp = start_build(&cb, name, branch).await?;
			let build_id: String = resp.build_value
				.unwrap()
				.id
				.unwrap();

			println!("[?] started: {build_id}");
			Ok(())
		}
		true => { 
			println!("[?] waiting to stop {build_id}");
			thread::sleep(Duration::from_secs(3));

			let ss = stop_build(&cb, build_id);
			println!("[+] stopped: {:#?}", ss.await.unwrap().build_value.unwrap().id.unwrap());
			Ok(())
		}
			
	}
}


#[cfg(test)]
mod tests {
    use super::*;
	use std::{thread, time::Duration};

    #[tokio::test]
    async fn test_start_build() -> Result<(), Error> {
		let region = Some("us-east-1".to_string());
		
		let config = get_config(region, "devops_dev".to_string()).await.unwrap();

		let cb = CBClient::new(&config);
		
		let resp = start_build(&cb, "devops-codebuild".to_string(), "master".to_string()).await?;
		let build_id: String = resp.build_value
					.unwrap()
					.id
					.unwrap();
		
		println!("[?] starting {build_id}");
		thread::sleep(Duration::from_secs(3));

		println!("[+] stopping {build_id}");
		let ss = stop_build(&cb, build_id);

		println!("{:#?}", ss.await.unwrap().build_value.unwrap().id.unwrap());
		Ok(())
    }

    #[tokio::test]
    async fn test_get_build() -> Result<(), Error> {
		let region = Some("us-east-1".to_string());
		
		let config = get_config(region, "devops_dev".to_string()).await.unwrap();

		let cb = CBClient::new(&config);

		let builds = get_builds(&cb, "devops-codebuild".to_string()).await;
		let status = get_build_status(&cb, "devops-codebuild".to_string()).await;

		println!("{builds:#?}\n{status:#?}");
		Ok(())
	}
}
