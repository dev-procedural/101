#![allow(clippy::result_large_err)]

use aws_sdk_eks::{Client as EKSClient, Error as EKSError};
use clap::Parser;
use std::fmt::Debug;
// use serde::{Serialize};
use std::{thread, time::Duration};
use aws_types::sdk_config::SdkConfig;
use anyhow::Error;
use tokio_stream::StreamExt;
use std::process;


mod assume;

#[derive(Debug, Parser)]
struct Opt {
    /// The AWS Region.
    #[clap(short, long)]
    region: Option<String>,

    /// The AWS profile
    #[clap(short, long, default_value_t = String::from("devops_dev"))]
    profile: String,
}

// #[derive(Debug, Serialize)]
// struct Res{
//     ip: String,
//     id: String,
//     name: String,
// }

fn typex<K>(_: &K) -> String {
    format!("{}", std::any::type_name::<K>())
}



#[tokio::main]
async fn main() -> Result<(), Error> {
    // tracing_subscriber::fmt::init();
    let Opt { region, profile } = Opt::parse();

	let config = assume::get_config(region, profile).await.unwrap();

    let eks = EKSClient::new(&config);

    Ok(())
}


#[cfg(test)]
mod tests {
    use super::*;
	// use std::{thread, time::Duration};

    #[tokio::test]
    async fn eks_list() -> Result<(), Error> {
		let region = Some("us-east-1".to_string());
		
		let config = assume::get_config(region, "devops_dev".to_string()).await.unwrap();

		let eks = EKSClient::new(&config);
        
        println!("{:#?}",eks
                .list_clusters()
                .send()
                .await
                .unwrap()
                .clusters
                .unwrap()
        );
		
		Ok(())
    }
}
