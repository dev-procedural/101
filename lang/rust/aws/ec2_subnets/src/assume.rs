use aws_config::meta::region::RegionProviderChain;
use aws_types::sdk_config::{SdkConfig};
use std::env;

pub async fn assume(profile: String, region_provider: RegionProviderChain) -> Result<SdkConfig, Box<dyn std::error::Error>> {

    let config: SdkConfig ;

    match profile.as_str() {
        
        pp @ _str if pp == "" => match env::var("AWS_ACCESS_KEY_ID") {
                Ok(_) => {
                    config = aws_config::from_env().region(region_provider).load().await;
                    Ok(config)
                },
                Err(e) => {
                    panic!("Err: AWS {e}")
                }
        }
        p @ _str => {
            config = aws_config::from_env()
                .profile_name(p.to_string())
                .region(region_provider)
                .load()
                .await ;
            Ok(config)
        }
    }
} 
