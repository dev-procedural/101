#![allow(clippy::result_large_err)]

use aws_config::meta::region::{RegionProviderChain};
use aws_sdk_ec2::{Client as EC2Client, Error as EC2Error};
use aws_types::region::{Region};
use clap::Parser;
use std::fmt::Debug;
use serde::{Serialize};


mod assume;

#[derive(Debug, Parser)]
struct Opt {
    /// The AWS Region.
    #[clap(short, long)]
    region: Option<String>,

    /// The AWS profile
    #[clap(short, long, default_value_t = String::from("devops_dev"))]
    profile: String,

    /// The AWS subnet type public, private
    #[clap(short, long, default_value_t = String::from(""))]
    subtype: String,

}

#[derive(Debug, Serialize)]
struct Res{
    ip: String,
    id: String,
    name: String,
}

// Filter subnets
async fn get_priv_subnets(client: &EC2Client, subtype: String) -> Result<Vec<Res>, EC2Error> {

    let resp = client 
        .describe_subnets()
        .send()
        .await?;

    let res = resp.subnets
        .unwrap()
        .into_iter()
        .map(|t| Res{
                ip: t.cidr_block.unwrap(), 
                id: t.subnet_id.unwrap(),
                name: t.tags.unwrap()
                 .into_iter()
                 .filter(|tt| 
                         tt.key.clone().unwrap() == "Name")
                 .map(|s| s.value.unwrap())
                 .collect::<String>() 
        })
        .filter(|rr| rr.name.contains(&subtype))
        .collect::<Vec<Res>>();

    Ok(res)
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    // tracing_subscriber::fmt::init();
    let Opt { region, profile, subtype } = Opt::parse();

    let region_provider = RegionProviderChain::first_try(region.map(Region::new))
        .or_default_provider()
        .or_else(Region::new("us-east-1"));

	let config = match assume::assume(profile, region_provider).await {
        Ok(v) => v,
        Err(e) => panic!("err: {e}")
    };

    let ec2 = EC2Client::new(&config);
    let resp = get_priv_subnets(&ec2, subtype).await?;
    let json = serde_json::to_string_pretty(&resp).unwrap();
    
    println!("{json}");
    Ok(())
}
