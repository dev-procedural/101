use assert_cmd::prelude::*;
use predicates::prelude::*;
use std::process::Command;

#[test]
fn run_with_defaults() {
    Command::cargo_bin("sts_assume")
        .expect("binary exists")
        .args(&["-p", "devops_dev"])
        .assert()
        .success();
}

#[test]
fn run_with_defaults2() {
    Command::cargo_bin("sts_assume")
        .expect("binary exists")
        .args(&["-p", "devops_dev"])
        .assert()
        .success()
        .stdout(predicate::str::contains("Success"));
}

#[test]
fn fail_on_non_existing_profile() -> Result<(), Box<dyn std::error::Error>> {
    Command::cargo_bin("sts_assume")
        .expect("binary exists")
        .args(&["-p", "devops"])
        .assert()
        .stderr(predicate::str::contains("could not be built"));
    Ok(())
}

#[test]
fn existing_profile() {
    Command::cargo_bin("sts_assume")
        .expect("aws profiles are working")
        .args(&["-p", "devops_dev"])
        .assert()
        .stdout(predicate::str::contains("Success"));
}
