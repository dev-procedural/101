use aws_types::sdk_config::{SdkConfig};
use aws_config::meta::region::RegionProviderChain;
use aws_config::BehaviorVersion;
use std::env;

pub async fn assume(profile: String, region: RegionProviderChain ) -> Result<SdkConfig, Box<dyn std::error::Error>> {

    let config: SdkConfig ;

    match profile.as_str() {
        
        pp @ _str if pp == "" => match env::var("AWS_ACCESS_KEY_ID") {
                Ok(_) => {
                    config = aws_config::from_env().region(region).load().await;
                    Ok(config)
                },
                Err(e) => {
                    panic!("Err: AWS {e}")
                }
        }
        p @ _str => {
            config = aws_config::from_env()
                .profile_name(p.to_string())
                .region(region)
                .load()
                .await ;
            Ok(config)
        }
    }
} 
