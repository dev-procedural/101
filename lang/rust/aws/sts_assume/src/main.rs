#![allow(clippy::result_large_err)]

use aws_config::meta::region::RegionProviderChain;
use aws_sdk_sts::{config::Region, meta::PKG_VERSION, Client, Error as STSError};
use clap::Parser;
use std::fmt::Debug;

mod assume;

#[derive(Debug, Parser)]
struct Opt {
    /// The AWS Region.
    #[clap(short, long)]
    region: Option<String>,

    /// The AWS profile
    #[clap(short, long, default_value_t = String::from(""))]
    profile: String,

    /// Whether to display additional information.
    #[clap(short, long)]
    verbose: bool,
}

// Displays the STS AssumeRole Arn.
async fn get_caller_identity(client: &Client) -> Result<(), STSError> {
    let response = client.get_caller_identity().send().await?;

    println!(
        "Success! AccountId = {}",
        response.account().unwrap_or_default()
    );
    println!(
        "Success! AccountArn = {}",
        response.arn().unwrap_or_default()
    );
    println!(
        "Success! UserID = {}",
        response.user_id().unwrap_or_default()
    );

    Ok(())
}

/// Displays information about the Amazon API Gateway REST APIs in the Region.
///
/// # Arguments
///
/// * `[-r REGION]` - The Region in which the client is created.
///   If not supplied, uses the value of the **AWS_REGION** environment variable.
///   If the environment variable is not set, defaults to **us-west-2**.
/// * `[-v]` - Whether to display information.
#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    // tracing_subscriber::fmt::init();
    let Opt { region, verbose, profile } = Opt::parse();

    let region_provider = RegionProviderChain::first_try(region.map(Region::new))
        .or_default_provider()
        .or_else(Region::new("us-east-1"));
    println!("");

    if verbose {
        println!("STS client version: {}", PKG_VERSION);
        println!(
            "Region:   {}",
            region_provider.region().await.unwrap().as_ref()
        );
        println!();
    }

	let config = match assume::assume(profile, region_provider).await {
        Ok(v) => v,
        Err(e) => panic!("err: {e}")
    };

    let sts = Client::new(&config);

    Ok(get_caller_identity(&sts).await?)
}
