#![allow(clippy::result_large_err)]

use aws_sdk_ec2::{Client as EC2Client, Error as EC2Error,
	types::{Instance, Filter},
};
use clap::Parser;
use std::fmt::Debug;
use serde::{Serialize};
use anyhow::Error;
use tokio_stream::StreamExt;
// use tokio::task;
extern crate chrono;

use chrono::{Utc, Duration};
use chrono::NaiveDateTime;

use std::fs::File;
use std::io::Write;

mod assume;

#[derive(Debug, Parser)]
struct Opt {
    /// The AWS Region.
    #[clap(short, long, default_value_t = String::from("us-east-1") )]
    region: String,

    /// The AWS profile
    #[clap(short, long, default_value_t)]
    profile: String,

    /// The Time in days
    #[clap(short, long, default_value_t = 60)]
    days: i64,
    
    /// The Filepath of the result
    #[clap(short, long, default_value_t = String::from("/tmp/ec2_report.json") )]
    filepath: String,
}

#[derive(Debug, Serialize, Clone, PartialEq)]
struct ResultInstance{
    ami: String,
	date: String,
    name: String,
    due: bool,
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    // tracing_subscriber::fmt::init();
    let Opt { region, profile, days, filepath } = Opt::parse();

	let config = assume::get_config(Some(region), profile).await.unwrap();

    let ec2 = EC2Client::new(&config);
    let resp = get_ec2_instances(&ec2, days).await?;
    let json = serde_json::to_string_pretty(&resp).unwrap();
    
	match write_json_to_file(&filepath, json.clone()) {
		Ok(_) => {
			println!("{json}");
			println!("Successfully wrote to the file: {}.", filepath);
			Ok(())
		},
		Err(e) => panic!("Error occurred: {}", e),
	}
}


fn calc_due(date_str: &str, days: i64) -> Result<bool, chrono::format::ParseError> {
    let date = NaiveDateTime::parse_from_str(date_str, "%Y-%m-%dT%H-%M-%SZ")?;
    let now = Utc::now().naive_utc();
    let duration = now.signed_duration_since(date);
    Ok(duration > Duration::days(days.into()))
}

fn write_json_to_file(filename: &str, data: String) -> Result<(), Error> {
    let mut file = File::create(filename)?;
    file.write_all(data.as_bytes())?;
    Ok(())
}


// Get ami
async fn get_ami_date(client: &EC2Client, ami: String) -> Result<String, EC2Error> {

    let ami = client 
        .describe_images()
		.filters(Filter::builder().name("image-id").values(ami).build())
        .send()
		.await?
		.images
		.unwrap()
		.into_iter()
		.map(|s| s.creation_date.unwrap())
		.collect::<String>();

	Ok(ami)
}


// Get from launch template
async fn get_launch_amis(ec2: &EC2Client) -> Result<Vec<ResultInstance>, Error> {

	let mut result: Vec<_> = ec2.describe_launch_templates()
		.send()
		.await
		.unwrap()
		.launch_templates
		.unwrap()
		.into_iter()
		.map(|s| s.launch_template_id.unwrap())
		.collect();

	result.sort();
	result.dedup();

	let mut n: Vec<_> = vec![];
	for lt in result.clone().into_iter(){

		n.push(ec2.describe_launch_template_versions()
				.launch_template_id(lt.clone())
				.send()
				.await
				.unwrap()
				.launch_template_versions
				.unwrap()
				.into_iter()
				.filter(|x| x.default_version.unwrap())
				.into_iter()
				.map(|s| 
					 ResultInstance{
						 ami:  s.launch_template_data
									 .clone()
									 .unwrap()
									 .image_id
									 .unwrap_or("".to_string()),
						 name: s.launch_template_name.unwrap(),
						 date: "".to_string(),
						 due: false
					 }
				)
				.collect::<Vec<_>>()
		);
	}

	let mut n = n.into_iter().flatten().collect::<Vec<_>>();

    // this remove dups
	let mut i = 0;
	while i < n.len() {
		let mut j = i + 1;
		while j < n.len() {
			if n[i] == n[j] {
				n.remove(j);
			} else {
				j += 1;
			}
		}
		i += 1;
	}
	
	Ok(n)
}

//  Get instances
async fn get_ec2_instances(client: &EC2Client, days: i64) -> Result<Vec<ResultInstance>, EC2Error> {

	let instances_lt = get_launch_amis(client).await.unwrap();

	let mut instances: Vec<Vec<Instance>> = Vec::new();
    let mut pages = client 
        .describe_instances()
		.into_paginator()
		.items()
        .send();

	while let Ok(Some(next_page)) = pages.try_next().await {
		instances.push(next_page.instances.unwrap())
	}

	let mut result = instances
		.into_iter()
		.flatten()
		.map(|s| ResultInstance{
			ami: s.image_id.clone().unwrap(),
			name: s.tags.unwrap()
				.clone()
				.into_iter()
				.filter(|s| s.key.clone().unwrap()== "Name")
				.map(|s| s.value.unwrap())
				.collect(),
			date: "".to_string(), // get_ami(client, s.image_id.expect("err").to_string()).await.unwrap()
			due: false
			},
		)
		.collect::<Vec<_>>();

	// Adding LT images
	for i in instances_lt.iter() {
		result.push(i.clone())
	}
	
    // Adding image and due date checker
	for (n,i) in result.clone().iter().enumerate() {
		let ami = i.clone();

        let ami_name = get_ami_date(client, ami.ami.to_string()).await.unwrap();
		result[n].ami_name = ami_name.clone();

        let ami_name_owned = ami_name.clone();
        let date = ami_name_owned
                .split("--")
                .collect::<Vec<_>>()
                .to_owned();

        if date.len().to_owned() > 1 {
            result[n].due = match calc_due(&date[1].to_string(), days).unwrap() {
                true => true,
                _ => false,
            };
        }
	}

	Ok(result)
}




#[cfg(test)]
mod tests {
    use super::*;
	use std::collections::HashSet;
    // use std::{thread, time::Duration};
    //

    #[tokio::test]
    async fn get_instance_struct() -> Result<(), Error> {
		let region = Some("us-east-1".to_string());
		let config = assume::get_config(region, "devops_dev".to_string()).await.unwrap();
		let ec2 = EC2Client::new(&config);

		let resp = get_ec2_instances(&ec2);
		println!("{:#?}", resp.await?);

		Ok(())
    }

    #[tokio::test]
    async fn get_ami_detail() -> Result<(), Error> {
		let region = Some("us-east-1".to_string());
		let config = assume::get_config(region, "devops_dev".to_string()).await.unwrap();
		let ec2 = EC2Client::new(&config);
		let resp = get_ami_date(&ec2, "ami-073c794557381f17c".to_string());

		println!("{:#?}", resp.await?);

		Ok(())
    }


    #[tokio::test]
    async fn get_launch_amis() -> Result<(), Error> {
		let region = Some("us-east-1".to_string());
		let config = assume::get_config(region, "devops_dev".to_string()).await.unwrap();
		let ec2 = EC2Client::new(&config);
		// let resp = get_ami(&ec2, "ami-073c794557381f17c".to_string());
        let mut result: Vec<_> = ec2.describe_launch_templates()
            .send()
            .await
            .unwrap()
            .launch_templates
            .unwrap()
            .into_iter()
            .map(|s| s.launch_template_id.unwrap())
            .collect();

        result.sort();
        result.dedup();

        let mut n: Vec<_> = vec![];
        for lt in result.clone().into_iter(){

			n.push(ec2.describe_launch_template_versions()
					.launch_template_id(lt.clone())
					.send()
					.await
					.unwrap()
					.launch_template_versions
					.unwrap()
					.into_iter()
					.filter(|x| x.default_version.unwrap())
					// .inspect(|x| println!("{x:#?}"))
					.into_iter()
					.map(|s| 
						 ResultInstance{
							 ami:  s.launch_template_data
										 .clone()
										 .unwrap()
										 .image_id
										 .unwrap_or("".to_string()),
							 name: s.launch_template_data
										 .clone()
										 .unwrap()
										 .tag_specifications.or(Some(vec![]))
										 // .inspect(|x| println!("{x:#?}"))
									     .map_or("None".to_string(),|x| {
												x.into_iter()
												.map(|y| format!("{:?}", y.tags
													.unwrap()
													.into_iter()
													.filter(|w| w.key.clone().unwrap() == "Name")
													.map(|z| z.value.unwrap())
													.collect::<Vec<_>>()
													// .into_iter()
													// .inspect(|x| println!("{x:#?}"))
													)
												).collect::<String>()
												 // .into_iter().collect::<HashSet<_>>().into_iter().collect::<Vec<_>>()
												
										 }),

							 ami_name: "".to_string(),
							 due: false,
						 }
					)
					.collect::<Vec<_>>()
			);
		}

		println!("{:#?}", n);

        Ok(())

    }

}
