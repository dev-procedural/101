use anyhow::Error;
use rayon::prelude::*;
// use reqwest::{blocking::Client, redirect};
// use std::{env, time::Duration};

pub mod models;
pub mod ports;
pub mod subdomains;
use models::Subdomain;

pub fn main() -> Result<(), Error> {
    let target = "noahkellman.com";
    let pool = rayon::ThreadPoolBuilder::new()
        .num_threads(256)
        .build()
        .unwrap();

    pool.install(|| {
        let scan_result: Vec<Subdomain> = subdomains::scan(target)
            .unwrap()
            .into_iter()
            .map(|s| ports::scan_ports(s))
            .map(|s| s.unwrap())
            .collect();
        println!("{scan_result:#?}");
    });

    Ok(())
}
