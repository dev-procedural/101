use crate::models::{ResultCert, Subdomain};
use anyhow::Error;
use reqwest::blocking::Client;
use std::{collections::HashSet, time::Duration};

use trust_dns_resolver::{
    config::{ResolverConfig, ResolverOpts},
    Resolver,
};

pub fn scan(target: &str) -> Result<Vec<Subdomain>, Error> {
    let client = Client::builder().build();
    let res: Vec<ResultCert> = client?
        .get(&format!("https://crt.sh/?q={}&output=json", target))
        .send()?
        .json()
        .unwrap();

    let subs: HashSet<String> = res
        .into_iter()
        .map(|s| s.name_value.clone().to_string())
        .map(|s| s.split("\n").map(str::to_owned).collect::<Vec<_>>())
        .flat_map(|s| s)
        .filter(|s| s != target)
        .filter(|s| !s.contains('*'))
        .collect();

    let mut subdomains: Vec<String> = subs.into_iter().collect::<Vec<_>>();
    subdomains.sort();

    let subdomains: Vec<Subdomain> = subdomains
        .into_iter()
        .map(|domain| Subdomain {
            domain,
            open_ports: Vec::new(),
        })
        .filter(resolves)
        .collect();
    Ok(subdomains)
}

pub fn resolves(domain: &Subdomain) -> bool {
    let mut opts = ResolverOpts::default();
    opts.timeout = Duration::from_secs(4);

    let dns_resolver =
        Resolver::new(ResolverConfig::default(), opts).expect("resolver: DNS client");
    dns_resolver.lookup_ip(domain.domain.as_str()).is_ok()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_subdomains() -> Result<(), Error> {
        let result = scan("noahkellman.com");
        println!("{result:#?}");
        Ok(())
    }
}
