use rayon::prelude::*;
use std::net::SocketAddr;
use std::net::ToSocketAddrs;
use std::{net::TcpStream, time::Duration};

#[derive(Debug, Clone)]
pub struct Port {
    pub port: u16,
    pub is_open: bool,
}

#[derive(Debug, Clone)]
pub struct Subdomain {
    pub domain: String,
    pub open_ports: Vec<Port>,
}

fn main() {
    let s = Subdomain {
        domain: "localhost".to_string(),
        open_ports: vec![],
    };
    let pp = scan_ports(s);
    println!("{pp:#?}");
}

fn scan_ports(mut subdomain: Subdomain) -> Subdomain {
    let addr: Vec<SocketAddr> = format!("{}:1024", subdomain.domain)
        .to_socket_addrs()
        .expect("Err")
        .collect();

    subdomain.open_ports = vec![8080, 8081]
        .into_par_iter()
        .map(|port| scan_port(addr[0], port))
        .collect();
    subdomain
}

fn scan_port(mut socket_address: SocketAddr, port: u16) -> Port {
    let timeout = Duration::from_secs(3);
    socket_address.set_port(port);

    let is_open = TcpStream::connect_timeout(&socket_address, timeout).is_ok();

    Port { port, is_open }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_scan_port() {
        let addr: Vec<SocketAddr> = format!("localhost:1024")
            .to_socket_addrs()
            .expect("Err")
            .collect();
        let pp = scan_port(addr[0], 8080);
        println!("{pp:#?}");
    }

    #[test]
    fn test_scan_ports() {
        let s = Subdomain {
            domain: "localhost".to_string(),
            open_ports: vec![],
        };
        let pp = scan_ports(s);
        println!("{pp:#?}");
    }
}
