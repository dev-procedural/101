use std::collections::HashMap;
use anyhow::Error;
use reqwest::{blocking::Client};
use serde::{Deserialize};
use serde_json::{json, Value};

#[derive(Deserialize, Debug)]
struct Ip {
    origin: String
}

fn main() -> Result<(), Error> {
    let client = Client::builder()
        .build()?;

    let res = client
        .get("https://httpbin.org/get")
        .send()?
        .json::<Value>();

    // let a = json!(ip["headers"]);

    println!("a:{:#?}", res);
    Ok(())
}
