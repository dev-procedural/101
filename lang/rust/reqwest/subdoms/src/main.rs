use anyhow::Error;
use reqwest::Client;
use serde::Deserialize;
// use serde_json::{Number, Value};
use std::collections::HashSet;

// #[serde(rename_all = "camelCase")]
#[derive(Debug, Clone, Deserialize)]
pub struct Res {
    name_value: String,
}

pub type Resp = Vec<Res>;

#[tokio::main]
async fn main() -> Result<(), Error> {
    let res = subdomains("skipthedishes.ca").await.unwrap();

    println!("{:#?}", res);
    Ok(())
}

async fn subdomains(target: &str) -> Result<Vec<String>, Error> {
    let client = Client::new();
    let res: HashSet<String> = client
        .get(&format!("https://crt.sh/?q={}&output=json", target))
        .send()
        .await
        .expect("error in client")
        .json::<Resp>()
        .await
        .unwrap()
        .into_iter()
        .map(|s| s.name_value.to_string())
        .map(|s| s.split("\n").map(str::to_owned).collect::<Vec<_>>())
        .flat_map(|s| s)
        .filter(|s| s != target)
        .filter(|s| !s.contains('*'))
        .collect();

    let mut list: Vec<String> = res.into_iter().collect::<Vec<_>>();
    list.sort();
    Ok(list)
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    fn typex<K>(_: &K) -> String {
        format!("{}", std::any::type_name::<K>())
    }

    #[test]
    fn test_main() -> Result<(), Error> {
        let _ = main();
        Ok(())
    }

    #[tokio::test]
    async fn test_subdomains() -> Result<(), Error> {
        let res = subdomains("skipthedishes.ca").await.unwrap();
        println!(">>> {}", typex(&res));
        match res {
            x if x.len() > 0 => println!("{x:#?}"),
            _vec => println!("test "),
        }
        Ok(())
    }
}
