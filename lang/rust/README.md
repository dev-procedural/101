## goodies from books

rustup self update
rustup update

### clippy
rustup component add clippy
cargo clippy

### update
cargo update
cargo install -f cargo-outdated
cargo outdated

### audit
cargo install -f cargo-audit
cargo audit


