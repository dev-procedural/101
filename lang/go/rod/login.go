package main

import (
	"fmt"

	"github.com/go-rod/rod"
)

func main() {

	page := rod.New().NoDefaultDevice().MustConnect().MustPage("https://rosacruziniciatica.org/wp-login.php")

	USERNAME := "MY_USER"
	PASS := "MY_PASS"

	page.MustElement(`#user_login`).MustInput(USERNAME)
	page.MustElement(`#user_pass`).MustInput(PASS)
	page.MustElement(`#wp-submit`).MustClick()

	page.Race().ElementR("a", "Cerrar").MustHandle(func(e *rod.Element) {

		fmt.Println(e.MustAttribute("href"))
		fmt.Println(e.MustResource())

	}).Element("#login_error").MustHandle(func(e *rod.Element) {

		panic(e.MustText())

	}).MustDo()

	// time.Sleep(99 * time.Second)
}
