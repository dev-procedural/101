package main

import (
	"time"

	"github.com/go-rod/rod"
	"github.com/go-rod/rod/lib/utils"
)

func main() {

	page := rod.New().NoDefaultDevice().MustConnect().MustPage("https://www.wikipedia.org/")

	page.MustElement(`#js-link-box-es`).MustClick()
	page.MustWaitLoad().MustScreenshot("a.png")

	el := page.MustElement(`#main-tfa > div.thumb.tright > div > a > img`)

	_ = utils.OutputFile("b.png", el.MustResource())

	time.Sleep(time.Hour)
}
