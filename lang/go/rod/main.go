package main

import (
	"fmt"

	"github.com/go-rod/rod"
)

func main() {
	// l := launcher.New()

	// For more info: https://pkg.go.dev/github.com/go-rod/rod/lib/launcher
	// u := l.MustLaunch()

	browser := rod.New().MustConnect()

	page := browser.MustPage("https://translate.google.com/?sl=auto&tl=es&op=translate")

	el := page.MustElement(`textarea[aria-label="Source text"]`)

	wait := page.MustWaitRequestIdle("https://accounts.google.com")
	el.MustInput("hello")

	wait()

	result := page.MustElement("[role=region] span[lang]").MustText()

	fmt.Printf("%#v \n", result)
}
