package main

import (
	"fmt"
	"os"
	"testing"

	"github.com/go-rod/rod"
)

const page = "https://passwordreset.microsoftonline.com"

func TestLogin(t *testing.T) {

	g := setup(t)
	p := g.page(page)

	fmt.Println("[!] getting logged in")
	USERNAME := os.Getenv("GH_USER")
	PASS := os.Getenv("GH_PASSWORD")

	fmt.Println("[!] getting captcha")
	img := p.MustElement(`img[id*='RepMapVisual']`)
	_ = os.WriteFile("/tmp/captcha.png", img.MustResource(), 0777)

	p.MustElement(`input[title*='email']`).MustInput(USERNAME)
	p.MustElement(`#password`).MustInput(PASS)
	p.MustElement(`input[name|="commit"]`).MustClick()

	fmt.Println("[!] waiting to get logged in")
	p.Race().ElementR("span", "user").MustHandle(func(e *rod.Element) {

		fmt.Println("[!] logged in")
		fmt.Println(e.MustAttribute("href"))
		fmt.Println(e.MustResource())
		return

	}).ElementR("div", "Invalid LDAP login credentials").MustHandle(func(e *rod.Element) {

		fmt.Println(e.MustText())
		return

	}).MustDo()

	// time.Sleep(99 * time.Second)
}
