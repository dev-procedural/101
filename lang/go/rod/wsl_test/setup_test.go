package main

import (
	"fmt"
	"testing"

	"github.com/go-rod/rod"
	"github.com/go-rod/rod/lib/launcher"
	"github.com/ysmood/got"
)

// test context.
type G struct {
	got.G

	browser *rod.Browser
}

// setup for tests.
var setup = func() func(t *testing.T) G {

	userDataDir := "/tmp/rodo"
	u := launcher.New().
		UserDataDir(userDataDir).
		Bin("/mnt/c/Program Files/Google/Chrome/Application/chrome.exe").
		MustLaunch()

	fmt.Println("[!] getting launcher running")
	browser := rod.New().
		ControlURL(u).
		NoDefaultDevice().
		MustConnect()

	return func(t *testing.T) G {
		t.Parallel() // run each test concurrently

		return G{got.New(t), browser}
	}
}()

// a helper function to create an incognito page.
func (g G) page(url string) *rod.Page {
	page := g.browser.MustIncognito().MustPage(url)
	g.Cleanup(page.MustClose)
	return page
}
