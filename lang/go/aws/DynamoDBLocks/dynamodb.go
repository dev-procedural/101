package main

import (
	"context"
	"flag"
	"log"
	"os"

	// "sort"
	// "errors"
	"module/conn"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb/types"
)

var (
	MASTER_     string
	REGION_     string
	cfg         aws.Config
	err         error
	TABLE       string
	PROJECT     string
	ENV         string
	VALS        string
	DRY_MSG     string
	DRYRUN      bool
	FULL_DELETE bool
)

func init() {
	// isbrown := flag.Bool("bf", false, "is brownfield")
	flag.BoolVar(&DRYRUN, "dryrun", false, "dryrun just print out matched values, this also serve as a list")
	key := flag.String("k", os.Getenv("AWS_ACCESS_KEY_ID"), "key, default env AWS_ACCESS_KEY_ID")
	secret := flag.String("s", os.Getenv("AWS_SECRET_ACCESS_KEY"), "secret, default env AWS_SECRET_ACCESS_KEY")
	token := flag.String("t", os.Getenv("AWS_SESSION_TOKEN"), "token, default env AWS_SESSION_TOKEN")
	profile := flag.String("profile", os.Getenv("AWS_PROFILE"), "Aws profile aka AWS_PROFILE")
	region := flag.String("r", "us-east-1", "region")
	flag.StringVar(&TABLE, "table", "terraform-locks", "table")
	flag.StringVar(&PROJECT, "p", "", "project or target lock")
	flag.StringVar(&ENV, "e", "", "environment target dev/qa/uat/prod")
	flag.Parse()

	sname := "GoClientDynamodb"

	creds := conn.Creds{
		AccessKeyId:     *key,
		SecretAccessKey: *secret,
		SessionToken:    *token,
		Profile:         *profile,
	}

	cfg, err = conn.ASSUME(*region, sname, creds)
	if err != nil {
		log.Fatalf("[!] ERR: %#v", err.Error())
	}

	DRY_MSG = map[bool]string{true: "DRYRUN ACTIVATED", false: ""}[DRYRUN]
}

func main() {
	db_client := dynamodb.NewFromConfig(cfg)

	resp, err := db_client.Scan(
		context.TODO(),
		&dynamodb.ScanInput{
			TableName: &TABLE,
			ExpressionAttributeValues: map[string]types.AttributeValue{
				":val": &types.AttributeValueMemberS{Value: PROJECT},
			},
			FilterExpression: aws.String("contains(LockID, :val)"),
		},
	)
	if err != nil {
		log.Fatal(err)
	}

	TARGET := []string{}
	for i := range resp.Items {
		switch v := resp.Items[i]["LockID"].(type) {
		case *types.AttributeValueMemberS:
			TARGET = append(TARGET, v.Value)
		}
	}

	if len(TARGET) > 0 {
		log.Printf("[+] Deleting entries %s", DRY_MSG)
		for _, VALUE := range TARGET {
			switch {
			case !DRYRUN && len(TARGET) <= 2:
				log.Println("[!]", VALUE)
				_, err := db_client.DeleteItem(
					context.TODO(),
					&dynamodb.DeleteItemInput{
						TableName: &TABLE,
						Key: map[string]types.AttributeValue{
							"LockID": &types.AttributeValueMemberS{Value: VALUE},
						},
					},
				)
				// log.Printf("[RESP] %#v", resp)
				if err != nil {
					log.Println(err)
				}

			case !DRYRUN && len(TARGET) >= 2 && FULL_DELETE:
				// TODO add comma separated bulk mode
				log.Printf("[!] reserved for full delete")

			case !DRYRUN && len(TARGET) >= 2:
				// TODO add comma separated bulk mode
				log.Printf("[!] You are deleting more than 2, this is not allowed")

			case DRYRUN:
				log.Println("[!]", VALUE)
				//log.Printf("[!] Dryrun running")

			}
		}
	}
}
