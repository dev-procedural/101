package main

import (
  "context"
	// "flag"
  "fmt"
  "os"
  "errors"
  // "io"
  // "encoding/json"
	// "log"
  "github.com/aws/aws-sdk-go-v2/config"
  "github.com/aws/aws-sdk-go-v2/service/sts"
  "github.com/aws/aws-sdk-go-v2/credentials"
  "github.com/aws/aws-sdk-go-v2/aws"
  "github.com/aws/aws-sdk-go-v2/service/ec2"
)

func ASSUME(cfg aws.Config, Account, Role, Region string) (aws.Config, error) {
	
  client := sts.NewFromConfig(cfg)
	// fmt.Printf("[!] %#v \n", client )

  ROLE:="arn:aws:sts::"+ Account +":role/" + Role
  SESS:="ASSUMED-GO"
  input := &sts.AssumeRoleInput{
		RoleArn:         &ROLE,
 		RoleSessionName: &SESS,
	}

  result, err := client.AssumeRole(context.TODO(), input)
  if err != nil {
		fmt.Printf("Got an error assuming the role: %s", err)
		return aws.Config{}, errors.New(err.Error())
  }

  creds := *result.Credentials
	// fmt.Printf("[!] %#v", cfg)

  cfgSub, err := config.LoadDefaultConfig(context.TODO(),
    config.WithRegion(Region),
    config.WithCredentialsProvider(credentials.StaticCredentialsProvider{
        Value: aws.Credentials{
            AccessKeyID:     *creds.AccessKeyId,
            SecretAccessKey: *creds.SecretAccessKey,
            SessionToken:    *creds.SessionToken,
            Source:          "assumerole",
        },
    }))

	if err != nil {
	  fmt.Println("error:", err)
	  return aws.Config{}, errors.New(err.Error())
	}

  return cfgSub, nil
}

func main(){

  cfg, _:= config.LoadDefaultConfig(context.TODO(),
    config.WithRegion("us-east-1"),
    config.WithSharedConfigProfile("master"),
  )
 
  cfgSub, err  := ASSUME(cfg, "YOURACCOUNT", "AWSControlTowerExecution", "us-east-1")

  ec2sub := ec2.NewFromConfig(cfgSub)
  stssub := sts.NewFromConfig(cfgSub)

   GCI, err  := stssub.GetCallerIdentity(
        context.TODO(), 
        &sts.GetCallerIdentityInput{},
  )
  if err != nil {
     fmt.Println("error:", err)
     os.Exit(1)
   }

   Instances, err := ec2sub.DescribeInstances(
        context.TODO(),
        &ec2.DescribeInstancesInput{},
  )
  if err != nil {
     fmt.Println("error:", err)
     os.Exit(1)
  }

	// fmt.Printf("\n[+] %#v \n", cfgSub)
  fmt.Printf("\n[+] %#v \n", aws.ToString(GCI.Arn))
  
  fmt.Printf("\n[+] %#v \n", len(Instances.Reservations))
  for i, num := range Instances.Reservations {
     fmt.Printf("\n[%#v] %#v ", i, aws.ToString(num.Instances[0].InstanceId))
  }
	// fmt.Printf("\n[+] %#v \n", aws.ToString(Instances.Reservations[].Instances[0].InstanceId))
}
