package main

import (
	"context"
	"fmt"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/s3"
)

type MyEvent struct {
	BucketName string `json:"bucket"`
}

type MyResponse struct {
	Message string `json:"message"`
}

func HandleRequest(ctx context.Context, event MyEvent) (MyResponse, error) {
	cfg, err := config.LoadDefaultConfig(ctx)
	if err != nil {
		panic("configuration error, " + err.Error())
	}

	client := s3.NewFromConfig(cfg)

	params := &s3.ListObjectsV2Input{
		Bucket: &event.BucketName,
	}

	output, err := client.ListObjectsV2(ctx, params)
	if err != nil {
		return MyResponse{Message: fmt.Sprintf("Could not list objects for bucket %s, %v", event.BucketName, err)}, err
	}

	var dirs []string
	for _, item := range output.Contents {
		if *item.Size == 0 {
			dirs = append(dirs, *item.Key)
		}
	}

	return MyResponse{Message: fmt.Sprintf("Directories: %v", dirs)}, nil
}

func main() {
	lambda.Start(HandleRequest)
}
