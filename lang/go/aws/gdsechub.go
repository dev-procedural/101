package main

import (
	"context"
	"flag"
	"fmt"
  // "os"
  "sort"
  "errors"
  // "io"
  // "encoding/json"
	// "log"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/sts"
  "github.com/aws/aws-sdk-go-v2/credentials"
	"github.com/aws/aws-sdk-go-v2/aws"
  // "github.com/aws/aws-sdk-go-v2/service/ec2"
  "github.com/aws/aws-sdk-go-v2/service/organizations"
  // "github.com/aws/aws-sdk-go-v2/service/organizations/types"
  "github.com/go-gota/gota/dataframe"
  // "github.com/go-gota/gota/series"
  "github.com/aws/aws-sdk-go-v2/service/securityhub"
  "github.com/aws/aws-sdk-go-v2/service/guardduty"
)


var cfg aws.Config
var (
  MASTER_  string
  ROLE_    string
  REGION_  string
  ACCOUNT_ string
)

func init() {
  isbrown := flag.Bool("bf", false, "is brownfield" )
  subacc := flag.String("a","", "account name" )
  flag.Parse()

  // fmt.Printf("[!] %#v", *subacc)

  ACCOUNT_ = *subacc
  REGION_ = "us-east-1"

  if *isbrown {
    MASTER_ = "master_brown"
    ROLE_   = "OrganizationAccountAccessRole"
  } else {
    MASTER_ = "master"
    ROLE_   = "AWSControlTowerExecution"
  }

  cfg, _ = config.LoadDefaultConfig(context.TODO(),
    config.WithRegion(REGION_),
    config.WithSharedConfigProfile(MASTER_),
  )
}

func ASSUME(cfg aws.Config, Account, Role, Region string) (aws.Config, error) {
	
  client := sts.NewFromConfig(cfg)
	// fmt.Printf("[!] %#v \n", client )

	ROLE:="arn:aws:sts::"+ Account +":role/" + Role
  SESS:="ASSUMED-GO"
  input := &sts.AssumeRoleInput{
		RoleArn:         &ROLE,
 		RoleSessionName: &SESS,
	}

	result, err := client.AssumeRole(context.TODO(), input)
  if err != nil {
		fmt.Printf("Got an error assuming the role: %s", err)
		return aws.Config{}, errors.New(err.Error())
	}

  creds := *result.Credentials
	// fmt.Printf("[!] %#v", cfg)

  cfgSub, err := config.LoadDefaultConfig(context.TODO(),
    config.WithRegion(Region),
    config.WithCredentialsProvider(credentials.StaticCredentialsProvider{
        Value: aws.Credentials{
            AccessKeyID:     *creds.AccessKeyId,
            SecretAccessKey: *creds.SecretAccessKey,
            SessionToken:    *creds.SessionToken,
            Source:          "assumerole",
        },
    }))

	if err != nil {
			fmt.Println("error:", err)
			return aws.Config{}, errors.New(err.Error())
	}

  return cfgSub, nil
}

type Account struct {
  Name string
  ID string
}

func SUBACCOUNTS() []Account {
  
  accounts := []Account{}

  orgs := organizations.NewFromConfig(cfg)
  response, _ := orgs.ListAccounts(context.TODO(), &organizations.ListAccountsInput{})
  for _, v := range response.Accounts{
    accounts = append(accounts, Account{ Name: *v.Name, ID: *v.Id })
  }
  TOKEN:=response.NextToken 

  for {
    response, err := orgs.ListAccounts(context.TODO(), 
            &organizations.ListAccountsInput{
              NextToken : TOKEN,
            },
    ) 
    if err != nil  {
      fmt.Printf("%#v", err.Error())
      break
    }

    for _, v := range response.Accounts {
      // fmt.Printf("\n%#v", v)
      if v.Status == "ACTIVE" {
        accounts = append(accounts, Account{ *v.Name, *v.Id} )
      }
    }

    TOKEN = response.NextToken
    if aws.ToString(response.NextToken) == "" {
      break
    } 

  }

  sort.SliceStable(accounts, func(i,j int) bool {
    return accounts[i].Name < accounts[j].Name
  })

  return accounts
}

func SECURITYHUB(subaccount string) (*securityhub.DisableSecurityHubOutput, error) {
  cfgSub, err  := ASSUME(cfg, subaccount, ROLE_, REGION_)
  SEC := securityhub.NewFromConfig(cfgSub)
  
  result, err := SEC.DisableSecurityHub(
      context.TODO(), 
      &securityhub.DisableSecurityHubInput{},
  )
  if err != nil {
    // fmt.Printf("\n[?] %s: %#v", subaccount,  err.Error())
    return nil, err
  }

  return result,nil

}

func GUARDDUTY(subaccount string) ( string, error) {
  cfgSub, err  := ASSUME(cfg, subaccount, ROLE_, REGION_)
  if err != nil {
    return "", err
  }
  GD := guardduty.NewFromConfig(cfgSub)
  DETECTORS, _ := GD.ListDetectors(
    context.TODO(),
    &guardduty.ListDetectorsInput{},
  )

  fmt.Printf("\n%#v\n", len(DETECTORS.DetectorIds))

  if len(DETECTORS.DetectorIds) > 0 {
    _, err := GD.DeleteDetector(
      context.TODO(),
      &guardduty.DeleteDetectorInput{ 
        DetectorId : &DETECTORS.DetectorIds[0],
      },
    )
    if err != nil {
      return "",err
    }
    return fmt.Sprintf("Deleted %v in acc: %v", DETECTORS.DetectorIds[0], subaccount ),nil
  }

  return "No detector found", nil
}

func main() {

  ACCOUNTS := SUBACCOUNTS()
  df := dataframe.LoadStructs(ACCOUNTS)

  ACCS := df.Col("ID").Records()
  fmt.Printf("%#+v", ACCS[2:3])
  for _,v := range ACCS {
    fmt.Printf("\n%#v", v)
  }
  
  ACC := ACCS[0:1][0]

  res, err := SECURITYHUB(ACC)
  if err != nil {
    fmt.Printf("[?] ERROR: %s: %#v", ACC, err.Error())
  } else {
    fmt.Printf("[!] %#+v", res)
  }

  res1, err1 := GUARDDUTY(ACC)
  if err1 != nil {
    fmt.Printf("[?] ERROR: %s: %#v", ACC, err1.Error())
  } else {
    fmt.Printf("[!] %#+v", res1)
  }
 
}
