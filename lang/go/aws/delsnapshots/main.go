package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"strings"

	. "main/conn"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/backup"
	"github.com/go-gota/gota/dataframe"
)

var cfg aws.Config

var (
	MASTER_    string
	ROLE_      string
	REGION_    string
	VAULT_NAME string
	VAULT_TYPE string
	MAX_RES    int32
	DELETE_    bool
)

var ACCOUNT_ interface{}

func init() {
	isbrown := flag.Bool("bf", false, "is brownfield")
	subacc := flag.String("a", "YOURACCOUNT", "account name")
	flag.StringVar(&REGION_, "r", "us-east-1", "region")
	flag.StringVar(&VAULT_NAME, "v", "YOURVAULTNAME", "vault name")
	flag.StringVar(&VAULT_TYPE, "t", "EBS", "vault type EBS,EFS... ")
	flag.BoolVar(&DELETE_, "del", false, "Delte entries")
	MAX_RES_ := flag.Int("max", 3, "max requests")
	MAX_RES = int32(*MAX_RES_)
	flag.Parse()

	if *isbrown {
		MASTER_ = "master_brown"
		ROLE_ = "OrganizationAccountAccessRole"
	} else {
		MASTER_ = "master"
		ROLE_ = "AWSControlTowerExecution"
	}

	cfg, _ = config.LoadDefaultConfig(context.TODO(),
		config.WithRegion("us-east-1"),
		config.WithSharedConfigProfile(MASTER_),
	)

	// 1 or Many Accounts or ALL o f them
	switch {
	case strings.Contains(*subacc, ","):
		ACCOUNT_ = strings.Split(strings.Trim(*subacc, " "), ",")
	case *subacc == "":
		ACCOUNT_ = SUBACCOUNTS(cfg)
	default:
		ACCOUNT_ = []string{*subacc}
	}

}

func BACKUP_DELETE(Subaccount, Region string) (bool, error) {

	cfgSub, err := ASSUME(cfg, Subaccount, ROLE_, Region)
	if err != nil {
		fmt.Println("[!] ", err.Error())
	}
	BKP := backup.NewFromConfig(cfgSub)

	// ADD VALIDATION
	resp, err := BKP.ListRecoveryPointsByBackupVault(
		context.Background(),
		&backup.ListRecoveryPointsByBackupVaultInput{
			BackupVaultName: &VAULT_NAME,
			ByResourceType:  &VAULT_TYPE,
		},
	)
	if err != nil {
		fmt.Printf("%#v", err.Error())
	}

	RESULT := []string{}
	for _, v := range resp.RecoveryPoints {
		// fmt.Printf("%#v", *v.RecoveryPointArn)
		RESULT = append(RESULT, *v.RecoveryPointArn)
	}

	// fmt.Printf("[!] %#v",resp.Volumes)

	TOKEN := resp.NextToken
	if TOKEN != nil {
		for {
			resp, err := BKP.ListRecoveryPointsByBackupVault(
				context.TODO(),
				&backup.ListRecoveryPointsByBackupVaultInput{
					BackupVaultName: &VAULT_NAME,
					ByResourceType:  &VAULT_TYPE,
					NextToken:       TOKEN,
				},
			)
			if err != nil {
				fmt.Printf("%#v", err.Error())
				break
			}

			for _, v := range resp.RecoveryPoints {
				RESULT = append(RESULT, *v.RecoveryPointArn)
			}

			TOKEN = resp.NextToken
			if aws.ToString(resp.NextToken) == "" {
				break
			}

		}
	}

	if len(RESULT) == 0 {
		fmt.Println("[!] No matches found", len(RESULT), Subaccount, Region)
		os.Exit(3)
	}

	fmt.Println(len(RESULT))
	// df := dataframe.LoadStructs(VOLUMES)
	if DELETE_ {
		for _, i := range RESULT[:3] {
			fmt.Println(Subaccount, Region, i)
			_, ERR := BKP.DeleteRecoveryPoint(
				context.TODO(),
				&backup.DeleteRecoveryPointInput{
					BackupVaultName:  &VAULT_NAME,
					RecoveryPointArn: &i,
				},
			)
			if ERR != nil {
				return false, ERR
			}
		}
	}

	return true, nil

}

func main() {
	// REGIONS := REGIONS()
	var ACCS interface{}

	switch ACCOUNT_.(type) {
	case []Account:
		df := dataframe.LoadStructs(ACCOUNT_)
		ACCS = df.Col("ID").Records()
	case []string:
		ACCS = ACCOUNT_
	}

	// BACKUP_DF := [][]string{}

	// RUN IN ALL ACCOUNTS
	for _, ACC := range ACCS.([]string) {
		_, err := BACKUP_DELETE(ACC, REGION_)
		if err != nil {
			fmt.Println("[?]", err.Error())
		}
		// BACKUP_DF = append(BACKUP_DF, DF)
	}

	// PARSE RESULT
	// for _, DD := range EBS_DF {
	// 	for _, j := range DD {
	// 		EBS_ALL = append(EBS_ALL, j)
	// 	}
	// }

	// fmt.Printf("%#v\n", BACKUP_DF[0][:2])
	// file, _ := os.Create("/tmp/ebsreport.csv")
	// defer file.Close()

	// result := dataframe.LoadStructs(EBS_ALL)
	// result.WriteCSV(file)

	// fmt.Println("Please check result report /tmp/ebsreport.csv")
}
