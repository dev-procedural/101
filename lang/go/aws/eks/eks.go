package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"main/conn"
	"os"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/eks"
)

var (
	MASTER_ string
	REGION_ string
	cfg     aws.Config
	err     error
	DRYRUN  bool
)

func init() {
	// isbrown := flag.Bool("bf", false, "is brownfield")
	flag.BoolVar(&DRYRUN, "dryrun", false, "dryrun just print out matched values, this also serve as a list")
	key := flag.String("k", os.Getenv("AWS_ACCESS_KEY_ID"), "key, default env AWS_ACCESS_KEY_ID")
	secret := flag.String("s", os.Getenv("AWS_SECRET_ACCESS_KEY"), "secret, default env AWS_SECRET_ACCESS_KEY")
	token := flag.String("t", os.Getenv("AWS_SESSION_TOKEN"), "token, default env AWS_SESSION_TOKEN")
	profile := flag.String("profile", os.Getenv("AWS_PROFILE"), "Aws profile aka AWS_PROFILE")
	region := flag.String("r", "us-east-1", "region")
	flag.Parse()

	sname := "GoClientEks"

	creds := conn.Creds{
		AccessKeyId:     *key,
		SecretAccessKey: *secret,
		SessionToken:    *token,
		Profile:         *profile,
	}

	cfg, err = conn.ASSUME(*region, sname, creds)
	if err != nil {
		log.Fatalf("[!] ERR: %#v", err.Error())
	}

	// DRY_MSG = map[bool]string{true: "DRYRUN ACTIVATED", false: ""}[DRYRUN]
}

func EksList() {

	eks_client := eks.NewFromConfig(cfg)
	clusters, _ := eks_client.ListClusters(
		context.TODO(),
		&eks.ListClustersInput{},
	)

	fmt.Printf("%#v", clusters.Clusters)
}

func main() {
	EksList()
}
