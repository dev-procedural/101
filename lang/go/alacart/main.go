package main

import (
	"fmt"
	"path/filepath"
	"regexp"
	"bufio"
	"net"
	//"os"
)


func filter(files []string) {
	// a := []string
	for _,v := range files {
		// fmt.Println(k,v)
		if r,_ := regexp.MatchString(`.*.pdf`, v); r {
			fmt.Println(v)
		}
	}
}

func ping(pings chan<- string, msg string) {
    pings <- msg
}

func pong(pings <-chan string, pongs chan<- string) {
    msg := <-pings
    pongs <- msg
}

func pingo() {
	conn, _ := net.Dial("tcp", "google.com:80")
	reader := bufio.NewReader(conn)
	str, _:= reader.ReadString('\n')
	fmt.Println(str)
}

func main() {
	files,_ := filepath.Glob("/tmp/*")
	fmt.Printf("%T\n",files)

	// filter(files)
	// go filter(files)

	// go func() {
	// 	filter(files)
	// }()

	// Routine sync channel
	ch := make(chan bool, 1)	
	go func() {
		// filter(files)
		fmt.Println("End of routine")
		ch <- true
	}()

	fmt.Println("This run before go routine completion")
	<- ch
	fmt.Println("This run after go routine completion")

	// Direction
	pings := make(chan string, 1)
	pongs := make(chan string, 1)
	ping(pings, "passed message")
	pong(pings, pongs)
	fmt.Println(<-pongs)

	// pingo()
	fmt.Println("TTT")
	conn, _ := net.Dial("tcp", "google.com:80")
	reader  := bufio.NewReader(conn)
	str, _  := reader.ReadString('\n')
	fmt.Println(str)
}
