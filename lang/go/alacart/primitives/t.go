package main

import "fmt"

type sample struct {
    data interface{}
}

func (s *sample) func1() {
    obj := map[string]interface{}{}
    obj["foo1"] = "bar1"
    s.data = obj
}

func (s *sample) addField(key, value string) {
    v, _ := s.data.(map[string]interface{})
    v[key] = value
    s.data = v
}

func main() {
    s := &sample{}
    s.func1()
    fmt.Println(s)

    s.addField("foo2", "bar2")
    fmt.Println(s)

}

