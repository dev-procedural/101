module nats

go 1.16

require (
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/nats-io/nats-server/v2 v2.8.1 // indirect
	github.com/nats-io/nats.go v1.14.0
	google.golang.org/protobuf v1.28.0 // indirect
)
