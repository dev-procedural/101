package main

import (
    "github.com/nats-io/nats.go"
    "fmt"
)

func main() {

    fmt.Println(nats.DefaultURL)
    nc, err := nats.Connect(nats.DefaultURL)

    fmt.Println("[!] here 1")

    if err != nil {
        fmt.Println("Error")
    }

    fmt.Println("[!] here 1")

    nc.Publish("foo", []byte("Hello World"))

    // Simple Async Subscriber
    nc.Subscribe("foo", func(m *nats.Msg) {
        a := string(m.Data)
        fmt.Printf("Received a message: %s\n", string(m.Data))
    })

    fmt.Println(a)
    fmt.Println("[!] here")

    // // // Responding to a request message
    // nc.Subscribe("request", func(m *nats.Msg) {
    //     m.Respond([]byte("answer is 42"))
    // })

    // // Simple Sync Subscriber
    // sub, err := nc.SubscribeSync("foo")
    // m, err   := sub.NextMsg(timeout)

    // // Channel Subscriber
    // ch       := make(chan *nats.Msg, 64)
    // sub, err := nc.ChanSubscribe("foo", ch)
    // msg      := <- ch

    // // Unsubscribe
    // // sub.Unsubscribe()

    // // // Drain
    // // sub.Drain()

    // // Requests
    // msg, err := nc.Request("help", []byte("help me"), 10*time.Millisecond)

    // // Replies
    // nc.Subscribe("help", func(m *nats.Msg) {
    //     nc.Publish(m.Reply, []byte("I can help!"))
    // })

}
