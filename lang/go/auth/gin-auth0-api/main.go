package main

import (
	"log"
	"net/http"
	"net/url"
	"os"
	"time"

	jwtmiddleware "github.com/auth0/go-jwt-middleware/v2"
	"github.com/auth0/go-jwt-middleware/v2/jwks"
	"github.com/auth0/go-jwt-middleware/v2/validator"

	"github.com/gin-gonic/gin"
	adapter "github.com/gwatts/gin-adapter"
	"github.com/joho/godotenv"
)

type Product struct {
	ID    int     `json:"id"`
	Title string  `json:"title"`
	Code  string  `json:"code"`
	Price float32 `json:"price"`
}

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	r := gin.Default()

	issuerURL, _ := url.Parse(os.Getenv("AUTH0_ISSUER_URL"))
	// audience := os.Getenv("AUTH0_AUDIENCE")

	provider := jwks.NewCachingProvider(issuerURL, time.Duration(5*time.Minute))

	jwtValidator, _ := validator.New(provider.KeyFunc,
		validator.RS256,
		issuerURL.String(),
		[]string{},
	)

	jwtMiddleware := jwtmiddleware.New(jwtValidator.ValidateToken)

	jj := r.Group("/v1")
	{
		jj.Use(adapter.Wrap(jwtMiddleware.CheckJWT))
		jj.GET("/products", func(c *gin.Context) {
			products := []Product{
				{ID: 1, Title: "Product 1", Code: "p1", Price: 100.0},
				{ID: 2, Title: "Product 2", Code: "p2", Price: 200.0},
				{ID: 3, Title: "Product 3", Code: "p3", Price: 300.0},
			}
			c.JSON(http.StatusOK, products)
		})
	}

	r.GET("/login", func(c *gin.Context) {
		c.Redirect(http.StatusMovedPermanently, "https://anjanamx.us.auth0.com/authorize?response_type=id_token%20token&response_mode=form_post&client_id=F2baPGWPRm0HMrr7dqz0gOxCF2OWu49v&redirect_uri=http://localhost:5005/callback&nonce=NONCE")
	})

	r.POST("/callback", func(c *gin.Context) {
		c.Redirect(http.StatusMovedPermanently, "/endpoint")
	})

	r.GET("/endpoint", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"msg": "all good"})
	})

	// Listen and Server in 0.0.0.0:5000
	r.Run(":5005")
}
