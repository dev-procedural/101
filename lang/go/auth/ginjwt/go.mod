module auth1

go 1.16

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.7.7
	github.com/gorilla/context v1.1.1
	github.com/gorilla/sessions v1.2.1
)
