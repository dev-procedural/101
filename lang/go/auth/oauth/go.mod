module gitauth

go 1.16

require (
	github.com/boj/redistore v0.0.0-20180917114910-cd5dcc76aeff // indirect
	github.com/gin-gonic/contrib v0.0.0-20201101042839-6a891bf89f19 // indirect
	github.com/gin-gonic/gin v1.7.7
	github.com/golang/glog v1.0.0 // indirect
	github.com/google/go-github v17.0.0+incompatible // indirect
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/gorilla/sessions v1.2.1 // indirect
	github.com/zalando/gin-oauth2 v1.5.3
	golang.org/x/oauth2 v0.0.0-20211104180415-d3ed0bb246c8 // indirect
)
