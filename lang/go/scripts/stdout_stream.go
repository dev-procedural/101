package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"sync"
)

var wg sync.WaitGroup
var s bool

func init() {
	flag.BoolVar(&s, "s", false, "this")
	flag.Parse()
}

func main() {
	cli := "ping -c3 google.com"

	switch {
	case s == true:
		log.Println("[!]", s)
		log.Println("this")
		this(cli)
	case s == false:
		log.Println("[!]", s)
		log.Println("that")
		that(cli)
	}
}

func that(cli string) {
	cmd := exec.Command("bash", "-c", cli)

	var stdoutBuf, stderrBuf bytes.Buffer
	cmd.Stdout = io.MultiWriter(os.Stdout, &stdoutBuf)
	cmd.Stderr = io.MultiWriter(os.Stderr, &stderrBuf)

	err := cmd.Run()
	if err != nil {
		log.Fatalf("cmd.Run() failed with %s\n", err)
	}
	outStr, errStr := string(stdoutBuf.Bytes()), string(stderrBuf.Bytes())
	fmt.Printf("\nout:\n%s\nerr:\n%s\n", outStr, errStr)
}

func this(cli string) {
	cmd := exec.Command("bash", "-c", cli)

	c := make(chan struct{})
	wg.Add(1)
	go func(cmd *exec.Cmd, c chan struct{}) {
		defer wg.Done()
		stdout, err := cmd.StdoutPipe()
		if err != nil {
			panic(err)
		}
		<-c
		scanner := bufio.NewScanner(stdout)
		for scanner.Scan() {
			m := scanner.Text()
			fmt.Println(m)
		}
	}(cmd, c)

	c <- struct{}{}
	cmd.Start()

	wg.Wait()
}
