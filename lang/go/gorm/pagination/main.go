package main

import (
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	//"time"
)

type Org struct {
	// gorm.Model
	Name   string `gorm:"primarykey"`
	Events []Event
}

type Event struct {
	gorm.Model
	Name string `gorm:"primarykey"`
	// Orgs []Org        `gorm:"primarykey;many2many:org_events"`
	Subs Subscription `gorm:"ommitempty"`
}

type Subscription struct {
	gorm.Model
	User []string
}

// SEPARATOR
type User struct {
	gorm.Model
	// CompanyID   int
	Name        string       `gorm:"unique"`
	Companies   []Company    `gorm:"constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
	CreditCards []CreditCard `gorm:"constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
}

type CreditCard struct {
	gorm.Model
	Number string
	UserID int
}

type Company struct {
	gorm.Model
	UserID int
	Name   string
}

func main() {

	db, err := gorm.Open(sqlite.Open("/tmp/gorm.db"), &gorm.Config{})

	if err != nil {
		panic("Failed to connect to database!")
	}

	// db.AutoMigrate(&Org{}, &Event{}, &Subscription{})
	db.AutoMigrate(&User{}, &CreditCard{}, &Company{})

	// org := Org{Name: "ORG2"}
	// orgev := Event{Name: "EVENT2"}
	// orgev1 := Event{Name: "EVENT3"}
	// orgevsubs := Event{
	// 	Name: "EVENT2",
	// 	Subs: Subscription{
	// 		User: []string{"User1", "User2"},
	// 	},
	// }
	// // orgevsubs := Subscription{Name: "USER1", Events: []Events{{Name: "EVENT2"}, {Name: "EVENT3"}}}
	// // // fmt.Printf("%#v", orgevsubs)
	// company := Company{ID: 1, Name: "IBM"}
	// company2 := Company{ID: 2, Name: "Google"}

	user := User{
		// CompanyID: 1,
		Name: "Chie",
		CreditCards: []CreditCard{
			CreditCard{Number: "123123"},
			CreditCard{Number: "223323"},
		},
		Companies: []Company{
			Company{Name: "IBM"},
			Company{Name: "Google"},
		},
	}

	user2 := User{
		Name: "Choi",
		// CompanyID: 1,
		CreditCards: []CreditCard{
			CreditCard{Number: "333333"},
			CreditCard{Number: "444444"},
		},
		Companies: []Company{
			Company{Name: "Oracle"},
			Company{Name: "Google"},
		},
	}

	// db.Create(&company)
	// db.Create(&company2)
	db.Create(&user)
	db.Create(&user2)

	// db.Create(&org)
	// db.Create(&orgev)
	// db.Create(&orgev1)
	// db.Create(&orgevsubs)
}
