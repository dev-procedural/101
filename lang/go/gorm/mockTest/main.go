package daba

import (
	"gorm.io/gorm"
	//"time"
)

type Org struct {
	ID          int    `gorm:"primary_key;auto_increment"`
	OrgName     string `gorm:"primaryKey;column:orgname;not null;unique;"`
	Description string `json:"description"`
}

type OrgEvents struct {
	ID          int    `gorm:"primary_key;auto_increment"`
	EventName   string `gorm:"primaryKey;column:eventname;not null;unique;"`
	Description string
	OrgName     string `gorm:"not null;foreignKey:OrgName"`
}

type OrgEventSubscription struct {
	gorm.Model    `json:"-"`
	OrgName       string `gorm:"not null"`
	VolunteerName string `gorm:"not null"`
	EventName     string `gorm:"not null"`
}

type Database struct {
	DB *gorm.DB
}

func (db Database) AddOrgs(org []Org) ([]Org, error) {
	err := db.DB.Create(&org).Error
	return org, err
}
