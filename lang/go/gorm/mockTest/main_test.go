package daba

import (
	"fmt"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	//"time"
)

func setup() (*Database, sqlmock.Sqlmock, error) {

	sqlDB, mock, err := sqlmock.New()
	if err != nil {
		panic(err)
	}

	db, err := gorm.Open(postgres.New(
		postgres.Config{
			PreferSimpleProtocol: true,
			Conn:                 sqlDB,
		}),
		&gorm.Config{},
	)
	if err != nil {
		panic("Failed to connect to database!")
	}

	return &Database{
		DB: db,
	}, mock, nil
}

// github.com/mattn/go-sqlite3
func TestOrgInserts(t *testing.T) {

	db, _, _ := setup()

	org := []Org{
		Org{ID: 1, OrgName: "ORG1"},
		Org{ID: 2, OrgName: "ORG2"},
	}

	EXP, _ = db.AddOrgs(org)

	// mock.ExpectExec("INSERT INTO Org ('ID','OrgName') VALUES (1, 'ORG2');").WillReturnResult(sqlmock.NewResult(1, 1))
	// EXP := mock.ExpectationsWereMet()
	// if EXP != nil {
	// 	t.Errorf("Error: %v", EXP)
	// }

	fmt.Printf("%#v", EXP)

}
