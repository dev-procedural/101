package main

import (
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	Name      string
	Followers []*User `gorm:"many2many:follower_following;association_foreignkey:id;joinTableName:follower_following;joinForeignKey:follower_id;joinRefer:following_id"`
	Following []*User `gorm:"many2many:follower_following;association_foreignkey:id;joinTableName:follower_following;joinForeignKey:following_id;joinRefer:follower_id"`
}

type FollowerFollowing struct {
	FollowerId  uint
	FollowingId uint
	gorm.Model
}

func main() {

	db, err := gorm.Open(sqlite.Open("/tmp/gorm.db"), &gorm.Config{})

	if err != nil {
		panic("Failed to connect to database!")
	}
	// drop the table if it exists
	// db.DropTableIfExists(&FollowerFollowing{})
	db.AutoMigrate(&User{}, &FollowerFollowing{})

	user := &User{Name: "user1"}
	follower := &User{Name: "follower"}
	following := &User{Name: "following"}

	db.Create(&user)
	db.Create(&follower)
	db.Create(&following)

	db.Model(user).Association("Followers").Append(follower)
	db.Model(user).Association("Following").Append(following)
}
