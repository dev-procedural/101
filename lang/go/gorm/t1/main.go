package main

import (
  "gorm.io/driver/sqlite"
  "gorm.io/gorm"
  //"time"
)

// equals
// type CreditCard struct {
//   gorm.Model
//   Number   string `gorm:"primaryKey"`
//   UserID   uint
// }

// type User struct {
//   gorm.Model
//   Name        string       `gorm:"primaryKey"`
//   Email       string       `gorm:"primaryKey;unique"`
//   CreditCards []CreditCard `gorm:"many2many:user_cards;constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
// }

// type Blog struct {
//   gorm.Model
//   Title string `gorm:"primaryKey"`
//   Tags []Tag   `gorm:"primaryKey;many2many:bt;"`
//   Like []Like  `gorm:"primaryKey;many2many:br;"`
// }

// type Tag struct {
//   gorm.Model
//   BlogID uint
//   Type string `gorm:"primaryKey"`
// }

// type Like struct {
//   gorm.Model
//   BlogID uint
//   count bool
// }


type Org struct {
	ID          int    `gorm:"primary_key;auto_increment"`
	OrgName     string `gorm:"primaryKey;column:orgname;not null;unique;"`
	Description string `json:"description"`
}

type OrgEvents struct {
	ID          int    `gorm:"primary_key;auto_increment"`
	EventName   string `gorm:"primaryKey;column:eventname;not null;unique;"`
	Description string 
	OrgName     string `gorm:"not null;foreignKey:OrgName"`
}

type OrgEventSubscription struct {
	gorm.Model    `json:"-"`
	OrgName       string `gorm:"not null"`
	VolunteerName string `gorm:"not null"`
	EventName     string `gorm:"not null"`
}


// github.com/mattn/go-sqlite3
func main(){

  db, err := gorm.Open(sqlite.Open("gorm.db"), &gorm.Config{})

	if err != nil {
		panic("Failed to connect to database!")
	}

  // db.AutoMigrate(&User{}, &CreditCard{})
  // db.AutoMigrate(&Blog{}, &Tag{}, &Like{})
  db.AutoMigrate(&Org{}, &OrgEvents{}, &OrgEventSubscription{})

  // user := User{
  //   Name: "juju",
  //   Email: "juju@ju.com",
  //   CreditCards: []CreditCard{ {Number: "333"}, {Number: "777"}  }  }

  // db.Create(&user)

  // db.Create(&Blog{
  //   Title: "blog title",
  //   Tags: []Tag{ {Type: "333"}, {Type: "777"}  },
  //   Like: []Like{{count: true}} })
	org := Org{OrgName: "ORG2"}
	orgev := OrgEvents{EventName: "EVENT2", OrgName: "ORG1"}
	orgevsubs := OrgEventSubscription{OrgName: "ORG1", VolunteerName: "jujo"}

	db.Create(&org)
	db.Create(&orgev)
	db.Create(&orgevsubs)
}



