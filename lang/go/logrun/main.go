package main 

import (
   "github.com/sirupsen/logrus"
   "github.com/davecgh/go-spew/spew"
)

func main() {
  log       := logrus.New()
  spew.Dump(log)
}
