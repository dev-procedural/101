/*
Copyright © 2023 julieto
*/
package main

import "awee/cmd"

func main() {
	cmd.Execute()
}
