/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"os"

	"github.com/spf13/cobra"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "awee",
	Short: "This application is done to find all aws binary files in one place",
	Long: `This application will install any official aws binary used in operations
        ____ | |     / /__  ___ 
       / __  / | /| / / _ \/ _ \
      / /_/ /| |/ |/ /  __/  __/
      \__,_/ |__/|__/\___/\___/ `,
	// Run: func(cmd *cobra.Command, args []string) { },
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	// rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
