package cmd

import (
	"archive/tar"
	"archive/zip"
	"compress/gzip"
	"context"
	"encoding/json"
	"io"
	"log"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"strings"

	"github.com/cavaliergopher/grab/v3"
	"github.com/google/go-github/github"
)

func installBinary(URL, SERVICE, INSTALL_PATH, TMP_DIR string) {

	_, err := grab.Get(TMP_DIR, URL)
	if err != nil {
		log.Fatal(err)
	}
	var FILE interface{}

	// TODO change mime type inspection
	URL_LIST := strings.Split(URL, "/")
	MIME_ := strings.Split(URL_LIST[len(URL_LIST)-1], ".")
	MIME := MIME_[len(MIME_)-1]

	FILE = strings.Split(URL, "/")
	FILE = FILE.([]string)[len(FILE.([]string))-1]
	FILE = path.Join(TMP_DIR, FILE.(string))

	switch {
	// EKSCTL
	case MIME == "gz":
		log.Printf("[!] Extracting...\n")
		err := untar(FILE.(string), TMP_DIR)
		if err != nil {
			log.Fatalf("[!] Err: %v", err.Error())
		}

		log.Printf("[!] Installing...\n")
		err = os.Rename(TMP_DIR+SERVICE, "/usr/local/bin/"+SERVICE)
		if err != nil {
			log.Fatal(err)
		}

		log.Printf("[!] Configuring...\n")
		err = os.Chmod("/usr/local/bin/"+SERVICE, 0775)
		if err != nil {
			log.Fatal(err)
		}

	// ELSE
	case MIME == "zip":
		log.Printf("[!] Extracting...\n")
		err = unzip(FILE.(string), TMP_DIR+SERVICE)
		if err != nil {
			log.Fatalf("[!] Err: %v", err.Error())
		}

		log.Printf("[!] Installing...\n")
		out, err := exec.Command(TMP_DIR + SERVICE + "/" + INSTALL_PATH).Output()
		if err != nil {
			log.Fatal(err)
		}
		log.Printf("[!] %s\n", out)
	}
}

func unzip(src, dest string) error {

	r, err := zip.OpenReader(src)
	if err != nil {
		log.Fatalf("ERR %#v", err)
	}
	defer r.Close()

	for _, f := range r.File {
		rc, err := f.Open()
		if err != nil {
			return err
		}
		defer rc.Close()

		fpath := filepath.Join(dest, f.Name)
		if f.FileInfo().IsDir() {
			os.MkdirAll(fpath, f.Mode())
		} else {
			var fdir string
			if lastIndex := strings.LastIndex(fpath, string(os.PathSeparator)); lastIndex > -1 {
				fdir = fpath[:lastIndex]
			}

			err = os.MkdirAll(fdir, f.Mode())
			if err != nil {
				log.Fatal(err)
				return err
			}
			f, err := os.OpenFile(
				fpath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, f.Mode())
			if err != nil {
				return err
			}
			defer f.Close()

			_, err = io.Copy(f, rc)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func untar(FILE_PATH, TMP_DIR string) error {

	gzipStream, err := os.Open(FILE_PATH)

	uncompressedStream, err := gzip.NewReader(gzipStream)
	if err != nil {
		log.Fatal("ExtractTarGz: NewReader failed")
		return err
	}

	tarReader := tar.NewReader(uncompressedStream)

	for true {
		header, err := tarReader.Next()

		if err == io.EOF {
			break
		}

		if err != nil {
			log.Fatalf("ExtractTarGz: Next() failed: %s", err.Error())
			return err
		}

		switch header.Typeflag {
		case tar.TypeDir:
			if err := os.Mkdir(header.Name, 0755); err != nil {
				log.Fatalf("ExtractTarGz: Mkdir() failed: %s", err.Error())
				return err
			}
		case tar.TypeReg:
			outFile, err := os.Create(TMP_DIR + header.Name)
			if err != nil {
				log.Fatalf("ExtractTarGz: Create() failed: %s", err.Error())
				return err
			}
			if _, err := io.Copy(outFile, tarReader); err != nil {
				log.Fatalf("ExtractTarGz: Copy() failed: %s", err.Error())
				return err
			}
			outFile.Close()

		default:
			log.Fatalf(
				"ExtractTarGz: uknown type: %s in %s",
				header.Typeflag,
				header.Name)
		}

	}
	return nil
}

type VERSION struct {
	Name, Link string
}

func getCliVersion(owner, repo string) (Result []VERSION) {
	// read github owner, repo, token from standard input

	file := filepath.Join(os.Getenv("HOME"), ".cache/awee.cache")
	file_type := filepath.Join(file, repo)
	results := []byte{}
	err := os.Mkdir(file, 0750)

	ctx := context.Background()
	client := github.NewClient(nil)
	// client := github.NewTokenClient(ctx, token)

	_, err = os.Stat(file_type)
	// cache does not exist
	if err != nil {
		log.Printf("[!] Creating cache: %v\n", err)

		tagProtections, _, err := client.Repositories.ListTags(ctx, owner, repo, &github.ListOptions{})
		if err != nil {
			log.Fatalf("Error: %v\n", err)
		}

		results, _ = json.Marshal(tagProtections)
		err = os.WriteFile(file_type, results, 0400)
		if err != nil {
			log.Fatalf("Error: %v\n", err)
		}

		// cache exitst
	} else {

		log.Println("[!] Reading file...")
		results, err := os.ReadFile(file_type)
		if err != nil {
			log.Fatalf("%#v", err)
		}

		var data interface{}
		err = json.Unmarshal(results, &data)
		if err != nil {
			log.Fatalf("%#v", err)
		}

		for _, i := range data.([]interface{}) {
			// switch elem := i.(type) {
			switch i.(type) {
			case map[string]interface{}:
				Result = append(Result, VERSION{
					Name: i.(map[string]interface{})["name"].(string),
					Link: i.(map[string]interface{})["zipball_url"].(string),
				})
			}
		}

		return Result
	}

	return []VERSION{}
}
