/*
Copyright © 2022
*/
package cmd

import (
	"fmt"
	"runtime"
	"strings"

	"github.com/spf13/cobra"
)

// installCmd represents the install command
var installCmd = &cobra.Command{
	Use:   "install",
	Short: "Install aws binaries for diverse projects",
	Long: `Install aws binares in your path so you can access to it with ease. All in one place.
`,
	Args: cobra.MatchAll(cobra.ExactArgs(1), cobra.OnlyValidArgs),

	ValidArgs: []string{"awscli", "sam"},
}

var awsCmd = &cobra.Command{
	Use:   "awscli",
	Short: "install awscli v2 by default",
	Long: ` install awscli v2 by default in local path
`,
	Run: func(cmd *cobra.Command, args []string) {

		AWSCLI := "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip"

		installBinary(AWSCLI, "awscli", "aws/install", "/tmp/")

	},
}

var samCmd = &cobra.Command{
	Use:   "sam",
	Short: "install sam v2 by default",
	Long: ` install sam v2 by default in local path
`,
	Run: func(cmd *cobra.Command, args []string) {

		SAM := "https://github.com/aws/aws-sam-cli/releases/latest/download/aws-sam-cli-linux-x86_64.zip"

		installBinary(SAM, "sam", "install", "/tmp/")

	},
}

var eksCmd = &cobra.Command{
	Use:   "eksctl",
	Short: "install eksctl v2 by default",
	Long: ` install eksctl v2 by default in local path
`,
	Run: func(cmd *cobra.Command, args []string) {

		OS := runtime.GOOS
		OS = strings.Title(runtime.GOOS)
		ARCH := runtime.GOARCH
		eks := fmt.Sprintf("https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_%s_%s.tar.gz", OS, ARCH)

		installBinary(eks, "eksctl", "install", "/tmp/")

	},
}

var (
	PATH string
	TMP  = "/tmp"
)

func init() {
	rootCmd.AddCommand(installCmd)
	installCmd.AddCommand(awsCmd)
	installCmd.AddCommand(samCmd)
	installCmd.AddCommand(eksCmd)

	samCmd.Flags().StringVarP(&PATH, "path", "d", "~/.local/bin", "AWCLI installation path")
	awsCmd.Flags().StringVarP(&PATH, "path", "d", "~/.local/bin", "AWCLI installation path")
}
