package integers

func Add(x, y int) int {
	return x + y
}

func Subs(x, y int) int {
	return x - y
}

func Repeat(x string) string {
	var repeated string
	for i := 0; i < 5; i++ {
		repeated += x
	}
	return repeated
}

func Sum(x []int) int {
	c := 0
	for _, i := range x {
		c += i
	}
	return c
}

type PersonAddr interface {
	Addr() string
}

type Doctor struct {
	Name    string
	Address string
	Office  bool
}

func (d Doctor) Addr() string {
	return d.Address
}

type Teacher struct {
	Name    string
	School  string
	Address string
}

func (t Teacher) Addr() string {
	return t.Address
}

//
