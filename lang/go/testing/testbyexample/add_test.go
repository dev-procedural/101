package integers

import (
	"testing"
)

// _________________________________[ INTS
func TestAdder(t *testing.T) {
	sum := Add(2, 2)
	expected := 4

	if sum != expected {
		t.Errorf("expected '%d' but got '%d'", expected, sum)
	}
}

func TestSubs(t *testing.T) {
	subs := Subs(2, 2)
	expected := 0

	if subs != expected {
		t.Errorf("expected '%d' but got '%d'", expected, subs)
	}
}

// _________________________________[ FOR
func TestRepeat(t *testing.T) {
	repeated := Repeat("a")
	expected := "aaaaa"

	if repeated != expected {
		t.Errorf("expected '%s' but got '%s'", expected, repeated)
	}
}

func BenchmarkRepeat(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Repeat("a")
	}
}

// _________________________________[ ARRAYS
func TestSum(t *testing.T) {
	t.Run("collection of 5 numbers", func(t *testing.T) {
		numbers := []int{1, 2, 3, 4, 5}

		got := Sum(numbers)
		want := 15

		if got != want {
			t.Errorf("got %d want %d given, %v", got, want, numbers)
		}
	})

	t.Run("collection of any size", func(t *testing.T) {
		numbers := []int{1, 2, 3}

		got := Sum(numbers)
		want := 6

		if got != want {
			t.Errorf("got %d want %d given, %v", got, want, numbers)
		}
	})
}

//_________________________________[ STRUCT,METHOD

//_________________________________[ INTERFACE

func TestPersons(t *testing.T) {
	persons := []struct {
		name   string
		field  PersonAddr
		result string
	}{
		{name: "Doctor", field: Doctor{Name: "Paco", Address: "123 ttt", Office: true}, result: "123 ttt"},
		{name: "Teacher", field: Teacher{Name: "Pepe", Address: "777 ddd", School: "teete"}, result: "777 ddd"},
	}

	for _, tt := range persons {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.field.Addr()
			if got != tt.result {
				t.Errorf("%#v got %#v want %#v\n", tt.field, got, tt.result)
			}
		})
	}
}

//
