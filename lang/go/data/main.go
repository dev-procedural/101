package main

import (
	"fmt"

	"github.com/xuri/excelize/v2"
)

func main() {

	f, err := excelize.OpenFile("/home/julio/Downloads/wo.xlsx")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer func() {
		// Close the spreadsheet.
		if err := f.Close(); err != nil {
			fmt.Println(err)
		}
	}()

	cell, err := f.GetCellValue("Tabellenblatt1", "A3")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(cell)

	rows, err := f.GetRows("Tabellenblatt1")
	if err != nil {
		fmt.Println(err)
		return
	}
	for _, row := range rows {
		for _, colCell := range row {
			fmt.Print(colCell, "\t")
		}
		fmt.Println()
	}
}
