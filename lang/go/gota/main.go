package main

import (
	"fmt"
	"os"

	"github.com/go-gota/gota/dataframe"
	"github.com/go-gota/gota/series"
	"github.com/gonum/floats"
	"gonum.org/v1/gonum/mat"
)

type User struct {
	Name    string
	Age     int
	Married bool
}

func main() {

	FILE, _ := os.Open("/tmp/annual-enterprise-survey-2021-financial-year-provisional-csv.csv")
	CSV := dataframe.ReadCSV(FILE)
	fmt.Println(CSV)

	df1 := dataframe.LoadStructs(
		[]User{
			{Name: "Juju", Age: 36, Married: true},
			{Name: "James", Age: 40, Married: true},
		})

	df2 := dataframe.LoadStructs(
		[]User{
			{Name: "Peter", Age: 36, Married: true},
			{Name: "Cam", Age: 40, Married: true},
		})

	df3 := dataframe.LoadStructs(
		[]User{
			{Name: "Culkin", Age: 36, Married: true},
			{Name: "Metip", Age: 40, Married: true},
		})

	DFS := []dataframe.DataFrame{df1, df2, df3}

	df4 := dataframe.DataFrame{}

	// MERGING
	for _, DF := range DFS {
		// fmt.Printf()
		df4 = df4.Concat(DF)
	}
	fmt.Println(df4)

	// COLUMN SELECT
	COL := df4.Col("Name")
	fmt.Printf("[!] COLS\n")
	fmt.Printf("%v\n", COL)

	// SUBSET ROWS
	rows := df4.Subset([]int{0, 2})
	fmt.Printf("[!] Subset Rows\n")
	fmt.Println(rows)

	// SELECT COLS
	cols := df4.Select([]string{"Name", "Age"})
	fmt.Printf("[!] Subset Cols\n")
	fmt.Println(cols)

	// UPDATE
	NEWUSER := []User{
		{Name: "uno", Age: 33, Married: false},
		{Name: "dos", Age: 33, Married: false},
		{Name: "tres", Age: 33, Married: false},
		{Name: "cuatro", Age: 33, Married: false},
	}

	df4 = df4.Set(
		series.Ints([]int{0, 1, 2, 3}),
		dataframe.LoadStructs(
			NEWUSER,
		),
	)
	fmt.Println(df4.String())

	a := mat.NewDense(3, 3, floats.Span(make([]float64, 9), 0, 8))
	fmt.Printf("%#v", a)

}
