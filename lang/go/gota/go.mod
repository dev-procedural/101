module main

go 1.16

require (
	github.com/go-gota/gota v0.12.0
	github.com/gonum/floats v0.0.0-20181209220543-c233463c7e82 // indirect
	github.com/gonum/internal v0.0.0-20181124074243-f884aa714029 // indirect
	golang.org/x/exp v0.0.0-20221031165847-c99f073a8326 // indirect
	golang.org/x/image v0.1.0 // indirect
	gonum.org/v1/gonum v0.12.0 // indirect
	gonum.org/v1/plot v0.12.0 // indirect
)
