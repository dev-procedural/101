package main

import (
	"fmt"
)

func main() {
	// buffered means ordered
	BUF := 3
	jobs := make(chan int, BUF)
	cooks := make(chan int, BUF)
	done := make(chan bool)

	go func() {
		for jobs != nil || cooks != nil {
			select {
			case q, more := <-cooks:
				if more {
					fmt.Println("cooked job", q)
				} else {
					fmt.Println("received all cooks")
					cooks = nil
					done <- true
					break
					return
				}

			case j, more := <-jobs:
				if more {
					fmt.Println("received job", j)
					cooks <- j
				} else {
					fmt.Println("received all jobs")
					// jobs = nil
					done <- true
					// break
				}
			}
		}
	}()

	for j := 1; j <= BUF; j++ {
		val := j
		jobs <- val
		fmt.Println("sent job", val)
	}
	fmt.Println("sent all jobs")
	close(jobs)

	<-done
}
