package main

import (
	"fmt"
	"time"
)

func merge(ch1, ch2 <-chan int) <-chan int {
	ch1Closed := false
	ch2Closed := false
	ch := make(chan int, 1)
	go func() {
		for {
			select {
			case v, open := <-ch1:
				if !open {
					fmt.Print(v, open)
					ch1Closed = true
					break
				}
				ch <- v
			case v, open := <-ch2:
				if !open {
					ch2Closed = true
					break
				}
				ch <- v
			}
			if ch1Closed && ch2Closed {
				close(ch)
				return
			}
		}
	}()
	return ch
}

func main() {
	BUF := 3
	ch1 := make(chan int, BUF)
	ch2 := make(chan int, BUF)

	for _, i := range []int{1, 2, 3} {
		val := i
		ch1 <- val
		ch2 <- val + 100
	}

	ch := merge(ch1, ch2)

	for ch != nil {
		select {
		case v, ok := <-ch:
			fmt.Println(v, ok)
			if ok {
				fmt.Println(v, ok)
				break
			} else {
				fmt.Println(v, ok)
			}
		case <-time.After(3 * time.Second):
			fmt.Println("this is the end")
			return
		}
		fmt.Println("Print a")
		time.Sleep(2 * time.Second)
	}

}
