module main

go 1.19

require inet.af/netaddr v0.0.0-20220811202034-502d2d690317

require (
	github.com/sourcegraph/conc v0.3.0 // indirect
	go4.org/intern v0.0.0-20211027215823-ae77deb06f29 // indirect
	go4.org/unsafe/assume-no-moving-gc v0.0.0-20220617031537-928513b29760 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
