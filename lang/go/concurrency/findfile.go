package main

import (
	"flag"
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	"sync"

	"gopkg.in/yaml.v2"
)

var (
	PATH string
)

func init() {
	flag.StringVar(&PATH, "p", "./", "path to scan")

	if PATH == "/" {
		fmt.Println("path argument must be fully qualified")
		os.Exit(1)
	}

	flag.Parse()
}

type fileEntry struct {
	p    string
	info os.FileInfo
}

func main() {
	files := make(map[string]interface{})
	input := make(chan fileEntry, 1)

	go func() {
		defer close(input)
		err := filepath.WalkDir(
			PATH,
			func(path string, d fs.DirEntry, err error) error {
				if err != nil {
					return err
				}
				if d.IsDir() {
					return nil
				}
				info, err := d.Info()
				if err != nil {
					return err
				}
				input <- fileEntry{p: path, info: info}
				return nil
			},
		)
		if err != nil {
			fmt.Println(err)
			return
			// os.Exit(1)
		}
	}()

	limit := make(chan struct{}, 1000)
	wg := sync.WaitGroup{}
	mu := sync.Mutex{}

	for fe := range input {
		fe := fe
		limit <- struct{}{}
		wg.Add(1)
		go func() {
			defer wg.Done()
			defer func() { <-limit }()

			mu.Lock()
			files[fe.p] = fe.info.Size()
			mu.Unlock()

		}()
	}
	wg.Wait()

	d, err := yaml.Marshal(files)
	if err != nil {
		panic(err)
	}
	fmt.Println(string(d))
}
