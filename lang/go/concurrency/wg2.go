package main

import (
	"context"
	"encoding/json"
	"fmt"
	"os/exec"
	"sync"
	"time"
)

func main() {
	channel := generator()
	rec := readChannel(channel)

	for c := range rec {
		b, _ := json.Marshal(c)
		fmt.Printf("[!] %s\n", b)
		// fmt.Printf("[!] %#v\n", c)
	}

}

func generator() chan int {
	// generate
	ch := make(chan int, 1)

	go func() {
		defer close(ch)

		for i := 1; i <= 100; i++ {
			// log.Println("[!] ", i)
			ch <- i
		}
	}()

	return ch
}

type record struct {
	IS string
}

func readChannel(numChan chan int) chan record {
	// read channel
	ch := make(chan record, 1)
	go func() {
		defer close(ch)

		limit := make(chan struct{}, 50)
		wg := sync.WaitGroup{}

		for i := range numChan {
			limit <- struct{}{}
			wg.Add(1)

			go func(val int) {
				defer func() { <-limit }()
				defer wg.Done()

				ctx, cancel := context.WithTimeout(
					context.Background(),
					3*time.Second,
				)
				defer cancel()
				rec := record{IS: fmt.Sprintf("%d", val)}
				check(ctx)

				ch <- rec
			}(i)
		}
		wg.Wait()
	}()
	return ch
}

func check(ctx context.Context) bool {
	// num := rand.Intn(5-2) + 2
	// log.Println(num)
	// cmd := exec.CommandContext(ctx, "sleep", string(num))
	cmd := exec.CommandContext(ctx, "sleep", "2")

	if err := cmd.Run(); err != nil {
		return false
	}

	return true
}
