package main

import (
	"fmt"
	"math/rand"
	"runtime"
	"time"
)

// Simple structure which tags chef and order numbe
type ChefOrder struct {
	Chef  string
	Order int
	// mu    sync.Mutex
}

// Same as above, duplex channel
type ChefCooked struct {
	Chef  string
	Order int
	// mu    sync.Mutex
}

// Function to select a Chef and Waiter for you
func selectWaiterAndChef() (string, string) {
	allChefs := []string{"Jack", "Bob", "Mark"}
	allWaiters := []string{"A", "B", "C"}

	randomC := rand.Intn(len(allChefs))
	randomW := rand.Intn(len(allWaiters))
	cpick := allChefs[randomC]
	wpick := allWaiters[randomW]

	return cpick, wpick
}

// Function where the waiter takes order, chef cooks it and waiter brings your order
func processOrderandCook(orderChan chan ChefOrder, cookChan chan ChefCooked, done chan bool) {
	_, waiter := selectWaiterAndChef()

	for {
		time.Sleep(time.Second)
		select {
		// step 3
		case cookedOrder, ok := <-cookChan:
			fmt.Println(cookedOrder, ok)
			time.Sleep(1 * time.Second)
			if !ok {
				done <- true
			}
			fmt.Printf("[!] step 3 ")
			fmt.Printf("Waiter %s brings order %d from chef %s \n", waiter, cookedOrder.Order, cookedOrder.Chef)
			fmt.Println("")

		// step 2
		case gotOrder, ok := <-orderChan:
			if ok {
				fmt.Printf("[!] step 2 ")
				fmt.Printf("Chef %s cooks order %d \n", gotOrder.Chef, gotOrder.Order)
				cookChan <- ChefCooked{gotOrder.Chef, gotOrder.Order}
			}

		// step 1
		default:
			fmt.Printf("[!] step 1 ")
		}
	}

}

// Main function
func main() {
	runtime.GOMAXPROCS(1)
	totalOrders := 3

	// mutex := sync.Mutex{}

	orderChan := make(chan ChefOrder)
	cookChan := make(chan ChefCooked)
	done := make(chan bool, 1)

	for order := 1; order <= totalOrders; order++ {
		order_ := order
		chef, _ := selectWaiterAndChef()
		orderChan <- ChefOrder{Chef: chef, Order: order_}
	}

	go processOrderandCook(orderChan, cookChan, done)
	fmt.Println("\n")
	<-done

}
