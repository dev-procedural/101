package main

import (
	"log"
	"time"
)

var quit chan bool
var flag chan string

func ListReader(list []string, c chan string) {
	for _, i := range list {
		c <- i // + string(i)
		time.Sleep(time.Millisecond * 20)
	}
	// flag <- false
}

func Output(c chan string) {
	for {
		select {
		case v := <-c:
			log.Println(v)

			if v == "C" {
				log.Println("this trigger a flag")
				time.Sleep(time.Millisecond * 500)
				flag <- "this"

			} else if v == "E" {
				log.Println("this trigger a flag")
				time.Sleep(time.Millisecond * 800)
				flag <- "that"
			}

		case v := <-flag:
			log.Println("this is a flag", v)
			log.Printf("%#v", len(flag))
			// flag <- "false"

		case <-time.After(3 * time.Second):
			log.Println("this is done")
			quit <- true
		}
	}
}

func main() {
	quit = make(chan bool)
	ch1 := make(chan string)
	flag = make(chan string, 1)
	flag <- ""

	list := []string{"A", "B", "C", "D", "E"}

	go ListReader(list, ch1)
	go Output(ch1)
	<-flag
	<-quit
}
