package main

import "fmt"

func ReadData(c chan<- int, quit <-chan bool) {
	counter := 1
	x := 1
	for {
		select {
		case c <- x:
			fmt.Println(x, counter)
			x = x + x
			counter += 1

		case <-quit:
			fmt.Println("quit")
			return
		}
	}
}

func main() {
	c := make(chan int, 1)
	quit := make(chan bool)

	go func() {
		for i := 0; i < 10; i++ {
			<-c
			// fmt.Printf("%#v\n", cap(c)-len(c))
		}
		quit <- true
	}()

	ReadData(c, quit)

}
