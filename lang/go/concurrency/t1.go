package main

import (
	"fmt"
	"time"
)

func stepper(val string, ch1, ch2, ch3 chan string) {
	// go func() {
	for {
		select {
		case msg, ok := <-ch2:
			if ok {
				fmt.Println(msg)
				ch3 <- "This is a test - " + val
			}
			return
		case msg, ok := <-ch1:
			if ok {
				fmt.Println(msg)
				ch2 <- "step 3 - " + val
			}
		default:
			ch1 <- "step 2 - " + val
			fmt.Println("step 1 - " + val)
		}
	}
	// }()

}

func main() {

	ch1 := make(chan string, 1)
	ch2 := make(chan string, 1)
	ch3 := make(chan string, 1)
	// done := make(chan bool)

	for _, i := range [3]string{"1", "2", "3"} {
		val := i
		// fmt.Printf("%#v\n", val)
		go stepper(val, ch1, ch2, ch3)
	}

	for {
		select {
		case news := <-ch3:
			fmt.Println(news)
		case <-time.After(3 * time.Second):
			fmt.Println("Time out: No news in one minute")
			return
			// default:
			// 	time.Sleep(5 * time.Second)
			// 	fmt.Println("default")
		}

	}

}
