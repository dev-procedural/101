package main

import (
	"fmt"
	"time"
)

var quit chan bool

func pingGenerator(bus chan<- string) {
	// The channel can only be sent to - a generator
	for i := 0; i < 2; i++ {
		bus <- "ping"
	}
}

func pongGenerator(bus chan<- string) {
	// Information can only be sent to the channel - a generator
	for i := 0; i < 2; i++ {
		bus <- "pong"
	}
}

func output(bus <-chan string) {
	// Information can only be received from the channel - a consumer
	for {
		time.Sleep(time.Second * 1)
		select {
		case value := <-bus:
			fmt.Println(value)
		case <-time.After(3 * time.Second):
			fmt.Println("Program timed out.")
			quit <- true
		}
	}
}

func main() {
	quit = make(chan bool)
	bus := make(chan string)
	go pingGenerator(bus)
	go pongGenerator(bus)
	go output(bus)
	<-quit
}
