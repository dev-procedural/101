package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net"
	"os/exec"
	"sync"
	"time"

	"inet.af/netaddr"
)

func main() {

	ipCh, _ := hosts("192.168.2.0/24")
	scanResults := scanPrefixes(ipCh)

	for rec := range scanResults {
		b, _ := json.Marshal(rec)
		if rec.Reachable {
			fmt.Printf("%s\n", b)
		}
	}

}

type record struct {
	// Host is the IP address of the host.
	Host net.IP
	// Reachable indicates if this host was pingable.
	Reachable bool
	// LoginSSH indicates if we were able to authenticate with SSH.
	// LoginSSH bool
	// Uname is the output of the "uname -a" command. If this is an empty string
	// but LoginSSH is true, this means uname was not supported by the host.
	// Uname string
}

// host takes a CIDR string (192.168.0.0/24) and returns all host IPs for that network.
// This will not send back the broadcast or network addresses. Does not support /31 addresses.
func hosts(cidr string) (chan net.IP, error) {
	ch := make(chan net.IP, 1)

	prefix, err := netaddr.ParseIPPrefix(cidr)
	if err != nil {
		return nil, err
	}

	go func() {
		defer close(ch)

		var last net.IP
		for ip := prefix.IP().Next(); prefix.Contains(ip); ip = ip.Next() {
			// Prevents sending the broadcast address.
			if len(last) != 0 {
				// log.Printf("sending: %s, contained: %v", last, prefix.Contains(ip))
				ch <- last
			}
			last = ip.IPAddr().IP
		}
	}()
	return ch, nil
}

// scanPrefixes takes a channel of net.IP and pings them. If an IP responds to ping it is put
// on the returned success channel, otherwise it is put on the fail channel.
func scanPrefixes(ipCh chan net.IP) chan record {
	ch := make(chan record, 1)
	go func() {
		defer close(ch)

		limit := make(chan struct{}, 300)
		wg := sync.WaitGroup{}
		for ip := range ipCh {
			// log.Println("[!] ", ip)
			limit <- struct{}{}
			wg.Add(1)

			go func(ip net.IP) {
				defer func() { <-limit }()
				defer wg.Done()

				ctx, cancel := context.WithTimeout(
					context.Background(),
					3*time.Second,
				)
				defer cancel()

				rec := record{Host: ip}
				if hostAlive(ctx, ip) {
					rec.Reachable = true
				}
				ch <- rec
			}(ip)
		}
		wg.Wait()
	}()
	return ch
}

func hostAlive(ctx context.Context, host net.IP) bool {
	cmd := exec.CommandContext(ctx, "ping", "-c", "1", "-t", "2", host.String())

	if err := cmd.Run(); err != nil {
		return false
	}
	return true
}
