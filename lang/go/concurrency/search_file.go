package main

import (
	"fmt"
	"sort"

	// "os"
	"path/filepath"
	"regexp"
	"time"
	// "sync"
)

func filter(files []string, ch chan string) {
	sort.Strings(files)
	for _, v := range files {
		if r, _ := regexp.MatchString(`.*.pdf`, v); r {
			ch <- v
		}
	}
}

func main() {

	// var wg sync.WaitGroup
	done := make(chan bool, 1)
	ch1 := make(chan string, 1)
	files, _ := filepath.Glob("/tmp/*")

	go filter(files, ch1)

	ch1 <- ""

	// routine in background reads channel
	// using synchronization
	go func() {
		// defer wg.Done()
		for {
			select {
			case val, ok := <-ch1:
				if ok && val != "" {
					fmt.Printf("VAL: %#v \n", val)
					// fmt.Printf("len: %s\n", len(ch1))
				}
			default:
				fmt.Printf("No val, exiting...")
				done <- true
				return
			}
			time.Sleep(time.Second * 1)
		}
	}()

	<-done
}
