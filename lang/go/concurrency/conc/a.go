package main

import (
	"fmt"
	"sort"

	"github.com/sourcegraph/conc/pool"
)

func main() {
	p := pool.NewWithResults[string]().WithMaxGoroutines(10)
	for i := 0; i <= 40; i++ {
		i := i
		p.Go(func() string {
			return fmt.Sprintf("conci %v\n", i*3)
		})
	}
	res := p.Wait()

	sort.Strings(res)
	fmt.Println(res)
}
