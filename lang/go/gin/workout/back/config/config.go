package config

import (
	"fmt"
	"os"

	"github.com/spf13/viper"
)

type DBConf struct {
	DB_USER string `mapstructure:"DB_USER"`
	DB_PASS string `mapstructure:"DB_PASS"`
	DB_URL  string `mapstructure:"DB_URL"`
	DB_NAME string `mapstructure:"DB_NAME"`
	DB_PORT string `mapstructure:"DB_PORT"`
	DB_PATH string `mapstructure:"DB_PATH"`
	DB_TYPE string `mapstructure:"DB_TYPE"` //myslq | postgres | sqlite
}

// DB
func init() {
	var dbconfig *DBConf

	viper.SetConfigName("config.env")
	viper.AddConfigPath("./config")
	viper.SetConfigType("env")
	err := viper.ReadInConfig()
	if err != nil { // Handle errors reading the config file
		panic(fmt.Errorf("fatal error config file: %w", err))
	}

	err = viper.ReadInConfig()
	if err != nil {
		return
	}
	err = viper.Unmarshal(&dbconfig)
	viper.AutomaticEnv()

	// fmt.Printf("%#v\n", viper.AllSettings())
	os.Setenv("DB_USER", dbconfig.DB_USER)
	os.Setenv("DB_PASS", dbconfig.DB_PASS)
	os.Setenv("DB_URL", dbconfig.DB_URL)
	os.Setenv("DB_NAME", dbconfig.DB_NAME)
	os.Setenv("DB_PORT", dbconfig.DB_PORT)
	os.Setenv("DB_TYPE", dbconfig.DB_TYPE)
	os.Setenv("DB_PATH", dbconfig.DB_PATH)
	os.Setenv("CRUD_URL", fmt.Sprintf("%s:%s", viper.Get("CRUD_SERVER_HOST").(string), viper.Get("CRUD_SERVER_PORT").(string)))
}

func DBConfig() *DBConf {
	return &DBConf{
		DB_USER: os.Getenv("DB_USER"),
		DB_NAME: os.Getenv("DB_NAME"),
		DB_URL:  os.Getenv("DB_URL"),
		DB_PASS: os.Getenv("DB_PASS"),
		DB_PORT: os.Getenv("DB_PORT"),
		DB_TYPE: os.Getenv("DB_TYPE"),
		DB_PATH: os.Getenv("DB_PATH"),
	}
}

func (c *DBConf) DBStringer() string {
	switch {

	case c.DB_TYPE == "mysql":
		return fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", c.DB_USER, c.DB_PASS, c.DB_URL, c.DB_PORT, c.DB_NAME)

	case c.DB_TYPE == "postgres":
		return fmt.Sprintf("user=%s password=%s dbname=%s host=%s port=%s sslmode=disable", c.DB_USER, c.DB_PASS, c.DB_NAME, c.DB_URL, c.DB_PORT)

	case c.DB_TYPE == "sqlite":
		return fmt.Sprintf("%s", c.DB_PATH)

	default:
		return "TYPE empty"

	}
}
