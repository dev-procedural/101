package middleware

import (
	"log"
	"micro/config"
	"net/http"

	// "log"
	// "encoding/json"
	// "strings"
	// "bytes"
	"io/ioutil"

	"github.com/gin-gonic/gin"
)

func AuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		// header, err := c.Request.Header["Token"]
		// log.Println(header)
		// return

		log.Printf("[!] cookie cors middle")
		cookie, err := c.Cookie("jwtTokenSession")
		log.Printf("[-] cookie cors mid: %#v", cookie)

		// if tokenValue == "" {
		if cookie == "" || err != nil {
			c.JSON(http.StatusNotFound, gin.H{"error": "jwtTokenSession not present or token expired"})
			c.AbortWithStatus(http.StatusNotFound)
		} else {
			client := &http.Client{}
			CONF := config.ServiceConfig()

			req, _ := http.NewRequest("POST", CONF.AUTH_MIDDLEWARE_ENDPOINT, nil)
			req.Header.Set("Cookie", "jwtTokenSession="+cookie)
			resp, err := client.Do(req)

			if err != nil {
				c.JSON(http.StatusNotFound, gin.H{"error": err.Error()})
				c.Abort()
			}

			data, err := ioutil.ReadAll(resp.Body)

			if resp.StatusCode == 404 {
				c.JSON(http.StatusNotFound, gin.H{"error": data})
				c.Abort()
			}

			flag := false
			for _, cookie := range resp.Cookies() {
				if cookie.Name == "jwtTokenSession" {
					flag = true
				}
			}

			if flag == false {
				c.JSON(http.StatusNotFound, gin.H{"error": "invalid token"})
				c.Abort()
			}

			// c.Set("jwtTokenSession", cookie)
		}

		if c.Request.Method != "OPTIONS" {
			c.Next()
		} else {
			c.AbortWithStatus(http.StatusOK)
		}
	}
}
