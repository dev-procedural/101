package models

// "gorm.io/gorm"

type Crud struct {
	// gorm.Model
	ID        int   `gorm:"primary_key;autoIncrement;index" json:"id"`
	CreatedAt int64 `gorm:"unique,autoCreateTime,size:10" json:"created_at"`
}

// func (*Crud) TableName() string {
// 	return "calendar"
// }
