package main

import (
	"log"
	"micro/pkg/rest"
	"os"
)

func main() {

	API, _ := rest.RunAPI()

	log.Println("Main log...")
	log.Fatal(API.Run(os.Getenv("CRUD_URL")))
}
