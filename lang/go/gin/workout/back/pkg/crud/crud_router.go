package crud

import (
	"github.com/gin-gonic/gin"
)

func CrudRouter(crud CrudHandlerInterface, r *gin.Engine) error {
	// private router
	crud_private := r.Group("/v1/crud")
	{
		crud_private.DELETE("/delete/:id", crud.Delete)
		crud_private.PUT("/update/:id", crud.Update)
		crud_private.PUT("/add", crud.Add)
		crud_private.GET("/get/:id", crud.Get)
		crud_private.GET("/get/all", crud.GetAll)
	}
	return nil
}
