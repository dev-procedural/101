package main

import (
	"log"
	"os"

	"github.com/gin-gonic/gin"
)

func main() {
	log.Println("[+] This is a log msg to validate app is ok")

	r := gin.Default()

	r.GET("/", func(c *gin.Context) {

		a := os.Environ()
		c.ShouldBind(&a)
		//take action
		c.JSON(200, gin.H{"msg": a})
	})

	r.POST("/", func(c *gin.Context) {

		a := make(map[string]string)
		c.ShouldBind(&a)
		//take action
		c.JSON(200, gin.H{"msg": a})
	})

	r.Run()
}
