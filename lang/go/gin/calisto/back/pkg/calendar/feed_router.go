package calendar

import (
	"github.com/gin-gonic/gin"
)

func FeedRouter(calendar CalendarHandlerInterface, r *gin.Engine) error {
	// private router
	calendar_private := r.Group("/v1/feed")
	{
		calendar_private.DELETE("/delete/:id", calendar.Delete)
		calendar_private.PUT("/update/:id", calendar.Update)
		calendar_private.PUT("/add", calendar.Add)
		calendar_private.GET("/get/:id", calendar.Get)
		calendar_private.GET("/get/all", calendar.GetAll)
	}
	return nil
}
