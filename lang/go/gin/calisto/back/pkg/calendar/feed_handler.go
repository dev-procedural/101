package calendar

import (
	"log"
	"micro/internal/models"
	"strconv"

	"micro/config"
	"micro/pkg/db"
	"micro/pkg/utils"
	"net/http"

	"github.com/gin-gonic/gin"
)

type CalendarHandler struct {
	DB db.FeedDBLayer
}

type CalendarHandlerInterface interface {
	Add(c *gin.Context)
	Get(c *gin.Context)
	GetAll(c *gin.Context)
	Delete(c *gin.Context)
	Update(c *gin.Context)
}

func NewcalendarHandler() (CalendarHandlerInterface, error) {

	CONF := config.DBConfig()

	db, err := db.NewORM(CONF.DBStringer())
	db.AutoMigrate(&models.Calendar{}, &models.Feed{})

	if err != nil {
		return nil, err
	}

	return &CalendarHandler{DB: db}, nil
}

// func date(unix int64) string {

// 	loc, _ := time.LoadLocation("EST")
// 	now := time.Unix(unix, 0).In(loc)
// 	new_date_time, _ := time.ParseInLocation(time.RFC3339, now.Format(time.RFC3339), loc)

// 	new_date_time_ := fmt.Sprintf("%+v", new_date_time)

// 	return new_date_time_
// }

// ________________________________[[  FEED  ]]
// @title Feed api
// @BasePath /v1/feed
// mail godoc
// @Summary add new feed entry
// @Schemes http
// @Description  add new feed entry per click
// @Tags        feed
// @Accept      json
// @Produce     json
// @Failure     400 {string} ERROR
// @Router      /v1/feed/add [put]
func (h *CalendarHandler) Add(c *gin.Context) {
	if h.DB == nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "server database error", "stage": "0"})
		return
	}

	var cal models.Feed

	request, err := h.DB.Add(cal)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error(), "stage": "2"})
		return
	}
	log.Printf("Added %#v - time %#v \n", request, utils.Date(request.CreatedAt))
	c.JSON(http.StatusOK, request)
}

// @title Feed api
// @BasePath /v1/feed
// @Summary get feed entry
// @Schemes http
// @Description  get feed entry per click
// @Tags        feed
// @Accept      json
// @Produce     json
// @Failure     400 {string} ERROR
// @Router      /v1/feed/get/:id [get]
func (h *CalendarHandler) Get(c *gin.Context) {
	if h.DB == nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "server database error", "stage": "0"})
		return
	}

	var cal models.Feed

	cal.ID, _ = strconv.Atoi(c.Param("id"))

	request, err := h.DB.Get(cal)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error(), "stage": "2"})
		return
	}
	log.Printf("Get: %#v \n", request)
	c.JSON(http.StatusOK, request)
}

// @title Feed api
// @BasePath /v1/feed
// @Summary get feed entry
// @Schemes http
// @Description  get all feed entries per click
// @Tags        feed
// @Accept      json
// @Produce     json
// @Failure     400 {string} ERROR
// @Router      /v1/feed/get/all [get]
func (h *CalendarHandler) GetAll(c *gin.Context) {
	if h.DB == nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "server database error", "stage": "0"})
		return
	}

	var request []models.Feed
	request, err := h.DB.GetAll()
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error(), "stage": "2"})
		return
	}
	c.JSON(http.StatusOK, request)
}

// @title Feed api
// @BasePath /v1/feed
// @Summary update feed entry
// @Schemes http
// @Description  update feed entry per click
// @Tags        feed
// @Accept      json
// @Produce     json
// @Param       created_at  query string true "created time unix/epoch"
// @Failure     400 {string} ERROR
// @Router      /v1/feed/update/:id [put]
func (h *CalendarHandler) Update(c *gin.Context) {
	if h.DB == nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "server database error", "stage": "0"})
		return
	}

	var cal models.Feed
	var new_date int64

	new_date, _ = strconv.ParseInt(c.PostForm("created_at"), 10, 64)

	// check size
	if len(c.PostForm("created_at")) != 10 {
		c.JSON(http.StatusNotFound, gin.H{"error": "String size incorrect, must be 10", "stage": "1"})
		return
	}

	cal.ID, _ = strconv.Atoi(c.Param("id"))
	cal.CreatedAt = new_date

	request, err := h.DB.Update(cal)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error(), "stage": "2"})
		return
	}
	new_date_time := utils.Date(new_date)
	log.Printf("Update: %+v %+v \n", new_date, new_date_time)

	c.JSON(http.StatusOK, request)
}

// @title delete feed entry
// @BasePath /v1/feed
// @Summary delete feed entry
// @Schemes http
// @Description  delete feed entry per click
// @Tags        feed
// @Accept      json
// @Produce     json
// @Failure     400 {string} ERROR
// @Router      /v1/feed/delete/:id [delete]
func (h *CalendarHandler) Delete(c *gin.Context) {
	if h.DB == nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "server database error", "stage": "0"})
		return
	}

	var cal models.Feed
	var request interface{}
	var err error
	var all []models.Feed

	cal.ID, _ = strconv.Atoi(c.Param("id"))

	if c.Param("id") == "all" {
		all, err = h.DB.GetAll()
		request, err = h.DB.DeleteAll(all)
	} else {
		request, err = h.DB.Delete(cal)
	}
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error(), "stage": "2"})
		return
	}

	log.Printf("Delete: %+v \n", request)

	c.JSON(http.StatusOK, request)
}
