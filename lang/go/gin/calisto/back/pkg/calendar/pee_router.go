package calendar

import (
	"github.com/gin-gonic/gin"
)

func PeeRouter(calendar PeeHandlerInterface, r *gin.Engine) error {
	// private router
	calendar_private := r.Group("/v1/pee")
	{
		calendar_private.DELETE("/delete/:id", calendar.DeletePee)
		calendar_private.PUT("/update/:id", calendar.UpdatePee)
		calendar_private.PUT("/add", calendar.AddPee)
		calendar_private.GET("/get/:id", calendar.GetPee)
		calendar_private.GET("/get/all", calendar.GetAllPee)
	}
	return nil
}
