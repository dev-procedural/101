package calendar

import (
	"log"
	"micro/internal/models"
	"strconv"

	"micro/config"
	"micro/pkg/db"
	"micro/pkg/utils"
	"net/http"

	"github.com/gin-gonic/gin"
)

type PeeHandler struct {
	DB db.PeeDBLayer
}

type PeeHandlerInterface interface {
	AddPee(c *gin.Context)
	GetPee(c *gin.Context)
	GetAllPee(c *gin.Context)
	DeletePee(c *gin.Context)
	UpdatePee(c *gin.Context)
}

func NewPeeHandler() (PeeHandlerInterface, error) {

	CONF := config.DBConfig()

	db, err := db.NewORM(CONF.DBStringer())
	db.AutoMigrate(&models.Calendar{}, &models.Pee{})

	if err != nil {
		return nil, err
	}

	return &PeeHandler{DB: db}, nil
}

// ________________________________[[  pee  ]]
// @title pee api
// @BasePath /v1/pee
// mail godoc
// @Summary add new pee entry
// @Schemes http
// @Description  add new pee entry per click
// @Tags        pee
// @Accept      json
// @Produce     json
// @Failure     400 {string} ERROR
// @Router      /v1/pee/add [put]
func (h *PeeHandler) AddPee(c *gin.Context) {
	if h.DB == nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "server database error", "stage": "0"})
		return
	}

	var cal models.Pee

	request, err := h.DB.AddPee(cal)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error(), "stage": "2"})
		return
	}
	log.Printf("Added %#v - time %#v \n", request, utils.Date(request.CreatedAt))
	c.JSON(http.StatusOK, request)
}

// @title pee api
// @BasePath /v1/pee
// @Summary get pee entry
// @Schemes http
// @Description  get pee entry per click
// @Tags        pee
// @Accept      json
// @Produce     json
// @Failure     400 {string} ERROR
// @Router      /v1/pee/get/:id [get]
func (h *PeeHandler) GetPee(c *gin.Context) {
	if h.DB == nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "server database error", "stage": "0"})
		return
	}

	var cal models.Pee

	cal.ID, _ = strconv.Atoi(c.Param("id"))

	request, err := h.DB.GetPee(cal)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error(), "stage": "2"})
		return
	}
	log.Printf("Get: %#v \n", request)
	c.JSON(http.StatusOK, request)
}

// @title pee api
// @BasePath /v1/pee
// @Summary get pee entry
// @Schemes http
// @Description  get all pee entries per click
// @Tags        pee
// @Accept      json
// @Produce     json
// @Failure     400 {string} ERROR
// @Router      /v1/pee/get/all [get]
func (h *PeeHandler) GetAllPee(c *gin.Context) {
	if h.DB == nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "server database error", "stage": "0"})
		return
	}

	var request []models.Pee
	request, err := h.DB.GetAllPee()
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error(), "stage": "2"})
		return
	}
	c.JSON(http.StatusOK, request)
}

// @title pee api
// @BasePath /v1/pee
// @Summary update pee entry
// @Schemes http
// @Description  update pee entry per click
// @Tags        pee
// @Accept      json
// @Produce     json
// @Param       created_at  query string true "created time unix/epoch"
// @Failure     400 {string} ERROR
// @Router      /v1/pee/update/:id [put]
func (h *PeeHandler) UpdatePee(c *gin.Context) {
	if h.DB == nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "server database error", "stage": "0"})
		return
	}

	var cal models.Pee
	var new_date int64

	new_date, _ = strconv.ParseInt(c.PostForm("created_at"), 10, 64)

	// check size
	if len(c.PostForm("created_at")) != 10 {
		c.JSON(http.StatusNotFound, gin.H{"error": "String size incorrect, must be 10", "stage": "1"})
		return
	}

	cal.ID, _ = strconv.Atoi(c.Param("id"))
	cal.CreatedAt = new_date

	request, err := h.DB.UpdatePee(cal)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error(), "stage": "2"})
		return
	}
	new_date_time := utils.Date(new_date)
	log.Printf("Update: %+v %+v \n", new_date, new_date_time)

	c.JSON(http.StatusOK, request)
}

// @title delete pee entry
// @BasePath /v1/pee
// @Summary delete pee entry
// @Schemes http
// @Description  delete pee entry per click
// @Tags        pee
// @Accept      json
// @Produce     json
// @Failure     400 {string} ERROR
// @Router      /v1/pee/delete/:id [delete]
func (h *PeeHandler) DeletePee(c *gin.Context) {
	if h.DB == nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "server database error", "stage": "0"})
		return
	}

	var cal models.Pee
	var request interface{}
	var err error
	var all []models.Pee

	cal.ID, _ = strconv.Atoi(c.Param("id"))

	if c.Param("id") == "all" {
		all, err = h.DB.GetAllPee()
		request, err = h.DB.DeleteAllPee(all)
	} else {
		request, err = h.DB.DeletePee(cal)
	}
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error(), "stage": "2"})
		return
	}

	log.Printf("Delete: %+v \n", request)

	c.JSON(http.StatusOK, request)
}
