package rest

import (
	"fmt"
	. "micro/pkg/calendar"
	"os"

	_ "micro/config"
	_ "micro/docs"

	"github.com/gin-gonic/gin"
	cors "github.com/rs/cors/wrapper/gin"

	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

func RunAPI() (*gin.Engine, error) {
	calendar, err := NewcalendarHandler()
	pee, err := NewPeeHandler()
	if err != nil {
		return nil, err
	}

	return RunAPIWithHandler(calendar, pee), nil
}

func RunAPIWithHandler(calendar_ CalendarHandlerInterface, pee PeeHandlerInterface) *gin.Engine {

	//Get gin's default engine
	r := gin.Default()

	r.Use(gin.Logger())
	r.Use(cors.AllowAll())
	// r.Use(gin.LoggerWithFormatter(models.CustomLog))

	FeedRouter(calendar_, r)
	PeeRouter(pee, r)
	calendar_URL := os.Getenv("calendar_URL")

	url := ginSwagger.URL(fmt.Sprintf("http://%s/swagger/doc.json", calendar_URL))
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))

	return r
}
