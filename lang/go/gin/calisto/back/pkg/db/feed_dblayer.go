package db

import (
	"micro/internal/models"
)

type FeedDBLayer interface {
	Add(models.Feed) (models.Feed, error)
	Get(models.Feed) (models.Feed, error)
	GetAll() ([]models.Feed, error)
	Delete(models.Feed) (models.Feed, error)
	DeleteAll([]models.Feed) ([]models.Feed, error)
	Update(models.Feed) (models.Feed, error)
}

// ________________[ FEED ]
func (db *DBORM) Add(calendar models.Feed) (models.Feed, error) {
	err := db.Create(&calendar).Error
	return calendar, err
}

func (db *DBORM) Get(calendar models.Feed) (models.Feed, error) {
	err := db.Where(&models.Feed{ID: calendar.ID}).Find(&calendar).Error
	return calendar, err
}

func (db *DBORM) GetAll() (calendar []models.Feed, err error) {
	err = db.Find(&calendar).Error
	return calendar, err
}

func (db *DBORM) Update(calendar models.Feed) (models.Feed, error) {
	err := db.Model(&calendar).Updates(
		models.Feed{
			ID:        calendar.ID,
			CreatedAt: calendar.CreatedAt,
		}).Error
	return calendar, err
}

func (db *DBORM) Delete(calendar models.Feed) (models.Feed, error) {
	err := db.Unscoped().Delete(&calendar).Error
	return calendar, err
}

func (db *DBORM) DeleteAll(calendar []models.Feed) ([]models.Feed, error) {
	err := db.Unscoped().Delete(&calendar).Error
	return calendar, err
}
