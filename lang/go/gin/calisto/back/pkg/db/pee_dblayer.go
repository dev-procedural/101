package db

import (
	"micro/internal/models"
)

type PeeDBLayer interface {
	AddPee(models.Pee) (models.Pee, error)
	GetPee(models.Pee) (models.Pee, error)
	GetAllPee() ([]models.Pee, error)
	DeletePee(models.Pee) (models.Pee, error)
	DeleteAllPee([]models.Pee) ([]models.Pee, error)
	UpdatePee(models.Pee) (models.Pee, error)
}

// ________________[ FEED ]
func (db *DBORM) AddPee(calendar models.Pee) (models.Pee, error) {
	err := db.Create(&calendar).Error
	return calendar, err
}

func (db *DBORM) GetPee(calendar models.Pee) (models.Pee, error) {
	err := db.Where(&models.Pee{ID: calendar.ID}).Find(&calendar).Error
	return calendar, err
}

func (db *DBORM) GetAllPee() (calendar []models.Pee, err error) {
	err = db.Find(&calendar).Error
	return calendar, err
}

func (db *DBORM) UpdatePee(calendar models.Pee) (models.Pee, error) {
	err := db.Model(&calendar).Updates(
		models.Pee{
			ID:        calendar.ID,
			CreatedAt: calendar.CreatedAt,
		}).Error
	return calendar, err
}

func (db *DBORM) DeletePee(calendar models.Pee) (models.Pee, error) {
	err := db.Unscoped().Delete(&calendar).Error
	return calendar, err
}

func (db *DBORM) DeleteAllPee(calendar []models.Pee) ([]models.Pee, error) {
	err := db.Unscoped().Delete(&calendar).Error
	return calendar, err
}
