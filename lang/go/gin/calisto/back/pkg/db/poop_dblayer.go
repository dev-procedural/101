package db

import (
	"micro/internal/models"
)

type PoopDBLayer interface {
	AddPoop(models.Poop) (models.Poop, error)
	GetPoop(models.Poop) (models.Poop, error)
	GetAllPoop() ([]models.Poop, error)
	DeletePoop(models.Poop) (models.Poop, error)
	UpdatePoop(models.Poop) (models.Poop, error)
}

// ________________[ FEED ]
func (db *DBORM) AddPoop(calendar models.Poop) (models.Poop, error) {
	err := db.Create(&calendar).Error
	return calendar, err
}

func (db *DBORM) GetPoop(calendar models.Poop) (models.Poop, error) {
	err := db.Where(&models.Poop{ID: calendar.ID}).Find(&calendar).Error
	return calendar, err
}

func (db *DBORM) GetAllPoop() (calendar []models.Poop, err error) {
	err = db.Find(&calendar).Error
	return calendar, err
}

func (db *DBORM) UpdatePoop(calendar models.Poop) (models.Poop, error) {
	err := db.Model(&calendar).Updates(
		models.Poop{
			ID:        calendar.ID,
			CreatedAt: calendar.CreatedAt,
		}).Error
	return calendar, err
}

func (db *DBORM) DeletePoop(calendar models.Poop) (models.Poop, error) {
	err := db.Unscoped().Delete(&calendar).Error
	return calendar, err
}
