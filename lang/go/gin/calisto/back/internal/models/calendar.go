package models

import (
	// "gorm.io/gorm"
	"time"
)

type Calendar struct {
	// gorm.Model
	ID        int `gorm:"primary_key;autoIncrement;index" json:"id"`
	CreatedAt time.Time
}

type Feed struct {
	ID        int   `gorm:"primary_key;autoIncrement;index" json:"id"`
	CreatedAt int64 `gorm:"unique,autoCreateTime,size:10" json:"created_at"`
}

type Poop struct {
	ID        int   `gorm:"primary_key;autoIncrement;index" json:"id"`
	CreatedAt int64 `gorm:"unique,autoCreateTime,size:10" json:"created_at"`
}

type Pee struct {
	ID        int   `gorm:"primary_key;autoIncrement;index" json:"id"`
	CreatedAt int64 `gorm:"unique,autoCreateTime,size:10" json:"created_at"`
}

// func (*Calendar) TableName() string {
// 	return "calendar"
// }
