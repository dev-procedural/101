import { createRouter, createWebHistory } from "vue-router";
// import SideMenu from "../layouts/SideMenu/SideMenu.vue";
// import SimpleMenu from "../layouts/SimpleMenu/SimpleMenu.vue";
import TopMenu from "../layouts/TopMenu/TopMenu.vue";
// import Page1 from "../pages/Tabulator.vue";
import Feed from "../pages/Feed.vue";
import Pee from "../pages/Pee.vue";

const routes = [
  {
    path: "/",
    component: TopMenu,
    children: [
      {
        path: "/",
        name: "Feed",
        component: Feed,
      },
      {
        path: "/pee",
        name: "Pee",
        component: Pee,
      },
    ],
  },
  // {
  //   path: "/",
  //   component: TopMenu,
  //   children: [
  //     {
  //       path: "page-2",
  //       name: "top-menu-page-2",
  //       component: Pee,
  //     },
  //   ],
  // },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
  scrollBehavior(to, from, savedPosition) {
    return savedPosition || { left: 0, top: 0 };
  },
});

export default router;
