package config

import (
	"fmt"
	"os"
	"path/filepath"
	"runtime"

	"github.com/spf13/viper"
)

var (
	_, b, _, _ = runtime.Caller(0)
	basepath   = filepath.Dir(b)
)

// type Configs struct {
// 	DB    DBConf    `mapstructure:"db"`
// 	CACHE CACHEConf `mapstructure:"cache"`
// }

type DBConf struct {
	DB_USER string `mapstructure:"DB_USER"`
	DB_PASS string `mapstructure:"DB_PASS"`
	DB_URL  string `mapstructure:"DB_URL"`
	DB_NAME string `mapstructure:"DB_NAME"`
	DB_PORT string `mapstructure:"DB_PORT"`
	DB_PATH string `mapstructure:"DB_PATH"`
	DB_TYPE string `mapstructure:"DB_TYPE"` //myslq | postgres | sqlite
}

type CACHEConf struct {
	CACHE_USER string `mapstructure:"CACHE_USER"`
	CACHE_PASS string `mapstructure:"CACHE_PASS"`
	CACHE_URL  string `mapstructure:"CACHE_URL"`
	CACHE_NAME string `mapstructure:"CACHE_NAME"`
	CACHE_PORT string `mapstructure:"CACHE_PORT"`
	CACHE_TYPE string `mapstructure:"CACHE_TYPE"` // redis | memory
}

// DB
func init() {
	// var config *Configs
	var DB *DBConf
	var CACHE *CACHEConf

	viper.SetConfigName("config.env")
	viper.AddConfigPath(basepath)
	viper.SetConfigType("env")
	err := viper.ReadInConfig()
	if err != nil { // Handle errors reading the config file
		panic(fmt.Errorf("fatal error config file: %w", err))
	}

	err = viper.ReadInConfig()
	if err != nil {
		return
	}

	viper.AutomaticEnv()
	viper.WatchConfig()

	fmt.Printf("[?] config: %#v\n", viper.AllSettings())

	err = viper.Unmarshal(&DB)
	err = viper.Unmarshal(&CACHE)
	os.Setenv("DB_USER", DB.DB_USER)
	os.Setenv("DB_PASS", DB.DB_PASS)
	os.Setenv("DB_URL", DB.DB_URL)
	os.Setenv("DB_NAME", DB.DB_NAME)
	os.Setenv("DB_PORT", DB.DB_PORT)
	os.Setenv("DB_TYPE", DB.DB_TYPE)
	os.Setenv("DB_PATH", DB.DB_PATH)

	os.Setenv("CACHE_USER", CACHE.CACHE_USER)
	os.Setenv("CACHE_PASS", CACHE.CACHE_PASS)
	os.Setenv("CACHE_URL", CACHE.CACHE_URL)
	os.Setenv("CACHE_NAME", CACHE.CACHE_NAME)
	os.Setenv("CACHE_PORT", CACHE.CACHE_PORT)
	os.Setenv("CACHE_TYPE", CACHE.CACHE_TYPE)
	os.Setenv("CRUD_URL", fmt.Sprintf("%s:%s", viper.Get("CRUD_SERVER_HOST").(string), viper.Get("CRUD_SERVER_PORT").(string)))

}

func DBConfig() *DBConf {
	return &DBConf{
		DB_USER: os.Getenv("DB_USER"),
		DB_NAME: os.Getenv("DB_NAME"),
		DB_URL:  os.Getenv("DB_URL"),
		DB_PASS: os.Getenv("DB_PASS"),
		DB_PORT: os.Getenv("DB_PORT"),
		DB_TYPE: os.Getenv("DB_TYPE"),
		DB_PATH: os.Getenv("DB_PATH"),
	}
}

func (c *DBConf) DBStringer() string {
	switch {

	case c.DB_TYPE == "mysql":
		return fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", c.DB_USER, c.DB_PASS, c.DB_URL, c.DB_PORT, c.DB_NAME)

	case c.DB_TYPE == "postgres":
		return fmt.Sprintf("user=%s password=%s dbname=%s host=%s port=%s sslmode=disable", c.DB_USER, c.DB_PASS, c.DB_NAME, c.DB_URL, c.DB_PORT)

	case c.DB_TYPE == "sqlite":
		return fmt.Sprintf("%s", c.DB_PATH)

	default:
		return "TYPE empty"

	}
}

func CACHEConfig() *CACHEConf {
	return &CACHEConf{
		CACHE_USER: os.Getenv("CACHE_USER"),
		CACHE_NAME: os.Getenv("CACHE_NAME"),
		CACHE_URL:  os.Getenv("CACHE_URL"),
		CACHE_PASS: os.Getenv("CACHE_PASS"),
		CACHE_PORT: os.Getenv("CACHE_PORT"),
		CACHE_TYPE: os.Getenv("CACHE_TYPE"),
	}
}

func (c *CACHEConf) CACHEStringer() string {
	switch {
	case c.CACHE_TYPE == "redis":
		return fmt.Sprintf("user=%s password=%s dbname=%s host=%s port=%s sslmode=disable", c.CACHE_USER, c.CACHE_PASS, c.CACHE_NAME, c.CACHE_URL, c.CACHE_PORT)

	default:
		return "TYPE empty"

	}
}
