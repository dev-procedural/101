package cache

import (
	"errors"
	"os"

	cfg "micro/config"

	"github.com/gin-contrib/sessions/cookie"
	"github.com/gin-contrib/sessions/redis"
)

type CACHE struct {
	redis  redis.Store
	cookie cookie.Store
}

func NewCACHE() (*CACHE, error) {

	CONF := cfg.CACHEConfig()
	// os.Setenv("CACHE_TYPE", "redis")
	// log.Printf("[!!!] %#v", os.Getenv("CACHE_TYPE"))

	switch {
	case os.Getenv("CACHE_TYPE") == "redis":
		// store := cookie.NewStore([]byte("secret"))
		store, err := redis.NewStore(10, "tcp", CONF.CACHE_URL+":"+CONF.CACHE_PORT, "", []byte(""))
		return &CACHE{redis: store}, err

	case os.Getenv("CACHE_TYPE") == "memory":
		store := cookie.NewStore([]byte("secret"))
		return &CACHE{cookie: store}, nil
	}

	return &CACHE{}, errors.New("Err not reaching cache")

}
