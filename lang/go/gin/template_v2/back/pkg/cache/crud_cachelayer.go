package cache

import (
	"micro/internal/models"
)

type CrudCACHELayer interface {
	Add(models.Crud) (models.Crud, error)
	Get(models.Crud) (models.Crud, error)
	GetAll() ([]models.Crud, error)
	Delete(models.Crud) (models.Crud, error)
	DeleteAll([]models.Crud) ([]models.Crud, error)
	Update(models.Crud) (models.Crud, error)
}

// ________________[ Crud ]
func (db *CACHE) Add(crud models.Crud) (models.Crud, error) {
	// err := db.Create(&crud).Error
	// return crud, err
	return crud, nil
}

func (db *CACHE) Get(crud models.Crud) (models.Crud, error) {
	// err := db.Where(&models.Crud{ID: crud.ID}).Find(&crud).Error
	// return crud, err
	return crud, nil
}

func (db *CACHE) GetAll() (crud []models.Crud, err error) {
	// err = db.Find(&crud).Error
	// return crud, err
	return crud, nil
}

func (db *CACHE) Update(crud models.Crud) (models.Crud, error) {
	// err := db.Model(&crud).Updates(
	// 	models.Crud{
	// 		ID:        crud.ID,
	// 		CreatedAt: crud.CreatedAt,
	// 	}).Error
	// return crud, err
	return crud, nil
}

func (db *CACHE) Delete(crud models.Crud) (models.Crud, error) {
	// err := db.Unscoped().Delete(&crud).Error
	// return crud, err
	return crud, nil
}

func (db *CACHE) DeleteAll(crud []models.Crud) ([]models.Crud, error) {
	// err := db.Unscoped().Delete(&crud).Error
	// return crud, err
	return crud, nil
}
