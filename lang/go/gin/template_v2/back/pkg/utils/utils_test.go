package utils

import (
	"flag"
	"log"
	"testing"
)

var HASH = flag.String("h", "", "this is a hash")
var PASS = flag.String("p", "", "this is a password")

var RES string

func TestUtils(t *testing.T) {

	t.Run("test hashpass", func(t *testing.T) {
		RES, _ = HashPassword(*PASS)
		log.Printf("%#v", RES)
	})

	t.Run("test checkpass", func(t *testing.T) {
		result := CheckPasswordHash(*PASS, RES)
		log.Printf("%#v", result)
	})
}

func TestUtilsManual(t *testing.T) {

	t.Run("test checkpass", func(t *testing.T) {
		result := CheckPasswordHash(*PASS, *HASH)
		log.Printf(">>> %#v", result)
	})
}
