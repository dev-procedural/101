package utils

import (
	"fmt"
	"time"

	"golang.org/x/crypto/bcrypt"
)

func Date(unix int64) string {

	loc, _ := time.LoadLocation("EST")
	now := time.Unix(unix, 0).In(loc)
	new_date_time, _ := time.ParseInLocation(time.RFC3339, now.Format(time.RFC3339), loc)

	new_date_time_ := fmt.Sprintf("%+v", new_date_time)

	return new_date_time_
}

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword(
		[]byte(hash),
		[]byte(password),
	)
	return err == nil
}
