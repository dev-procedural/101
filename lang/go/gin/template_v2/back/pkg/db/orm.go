package db

import (
	"database/sql"
	"errors"
	"os"

	"gorm.io/driver/postgres"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

type DBORM struct {
	*gorm.DB
}

// type CrudDBLayer interface {
// 	FeedDBLayer
// }

func NewORM(config string) (*DBORM, error) {

	var db *gorm.DB
	var err error

	switch {
	case os.Getenv("DB_TYPE") == "postgres":
		sqlDB, _ := sql.Open("pgx", config)
		defer sqlDB.Close()

		db, err = gorm.Open(postgres.New(
			postgres.Config{
				PreferSimpleProtocol: true,
				Conn:                 sqlDB}),
			&gorm.Config{})

		return &DBORM{db}, err

	case os.Getenv("DB_TYPE") == "sqlite":
		db, err := gorm.Open(sqlite.Open(config), &gorm.Config{})

		return &DBORM{db}, err
	}

	return &DBORM{}, errors.New("Err not reaching endpoint")

}
