package rest

import (
	"fmt"
	"log"
	"micro/pkg/cache"
	. "micro/pkg/crud"
	"os"

	_ "micro/docs"

	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/redis"
	"github.com/gin-gonic/gin"
	cors "github.com/rs/cors/wrapper/gin"

	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

func RunAPI() (*gin.Engine, error) {
	crud, err := NewCrudHandler()
	// pee, err := NewPeeHandler()
	if err != nil {
		log.Fatalf("[?] err: %v#", err.Error())
		return nil, err
	}

	return RunAPIWithHandler(crud), nil
}

func RunAPIWithHandler(crud_ CrudHandlerInterface) *gin.Engine {

	// CONF := cfg.CACHEConfig()
	store1, err := cache.NewCACHE()
	log.Printf("cache: %T", *store1)
	log.Printf(">>> %#v", store1)
	// store := cookie.NewStore([]byte("secret"))
	store, err := redis.NewStore(10, "tcp", "192.168.2.239:6379", "", []byte(""))
	if err != nil {
		fmt.Printf(">>> %s", err.Error())
	}
	r := gin.Default()

	r.Use(cors.AllowAll())
	r.Use(sessions.Sessions("Session", store))
	// r.Use(gin.LoggerWithFormatter(models.CustomLog))

	CrudRouter(crud_, r)
	CRUD_URL := os.Getenv("CRUD_URL")

	url := ginSwagger.URL(fmt.Sprintf("http://%s/swagger/doc.json", CRUD_URL))
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))

	return r
}
