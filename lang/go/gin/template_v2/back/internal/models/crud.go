package models

// "gorm.io/gorm"

type Crud struct {
	// gorm.Model
	ID        int   `gorm:"primary_key;autoIncrement;index" json:"id"`
	CreatedAt int64 `gorm:"unique,autoCreateTime,size:10" json:"created_at"`
}

type User struct {
	// gorm.Model
	ID        int    `gorm:"primary_key;autoIncrement;index" json:"id"`
	Username  string `gorm:"unique" json:"username"`
	Email     string `gorm:"unique,default:null" json:"email,omitempty"`
	Password  string `json:"password"`
	Role      string `gorm:"default:user" json:"role,omitempty"`
	CreatedAt int64  `gorm:"unique,autoCreateTime,size:10" json:"created_at"`
}

// func (*Crud) TableName() string {
// 	return "calendar"
// }
