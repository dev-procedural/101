package middleware

import (
	"log"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
)

type Claims struct {
	Username string `json:"username"`
	jwt.StandardClaims
}

func AuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Session validation
		// log.Printf("[!] Request: %#v", c.Keys)
		var tokenValue string
		claims := &Claims{}

		session := sessions.Default(c)
		sessionJwt := session.Get("jwt")
		sessionToken := session.Get("token")
		if sessionToken == nil && sessionJwt == nil {
			c.JSON(http.StatusForbidden, gin.H{
				"message": "Not logged",
			})
			c.Abort()
		}
		// JWT token validation
		if c.GetHeader("Authorization") != "" {
			tokenValue = c.GetHeader("Authorization")
		} else {
			tokenValue = sessionJwt.(string)
		}

		tkn, err := jwt.ParseWithClaims(tokenValue, claims, func(token *jwt.Token) (interface{}, error) {
			return []byte("secret"), nil
		})
		// TODO ADD viper
		log.Printf("[!] TKN: %#v", tkn.Claims)
		if err != nil {
			c.AbortWithStatus(http.StatusUnauthorized)
		}

		if !tkn.Valid {
			c.AbortWithStatus(http.StatusUnauthorized)
		}

		c.Next()
	}
}
