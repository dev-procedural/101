/*
Copyright © 2023
*/
package cli

import (
	"fmt"
	"log"
	"micro/pkg/rest"
	"os"

	"github.com/spf13/cobra"
)

// apiCmd represents the api command
var serverCmd = &cobra.Command{
	Use:   "api",
	Short: "api run server",
	Long:  `run server with all endpoints activated`,
	Run: func(cmd *cobra.Command, args []string) {

		VB, _ := cmd.Flags().GetBool("verbose")
		VERBOSE := map[bool]string{true: fmt.Sprintf("[!] %s", os.Environ()), false: ""}[VB]

		API, _ := rest.RunAPI()
		log.Printf("Main log...\n%#v", VERBOSE)
		log.Fatal(API.Run(os.Getenv("CRUD_URL")))
	},
}

func init() {
	rootCmd.AddCommand(serverCmd)

	// apiCmd.PersistentFlags().String("foo", "", "A help for foo")
	serverCmd.Flags().BoolP("verbose", "v", false, "verbose")
	serverCmd.Flags().BoolP("test", "t", false, "testing version")
}
