/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package cli

import (
	"fmt"
	"log"

	"micro/config"
	"micro/internal/models"
	"micro/pkg/db"
	"micro/pkg/utils"

	"github.com/spf13/cobra"
)

type DB struct {
	db.CrudDBLayer
}

var u string
var p string
var d bool

// adminCmd represents the admin command
var adminCmd = &cobra.Command{
	Use:   "admin",
	Short: "common procedures",
	Long:  `add users, remove users, etc`,
	// Args:  cobra.MinimumNArgs(1),
}

var userCmd = &cobra.Command{
	Use:   "user",
	Short: "interact with db users",
	Args:  cobra.MinimumNArgs(1),
}

var addCmd = &cobra.Command{
	Use:   "add",
	Short: "add users",
	Run: func(cmd *cobra.Command, args []string) {
		db := initDB()

		password, err := utils.HashPassword(p)
		if err != nil {
			log.Fatalf("%s", err.Error())
		}
		user := models.User{
			Username: u,
			Password: password,
		}
		if u == "admin" {
			user.Role = "admin"
		}
		_, err = db.Register(user)
		if err != nil {
			log.Fatalf("%s", err.Error())
			return
		}
		fmt.Printf("%#v", user)
	},
}

var getCmd = &cobra.Command{
	Use:   "get",
	Short: "get users",
	Run: func(cmd *cobra.Command, args []string) {
		db := initDB()

		user := models.User{
			Username: u,
		}

		result, err := db.GetUser(user)
		if err != nil {
			log.Fatalf("%s", err.Error())
			return
		}

		var check bool
		if p != "" {
			check = utils.CheckPasswordHash(p, user.Password)
		}

		fmt.Printf("%#v %#v", result, check)
	},
}

var rmCmd = &cobra.Command{
	Use:   "rm",
	Short: "remove users",
	Run: func(cmd *cobra.Command, args []string) {
		// db := initDB()
		fmt.Println("print rm called")
	},
}

var resetCmd = &cobra.Command{
	Use:   "reset",
	Short: "reset password, arg is string with new password",
	Run: func(cmd *cobra.Command, args []string) {
		db := initDB()
		password, _ := utils.HashPassword(p)
		user := models.User{
			Username: u,
		}

		user_in_db, err := db.GetUser(user)
		if err != nil {
			log.Fatalf("[?] %s", err.Error())
			return
		}

		new_user := models.User{
			Username: u,
			Password: password,
		}

		_, err = db.UpdateUser(new_user)
		if err != nil {
			log.Fatalf("[?] %s", err.Error())
			return
		}
		user_in_db, err = db.GetUser(user)
		if err != nil {
			log.Fatalf("[?] %s", err.Error())
			return
		}
		log.Printf("UPDATED: %#v", user_in_db)
	},
}

func initDB() db.CrudDBLayer {

	CONFDB := config.DBConfig()

	db, err := db.NewORM(CONFDB.DBStringer())
	if err != nil {
		log.Fatalf("[?] Err: %#v", err)
	}
	return db
}

func init() {

	rootCmd.AddCommand(adminCmd)
	adminCmd.AddCommand(userCmd)
	// adminCmd.MarkFlagRequired("user")
	// adminCmd.MarkFlagRequired("password")

	resetCmd.Flags().StringVarP(&u, "user", "u", "", "admin user")
	resetCmd.Flags().StringVarP(&p, "password", "p", "", "admin password")
	resetCmd.Flags().BoolVarP(&d, "default", "d", false, "default credentials")
	resetCmd.MarkFlagRequired("user")
	resetCmd.MarkFlagRequired("password")

	addCmd.Flags().StringVarP(&u, "user", "u", "", "admin user")
	addCmd.Flags().StringVarP(&p, "password", "p", "", "admin password")
	addCmd.Flags().BoolVarP(&d, "default", "d", false, "default credentials")
	addCmd.MarkFlagRequired("user")
	addCmd.MarkFlagRequired("password")

	getCmd.Flags().StringVarP(&u, "user", "u", "", "admin user")
	getCmd.Flags().BoolVarP(&d, "default", "d", false, "default credentials")
	getCmd.MarkFlagRequired("user")
	// getCmd.MarkFlagRequired("password")

	userCmd.AddCommand(addCmd)
	userCmd.AddCommand(getCmd)
	userCmd.AddCommand(rmCmd)
	userCmd.AddCommand(resetCmd)
}
