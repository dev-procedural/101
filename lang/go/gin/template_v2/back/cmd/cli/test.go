/*
Copyright © 2023
*/
package cli

import (
	"log"

	"github.com/spf13/cobra"
)

// apiCmd represents the api command
var testCmd = &cobra.Command{
	Use:   "test",
	Short: "api run server tests",
	Long:  `run server with all endpoints activated in test mode`,
	Run: func(cmd *cobra.Command, args []string) {

		log.Println("[!] run tests")

	},
}

func init() {
	rootCmd.AddCommand(testCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// apiCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// testCmd.Flags().BoolP("verbose", "v", false, "verbose")
}
