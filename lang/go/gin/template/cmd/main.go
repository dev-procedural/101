package main

import (
	"log"
	"micro/pkg/rest"
)

func main() {

	API, _ := rest.RunAPI()

	log.Println("Main log...")
	log.Fatal(API.Run("localhost:8081"))
}
