package YOUR_SERVICE

import (
	"github.com/gin-gonic/gin"
)

func YOUR_SERVICERouter(YOUR_SERVICE YOUR_SERVICEHandlerInterface, r *gin.Engine) error {
	// private router
	cases_private := r.Group("/v1/YOUR_SERVICE")
	{
		cases_private.DELETE("/delete/:id", YOUR_SERVICE.Delete)
		cases_private.PUT("/update/:id", YOUR_SERVICE.Update)
		cases_private.POST("/add", YOUR_SERVICE.Add)
	}
	return nil
}
