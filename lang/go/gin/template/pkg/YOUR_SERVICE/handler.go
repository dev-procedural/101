package YOUR_SERVICE

import (
	"micro/internal/models"

	"micro/config"
	"micro/pkg/db"
	"net/http"

	"github.com/gin-gonic/gin"
)

type YOUR_SERVICEHandler struct {
	DB db.YOUR_SERVICEDBLayer
}

type YOUR_SERVICEHandlerInterface interface {
	Add(c *gin.Context)
	Delete(c *gin.Context)
	Update(c *gin.Context)
}

func NewYOUR_SERVICEHandler() (YOUR_SERVICEHandlerInterface, error) {

	CONF := config.DBConfig()

	db, err := db.NewORM(CONF.DBStringer())
	db.AutoMigrate(&models.YOUR_SERVICE{})

	if err != nil {
		return nil, err
	}

	return &YOUR_SERVICEHandler{DB: db}, nil
}

// ________________________________[[  CASE  ]]
// PRODUCER
// @title Org api
// @BasePath /v1/case
// mail godoc
// @Summary add new case
// @Schemes http
// @Description add new case and send notification, also this is the producer for the consumer
// @Tags        case
// @Accept      json
// @Produce     json
// @Param  type query string true "unknown, family legal, civil matter, criminal defence, civil rights, immigrant, indigenous"
// @Param  useremail query string true "useremail"
// @Param  cookie query string true "cookie bearer token"
// @Failure     400 {string} ERROR
// @Router      /v1/case/add [post]
func (h *YOUR_SERVICEHandler) Add(c *gin.Context) {
	if h.DB == nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "server database error", "stage": "0"})
		return
	}

}

func (h *YOUR_SERVICEHandler) Update(c *gin.Context) {
	if h.DB == nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "server database error", "stage": "0"})
		return
	}

}

func (h *YOUR_SERVICEHandler) Delete(c *gin.Context) {
	if h.DB == nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "server database error", "stage": "0"})
		return
	}
}
