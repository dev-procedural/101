package db

import (
	"micro/internal/models"
)

type YOUR_SERVICEDBLayer interface {
	//ORG
	Add(models.YOUR_SERVICE) (models.YOUR_SERVICE, error)
	Delete(models.YOUR_SERVICE) (models.YOUR_SERVICE, error)
	Update(models.YOUR_SERVICE) (models.YOUR_SERVICE, error)
}

// ________________[ ORG ]
func (db *DBORM) Add(YOUR_SERVICE models.YOUR_SERVICE) (models.YOUR_SERVICE, error) {
	err := db.Create(&YOUR_SERVICE).Error
	return YOUR_SERVICE, err
}

func (db *DBORM) Update(YOUR_SERVICE models.YOUR_SERVICE) (models.YOUR_SERVICE, error) {
	err := db.Model(&YOUR_SERVICE).Updates(
		models.YOUR_SERVICE{
			ID:        YOUR_SERVICE.ID,
			CreatedAt: YOUR_SERVICE.CreatedAt,
		}).Error
	return YOUR_SERVICE, err
}

func (db *DBORM) Delete(YOUR_SERVICE models.YOUR_SERVICE) (models.YOUR_SERVICE, error) {
	err := db.Unscoped().Delete(&YOUR_SERVICE).Error
	return YOUR_SERVICE, err
}
