package rest

import (
	"fmt"
	. "micro/pkg/YOUR_SERVICE"
	"os"

	_ "micro/config"
	_ "micro/docs"
	"micro/internal/models"

	"github.com/gin-gonic/gin"
	cors "github.com/rs/cors/wrapper/gin"

	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

func RunAPI() (*gin.Engine, error) {
	YOUR_SERVICE, err := NewYOUR_SERVICEHandler()
	if err != nil {
		return nil, err
	}

	return RunAPIWithHandler(YOUR_SERVICE), nil
}

func RunAPIWithHandler(YOUR_SERVICE_ YOUR_SERVICEHandlerInterface) *gin.Engine {

	//Get gin's default engine
	r := gin.Default()

	r.Use(gin.Logger())
	r.Use(cors.AllowAll())
	r.Use(gin.LoggerWithFormatter(models.CustomLog))

	YOUR_SERVICERouter(YOUR_SERVICE_, r)
	YOUR_SERVICE_URL := os.Getenv("YOUR_SERVICE_URL")

	url := ginSwagger.URL(fmt.Sprintf("http://%s/swagger/doc.json", YOUR_SERVICE_URL))
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))

	return r
}
