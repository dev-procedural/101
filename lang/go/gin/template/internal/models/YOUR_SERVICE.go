package models

import (
	// "gorm.io/gorm"
	"time"
)

type YOUR_SERVICE struct {
	// gorm.Model
	ID        int `gorm:"primary_key;autoIncrement;index" json:"id"`
	CreatedAt time.Time
}

func (*YOUR_SERVICE) TableName() string {
	return "YOUR_SERVICE"
}
