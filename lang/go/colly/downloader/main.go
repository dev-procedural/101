package main

import (
	"encoding/json"
	"flag"
	"log"
	"os"
	"regexp"
	"strings"
	"time"

	"github.com/gocolly/colly/v2"
	"github.com/joho/godotenv"
)

var (
	BASEURL   string
	COURSE    string
	LOGIN     string
	PWD       string
	USERNAME  string
	DOWNLOADS string
)

func init() {

	flag.StringVar(&COURSE, "c", "", "course name")
	flag.StringVar(&DOWNLOADS, "f", "./downloads/", "download directory")
	flag.Parse()

	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	BASEURL = os.Getenv("BASEURL")
	COURSE = map[bool]string{true: os.Getenv("COURSE"), false: COURSE}[os.Getenv("COURSE") != ""]
	LOGIN = os.Getenv("LOGIN")
	PWD = os.Getenv("PASS")
	USERNAME = os.Getenv("USERNAME")

	if COURSE == "" {
		log.Fatal("[!] no course defined")
	}

	if _, err := os.Stat(DOWNLOADS + COURSE); os.IsNotExist(err) {
		os.MkdirAll(DOWNLOADS+COURSE, 0755)
	}

}

func main() {
	// VARS
	VIDEOS := map[string]string{}
	VIDEOS1 := map[string]string{}
	COURSES := BASEURL + "/courses/" + COURSE
	DOWNLOADS = DOWNLOADS + COURSE + "/"
	// println(BASEURL, COURSE, LOGIN, PWD, USERNAME)

	// GET LOGIN
	c := colly.NewCollector(
		colly.UserAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36"),
	)

	cr := c.Clone()
	cLink := c.Clone()
	cFile := c.Clone()
	cDownload := c.Clone()
	// CONFIGS
	cDownload.MaxBodySize = 5 * 1024 * 1024 * 1024 // GB
	cDownload.SetRequestTimeout(20 * time.Minute)

	err := c.Post(LOGIN, map[string]string{"log": USERNAME, "pwd": PWD})
	if err != nil {
		log.Fatal(err)
	}

	// RESPONSES
	cDownload.OnResponse(func(r *colly.Response) {
		// log.Printf("[S] %#v", r.Request.Ctx)
		DIR := DOWNLOADS
		_, err := os.Stat(DIR)
		if err != nil {
			log.Println("[S]", err.Error())
		} else {
			os.Mkdir(DIR, os.ModePerm)
		}

		log.Printf("[S] Saving file")
		r.Save(DOWNLOADS + r.FileName())
	})

	// ON HTMLS
	c.OnHTML("html", func(e *colly.HTMLElement) {
		err = cr.Request("GET", COURSES, strings.NewReader(""), nil, nil)
		if err != nil {
			log.Fatal(err)
		}
		_ = e.Request.Visit(COURSES)
		cr.Wait()
	})

	cr.OnHTML("a[href*='lesson-']", func(e *colly.HTMLElement) {

		LINK := e.Attr("href")

		if visited, _ := cLink.HasVisited(LINK); !visited {
			log.Printf("[+] %#v", LINK)
			// VIDEOS["a"] = LINK
			err = cLink.Request("GET", LINK, strings.NewReader(""), nil, nil)
			if err != nil {
				log.Fatal(err)
			}
			_ = e.Request.Visit(LINK)
			cLink.Wait()
		}
	})

	// GET COURSE
	cLink.OnHTML("iframe[src*='fast']", func(e *colly.HTMLElement) {
		PATH := strings.Split(e.Request.URL.Path, "/")[2]
		LL := strings.Split(e.Attr("src"), "?")[0]
		VIDEOS[PATH] = LL

		if visited, _ := cFile.HasVisited(LL); !visited {
			err = cFile.Request("GET", LL, strings.NewReader(""), nil, nil)
			if err != nil {
				log.Fatal(err)
			}
			_ = e.Request.Visit(LL)
			cFile.Wait()
		}
	})

	// DOWNLOAD LINKS
	cFile.OnHTML("script", func(e *colly.HTMLElement) {

		var JSONS interface{}

		RE := regexp.MustCompile(`\[([^]]+)\]`).FindAllString(e.Text, -1)
		if len(RE) > 0 {

			json.Unmarshal([]byte(RE[0]), &JSONS)

			GET_FILE_BY_RES := func(JSONS interface{}) string {

				for _, JS := range JSONS.([]interface{}) {
					JSON := JS.(interface{})
					FILE := JSON.(map[string]interface{})["url"]
					HEIGHT := JSON.(map[string]interface{})["height"]

					if HEIGHT == float64(720) {
						// log.Println("[J]", JSON.(map[string]interface{})["url"])
						return FILE.(string)

					}
				}
				return ""
			}

			FILE := GET_FILE_BY_RES(JSONS)
			VIDEOS1[e.Request.URL.Path] = FILE
			log.Printf("[R] %#v", FILE)

			if visited, _ := cDownload.HasVisited(FILE); !visited {
				// log.Println("[!] visiting file", FILE)
				err = cDownload.Request("GET", FILE, strings.NewReader(""), nil, nil)
				if err != nil {
					log.Fatal(err)
				}
				_ = e.Request.Visit(FILE)
				log.Println("[!] visited file", FILE)
				cDownload.Wait()
			}
		}

	})

	c.Visit(BASEURL)

	// PARSING VIDEOS
	VIDS := map[string]string{}

	// CORRECT NAMING
	for k, v := range VIDEOS1 {
		PA := strings.Split(k, "/")[3:]
		for kk, vv := range VIDEOS {
			PATH := strings.Split(vv, "/")[5:]
			if PA[0] == PATH[0] {
				VIDS[kk] = v
			}
		}
	}

	DIR, _ := os.Open(DOWNLOADS)
	FILES, _ := DIR.Readdir(0)
	var FILENAME interface{}

	for LESSON, v := range VIDS {
		FILENAME = strings.Split(v, "/")
		FILENAME = FILENAME.([]string)[len(FILENAME.([]string))-1]

		for _, FILE := range FILES {
			if strings.Contains(FILE.Name(), FILENAME.(string)) {
				os.Rename(DOWNLOADS+FILE.Name(), DOWNLOADS+LESSON)
			}
		}
	}

}
