package main

import (
	"encoding/csv"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
	"strings"
	"time"
	"unicode"

	"github.com/gocolly/colly/v2"
)

var (
	BASEURL    string
	YEAR       string
	FILE       string
	VERBOSE    bool
	OUTER_ONLY bool
	ICAL       bool
)

func init() {

	flag.StringVar(&BASEURL, "u", "https://cafeastrology.com/astrology-of-2024.html", "base url")
	flag.StringVar(&YEAR, "y", "2024", "base url")
	flag.BoolVar(&VERBOSE, "v", false, "verbose")
	flag.BoolVar(&OUTER_ONLY, "o", false, "outer planets only")
	flag.StringVar(&FILE, "f", "/tmp/result_planets.json", "save file")
	flag.BoolVar(&ICAL, "c", false, "export to google cal")
	flag.Parse()

}

type Calendar struct {
	Date  string
	Event string
}

func ExportCalendar(data []map[string]string) {
	headers := []string{"Subject", "Start Date", "Start Time", "Description"}

	file, err := os.Create(FILE)
	if err != nil {
		log.Fatalf("[!] File error %#v", err)
	}
	defer file.Close()

	// creafting container
	writer := csv.NewWriter(file)
	defer writer.Flush()

	writer.Write(headers)

	for _, row := range data {
		date := strings.Split(row["Date"], " ")
		entry := []string{row["Event"], date[0], date[1], row["Event"]}

		log.Printf("[!] %#v", entry)

		err := writer.Write(entry)
		if err != nil {
			log.Fatalf("[!] error %#v", err)
		}
	}

}

func DateConv(date, timer string) string {

	const layout = "Jan 2, 2006 at 3:04 pm MST"
	const layout2 = "January 2, 2006 at 3:04 pm MST"
	var DATE time.Time

	D := ""
	D2 := ""
	tryLayout := func(date, timer string) {
		D = fmt.Sprintf("%s, %s at %s EST", date, YEAR, timer)
		DATE, _ = time.Parse(layout, D)
		if strings.Contains(fmt.Sprint(DATE), "00:00:00") {
			D2 = fmt.Sprintf("%s, %s at %s", date, YEAR, timer)
			DATE, _ = time.Parse(layout2, D2)
		}
	}

	tryLayout(date, timer)
	DATE_S := fmt.Sprint(DATE)

	switch {
	case strings.Contains(DATE_S, "EST"):
		tryLayout(date, timer)
		DATE_S = fmt.Sprint(DATE)
		DATE_ := strings.Replace(fmt.Sprint(DATE), " -0500 EST", "", -1)

		return DATE_

	case strings.Contains(DATE_S, "EDT"):
		tryLayout(date, timer)
		DATE_S = fmt.Sprint(DATE)
		DATE_ := strings.Replace(fmt.Sprint(DATE), " -0400 EDT", "", -1)

		return DATE_
	}

	return ""

}

func DelUnicode(s string) string {

	S := strings.Map(func(r rune) rune {
		if unicode.IsPrint(r) {
			return r
		}
		return -1
	}, s)

	return S

}

func main() {
	// VARS
	ENTRIES := []Calendar{}

	// ADD COLLECTOR
	c := colly.NewCollector(
		colly.UserAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36"),
	)

	// Find and visit all links
	c.OnHTML("tbody tr", func(e *colly.HTMLElement) {
		if OUTER_ONLY {
			return
		}
		// S := strings.Replace(e.Text, "\n", "", -1)
		DATE_ := e.ChildText(".column-1")
		TIME := e.ChildText(".column-2")
		EVENT := e.ChildText(".column-3")

		DATE_ = DelUnicode(DATE_)
		// TIME = strings.Replace(TIME, " ", "", -1)
		TIME = strings.ToLower(TIME)
		DATE := DateConv(DATE_, TIME)

		// re := regexp.MustCompile("^(?P<date>[a-zA-Z]{3} [0-9]{1,2})( )*(?P<time>[0-9]{1,2}:[0-9]{1,2} (AM|PM)+)(?P<event>.*)").FindAllStringSubmatch(S, -1)
		if DATE != "" && EVENT != "" {
			if !strings.Contains(EVENT, "Moon") == true {
				if VERBOSE {
					log.Println("[!]", DATE_, TIME, DATE)
				}
				ENTRIES = append(ENTRIES, Calendar{Date: DATE, Event: EVENT})
			}
		}
	})

	c.OnHTML("p", func(e *colly.HTMLElement) {
		// OUTER PLANETS
		OCCUR := regexp.MustCompile(`[0-9]{2} [a-zA-Z]+:`).FindAllString(e.Text, -1)
		if OCCUR != nil {
			// DATE
			DATE_ := strings.Trim(OCCUR[0], ":")
			DATE_ = DelUnicode(DATE_)
			//TIME
			TIME_ := regexp.MustCompile(`([0-9]{1,2}:[0-9]{1,2})+ (am|pm)+ (EST|EDT)`).FindAllString(e.Text, -1)
			TIME := ""
			if len(TIME_) > 0 {
				TIME = TIME_[0]
			}

			DATE_1 := strings.Split(DATE_, " ")
			DATE_ = fmt.Sprintf("%s %s", DATE_1[1], DATE_1[0])
			DATE := DateConv(DATE_, TIME)
			EVENT := regexp.MustCompile(`(([0-9]{1,2}:[0-9]{1,2})+ (am|pm)+ (EST|EDT)|[0-9]{2} [a-zA-Z]+:)`).ReplaceAllString(e.Text, "")

			if !regexp.MustCompile(`(?i).*summary.*`).MatchString(DATE) {
				// if VERBOSE {
				// 	log.Printf("%v %v %v", DATE, TIME, EVENT)
				// }

				if DATE != "" {
					if !strings.Contains(EVENT, "Moon") == true {
						log.Println("[+] EV:", EVENT)
						ENTRIES = append(ENTRIES, Calendar{Date: DATE, Event: EVENT})
					}
				}
			}
		}
	})

	c.OnRequest(func(r *colly.Request) {
		log.Println("Visiting", r.URL)
	})

	c.Visit(BASEURL)

	RESULT_CSV := []map[string]string{}
	RESULT_JSON, _ := json.MarshalIndent(ENTRIES, "", "\t")

	_ = json.Unmarshal(RESULT_JSON, &RESULT_CSV)

	// for _, i := range RESULT_2 {
	//      DATA = append(DATA, []string{i["Date"], i["Event"]})
	// }

	if ICAL {
		log.Printf("[+] exporting as ical")
		ExportCalendar(RESULT_CSV)
		if VERBOSE {
			log.Printf("%#v", RESULT_CSV)
		}
	} else {
		log.Printf("[+] exporting as json")
		os.WriteFile(FILE, RESULT_JSON, 0644)
		if VERBOSE {
			log.Printf("%#v", string(RESULT_JSON))
		}
	}
}
