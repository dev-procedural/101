package main

import (
	"fmt"

	"github.com/samber/lo"
)

type Person struct {
	Name string
	Age  int
}

func main() {

	names := lo.Uniq[string]([]string{"Samuel", "John", "Samuel"})
	fmt.Println(names)

	Persons := []Person{
		{"Pepe", 10},
		{"Pepe", 10},
		{"Oscar", 20},
		{"Himanshu", 30},
		{"Himanshu", 30},
	}
	names2 := lo.Uniq[Person](Persons)
	fmt.Println(names2)

	Persons2 := []Person{
		{"Pepe", 12},
		{"Pepe", 13},
		{"Pepe", 14},
		{"Pepe", 15},
		{"Pepe", 16},
		{"Oscar", 21},
		{"Himanshu", 31},
		{"Himanshu", 33},
	}
	names3 := lo.UniqBy[Person, bool](Persons2, func(p Person) bool {
		return p.Name == "Pepe"
	})

	fmt.Println(names3)

}
