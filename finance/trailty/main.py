'''
In this bot an examle using MACD Crossover is presented.
If you are new and trying to get started we recommend checking our https://app.trality.com/masterclass
For more information on each part of a trading bot, please visit our documentation pages: https://docs.trality.com
'''

'''
Initializing state object and setting number_offset_trades to 0 before the first bot execution.
'''

import numpy as np

def initialize(state):
    state.number_offset_trades = 0;
    state.GRID = {}
    state.init_price = 0

def grid(n,k="up"):
    init = 4
    ratio={"up": p3, "down": p1}
    U = []
    D = []
    price = n
    for i in range(price,price+init):
      price += (price * ratio[k])
      U.append(float(f"{price:0.08f}"))
    price = n
    for i in range(price,price+10):
      price -= (price * ratio[k])
      D.append(float(f"{price:0.08f}"))
    result = {"up": U, "down": D}
    return result

def grid(price: int, init=10):
  ratio={"up": 0.03, "down": 0.01}
  df = pd.DataFrame()
  df["price"] = [price for i in range(price,price+init)]
  df["up"] = df["price"].apply(lambda x: x * 0.03)
  df["down"] = df["price"].apply(lambda x: x * 0.01)

  df["up_c"] = df["price"]+df["up"].cumsum()
  df["down_c"] = df["price"]+df["down"].cumsum()
  df["down_c"] = df["price"]-(df["down"].cumsum())

def pairs(price: int, k="up"):
    p1 = price * 0.01
    p3 = price * 0.03
    ratio={"up": p3, "down": p1}
  
    win  = price + (price * p3) 
    loss = price - (price * p1)
    return {"WIN": win, "LOSS": loss}
    

# def calc_grid(price: int) -> dict:
#     init = 5
#     p1 = price * 0.01
#     p3 = price * 0.03
#     ratio={"up": p3, "down": p1}
#     WIN_GRID = []
#     LOSS_GRID = []
    
#     for k,v in ratio.items():
#       price_ = price
#       for i in range(init):
#         price_ += (price_ * ratio[k])
#         if k == "up":
#           WIN_GRID.append(float(f"{price_:0.08f}"))
#         else:
#           LOSS_GRID.append(float(f"{price_:0.08f}"))

#       price_ = price
#       for i in range(init):
#         price_ -= (price * ratio[k])
#         if k == "up":
#           WIN_GRID.append(float(f"{price_:0.08f}"))
#         else:
#           LOSS_GRID.append(float(f"{price_:0.08f}"))

#     result = {"UP": WIN_GRID, "DOWN": LOSS_GRID}

#     return result

# def takeClosest(LIST, PRICE):
#     # TODO get ratio relationship
#     UP = PRICE + (PRICE * 0.03)
#     DOWN = PRICE - (PRICE * 0.01)
    
    # myArray = np.array(myList)
    # pos = (np.abs(myArray-myNumber)).argmin()

    # return myArray[pos]


@schedule(interval="1h", symbol="ETHBTC")
def handler(state, data):

    #takeClosest = lambda num,collection:min(collection,key=lambda x:abs(x-num))

    if state.init_price == 0:
        state.init_price = data.price_last
        state.GRID = calc_grid(data.price_last)

    position = query_open_position_by_symbol(data.symbol,include_dust=False)
    has_position = position is not None

    portfolio = query_portfolio()
    balance_quoted = portfolio.excess_liquidity_quoted
    buy_value = float(balance_quoted) * 0.80
    #print(portfolio.balances)


    # NEAR_FROM_DOWN = takeClosest(state.GRID["DOWN"],data.price_last)
    # NEAR_FROM_UP   = takeClosest(state.GRID["UP"],data.price_last)

    #print("[!]"+"="*10+">",data.price_last, NEAR_FROM_DOWN, NEAR_FROM_UP)

    if data.price_last >= NEAR_FROM_UP and not has_position:
        print("[!]"+"="*10+">",data.price_last, NEAR_FROM_DOWN, NEAR_FROM_UP)
        order_market_value(symbol=data.symbol, value=buy_value) # creating market order

    elif data.price_last < NEAR_FROM_DOWN and has_position:
        print("[!]"+"_"*10+">",data.price_last, NEAR_FROM_DOWN, NEAR_FROM_UP)
        close_position(data.symbol)

    elif data.price_last > NEAR_FROM_UP  and has_position:
        close_position(data.symbol)

    # with PlotScope.root(data.symbol):
    #     plot_line("sell",data.price_last)


    #res = float( f"{(data.price_last * a) / 100:0.08f}" )

    #print("[!]",state.init_price, data.price_last, a, res)


    # on erroneous data return early (indicators are of NoneType)
    # if macd_ind is None:
    #     return

    # signal = macd_ind['macd_signal'].last
    # macd = macd_ind['macd'].last

    # current_price = data.close_last

    # '''
    # 2) Fetch portfolio
    #     > check liquidity (in quoted currency)
    #     > resolve buy value
    # '''

    # portfolio = query_portfolio()
    # balance_quoted = portfolio.excess_liquidity_quoted
    # # we invest only 80% of available liquidity
    # buy_value = float(balance_quoted) * 0.80

    # '''
    # 3) Fetch position for symbol
    #     > has open position
    #     > check exposure (in base currency)
    # '''

    # position = query_open_position_by_symbol(data.symbol,include_dust=False)
    # has_position = position is not None

    # '''
    # 4) Resolve buy or sell signals
    #     > create orders using the order api
    #     > print position information

    # '''
    # if macd > signal and not has_position:
    #     print("-------")
    #     print("Buy Signal: creating market order for {}".format(data.symbol))
    #     print("Buy value: ", buy_value, " at current market price: ", data.close_last)
    #     order_market_value(symbol=data.symbol, value=buy_value) # creating market order

    # elif macd < signal and has_position:
    #     print("-------")
    #     logmsg = "Sell Signal: closing {} position with exposure {} at current market price {}"
    #     print(logmsg.format(data.symbol,float(position.exposure),data.close_last))
    #     close_position(data.symbol) # closing position


    # '''
    # 5) Check strategy profitability
    #     > print information profitability on every offsetting trade
    # '''

    if state.number_offset_trades < portfolio.number_of_offsetting_trades:

        pnl = query_portfolio_pnl()
        print("-------")
        print("Accumulated Pnl of Strategy: {}".format(pnl))

        offset_trades = portfolio.number_of_offsetting_trades
        number_winners = portfolio.number_of_winning_trades
        print("Number of winning trades {}/{}.".format(number_winners,offset_trades))
        print("Best trade Return : {:.2%}".format(portfolio.best_trade_return))
        print("Worst trade Return : {:.2%}".format(portfolio.worst_trade_return))
        print("Average Profit per Winning Trade : {:.2f}".format(portfolio.average_profit_per_winning_trade))
        print("Average Loss per Losing Trade : {:.2f}".format(portfolio.average_loss_per_losing_trade))
        # reset number offset trades
        state.number_offset_trades = portfolio.number_of_offsetting_trades


--------------

import numpy as np

def initialize(state):
    state.number_offset_trades = 0;
    state.LOSS = 0
    state.WIN = 0


def pairs(price: int, short=False):
    p1 = price * 0.01
    p3 = price * 0.03
    ratio={"up": p3, "down": p1}
  
    if short == True:
        win  = price - (price * p3) 
        loss = price + (price * p1)
    else:
        win  = price + (price * p3) 
        loss = price - (price * p1)

    return {"WIN": win, "LOSS": loss}


@schedule(interval="1h", symbol="ETHBTC")
def handler(state, data):

    #takeClosest = lambda num,collection:min(collection,key=lambda x:abs(x-num))


    position = query_open_position_by_symbol(data.symbol,include_dust=False)
    has_position = position is not None

    portfolio = query_portfolio()
    balance_quoted = portfolio.excess_liquidity_quoted
    buy_value = float(balance_quoted) * 0.50

    RESULT = pairs(data.price_last, short=True)
    WIN = RESULT["WIN"]
    LOSS = RESULT["LOSS"]



    # entrada inicial <<< $
    if not has_position:
        state.WIN = WIN
        state.LOSS = LOSS
        print(data.price_last,state.WIN, state.LOSS)

        order_market_value(symbol=data.symbol, value=buy_value)
    
    # salir/vender  <<< $
    if state.LOSS >= data.price_last and has_position:
        close_position(data.symbol) # creating market order
    # salir/vender  <<< $
    elif state.WIN <= data.price_last and has_position:
        close_position(data.symbol)



    if state.number_offset_trades < portfolio.number_of_offsetting_trades:

        pnl = query_portfolio_pnl()
        print("-------")
        print("Accumulated Pnl of Strategy: {}".format(pnl))

        offset_trades = portfolio.number_of_offsetting_trades
        number_winners = portfolio.number_of_winning_trades
        print("Number of winning trades {}/{}.".format(number_winners,offset_trades))
        print("Best trade Return : {:.2%}".format(portfolio.best_trade_return))
        print("Worst trade Return : {:.2%}".format(portfolio.worst_trade_return))
        print("Average Profit per Winning Trade : {:.2f}".format(portfolio.average_profit_per_winning_trade))
        print("Average Loss per Losing Trade : {:.2f}".format(portfolio.average_loss_per_losing_trade))
        # reset number offset trades
        state.number_offset_trades = portfolio.number_of_offsetting_trades


