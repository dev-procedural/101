import { createApp } from 'vue'
import { createPinia } from "pinia";
import App from "./App.vue";
import router from "./router";
import "./assets/css/app.css";
import VueCookies from 'vue3-cookies'

const app = createApp(App).use(router).use(createPinia());

app.use(VueCookies, {
    expireTimes: "1h",
    path: "/",
    domain: "localhost",
    secure: true,
    sameSite: "None"
});

app.mount("#app");