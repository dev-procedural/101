import { createRouter, createWebHistory } from "vue-router";
import SideMenu   from "../layouts/SideMenu/SideMenu.vue";
import SimpleMenu from "../layouts/SimpleMenu/SimpleMenu.vue";
import TopMenu    from "../layouts/TopMenu/TopMenu.vue";
import Page1      from "../pages/Page1.vue";
import Page2      from "../pages/Page2.vue";
import Login      from "../pages/Login.vue";
import Register   from "../pages/Register.vue";
import Forgot     from "../pages/Forgotpass.vue";
import axios      from "axios";

import { useCookies } from "vue3-cookies";

const routes = [
  {
    path: "/auth/",
    children: [
      {
        path: "/login",
        name: "login",
        component: Login,
      },
      {
        path: "/register",
        name: "register",
        component: Register,
      },
      {
        path: "/forgot",
        name: "forgot",
        component: Forgot,
      },
    ],
  },
  {
    path: "/",
    component: TopMenu,
    children: [
      {
        path: "page-1",
        name: "top-menu-page-1",
        component: Page1,
        beforeEnter: checkLogin
      },
      {
        alias: "/",
        path: "page-2",
        name: "top-menu-page-2",
        component: Page2,
      },
    ],
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
  scrollBehavior(to, from, savedPosition) {
    return savedPosition || { left: 0, top: 0 };
  },
});

export default router;

function checkLogin(to, from){

      const { cookies } = useCookies();
      axios
        .get('http://localhost:8081/v1/crud/isauth',{
          withCredentials: true,
          headers: {
              'Content-Type': 'application/json',
          }
        })
        .then((response) => {
          // Handle a successful login response here
          console.log("[+]", response)
          //router.push(to.fullPath);
        })
        .catch((error) => {
          // Handle authentication error
          console.log('[!]', error);
          console.log('[!] Login failed. Please check your credentials.');
          router.push({ 
            name: "login",
            query: {from: to.fullPath }
          });
        });
}
