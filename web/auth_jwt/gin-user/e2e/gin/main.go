package main

import (
	"net/http"

	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/cookie"
	"github.com/gin-gonic/gin"
)

var (
	store = cookie.NewStore([]byte("secret"))
)

func main() {
	r := gin.Default()

	// Initialize session middleware
	r.Use(sessions.Sessions("mySession", store))

	// Routes
	r.GET("/", func(c *gin.Context) {
		session := sessions.Default(c)
		user := session.Get("user")

		if user != nil {
			c.JSON(http.StatusOK, gin.H{"message": "You are logged in as " + user.(string)})
		} else {
			c.JSON(http.StatusOK, gin.H{"message": "You are not logged in."})
		}
	})

	r.POST("/register", func(c *gin.Context) {
		username := c.PostForm("username")
		password := c.PostForm("password")

		// In a real application, you should hash and securely store the password.
		// For simplicity, we're storing it as-is in memory here.
		// You should use a proper database for user management.
		users[username] = password

		c.JSON(http.StatusOK, gin.H{"message": "User registered successfully"})
	})

	r.POST("/login", func(c *gin.Context) {
		username := c.PostForm("username")
		password := c.PostForm("password")

		// In a real application, you should compare the hashed password with the stored hash.
		// For simplicity, we're comparing it as-is in memory here.
		// You should use a proper database for user management.
		storedPassword, ok := users[username]
		if !ok || storedPassword != password {
			c.JSON(http.StatusUnauthorized, gin.H{"message": "Authentication failed"})
			return
		}

		// Create a session to store the user's name
		session := sessions.Default(c)
		session.Set("user", username)
		session.Save()

		c.JSON(http.StatusOK, gin.H{"message": "Logged in successfully"})
	})

	r.GET("/logout", func(c *gin.Context) {
		session := sessions.Default(c)
		session.Delete("user")
		session.Save()

		c.JSON(http.StatusOK, gin.H{"message": "Logged out successfully"})
	})

	r.Run(":8080")
}

var users = make(map[string]string)
