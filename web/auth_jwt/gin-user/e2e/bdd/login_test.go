package main

import (
	"fmt"
	"math/rand"
	"testing"
	"time"

	"github.com/go-rod/rod"
	"github.com/stretchr/testify/assert"
)

const page = "http://localhost:5173"
const TIMEOUT = 7

func TestRegister(t *testing.T) {

	g := setup(t)

	RANDN := rand.Intn(100)
	USERNAME := fmt.Sprintf("user%d", RANDN)
	PASS := fmt.Sprintf("user%d", RANDN)

	testCases := []struct {
		CASE     string
		USERNAME string
		PASSWORD string
	}{
		{CASE: "UserFunc", USERNAME: "user1", PASSWORD: "user1"},
		{CASE: "UserAndPassOk", USERNAME: USERNAME, PASSWORD: PASS},
		{CASE: "PassNotOk", USERNAME: USERNAME, PASSWORD: "notworkingpass"},
		//{CASE: "UserNotOk", USERNAME: "notworkinguser", PASSWORD: PASS},
	}

	for _, tc := range testCases {

		t.Run("Register"+tc.CASE, func(t *testing.T) {
			// t.Parallel()
			p := g.page(page + "/register")

			p.MustElement(`#username`).MustInput(tc.USERNAME)
			p.MustElement(`#password`).MustInput(tc.PASSWORD)
			p.MustElement(`#password_confirm`).MustInput(tc.PASSWORD)
			p.MustElement(`#submit`).MustClick()

			fmt.Printf("   >>> CONF: %#v,%#v\n", tc.USERNAME, tc.PASSWORD)
			time.Sleep(3 * time.Second)
			// p.Timeout(TIMEOUT*time.Second).Race().ElementR("div", "/examplo/i").MustHandle(func(e *rod.Element) {
			save := p.Race().
				ElementR("div #examplo", "/cat is wrong/i").
				MustHandle(func(e *rod.Element) {
					// fmt.Println(e.MustText())
				}).Element("#basicNonStickyNotification").MustHandle(func(e *rod.Element) {
				// fmt.Println("[?]", e.MustText())
				time.Sleep(1 * time.Second)
			}).MustDo()

			switch {
			case tc.CASE == "PassNotOk":
				assert.Equal(
					t, save.MustText(),
					"Login failed. Please check your credentials.",
					"get msg from alert",
				)
			case tc.CASE == "UserAndPassOk":
				assert.Equal(
					t, save.MustText(),
					"Examplo the cat is wrong",
					"get div of public page",
				)
			}

			//fmt.Printf("[?] modal msg %#v\n", save.MustText())
		})
	}
}

func TestLogin(t *testing.T) {

	g := setup(t)

	USERNAME := "user1"
	PASS := "user1"

	testCases := []struct {
		CASE     string
		USERNAME string
		PASSWORD string
	}{
		{CASE: "UserAndPassOk", USERNAME: USERNAME, PASSWORD: PASS},
		{CASE: "PassNotOk", USERNAME: USERNAME, PASSWORD: "notworkingpass"},
		//{CASE: "UserNotOk", USERNAME: "notworkinguser", PASSWORD: PASS},
	}

	for _, tc := range testCases {

		t.Run("Login"+tc.CASE, func(t *testing.T) {

			p := g.page(page + "/login")

			p.MustElement(`#username`).MustInput(tc.USERNAME)
			p.MustElement(`#password`).MustInput(tc.PASSWORD)
			p.MustElement(`#submit`).MustClick()

			fmt.Printf("   >>> CONF: %#v,%#v\n", tc.USERNAME, tc.PASSWORD)

			cookies := []string{}
			time.Sleep(3 * time.Second)

			save := p.Race().
				ElementR("div #examplo", "cat").
				MustHandle(func(e *rod.Element) {

					for _, cookie := range p.MustCookies() {
						cookies = append(cookies, cookie.Name)
					}
					// fmt.Printf("[!] %#v\n", cookies)
				}).Element("#basicNonStickyNotification").MustHandle(func(e *rod.Element) {
				// fmt.Println(e.MustText())
				// time.Sleep(1 * time.Second)
			}).MustDo()

			switch {
			case tc.CASE == "PassNotOk":
				assert.Contains(
					t, save.MustText(),
					"Login failed. Please check your credentials.",
					"get msg from alert",
				)
			case tc.CASE == "UserAndPassOk":
				assert.NotEqual(
					t, save.MustText(),
					"Login failed. Please check your credentials.",
					"get msg from alert, user might not exist",
				)
				assert.Contains(
					t, cookies,
					"Session",
					"get session cookie",
				)
			}

			// p.MustWaitLoad().MustScreenshot("/tmp/a.png")
		})
	}
}
