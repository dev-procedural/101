package main

import (
	"flag"
	"integration/utils"
	"log"
	"math/rand"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
)

var RANDOM = rand.Intn(1000)
var USER = flag.String("u", "user"+strconv.Itoa(RANDOM), "user name")
var PASS = flag.String("p", "user"+strconv.Itoa(RANDOM), "password")
var VERBOSE = flag.Bool("v", false, "this is to print more info")

func TestRegister(t *testing.T) {
	t.Run("Register", func(t *testing.T) {
		res := utils.HTTPRequest(
			"POST",
			"http://localhost:8081/v1/auth/register",
			[]byte(`{"username": "`+*USER+`", "password": "`+*PASS+`"}`),
			utils.Options{},
		)

		if *VERBOSE {
			log.Printf("%#v", res)
		}
		assert.Equal(t, "200 OK", res["status"], "this must be equal")
	})
}

func TestLogin(t *testing.T) {
	Cookie := ""
	Token := ""

	t.Run("GetCookieAndToken", func(t *testing.T) {
		opt := utils.Options{LOGIN: true}
		res := utils.HTTPRequest(
			"POST",
			"http://localhost:8081/v1/auth/login",
			[]byte(`{"username": "`+*USER+`", "password": "`+*PASS+`"}`),
			opt,
		)
		Cookie = res["cookie"].(string)
		Token = res["token"].(map[string]interface{})["token"].(string)

		assert.Equal(t, "200 OK", res["status"], "this must be equal")
	})
	t.Run("CheckCookieAndToken", func(t *testing.T) {
		if *VERBOSE {
			log.Println("Cookie:", Cookie)
			log.Println("Token:", Token)
		}
		assert.NotEqual(t, Cookie, "", "they should be equal")
		assert.NotEqual(t, Token, "", "they should be equal")
	})
	t.Run("CheckTokenWorks", func(t *testing.T) {
		opt := utils.Options{TOKEN: Token, LOGIN2: true}
		res := utils.HTTPRequest(
			"GET",
			"http://localhost:8081/v1/crud/get/all",
			nil,
			opt,
		)
		if *VERBOSE {
			log.Printf(">>> %#v", res)
		}
		assert.Equal(t, "200 OK", res["status"], "this must be equal")
	})
}
