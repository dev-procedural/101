package utils

import (
	"bytes"
	"encoding/json"
	"io"
	"log"
	"net/http"
)

type Data struct {
	username, password string
}

type Options struct {
	LOGIN  bool
	LOGIN2 bool
	COOKIE string
	TOKEN  string
}

func HTTPRequest(method, url string, data []byte, options Options) map[string]interface{} {

	cookie := ""
	client := &http.Client{}
	req, err := http.NewRequest(method, url, bytes.NewBuffer(data))
	if err != nil {
		log.Fatalf("%#v", err)
		// return map[string]interface{}{},err
	}
	if options.LOGIN2 {
		req.Header.Set("Authorization", options.TOKEN)
	}

	resp, err := client.Do(req)
	if err != nil {
		log.Fatalf("%#v", err)
	}

	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)

	response := make(map[string]interface{})
	json.Unmarshal(body, &response)
	if options.LOGIN {
		for _, c := range resp.Cookies() {
			cookie = c.Name + " " + c.Value
		}
		response["cookie"] = cookie
	}
	response["status"] = resp.Status

	return response

}
