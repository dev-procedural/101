/*
Copyright © 2023
*/
package cli

import (
	"fmt"
	"log"
	"micro/internal/models"
	"micro/pkg/rest"
	"micro/pkg/utils"
	"os"
	"strconv"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// apiCmd represents the api command
var serverCmd = &cobra.Command{
	Use:   "api",
	Short: "api run server",
	Long:  `run server with all endpoints activated`,
	Run: func(cmd *cobra.Command, args []string) {

		viper.AutomaticEnv()
		VB, _ := cmd.Flags().GetBool("verbose")
		viper.Set("verbose", strconv.FormatBool(VB))

		os.Setenv("verbose", strconv.FormatBool(VB))
		fmt.Printf("%#v", viper.AllSettings())

		//bootstrap users
		//run server
		VERBOSE := map[bool]string{true: fmt.Sprintf("[!] %s", os.Environ()), false: ""}[VB]
		log.Printf("[!] Main log...\n>>> %#v", VERBOSE)

		API, _ := rest.RunAPI()

		go func() {
			log.Fatal(API.Run(os.Getenv("CRUD_URL")))
		}()

		BootstrapUsers()
	},
}

func BootstrapUsers() {

	log.Println("[!] Bootstrapping users")

	db := initDB()

	for u, password := range map[string]string{
		"admin": "admin",
		"user1": "user1",
	} {
		checkUser := models.User{
			Username: u,
		}

		_, err := db.GetUser(checkUser)
		if err == nil {
			log.Printf("[!] User: %s is already in db", u)
			continue
		}

		p, err := utils.HashPassword(password)
		if err != nil {
			log.Fatalf("%s", err.Error())
		}
		user := models.User{
			Username: u,
			Password: p,
		}
		if u == "admin" {
			user.Role = "admin"
		}
		_, err = db.Register(user)
		if err != nil {
			log.Fatalf("%s", err.Error())
			return
		}
		log.Printf("%#v", user)
	}

}

func init() {
	rootCmd.AddCommand(serverCmd)

	// apiCmd.PersistentFlags().String("foo", "", "A help for foo")
	serverCmd.Flags().BoolP("verbose", "v", false, "verbose")
	serverCmd.Flags().BoolP("test", "t", false, "testing version")
}
