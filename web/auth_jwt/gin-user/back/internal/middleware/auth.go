package middleware

import (
	"log"
	"net/http"
	"os"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
)

func init() {
	log.Printf("[!] MIDDLEWARE ENVS %#v", os.Environ())
	// CONF := cfg
	// fmt.Printf("[!] %#v", CONF)
}

type Claims struct {
	Username string `json:"username"`
	jwt.StandardClaims
}

func AuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Session validation
		// log.Printf("[!] Request: %#v", c.Keys)
		var tokenValue string
		claims := &Claims{}

		session := sessions.Default(c)
		sessionJwt := session.Get("jwt")
		sessionToken := session.Get("token")
		// fmt.Printf("[!] %#v", CONF)

		switch {
		case sessionToken == nil && c.GetHeader("Authorization") == "":
			c.JSON(http.StatusForbidden, gin.H{
				"message": "Not logged",
			})
			c.Abort()
		}

		// TODO revisit:
		// - if token||session check in redis, decode get time and refresh when necessary
		// JWT token validation
		switch {
		case c.GetHeader("Authorization") != "":
			log.Println("[1] <<< header auth")
			tokenValue = c.GetHeader("Authorization")

			tkn, err := jwt.ParseWithClaims(tokenValue, claims, func(token *jwt.Token) (interface{}, error) {
				// TODO add viper or env var
				return []byte("secret"), nil
			})
			// TODO ADD viper
			log.Printf("[!] >TKN: %#v || %#v", tkn, err)
			if err != nil {
				c.AbortWithStatus(http.StatusUnauthorized)
			}

			if !tkn.Valid {
				c.AbortWithStatus(http.StatusUnauthorized)
			}

			c.Next()

		case sessionToken != "":
			log.Println("[1] <<< session exist")
			tokenValue = sessionJwt.(string)

			tkn, err := jwt.ParseWithClaims(tokenValue, claims, func(token *jwt.Token) (interface{}, error) {
				// TODO add viper or env var
				return []byte("secret"), nil
			})
			// TODO ADD viper
			log.Printf("[!] >TKN: %#v || %#v", tkn, err)
			if err != nil {
				c.AbortWithStatus(http.StatusUnauthorized)
			}

			if !tkn.Valid {
				c.AbortWithStatus(http.StatusUnauthorized)
			}
			c.SetCookie("JWT", sessionJwt.(string), 3600, "/", "localhost", false, true)

			c.Next()
		}

	}
}
