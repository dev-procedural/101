package db

import (
	"micro/internal/models"
)

type CrudDBLayer interface {
	Add(models.Crud) (models.Crud, error)
	Get(models.Crud) (models.Crud, error)
	GetAll() ([]models.Crud, error)
	Delete(models.Crud) (models.Crud, error)
	DeleteAll([]models.Crud) ([]models.Crud, error)
	Update(models.Crud) (models.Crud, error)
	// USER
	Register(models.User) (models.User, error)
	GetUser(models.User) (models.User, error)
	UpdateUser(models.User) (models.User, error)
}

// ________________[ Crud ]
func (db *DBORM) Add(crud models.Crud) (models.Crud, error) {
	err := db.Create(&crud).Error
	return crud, err
}

func (db *DBORM) Get(crud models.Crud) (models.Crud, error) {
	err := db.Where(&models.Crud{ID: crud.ID}).Find(&crud).Error
	return crud, err
}

func (db *DBORM) GetAll() (crud []models.Crud, err error) {
	err = db.Find(&crud).Error
	return crud, err
}

func (db *DBORM) Update(crud models.Crud) (models.Crud, error) {
	err := db.Model(&crud).Updates(
		models.Crud{
			ID:        crud.ID,
			CreatedAt: crud.CreatedAt,
		}).Error
	return crud, err
}

func (db *DBORM) Delete(crud models.Crud) (models.Crud, error) {
	err := db.Unscoped().Delete(&crud).Error
	return crud, err
}

func (db *DBORM) DeleteAll(crud []models.Crud) ([]models.Crud, error) {
	err := db.Unscoped().Delete(&crud).Error
	return crud, err
}

//__________________[ USER ]

func (db *DBORM) GetUser(user models.User) (models.User, error) {
	err := db.Model(&user).Where("Username = ?", user.Username).Select("*").First(&user).Error
	// log.Printf("%#v", user)
	return user, err
}

func (db *DBORM) Register(user models.User) (models.User, error) {
	// log.Printf("user: %#v", user.Password)
	err := db.Create(&user).Error
	return user, err
}

func (db *DBORM) UpdateUser(user models.User) (models.User, error) {
	// log.Printf("CRUD-UPDATE: %#v", user)
	err := db.Model(&user).Where("Username = ?", user.Username).Select("Username", "Password").Updates(
		models.User{
			Username: user.Username,
			Password: user.Password,
		}).Error
	return user, err
}
