package crud

import (
	"log"
	"micro/internal/models"
	"strconv"
	"time"

	"micro/config"
	"micro/pkg/cache"
	"micro/pkg/db"
	utils "micro/pkg/utils"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"github.com/rs/xid"
)

type CrudHandlerInterface interface {
	Get(c *gin.Context)
	GetAll(c *gin.Context)
	Delete(c *gin.Context)
	Update(c *gin.Context)
	Login(c *gin.Context)
	Register(c *gin.Context)
	IsAuth(c *gin.Context)
}

type CrudHandler struct {
	DB    db.CrudDBLayer
	CACHE cache.CrudCACHELayer
}

type Claims struct {
	Username string `json:"username"`
	jwt.StandardClaims
}

type JWTOutput struct {
	Token   string    `json:"token"`
	Expires time.Time `json:"expires"`
}

func NewCrudHandler() (CrudHandlerInterface, error) {

	CONFDB := config.DBConfig()

	db, err := db.NewORM(CONFDB.DBStringer())
	if err != nil {
		return nil, err
	}

	err = db.AutoMigrate(&models.Crud{}, &models.User{})
	if err != nil {
		log.Println("[!] error on migrate", err)
		return nil, err
	}

	cache, err := cache.NewCacheApp()
	if err != nil {
		log.Println("[!] error on cache", err)
		return nil, err
	}

	return &CrudHandler{DB: db, CACHE: cache}, nil
}

// @title Crud api
// @BasePath /v1/crud
// @Description  add new crud entry per click
// @Tags        crud
// @Accept      json
// @Produce     json
// @Failure     400 {string} ERROR
// @Router      /v1/crud/add [put]
func (h *CrudHandler) Add(c *gin.Context) {
	if h.DB == nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "server database error", "stage": "1"})
		return
	}

	var cal models.Crud

	request, err := h.DB.Add(cal)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error(), "stage": "1"})
		return
	}
	// log.Printf("Added %#v - time %#v \n", request, utils.Date(request.CreatedAt))
	c.JSON(http.StatusOK, request)
}

// func date(unix int64) string {

// @title Crud api
// @BasePath /v1/crud
// @Summary get crud entry
// @Schemes http
// @Description  get crud entry per click
// @Tags        crud
// @Accept      json
// @Produce     json
// @Failure     400 {string} ERROR
// @Router      /v1/crud/get/:id [get]
func (h *CrudHandler) Get(c *gin.Context) {
	if h.DB == nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "server database error", "stage": "0"})
		return
	}

	var cal models.Crud

	cal.ID, _ = strconv.Atoi(c.Param("id"))

	request, err := h.DB.Get(cal)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error(), "stage": "2"})
		return
	}
	log.Printf("Get: %#v \n", request)
	c.JSON(http.StatusOK, request)
}

// @title Crud api
// @BasePath /v1/crud
// @Summary get crud entry
// @Schemes http
// @Description  get all crud entries per click
// @Tags        crud
// @Accept      json
// @Produce     json
// @Failure     400 {string} ERROR
// @Router      /v1/crud/get/all [get]
func (h *CrudHandler) GetAll(c *gin.Context) {
	if h.DB == nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "server database error", "stage": "0"})
		return
	}

	var request []models.Crud
	request, err := h.DB.GetAll()
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error(), "stage": "2"})
		return
	}
	c.JSON(http.StatusOK, request)
}

// @title Crud api
// @BasePath /v1/crud
// @Summary update crud entry
// @Schemes http
// @Description  update crud entry per click
// @Tags        crud
// @Accept      json
// @Produce     json
// @Param       created_at  query string true "created time unix/epoch"
// @Failure     400 {string} ERROR
// @Router      /v1/crud/update/:id [put]
func (h *CrudHandler) Update(c *gin.Context) {
	if h.DB == nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "server database error", "stage": "0"})
		return
	}

	var cal models.Crud
	var new_date int64

	new_date, _ = strconv.ParseInt(c.PostForm("created_at"), 10, 64)

	// check size
	if len(c.PostForm("created_at")) != 10 {
		c.JSON(http.StatusNotFound, gin.H{"error": "String size incorrect, must be 10", "stage": "1"})
		return
	}

	cal.ID, _ = strconv.Atoi(c.Param("id"))
	cal.CreatedAt = new_date

	request, err := h.DB.Update(cal)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error(), "stage": "2"})
		return
	}
	new_date_time := utils.Date(new_date)
	log.Printf("Update: %+v %+v \n", new_date, new_date_time)

	c.JSON(http.StatusOK, request)
}

// @title delete crud entry
// @BasePath /v1/crud
// @Summary delete crud entry
// @Schemes http
// @Description  delete crud entry per click
// @Tags        crud
// @Accept      json
// @Produce     json
// @Failure     400 {string} ERROR
// @Router      /v1/crud/delete/:id [delete]
func (h *CrudHandler) Delete(c *gin.Context) {
	if h.DB == nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "server database error", "stage": "0"})
		return
	}

	var cal models.Crud
	var request interface{}
	var err error
	var all []models.Crud

	cal.ID, _ = strconv.Atoi(c.Param("id"))

	if c.Param("id") == "all" {
		all, err = h.DB.GetAll()
		request, err = h.DB.DeleteAll(all)
	} else {
		request, err = h.DB.Delete(cal)
	}
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error(), "stage": "2"})
		return
	}

	log.Printf("Delete: %+v \n", request)

	c.JSON(http.StatusOK, request)
}

// @title login user
// @BasePath /v1/auth/login
// @Summary login user
// @Schemes http
// @Description  login a user and return a jwt
// @Tags        auth
// @Accept      json
// @Produce     json
// @Success     200 {string} "this"
// @Failure     401 {string} ERROR
// @Router      /v1/auth/login [post]
func (h *CrudHandler) Login(c *gin.Context) {
	var user models.User
	if err := c.ShouldBindJSON(&user); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// user validation
	get_user, err := h.DB.GetUser(user)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error(), "stage": "2"})
		return
	}
	check_pass := utils.CheckPasswordHash(user.Password, get_user.Password)
	// log.Printf("[?] \nuser:%s\ndb:%s\n%b", user.Password, get_user.Password, check_pass)
	if !check_pass {
		c.JSON(http.StatusNotFound, gin.H{"error": "user or pass not in db, please try again", "stage": "2"})
		return
	}

	// jwt token setup
	expirationTime := time.Now().Add(30 * time.Minute)
	claims := &Claims{
		Username: user.Username,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	// TODO add viper or env
	tokenString, err := token.SignedString([]byte("secret"))
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	jwtOutput := JWTOutput{
		Token:   tokenString,
		Expires: expirationTime,
	}
	// log.Printf("%#v", jwtOutput)

	// session setup
	sessionToken := xid.New().String()
	session := sessions.Default(c)
	session.Set("username", user.Username)
	session.Set("token", sessionToken)
	session.Set("jwt", jwtOutput.Token)
	session.Save()

	// cookie
	// c.JSON(http.StatusOK, gin.H{"message": fmt.Sprintf("%#v", c.Header)})
	c.JSON(http.StatusOK, gin.H{"message": "User signed in", "token": jwtOutput})
}

// @title register a user
// @BasePath /v1/auth/register
// @Summary login user
// @Schemes http
// @Description  login a user and return a jwt
// @Tags        auth
// @Accept      json
// @Produce     json
// @Failure     401 {string} ERROR
// @Router      /v1/auth/register [post]
func (h *CrudHandler) Register(c *gin.Context) {

	if h.DB == nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "server database error", "stage": "0"})
		return
	}

	var user models.User
	if err := c.ShouldBindJSON(&user); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error(), "stage": "1"})
		return
	}

	// check if user in db
	_, err := h.DB.GetUser(user)
	if err == nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "username already taken, please try again", "stage": "2"})
		return
	}

	// register
	user.Password, _ = utils.HashPassword(user.Password)
	_, err = h.DB.Register(user)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error(), "stage": "3"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "User registered"})
}

// @title register a user
// @BasePath /v1/auth/isauth
// @Summary check auth user
// @Schemes http
// @Description  login a user and return a jwt
// @Tags        auth
// @Accept      json
// @Produce     json
// @Failure     401 {string} ERROR
// @Router      /v1/crud/isauth [get]
func (h *CrudHandler) IsAuth(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"message": "is auth"})
}
