package crud

import (
	"micro/internal/middleware"

	"github.com/gin-gonic/gin"
)

func CrudRouter(crud CrudHandlerInterface, r *gin.Engine) error {
	crud_pub := r.Group("v1/auth/")
	crud_pub.POST("/login", crud.Login)
	crud_pub.POST("/register", crud.Register)
	// crud_pub.POST("/refresh", crud.Refresh)

	// private router
	crud_private := r.Group("/v1/crud")
	crud_private.Use(middleware.AuthMiddleware())
	{
		crud_private.DELETE("/delete/:id", crud.Delete)
		crud_private.PUT("/update/:id", crud.Update)
		crud_private.POST("/register", crud.Register)
		crud_private.GET("/get/:id", crud.Get)
		crud_private.GET("/get/all", crud.GetAll)
		crud_private.GET("/isauth", crud.IsAuth)
	}
	return nil
}
