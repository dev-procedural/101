package cache

import (
	"micro/internal/models"
)

type CrudCACHELayer interface {
	Add(models.Crud) (models.Crud, error)
	Get(models.Crud) (models.Crud, error)
	GetAll() ([]models.Crud, error)
	Delete(models.Crud) (models.Crud, error)
	DeleteAll([]models.Crud) ([]models.Crud, error)
	Update(models.Crud) (models.Crud, error)
}

// ________________[ Crud ]
func (db *CACHEAPP) Add(crud models.Crud) (models.Crud, error) {
	// err := db.Create(&crud).Error
	// return crud, err
	return crud, nil
}

func (db *CACHEAPP) Get(crud models.Crud) (models.Crud, error) {
	// err := db.Where(&models.Crud{ID: crud.ID}).Find(&crud).Error
	// return crud, err
	return crud, nil
}

func (db *CACHEAPP) GetAll() (crud []models.Crud, err error) {
	// err = db.Find(&crud).Error
	// return crud, err
	return crud, nil
}

func (db *CACHEAPP) Update(crud models.Crud) (models.Crud, error) {
	// err := db.Model(&crud).Updates(
	// 	models.Crud{
	// 		ID:        crud.ID,
	// 		CreatedAt: crud.CreatedAt,
	// 	}).Error
	// return crud, err
	return crud, nil
}

func (db *CACHEAPP) Delete(crud models.Crud) (models.Crud, error) {
	// err := db.Unscoped().Delete(&crud).Error
	// return crud, err
	return crud, nil
}

func (db *CACHEAPP) DeleteAll(crud []models.Crud) ([]models.Crud, error) {
	// err := db.Unscoped().Delete(&crud).Error
	// return crud, err
	return crud, nil
}
