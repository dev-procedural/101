package cache

import (
	"errors"
	"os"

	cfg "micro/config"

	sessions "github.com/gin-contrib/sessions"
	cookie "github.com/gin-contrib/sessions/cookie"
	redis_s "github.com/gin-contrib/sessions/redis"
	"github.com/go-redis/redis"
)

type CACHE struct {
	Store sessions.Store
}

type CACHEAPP struct {
	*redis.Client
}

func NewCacheSession() (CACHE, error) {

	CONF := cfg.CACHEConfig()

	switch {
	case os.Getenv("CACHE_TYPE") == "redis":
		// TODO add secret password
		store, err := redis_s.NewStore(10, "tcp", CONF.CACHE_URL+":"+CONF.CACHE_PORT, "", []byte("secret"))
		return CACHE{Store: store}, err

	case os.Getenv("CACHE_TYPE") == "memory":
		store := cookie.NewStore([]byte("secret"))
		return CACHE{Store: store}, nil
	}

	return CACHE{}, errors.New("Err not reaching cache")

}

func NewCacheApp() (*CACHEAPP, error) {

	CONF := cfg.CACHEConfig()
	// os.Setenv("CACHE_TYPE", "redis")
	// log.Printf("[!!!] %#v", os.Getenv("CACHE_TYPE"))
	cache := redis.NewClient(&redis.Options{
		Addr:     CONF.CACHE_URL + ":" + CONF.CACHE_PORT,
		Password: "",
		DB:       0,
	})

	return &CACHEAPP{cache}, nil

}
