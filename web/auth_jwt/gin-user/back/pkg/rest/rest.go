package rest

import (
	"fmt"
	"log"
	"micro/pkg/cache"
	. "micro/pkg/crud"
	"os"
	"time"

	_ "micro/docs"

	cors "github.com/gin-contrib/cors"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"

	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

func RunAPI() (*gin.Engine, error) {
	crud, err := NewCrudHandler()
	// pee, err := NewPeeHandler()
	if err != nil {
		log.Fatalf("[?] err: %v#", err.Error())
		return nil, err
	}

	return RunAPIWithHandler(crud), nil
}

func RunAPIWithHandler(crud_ CrudHandlerInterface) *gin.Engine {

	store1, err := cache.NewCacheSession()
	store := store1.Store
	if err != nil {
		fmt.Printf(">>> %s", err.Error())
	}
	r := gin.Default()

	// r.Use(cors.AllowAll())
	r.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"http://localhost:5173"},
		AllowMethods:     []string{"PUT", "PATCH", "POST", "GET"},
		AllowHeaders:     []string{"Origin", "content-type"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		MaxAge:           12 * time.Hour,
	}))

	r.Use(sessions.Sessions("Session", store))
	// r.Use(gin.LoggerWithFormatter(models.CustomLog))

	CrudRouter(crud_, r)
	CRUD_URL := os.Getenv("CRUD_URL")

	url := ginSwagger.URL(fmt.Sprintf("http://%s/swagger/doc.json", CRUD_URL))
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))

	return r
}
