package crud

import (
	"github.com/gin-gonic/gin"
)

func CrudRouter(crud CrudHandlerInterface, r *gin.Engine) error {
	crud_private := r.Group("/v1/crud")
	crud_private.Use(crud.AuthMiddleware())
	{
		crud_private.GET("/token", crud.createToken)
		crud_private.GET("/valid", crud.protectedEp)
	}
	return nil
}
