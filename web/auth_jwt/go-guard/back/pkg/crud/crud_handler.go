package crud

import (
	"context"
	"fmt"
	"log"
	"micro/internal/models"
	"strings"
	"time"

	"micro/config"
	"micro/pkg/db"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/shaj13/go-guardian/v2/auth"
	"github.com/shaj13/go-guardian/v2/auth/strategies/basic"
	"github.com/shaj13/go-guardian/v2/auth/strategies/jwt"
	"github.com/shaj13/go-guardian/v2/auth/strategies/union"
	"github.com/shaj13/libcache"
	_ "github.com/shaj13/libcache/fifo"
)

type CrudHandler struct {
	DB  db.CrudDBLayer
	CTX CTX
}

type CTX struct {
	strategy union.Union
	keeper   jwt.SecretsKeeper
}

type CrudHandlerInterface interface {
	createToken(c *gin.Context)
	AuthMiddleware() gin.HandlerFunc
	protectedEp(c *gin.Context)
}

func NewCrudHandler() (CrudHandlerInterface, error) {

	CONF := config.DBConfig()

	db, err := db.NewORM(CONF.DBStringer())
	db.AutoMigrate(&models.Crud{}, &models.Crud{})
	if err != nil {
		return nil, err
	}

	k, s := setupGoGuardian()
	ctx := CTX{keeper: k, strategy: s}

	return &CrudHandler{DB: db, CTX: ctx}, nil
}

// ________________________________[[  crud  ]]
// @title Crud api
// @BasePath /v1/crud
// mail godoc
// @Summary add new crud entry
// @Schemes http
// @Description  add new crud entry per click
// @Tags        crud
// @Accept      json
// @Produce     json
// @Failure     400 {string} ERROR
// @Router      /v1/crud/add [put]
func (h *CrudHandler) createToken(c *gin.Context) {
	u := auth.User(c.Request)
	token, err := jwt.IssueAccessToken(u, h.CTX.keeper)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"err": err})
	}
	c.JSON(http.StatusOK, gin.H{"token": token})
}

// @title Crud api
// @BasePath /v1/crud
// mail godoc
// @Summary add new crud entry
// @Schemes http
// @Description  add new crud entry per click
// @Tags        crud
// @Accept      json
// @Produce     json
// @Failure     400 {string} ERROR
// @Router      /v1/crud/add [put]
func (h *CrudHandler) protectedEp(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"status": "passed"})
}

func validateUser(ctx context.Context, r *http.Request, userName, password string) (auth.Info, error) {
	// here connect to db or any other service to fetch user and validate it.
	if userName == "admin" && password == "admin" {
		return auth.NewDefaultUser("admin", "1", nil, nil), nil
	}

	return nil, fmt.Errorf("Invalid credentials")
}

func setupGoGuardian() (jwt.StaticSecret, union.Union) {
	keeper := jwt.StaticSecret{
		ID:        "secret-id",
		Secret:    []byte("secret"),
		Algorithm: jwt.HS256,
	}
	cache := libcache.FIFO.New(0)
	cache.SetTTL(time.Minute * 20)
	basicStrategy := basic.NewCached(validateUser, cache)
	jwtStrategy := jwt.New(cache, keeper)
	strategy := union.New(jwtStrategy, basicStrategy)

	return keeper, strategy
}

func (h *CrudHandler) AuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {

		log.Println("Executing Auth Middleware")
		if strings.Contains(fmt.Sprint(c.Request.Header), "Authorization") == false {
			c.JSON(http.StatusNotFound, gin.H{"error": "no auth, protected endpoint"})
			c.AbortWithStatus(http.StatusNotFound)
			return
		}
		_, user, err := h.CTX.strategy.AuthenticateRequest(c.Request)
		if err != nil {
			c.JSON(http.StatusNotFound, gin.H{"error": "wrong auth"})
			c.AbortWithStatus(http.StatusNotFound)
			return
		}
		log.Printf("User %s Authenticated\n", user.GetUserName())
		c.Request = auth.RequestWithUser(user, c.Request)
		c.Next()
	}
}
