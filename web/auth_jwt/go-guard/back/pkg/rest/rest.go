package rest

import (
	"fmt"
	. "micro/pkg/crud"
	"os"

	_ "micro/config"
	_ "micro/docs"

	"github.com/gin-gonic/gin"
	cors "github.com/rs/cors/wrapper/gin"

	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

func RunAPI() (*gin.Engine, error) {

	crud, err := NewCrudHandler()
	// pee, err := NewPeeHandler()
	if err != nil {
		return nil, err
	}

	return RunAPIWithHandler(crud), nil
}

func RunAPIWithHandler(crud_ CrudHandlerInterface) *gin.Engine {

	//Get gin's default engine
	r := gin.Default()

	r.Use(gin.Logger())
	r.Use(cors.AllowAll())
	// r.Use(gin.LoggerWithFormatter(models.CustomLog))

	CrudRouter(crud_, r)
	// PeeRouter(pee, r)
	crud_URL := os.Getenv("crud_URL")

	url := ginSwagger.URL(fmt.Sprintf("http://%s/swagger/doc.json", crud_URL))
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))

	return r
}
