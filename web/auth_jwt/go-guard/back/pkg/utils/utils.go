package utils

import (
	"fmt"
	"time"
)

func Date(unix int64) string {

	loc, _ := time.LoadLocation("EST")
	now := time.Unix(unix, 0).In(loc)
	new_date_time, _ := time.ParseInLocation(time.RFC3339, now.Format(time.RFC3339), loc)

	new_date_time_ := fmt.Sprintf("%+v", new_date_time)

	return new_date_time_
}
