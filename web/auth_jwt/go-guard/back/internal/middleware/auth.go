package middleware

import (
	"log"

	// "log"
	// "encoding/json"
	// "strings"
	// "bytes"

	"github.com/gin-gonic/gin"
	"github.com/shaj13/go-guardian/v2/auth/strategies/jwt"
	"github.com/shaj13/go-guardian/v2/auth/strategies/union"
)

var strategy union.Union
var keeper jwt.SecretsKeeper

func AuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {

		log.Println("Executing Auth Middleware")
		// log.Printf("[?] %#v", c)
		// _, user, err := strategy.AuthenticateRequest(c.Request)
		// if err != nil {
		// 	c.JSON(http.StatusNotFound, gin.H{"error": err})
		// 	c.AbortWithStatus(http.StatusNotFound)
		// 	return
		// }
		// log.Printf("User %s Authenticated\n", user.GetUserName())
		// c.Request = auth.RequestWithUser(user, c.Request)
		// log.Printf("%#v", c.Request)
		c.Next()
	}
}
