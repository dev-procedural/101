import { describe, it, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import App from '../../App.vue'

describe('App', () => {
	it('App contains text', () => {
	const wrapper = shallowMount(App)
	expect(wrapper.text()).toContain('Examples from book')
	})
})
