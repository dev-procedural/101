import { createApp } from 'vue'
import { createPinia } from "pinia";
import App from "./App.vue";
import router from "./router";
import axiosPlugin from './utils/axios';
import "./assets/css/app.css";

const app = createApp(App)

app.use(router)
app.use(createPinia())
app.use(axiosPlugin)

app.mount('#app')