import {ref,onBeforeMount} from 'vue';
import axios from 'axios';

export const useQuotes = () => {
    const quotes = ref([]);
    const isLoading = ref(false);
    const error = ref(null);

    const getQuotes = async () => {
        isLoading.value = true;
        error.value = null;
        await new Promise(resolve => setTimeout(resolve, 3000));
        axios.get('http://localhost:8080/json', {
                headers: {
                  "Content-Type": "application/x-www-form-urlencoded",
                  "Access-Control-Allow-Origin": "*",
                },
        }).then((response) => {
            console.log("[!] resp", response)
            quotes.value = response.data
        }).catch((err) => {
            console.log("[!] error:", err)
            error.value = err
        }).finally(() => {
            isLoading.value = false;
        })
    }

    onBeforeMount(getQuotes);

    return {
        quotes,
        isLoading,
        error,
    }
}


