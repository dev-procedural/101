import { type Menu } from "@/stores/menu";

const menu: Array<Menu | "divider"> = [
  {
    icon: "Home",
    pageName: "dashboard",
    title: "Dashboard",
    subMenu: [
      {
        icon: "Activity",
        pageName: "dashboard-overview-1",
        title: "Overview 1",
      },
      {
        icon: "Activity",
        pageName: "dashboard-overview-2",
        title: "Overview 2",
      },
      {
        icon: "Activity",
        pageName: "dashboard-overview-3",
        title: "Overview 3",
      },
      {
        icon: "Activity",
        pageName: "dashboard-overview-4",
        title: "Overview 4",
      },
    ],
  },
];

export default menu;
