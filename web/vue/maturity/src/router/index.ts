import { createRouter, createWebHistory } from "vue-router";
import Layout from "@/themes";
import Wizard from "@/pages/WizardLayout3.vue"
import ErrorPage from "@/pages/ErrorPage.vue"
import Login from "@/pages/Login.vue"
import SelectionP from "@/pages/Selection.vue"
import Badge from "@/pages/Badges.vue"

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      component: Layout,
      children: [
        {
          path: "/project/:project?",
          name: "wizard-layout-3",
          component: Wizard
        },
        {
          path: "/badge/:project?",
          name: "badge",
          component: Badge
        },
        {
          path: "/badges",
          name: "badges",
          component: Badge
        },
        {
          path: "/",
          name: "list",
          component: SelectionP
        },
      ],
    },
    {
      path: "/login",
      name: "login",
      component: Login
    },
    {
      path: '/:catchAll(.*)*',
      name: "error-page",
      component: ErrorPage
    },
  ],
});

export default router;
