import { createStore } from 'vuex'

export default createStore({
  state: {
      count: 1,
      L: JSON.parse(localStorage.getItem("TEST"))
  },
  mutations: {
      increment (state, n) {
            state.count+= n
      },

      updateL (state, n) {
          localStorage.setItem("TEST", state.L=JSON.stringify(n) );
      },

      delL (state) {
          localStorage.setItem("TEST", state.L=0 );
      }
  },
  actions: {
  },
  modules: {
  }
})
