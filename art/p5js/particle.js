function Particle(x, y, targetX, targetY, maxForce) {
  
  this.pos = new p5.Vector(x, y);
  this.vel = new p5.Vector(0, 0);
  this.acc = new p5.Vector(0, 0);
	this.target = new p5.Vector(targetX, targetY);
  this.maxForce = maxForce * random(0.8, 1.2);
  
  this.move = function() {
    let distThreshold = 20;
    let distance = dist(this.pos.x, this.pos.y, this.target.x, this.target.y);
		
    let steer = new p5.Vector(this.target.x, this.target.y);
		steer.sub(this.pos);
    steer.normalize();
    steer.mult(map(min(distance, distThreshold), 0, distThreshold, 0, this.maxForce));
    this.acc.add(steer);
    
		let mouseDistance = dist(this.pos.x, this.pos.y, mouseX, mouseY);
		if (mouseDistance < repulsionRadius) {
			let repulse = new p5.Vector(this.pos.x, this.pos.y);
			repulse.sub(mouseX, mouseY);
			repulse.mult(map(mouseDistance, repulsionRadius, 0, 0, 0.05));
			this.acc.add(repulse);
		}
		
    this.vel.mult(0.95);
    
    this.vel.add(this.acc);
    this.pos.add(this.vel);
    this.acc.mult(0);
  }
  
  this.display = function() {
    strokeWeight(3);
    stroke(50, 150, 200);
    point(this.pos.x, this.pos.y);
  }
}
