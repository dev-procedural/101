// an array to add multiple particles
let   particles = [];
const fontSize  = 250;
const word      = "LVX";
// let   textPositions = []

// this class describes the properties of a single particle.
class Particle {
  // setting the co-ordinates, radius and the
  // speed of a particle in both the co-ordinates axes.
  constructor(x=0,y=0,te=false){
    this.x = x
    this.y = y
    this.te = te
    this.r = random(1,20);
    this.xSpeed = random(-2,2);
    this.ySpeed = random(-1,1.5);
    this.distance = 0
  }

  // creation of a particle.
  createParticle() {
    noStroke();
    fill('rgba(200,2,169,0.5)');
    // console.log(this.te)
    if ( !this.te ) {
      circle(this.x,this.y,this.r);
    }
  }

  // setting the particle in motion.
  moveParticle() {
    if(this.x < 0 || this.x > width)
      this.xSpeed*=-1;
    if(this.y < 0 || this.y > height)
      this.ySpeed*=-1;
    this.x+=this.xSpeed;
    this.y+=this.ySpeed;
  }

  // this function creates the connections(lines)
  // between particles which are less than a certain distance apart
  joinParticles(particles) {
    particles.forEach(element =>{
      let dis = dist(this.x,this.y,element.x,element.y);

      if (dis<133 && !this.te) {
        stroke('rgba(255,10,255,0.94)');
        line(this.x,this.y,element.x,element.y);
      } 

      if (dis<random(1,60) && this.te && dis % frameCount) {
        stroke('rgba(25,10,255,0.94)');
        line(this.x,this.y,element.x,element.y);
      }

    });
  }

}

function preload() {
  font = loadFont("ostrich-regular.ttf");
}

function setup() {
  createCanvas(windowWidth, windowHeight);

  stroke(251)
  strokeWeight(1)
  noFill()

  let textPositions = font.textToPoints(
		word, 100, 400, fontSize, 
		{sampleFactor: 0.1, simplifyThreshold: 0});

  console.log(textPositions.length)
  for (let i = 0; i < textPositions.length; i++) {
    particles.push(new Particle( 
      textPositions[i].x, 
      textPositions[i].y, 
      true
    ));
  }

  console.log(width/10)
  for(let i = 0;i<width/10;i++){
    particles.push(new Particle(
      random(0,width), 
      random(0,height),
      false
    ));
  }

}

function draw() {
  background('gray');



  for(let i = 0;i<particles.length;i++) {

    particles[i].createParticle();

    if (particles[i].te == false){
      particles[i].moveParticle();
    }

    particles[i].joinParticles(particles.slice(i));
  }

  // let textPositions = font.textToPoints(
		// word, 100, 200, fontSize, 
		// {sampleFactor: 0.1, simplifyThreshold: 0.001}
  // );

  // beginShape();
  // for(let i = 0;i<textPositions.length;i++) {
  //   vertex(textPositions[i].x,textPositions[i].y);
  // }
  // endShape(CLOSE);

}
