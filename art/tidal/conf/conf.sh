 mv ~/.vim/plugged/vim-tidal/ftplugin/tidal.vim{,_old}
ln -s $PWD/ftdetect_tidal.vim ~/.vim/plugged/vim-tidal/ftplugin/tidal.vim

mv ~/.config/SuperCollider/startup.scd{,_old}
ln -s ~/Projects/101/tidal/conf/startup.scd ~/.config/SuperCollider/startup.scd
