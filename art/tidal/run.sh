
git clone --recurse-submodules https://github.com/cleary/ansible-tidalcycles.git

cd ansible-tidalcycles

virtualenv -p /usr/bin/python3 venv

. venv/bin/activate 

pip install ansible

sudo $(which ansible-playbook) --connection=local -i localhost, tidal_vim.play.yml

# vim
# Add tidal to Plug
# Plug 'tidalcycles/vim-tidal'
# :PlugInstall 

# point tmux target on vim
# :TidalConfig  -> default -> paneNum where tidal is running -> sclang running

